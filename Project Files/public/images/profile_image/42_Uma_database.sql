/*
SQLyog Community Edition- MySQL GUI v8.14 
MySQL - 5.5.8-log : Database - artex33
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `artex33`;

/*Table structure for table `art_admin` */

DROP TABLE IF EXISTS `art_admin`;

CREATE TABLE `art_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `add_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `active` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `art_admin` */

insert  into `art_admin`(`id`,`firstname`,`lastname`,`email`,`password`,`add_date`,`modify_date`,`active`) values (1,'Admin',NULL,'admin@artex.com','admin','2011-08-16 16:18:31','0000-00-00 00:00:00','1');

/*Table structure for table `art_banner` */

DROP TABLE IF EXISTS `art_banner`;

CREATE TABLE `art_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `banner_type` varchar(80) DEFAULT NULL,
  `banner_path` text,
  `publish` tinyint(1) DEFAULT '1',
  `create_date` datetime DEFAULT NULL,
  `modifiy_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

/*Data for the table `art_banner` */

insert  into `art_banner`(`id`,`page_id`,`location`,`banner_type`,`banner_path`,`publish`,`create_date`,`modifiy_date`) values (3,1,'Top','Image','Top_1Katherine-Pierce-drawing-katherine-pierce-17772911-900-1168.jpg',1,'2012-02-27 10:00:37',NULL);
insert  into `art_banner`(`id`,`page_id`,`location`,`banner_type`,`banner_path`,`publish`,`create_date`,`modifiy_date`) values (4,3,'Bottom','Image','Bottom_3insect_wallpaper17.jpg',1,'2012-02-27 10:38:43',NULL);
insert  into `art_banner`(`id`,`page_id`,`location`,`banner_type`,`banner_path`,`publish`,`create_date`,`modifiy_date`) values (28,1,'Top','HTML','',1,'2012-02-27 11:05:53',NULL);

/*Table structure for table `art_banner_pagename` */

DROP TABLE IF EXISTS `art_banner_pagename`;

CREATE TABLE `art_banner_pagename` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(100) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `art_banner_pagename` */

insert  into `art_banner_pagename`(`id`,`page_name`,`create_date`) values (1,'Profile Page','2012-02-27');
insert  into `art_banner_pagename`(`id`,`page_name`,`create_date`) values (2,'Artwork Details','2012-02-27');
insert  into `art_banner_pagename`(`id`,`page_name`,`create_date`) values (3,'Home Page','2012-02-27');

/*Table structure for table `art_blog` */

DROP TABLE IF EXISTS `art_blog`;

CREATE TABLE `art_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creator` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `image` varchar(100) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `publish` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=491 DEFAULT CHARSET=latin1;

/*Data for the table `art_blog` */

insert  into `art_blog`(`id`,`creator`,`title`,`description`,`image`,`create_date`,`modify_date`,`publish`) values (1,1,'happy happy f','happy happy ga ga\r\nhappy happy ga ga1111\r\nhappy happy ga ga\r\nhappy happy ga ga fffff',NULL,'2011-12-26 00:00:00','2012-02-29 09:02:50',1);
insert  into `art_blog`(`id`,`creator`,`title`,`description`,`image`,`create_date`,`modify_date`,`publish`) values (2,11,'KKKKKKKKK','kkkkkkkkkk','file_201112301259.jpg','2011-12-30 00:00:00','2011-12-30 00:00:00',1);
insert  into `art_blog`(`id`,`creator`,`title`,`description`,`image`,`create_date`,`modify_date`,`publish`) values (4,11,'fdsdfsdfs','fxdfvxdfvsx',NULL,'2011-12-30 00:00:00','2012-02-29 13:29:47',1);

/*Table structure for table `art_blog_comments` */

DROP TABLE IF EXISTS `art_blog_comments`;

CREATE TABLE `art_blog_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text,
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `publish` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `art_blog_comments` */

insert  into `art_blog_comments`(`id`,`blog_id`,`user_id`,`comment`,`create_date`,`modify_date`,`publish`) values (1,2,11,'ewr','2012-02-29 00:00:00',NULL,1);
insert  into `art_blog_comments`(`id`,`blog_id`,`user_id`,`comment`,`create_date`,`modify_date`,`publish`) values (2,4,1,'ok','2012-02-29 00:00:00',NULL,1);
insert  into `art_blog_comments`(`id`,`blog_id`,`user_id`,`comment`,`create_date`,`modify_date`,`publish`) values (3,2,1,'dfg','2012-02-29 00:00:00',NULL,0);
insert  into `art_blog_comments`(`id`,`blog_id`,`user_id`,`comment`,`create_date`,`modify_date`,`publish`) values (9,1,1,'gfdgdf','2012-03-02 11:45:58',NULL,1);

/*Table structure for table `art_category` */

DROP TABLE IF EXISTS `art_category`;

CREATE TABLE `art_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) DEFAULT NULL,
  `category_name` varchar(80) DEFAULT NULL,
  `category_description` text,
  `status` int(11) DEFAULT '1',
  `add_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_art_category` (`section_id`),
  CONSTRAINT `FK_art_category` FOREIGN KEY (`section_id`) REFERENCES `art_section` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `art_category` */

insert  into `art_category`(`id`,`section_id`,`category_name`,`category_description`,`status`,`add_date`) values (5,1,'About Artex','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',1,'2012-01-25 13:15:01');
insert  into `art_category`(`id`,`section_id`,`category_name`,`category_description`,`status`,`add_date`) values (6,1,'Contact Us','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:15:22');
insert  into `art_category`(`id`,`section_id`,`category_name`,`category_description`,`status`,`add_date`) values (7,2,'How Artex Works','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:18:43');
insert  into `art_category`(`id`,`section_id`,`category_name`,`category_description`,`status`,`add_date`) values (8,2,'Resolving issues','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,NULL);
insert  into `art_category`(`id`,`section_id`,`category_name`,`category_description`,`status`,`add_date`) values (9,2,'Security','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',1,NULL);
insert  into `art_category`(`id`,`section_id`,`category_name`,`category_description`,`status`,`add_date`) values (13,3,'Terms','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:24:48');

/*Table structure for table `art_country` */

DROP TABLE IF EXISTS `art_country`;

CREATE TABLE `art_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(2) NOT NULL DEFAULT '',
  `country` varchar(100) NOT NULL DEFAULT '',
  `code3` char(3) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=242 DEFAULT CHARSET=latin1;

/*Data for the table `art_country` */

insert  into `art_country`(`id`,`code`,`country`,`code3`) values (1,'US','United States','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (2,'CA','Canada','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (3,'UK','United Kingdom','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (4,'IE','Ireland','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (5,'AU','Australia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (6,'NZ','New Zealand','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (7,'AF','Afghanistan','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (8,'AL','Albania','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (9,'DZ','Algeria','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (10,'AS','American Samoa','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (11,'Br','Andorra','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (12,'AO','Angola','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (13,'AI','Anguilla','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (14,'AO','Angola','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (15,'AQ','Antarctica','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (16,'AG','Antigua and Barbuda','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (17,'AR','Argentina','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (18,'AM','Armenia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (19,'AW','Aruba','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (20,'AU','Australia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (21,'AT','Austria','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (22,'AZ','Azerbaijan','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (23,'BA','Bosnia-Herzegovina','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (24,'HA','Barbados','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (25,'BB','Bahamas','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (26,'BH','Bahrain','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (27,'BD','Bangladesh','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (28,'BR','Belarus','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (29,'BE','Belgium','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (30,'BZ','Belize','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (31,'BJ','Benin','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (32,'BM','Bermuda','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (33,'BT','Bhutan','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (34,'BO','Bolivia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (35,'BC','Bosnia and Herzegovina','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (36,'BW','Botswana','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (37,'VT','Bouvet Island','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (38,'ZI','Brazil','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (39,'IO','British Indian Territory','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (40,'BN','Brunei Darussalam','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (41,'BG','Bulgaria','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (42,'BF','Burkina Faso','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (43,'BI','Burundi','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (44,'CO','Colombia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (45,'CM','Cameroon','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (46,'CV','Cape Verde','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (47,'YM','Cayman Islands','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (48,'CF','Central African Republic','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (49,'HA','Chad','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (50,'CL','Chile','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (51,'CN','China','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (52,'CX','Christmas Island','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (53,'CC','Cocos (Keeling) Islands','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (54,'KH','Cambodia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (55,'KM','Comoros','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (56,'CG','Congo','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (57,'CK','Cook Islands','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (58,'CR','Costa Rica','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (59,'CD','Cote D`ivoire','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (60,'OA','Croatia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (61,'CU','Cuba','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (62,'CY','Cyprus','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (63,'CZ','Czech Republic','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (64,'DK','Denmark','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (65,'DJ','Djibouti','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (66,'DM','Dominica','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (67,'DO','Dominican Republic','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (68,'EC','Ecuador','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (69,'EG','Egypt','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (70,'AL','El Salvador','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (71,'GQ','Equatorial Guinea','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (72,'ER','Eritrea','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (73,'EE','Estonia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (74,'ET','Ethiopia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (75,'FK','Falkland Islands (Malvinas)','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (76,'FO','Faroe Islands','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (77,'FJ','Fiji','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (78,'FI','Finland','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (79,'FR','France','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (80,'FG','French Guiana','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (81,'BV','British Virgin Islands','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (82,'FP','French Polynesia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (83,'FS','French Southern Territories','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (84,'GA','Gabon','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (85,'GM','Gambia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (86,'GE','Georgia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (87,'DE','Germany','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (88,'GH','Ghana','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (89,'GI','Gibraltar','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (90,'GR','Greece','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (91,'GL','Greenland','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (92,'GP','Guadeloupe','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (93,'GU','Guam','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (94,'GT','Guatemala','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (95,'GN','Guinea','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (96,'GD','Grenada','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (97,'GW','Guinea Bissau','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (98,'GY','Guyana','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (99,'HT','Haiti','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (100,'HM','Heard and McDonald Islands','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (101,'VA','Vatican City State','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (102,'HN','Honduras','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (103,'HK','Hong Kong','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (104,'HU','Hungary','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (105,'IS','Iceland','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (106,'IN','India','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (107,'ID','Indonesia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (108,'II','Iran, Islamic Republic of','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (109,'IQ','Iraq','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (110,'IE','Ireland','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (111,'IL','Israel','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (112,'IT','Italy','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (113,'JM','Jamaica','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (114,'JP','Japan','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (115,'JO','Jordan','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (116,'KZ','Kazakhstan','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (117,'KE','Kenya','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (118,'KI','Kiribati','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (119,'KP','North Korea','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (120,'KR','South Korea','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (121,'KW','Kuwait','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (122,'KG','Kyrgyzstan','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (123,'LA','Lao','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (124,'LV','Latvia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (125,'LB','Lebanon','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (126,'LS','Lesotho','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (127,'LR','Liberia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (128,'LA','Libyan Arab Jamahiriya','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (129,'LI','Liechtenstein','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (130,'LT','Lithuania','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (131,'LU','Luxembourg','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (132,'LM','Macao','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (133,'MK','Macedonia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (134,'MG','Madagascar','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (135,'MW','Malawi','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (136,'MY','Malaysia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (137,'MV','Maldives','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (138,'ML','Mali','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (139,'MT','Malta','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (140,'MH','Marshall Islands','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (141,'CH','Switzerland','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (142,'MQ','Martinique (Fr)','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (143,'MR','Mauritania','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (144,'MU','Mauritius','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (145,'MS','Montserrat','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (146,'MA','Morocco','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (147,'MZ','Mozambique','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (148,'MM','Myanmar','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (149,'NA','Namibia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (150,'NR','Nauru','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (151,'NP','Nepal','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (152,'NL','Netherlands','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (153,'AN','Netherlands Antilles','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (154,'NC','New Caledonia ','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (155,'NZ','New Zealand','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (156,'NI','Nicaragua','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (157,'NE','Niger','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (158,'NG','Nigeria','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (159,'NU','Niue','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (160,'NF','Norfolk Island','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (161,'MP','Northern Mariana Islands','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (162,'NO','Norway','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (163,'OM','Oman','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (164,'PK','Pakistan','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (165,'PW','Palau','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (166,'PO','Palestinian Territory','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (167,'PA','Panama','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (168,'PG','Papua New Guinea','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (169,'PY','Paraguay','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (170,'PE','Peru','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (171,'PH','Philippines','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (172,'PN','Pitcairn','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (173,'PL','Poland','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (174,'PT','Portugal','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (175,'PR','Puerto Rico','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (176,'QA','Qatar','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (177,'RE','Reunion','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (178,'RO','Romania','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (179,'RU','Russian Federation','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (180,'RW','Rwanda','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (181,'SH','Saint Helena','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (182,'KN','Saint Kitts and Nevis','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (183,'LC','Saint Lucia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (184,'PM','Saint Pierre/Miquelon','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (185,'VC','Saint Vincent/Grenadines','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (186,'WS','Samoa','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (187,'SM','San Marino','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (188,'ST','Sao Tome/Principe','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (189,'SA','Saudi Arabia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (190,'SN','Senegal','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (191,'SP','Serbia/Montenegro','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (192,'SC','Seychelles','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (193,'SL','Sierra Leone','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (194,'SG','Singapore','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (195,'CS','Slovakia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (196,'SI','Slovenia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (197,'SB','Solomon Islands','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (198,'SO','Somalia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (199,'ZA','South Africa','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (200,'SG','South Georgia/Sandwich Islands','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (201,'ES','Spain','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (202,'LK','Sri Lanka','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (203,'SD','Sudan','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (204,'SR','Suriname','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (205,'SJ','Svalbard and Jan Mayen','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (206,'SZ','Swaziland','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (207,'SE','Sweden','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (208,'CH','Switzerland','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (209,'SY','Syria','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (210,'TW','Taiwan','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (211,'TJ','Tajikistan','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (212,'TZ','Tanzania','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (213,'TH','Thailand','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (214,'TI','Timor-leste','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (215,'TG','Togo','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (216,'TK','Tokelau','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (217,'TO','Tonga','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (218,'TT','Trinidad/Tobago','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (219,'TN','Tunisia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (220,'TR','Turkey','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (221,'TM','Turkmenistan','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (222,'TC','Turks/Caicos','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (223,'TV','Tuvalu','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (224,'XX','UG','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (225,'UA','Ukraine','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (226,'UA','United Arab Emirates','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (227,'UK','United Kingdom','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (228,'US','United States','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (229,'UM','U.S. Outlying Islands','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (230,'UY','Uruguay','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (231,'UZ','Uzbekistan','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (232,'VU','Vanuatu','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (233,'VE','Venezuela','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (234,'VN','Viet Nam','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (235,'BV','Virgin Islands, British','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (236,'VI','Virgin Islands, U.S.','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (237,'WF','Wallis and Futuna','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (238,'EH','Western Sahara','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (239,'YE','Yemen','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (240,'ZM','Zambia','');
insert  into `art_country`(`id`,`code`,`country`,`code3`) values (241,'ZW','Zimbabwe ','');

/*Table structure for table `art_email_notifications` */

DROP TABLE IF EXISTS `art_email_notifications`;

CREATE TABLE `art_email_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `send_date` date NOT NULL,
  `sent_status` tinyint(1) DEFAULT '0',
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `receipients` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `art_email_notifications` */

insert  into `art_email_notifications`(`id`,`subject`,`message`,`send_date`,`sent_status`,`create_date`,`modify_date`,`receipients`) values (1,'fg','<p>\r\n	fdgdf f gdg</p>','2012-02-15',0,'2012-02-02 00:00:00','2012-02-04 13:29:20','all');
insert  into `art_email_notifications`(`id`,`subject`,`message`,`send_date`,`sent_status`,`create_date`,`modify_date`,`receipients`) values (2,'ret','<p>\r\n	ertert</p>','2012-02-17',0,'2012-02-03 10:42:04','2012-02-04 13:29:35','1');

/*Table structure for table `art_feedbacks_on_site` */

DROP TABLE IF EXISTS `art_feedbacks_on_site`;

CREATE TABLE `art_feedbacks_on_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `subject` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `art_feedbacks_on_site` */

insert  into `art_feedbacks_on_site`(`id`,`user_id`,`email`,`subject`,`message`,`create_date`) values (4,1,'ewre@fdfd.mkl','gfhgh','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','2012-02-02 00:00:00');
insert  into `art_feedbacks_on_site`(`id`,`user_id`,`email`,`subject`,`message`,`create_date`) values (5,2,'asd@ghj.mkl','fgfd','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','2012-02-02 00:00:00');

/*Table structure for table `art_globelinfo` */

DROP TABLE IF EXISTS `art_globelinfo`;

CREATE TABLE `art_globelinfo` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `meta` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `offline_message` text,
  `admin_email` varchar(200) NOT NULL,
  `contact_email` varchar(100) DEFAULT NULL,
  `auction_duration` int(11) DEFAULT NULL,
  `auction_percent` float(10,2) DEFAULT NULL,
  `auction_sold` float(10,2) DEFAULT NULL,
  `scout_percent` float(10,2) DEFAULT NULL,
  `bid_interval` text,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `signature` varchar(100) DEFAULT NULL,
  `merchant_email` varchar(100) DEFAULT NULL,
  `image_small_width` int(11) DEFAULT NULL,
  `image_small_height` int(11) DEFAULT NULL,
  `image_mid_width` int(11) DEFAULT NULL,
  `image_mid_height` int(11) DEFAULT NULL,
  `image_large_width` int(11) DEFAULT NULL,
  `image_large_height` int(11) DEFAULT NULL,
  `adddate` date NOT NULL,
  `modifydate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `art_globelinfo` */

insert  into `art_globelinfo`(`id`,`meta`,`description`,`status`,`offline_message`,`admin_email`,`contact_email`,`auction_duration`,`auction_percent`,`auction_sold`,`scout_percent`,`bid_interval`,`username`,`password`,`signature`,`merchant_email`,`image_small_width`,`image_small_height`,`image_mid_width`,`image_mid_height`,`image_large_width`,`image_large_height`,`adddate`,`modifydate`) values (1,'Artex','Art Ex: The go to place for art online',1,'maintenance of site.','admin@artex.com','admin@contact.com',44,2.20,2.30,2.00,'a:4:{i:0;s:1:\"5\";i:1;s:2:\"10\";i:2;s:2:\"15\";i:3;s:2:\"20\";}','deepak_1324646632_biz_api1.inoday.com','1324646670','An5ns1Kso7MWUdW4ErQKJJJ4qi4-Az1utoWx0MLHX5Xe77eTOq','admin@admin.com',78,78,148,148,800,600,'2011-11-15','2012-03-01');

/*Table structure for table `art_group` */

DROP TABLE IF EXISTS `art_group`;

CREATE TABLE `art_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_type` varchar(222) NOT NULL,
  `my_wall` enum('yes','no') NOT NULL DEFAULT 'yes',
  `auction` enum('yes','no') NOT NULL DEFAULT 'yes',
  `upload_aw` enum('yes','no') NOT NULL DEFAULT 'yes',
  `collection` enum('yes','no') NOT NULL DEFAULT 'yes',
  `view_aw` enum('yes','no') NOT NULL DEFAULT 'yes',
  `watchlist` enum('yes','no') NOT NULL DEFAULT 'yes',
  `manage_aw` enum('yes','no') NOT NULL DEFAULT 'yes',
  `user_profile` enum('yes','no') NOT NULL DEFAULT 'yes',
  `view_bidding` enum('yes','no') NOT NULL DEFAULT 'yes',
  `message` enum('yes','no') NOT NULL DEFAULT 'yes',
  `aw_approval` enum('yes','no') NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `art_group` */

insert  into `art_group`(`group_id`,`group_type`,`my_wall`,`auction`,`upload_aw`,`collection`,`view_aw`,`watchlist`,`manage_aw`,`user_profile`,`view_bidding`,`message`,`aw_approval`) values (1,'Admin','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes');
insert  into `art_group`(`group_id`,`group_type`,`my_wall`,`auction`,`upload_aw`,`collection`,`view_aw`,`watchlist`,`manage_aw`,`user_profile`,`view_bidding`,`message`,`aw_approval`) values (2,'User','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes');
insert  into `art_group`(`group_id`,`group_type`,`my_wall`,`auction`,`upload_aw`,`collection`,`view_aw`,`watchlist`,`manage_aw`,`user_profile`,`view_bidding`,`message`,`aw_approval`) values (3,'Artist','yes','no','no','no','yes','yes','yes','yes','no','no','yes');
insert  into `art_group`(`group_id`,`group_type`,`my_wall`,`auction`,`upload_aw`,`collection`,`view_aw`,`watchlist`,`manage_aw`,`user_profile`,`view_bidding`,`message`,`aw_approval`) values (4,'Buyer','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes');
insert  into `art_group`(`group_id`,`group_type`,`my_wall`,`auction`,`upload_aw`,`collection`,`view_aw`,`watchlist`,`manage_aw`,`user_profile`,`view_bidding`,`message`,`aw_approval`) values (5,'Dealer','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes');
insert  into `art_group`(`group_id`,`group_type`,`my_wall`,`auction`,`upload_aw`,`collection`,`view_aw`,`watchlist`,`manage_aw`,`user_profile`,`view_bidding`,`message`,`aw_approval`) values (6,'Member','yes','no','no','no','yes','no','yes','no','yes','no','no');

/*Table structure for table `art_help` */

DROP TABLE IF EXISTS `art_help`;

CREATE TABLE `art_help` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `publish` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `art_help` */

/*Table structure for table `art_news` */

DROP TABLE IF EXISTS `art_news`;

CREATE TABLE `art_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `display_start_date` datetime NOT NULL,
  `display_end_date` datetime NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `publish` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `art_news` */

insert  into `art_news`(`id`,`title`,`description`,`display_start_date`,`display_end_date`,`create_date`,`modify_date`,`publish`) values (1,'df','<p>\r\n	<img alt=\\\"\\\" src=\\\"http://192.168.1.43/artex/public/images/admin/delete_1.png\\\" style=\\\"width: 25px; height: 25px;\\\" />&nbsp; y tryb&nbsp; tr ytry tr try tr try tr</p>','2012-02-01 00:00:00','2012-02-16 00:00:00','2012-02-02 00:00:00','2012-02-04 13:20:25',1);
insert  into `art_news`(`id`,`title`,`description`,`display_start_date`,`display_end_date`,`create_date`,`modify_date`,`publish`) values (5,'todays','<p>\r\n	<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','2012-02-08 00:00:00','2012-02-29 00:00:00','2012-02-06 08:39:51',NULL,1);

/*Table structure for table `art_section` */

DROP TABLE IF EXISTS `art_section`;

CREATE TABLE `art_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_name` varchar(80) DEFAULT NULL,
  `section_description` text,
  `status` int(11) DEFAULT '1',
  `add_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `art_section` */

insert  into `art_section`(`id`,`section_name`,`section_description`,`status`,`add_date`) values (1,'About','<p>\r\n	At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.&nbsp;&nbsp;</p>\r\n<p>\r\n	At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>',1,'2012-01-25 13:14:42');
insert  into `art_section`(`id`,`section_name`,`section_description`,`status`,`add_date`) values (2,'Help','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:18:12');
insert  into `art_section`(`id`,`section_name`,`section_description`,`status`,`add_date`) values (3,'Legal','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:18:21');

/*Table structure for table `art_site_pages` */

DROP TABLE IF EXISTS `art_site_pages`;

CREATE TABLE `art_site_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(100) NOT NULL,
  `publish` tinyint(1) DEFAULT '1',
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `art_site_pages` */

/*Table structure for table `art_subcategory` */

DROP TABLE IF EXISTS `art_subcategory`;

CREATE TABLE `art_subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `subcategory_name` varchar(80) DEFAULT NULL,
  `subcategory_description` text,
  `status` int(11) DEFAULT '1',
  `add_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_art_subcategory` (`category_id`),
  CONSTRAINT `FK_art_subcategory1` FOREIGN KEY (`category_id`) REFERENCES `art_category` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `art_subcategory` */

insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (5,1,5,'Company Mission','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:15:45');
insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (6,1,5,'Company Info','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:15:59');
insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (7,1,5,'Vacancies (We\\\'re hiring)','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:16:44');
insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (8,1,6,'Contact Details','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:17:39');
insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (9,1,6,'Press','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:17:49');
insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (10,2,7,'How to buy','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:18:57');
insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (11,2,7,'How to sell','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:19:09');
insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (12,2,7,'How to collect','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:19:24');
insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (13,2,7,'How to deal','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:19:35');
insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (14,2,7,'Rating system','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:20:01');
insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (15,2,9,'Buying','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:24:14');
insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (16,2,9,'selling','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:24:22');
insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (17,3,13,'who can sell','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:25:03');
insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (18,3,13,'Privacy','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:25:16');
insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (19,3,13,'Copyright','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',1,'2012-01-25 13:25:28');
insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (20,3,13,'Other T&C','',1,'2012-01-25 13:25:43');
insert  into `art_subcategory`(`id`,`section_id`,`category_id`,`subcategory_name`,`subcategory_description`,`status`,`add_date`) values (21,1,5,'Add Subcat','',1,'2012-03-03 09:32:35');

/*Table structure for table `art_user` */

DROP TABLE IF EXISTS `art_user`;

CREATE TABLE `art_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `user_id` varchar(80) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `block` tinyint(1) DEFAULT NULL,
  `activation_word` varchar(100) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `scoutcode` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `art_user` */

insert  into `art_user`(`id`,`first_name`,`last_name`,`email`,`user_id`,`password`,`active`,`block`,`activation_word`,`create_date`,`modify_date`,`group_id`,`scoutcode`) values (1,'Micle','flower','reeta@inoday.com','userID','5f4dcc3b5aa765d61d8327deb882cf99',0,NULL,'1516986022',NULL,NULL,1,1);
insert  into `art_user`(`id`,`first_name`,`last_name`,`email`,`user_id`,`password`,`active`,`block`,`activation_word`,`create_date`,`modify_date`,`group_id`,`scoutcode`) values (11,'reeta','verma','reeta.verma@inoday.com','reeta.verma@inoday.com','25d55ad283aa400af464c76d713c07ad',NULL,NULL,'1107843698',NULL,NULL,1,1);
insert  into `art_user`(`id`,`first_name`,`last_name`,`email`,`user_id`,`password`,`active`,`block`,`activation_word`,`create_date`,`modify_date`,`group_id`,`scoutcode`) values (30,'First Name','Last Name','reeta.inoday@gmail.com','reeta.inoday@gmail.com','25d55ad283aa400af464c76d713c07ad',NULL,NULL,'2085600880',NULL,NULL,1,1);
insert  into `art_user`(`id`,`first_name`,`last_name`,`email`,`user_id`,`password`,`active`,`block`,`activation_word`,`create_date`,`modify_date`,`group_id`,`scoutcode`) values (31,'First Name','Last Name','reeta.inoday@gmail.com','reeta.inoday@gmail.com','25d55ad283aa400af464c76d713c07ad',NULL,NULL,'198955910',NULL,NULL,1,1);
insert  into `art_user`(`id`,`first_name`,`last_name`,`email`,`user_id`,`password`,`active`,`block`,`activation_word`,`create_date`,`modify_date`,`group_id`,`scoutcode`) values (32,'First Name','Last Name','reeta.inoday@gmail.com','reeta.inoday@gmail.com','25d55ad283aa400af464c76d713c07ad',NULL,NULL,'394766824',NULL,NULL,1,1);
insert  into `art_user`(`id`,`first_name`,`last_name`,`email`,`user_id`,`password`,`active`,`block`,`activation_word`,`create_date`,`modify_date`,`group_id`,`scoutcode`) values (33,'First Name','Last Name','reeta.inoday@gmail.com','reeta.inoday@gmail.com','25d55ad283aa400af464c76d713c07ad',NULL,NULL,'1484097114',NULL,NULL,1,1);
insert  into `art_user`(`id`,`first_name`,`last_name`,`email`,`user_id`,`password`,`active`,`block`,`activation_word`,`create_date`,`modify_date`,`group_id`,`scoutcode`) values (34,'First Name','Last Name','reeta.inoday@gmail.com','reeta.inoday@gmail.com','25d55ad283aa400af464c76d713c07ad',NULL,NULL,'1335753039',NULL,NULL,1,1);
insert  into `art_user`(`id`,`first_name`,`last_name`,`email`,`user_id`,`password`,`active`,`block`,`activation_word`,`create_date`,`modify_date`,`group_id`,`scoutcode`) values (20,'fgf','fdg','dfds@gf.mkl','gfhgf','ghgf',1,NULL,'135298150',NULL,NULL,NULL,1);
insert  into `art_user`(`id`,`first_name`,`last_name`,`email`,`user_id`,`password`,`active`,`block`,`activation_word`,`create_date`,`modify_date`,`group_id`,`scoutcode`) values (35,'uma Shankar','singh','umashankar@inoday.com','umashankar','e10adc3949ba59abbe56e057f20f883e',1,NULL,'809354287',NULL,NULL,NULL,0);

/*Table structure for table `art_user_details` */

DROP TABLE IF EXISTS `art_user_details`;

CREATE TABLE `art_user_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `dob` date DEFAULT NULL,
  `biography` text,
  `house_no` varchar(100) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `country` int(3) DEFAULT NULL,
  `profile_image` text,
  `paypal_username` varchar(100) DEFAULT NULL,
  `paypal_password` varchar(100) DEFAULT NULL,
  `paypal_signature` varchar(100) DEFAULT NULL,
  `billing_address` varchar(100) DEFAULT NULL,
  `billing_city` varchar(100) DEFAULT NULL,
  `billing_state` varchar(100) DEFAULT NULL,
  `billing_country` int(3) DEFAULT NULL,
  `billing_zip` varchar(10) DEFAULT NULL,
  `shipping_address` varchar(100) DEFAULT NULL,
  `shipping_city` varchar(100) DEFAULT NULL,
  `shipping_state` varchar(100) DEFAULT NULL,
  `shipping_country` int(3) DEFAULT NULL,
  `shipping_zip` varchar(10) DEFAULT NULL,
  `contact_number` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `art_user_details` */

insert  into `art_user_details`(`id`,`user_id`,`dob`,`biography`,`house_no`,`street`,`city`,`zip`,`country`,`profile_image`,`paypal_username`,`paypal_password`,`paypal_signature`,`billing_address`,`billing_city`,`billing_state`,`billing_country`,`billing_zip`,`shipping_address`,`shipping_city`,`shipping_state`,`shipping_country`,`shipping_zip`,`contact_number`) values (1,1,NULL,NULL,'House Name/no','Street','Town / City','Post / ZIP',2,NULL,NULL,NULL,NULL,'1 Main St','San Jose','CA',1,'95131',NULL,NULL,NULL,NULL,NULL,NULL);
insert  into `art_user_details`(`id`,`user_id`,`dob`,`biography`,`house_no`,`street`,`city`,`zip`,`country`,`profile_image`,`paypal_username`,`paypal_password`,`paypal_signature`,`billing_address`,`billing_city`,`billing_state`,`billing_country`,`billing_zip`,`shipping_address`,`shipping_city`,`shipping_state`,`shipping_country`,`shipping_zip`,`contact_number`) values (11,33,NULL,NULL,'fdg','fdg','Town / City','Post / ZIP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
insert  into `art_user_details`(`id`,`user_id`,`dob`,`biography`,`house_no`,`street`,`city`,`zip`,`country`,`profile_image`,`paypal_username`,`paypal_password`,`paypal_signature`,`billing_address`,`billing_city`,`billing_state`,`billing_country`,`billing_zip`,`shipping_address`,`shipping_city`,`shipping_state`,`shipping_country`,`shipping_zip`,`contact_number`) values (20,32,NULL,NULL,'fdg','fdg','Town / City','Post / ZIP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
insert  into `art_user_details`(`id`,`user_id`,`dob`,`biography`,`house_no`,`street`,`city`,`zip`,`country`,`profile_image`,`paypal_username`,`paypal_password`,`paypal_signature`,`billing_address`,`billing_city`,`billing_state`,`billing_country`,`billing_zip`,`shipping_address`,`shipping_city`,`shipping_state`,`shipping_country`,`shipping_zip`,`contact_number`) values (17,29,NULL,NULL,'456','Street','San Jose','95131',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
insert  into `art_user_details`(`id`,`user_id`,`dob`,`biography`,`house_no`,`street`,`city`,`zip`,`country`,`profile_image`,`paypal_username`,`paypal_password`,`paypal_signature`,`billing_address`,`billing_city`,`billing_state`,`billing_country`,`billing_zip`,`shipping_address`,`shipping_city`,`shipping_state`,`shipping_country`,`shipping_zip`,`contact_number`) values (18,30,NULL,NULL,'456','Street','San Jose','95131',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
insert  into `art_user_details`(`id`,`user_id`,`dob`,`biography`,`house_no`,`street`,`city`,`zip`,`country`,`profile_image`,`paypal_username`,`paypal_password`,`paypal_signature`,`billing_address`,`billing_city`,`billing_state`,`billing_country`,`billing_zip`,`shipping_address`,`shipping_city`,`shipping_state`,`shipping_country`,`shipping_zip`,`contact_number`) values (19,31,NULL,NULL,'fdg','fdg','Town / City','Post / ZIP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
insert  into `art_user_details`(`id`,`user_id`,`dob`,`biography`,`house_no`,`street`,`city`,`zip`,`country`,`profile_image`,`paypal_username`,`paypal_password`,`paypal_signature`,`billing_address`,`billing_city`,`billing_state`,`billing_country`,`billing_zip`,`shipping_address`,`shipping_city`,`shipping_state`,`shipping_country`,`shipping_zip`,`contact_number`) values (22,34,NULL,NULL,'fdg','fdg','Town / City','Post / ZIP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
insert  into `art_user_details`(`id`,`user_id`,`dob`,`biography`,`house_no`,`street`,`city`,`zip`,`country`,`profile_image`,`paypal_username`,`paypal_password`,`paypal_signature`,`billing_address`,`billing_city`,`billing_state`,`billing_country`,`billing_zip`,`shipping_address`,`shipping_city`,`shipping_state`,`shipping_country`,`shipping_zip`,`contact_number`) values (23,35,NULL,NULL,'16','noida','noida','201301',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `art_users_images` */

DROP TABLE IF EXISTS `art_users_images`;

CREATE TABLE `art_users_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `order` int(3) DEFAULT '0',
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `publish` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `art_users_images` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

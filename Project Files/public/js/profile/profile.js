
function checkUser(){
	var errors = [];
	 
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	var emailaddress = $('#pemail').val();

	if(reg.test(emailaddress) == false && emailaddress!='') {
		   errors[errors.length] = 'Please enter correct email';
		   $('#pemail').css("border-color", '#CC0000');
	      
	}
	else{
			$('#pemail').css("border-color", '');
			}
	if(jQuery.trim($('#paypal_username').val())==''){
			errors[errors.length] = 'Please enter paypal username ';
			$('#paypal_username').css("border-color", '#CC0000');
		}
		else{
			$('#paypal_username').css("border-color", '');
			}
	if(jQuery.trim($('#paypal_password').val())==''){
			errors[errors.length] = 'Please enter paypal password ';
			$('#paypal_password').css("border-color", '#CC0000');
		}
		else{
			$('#paypal_password').css("border-color", '');
			}
	if(jQuery.trim($('#paypal_signature').val())==''){
			errors[errors.length] = 'Please enter paypal signature ';
			$('#paypal_signature').css("border-color", '#CC0000');
		}
		else{
			$('#paypal_signature').css("border-color", '');
			}
	if(jQuery.trim($('#creditcardnumber').val())==''){
			errors[errors.length] = 'Please enter credit card number ';
			$('#creditcardnumber').css("border-color", '#CC0000');
		}
		else{
			$('#creditcardnumber').css("border-color", '');
			}
		if(jQuery.trim($('#creditcardnumber').val())!='')
		{
			var specialChars = "*|,',\":<>[]{}`\;()@&$!#%^?abcdefghijklmnopqrstuvwxyz";
			var creditcardnumber=$('#creditcardnumber').val();
			var flg1=0;
			for (var i = 0; i < creditcardnumber.length; i++) 
			{
			if (specialChars.indexOf(creditcardnumber.charAt(i)) != -1)
			{
				flg1=1; break;
			}
			}
			if(flg1==1)
			{
			errors[errors.length] = 'Please enter vaild credit card number.';
			$('#creditcardnumber').css("border-color", '#CC0000');
			}
		}
			if(jQuery.trim($('#cvv_number').val())==''){
			errors[errors.length] = 'Please enter CVV2 number ';
			$('#cvv_number').css("border-color", '#CC0000');
		}
		else{
			$('#cvv_number').css("border-color", '');
			}
		if(jQuery.trim($('#cvv_number').val())!='')
		{
			var specialChars = "*|,',\":<>[]{}`\;()@&$!#%^?abcdefghijklmnopqrstuvwxyz";
			var cvv_number=$('#cvv_number').val();
			var flg1=0;
			for (var i = 0; i < cvv_number.length; i++) 
			{
			if (specialChars.indexOf(cvv_number.charAt(i)) != -1)
			{
				flg1=1; break;
			}
			}
			if(flg1==1)
			{
			errors[errors.length] = 'Please enter vaild CVV2 number.';
			$('#cvv_number').css("border-color", '#CC0000');
			}
		}
		
	if (errors.length > 0) {
		  reportErrors(errors);
		  return false;
	}
	
	
}

function reportErrors(errors){ 
	 	var msg = "<b>Please Enter Valid Data</b>";
		for (var i = 0; i<errors.length; i++) {
		var numError = i + 1;
		msg += "<br>" + numError + ". " + errors[i];
		}$('#error_msg').show();
		
		$('#error_msg').html(msg);
		return false;
	}
//////////////////   Number validation /////////////
function isNumberKey(evt)
{
         var charCode = (evt.which) ? evt.which : event.keyCode
         
         if (charCode > 31 && charCode > 46 && (charCode < 48 || charCode > 57))
            return false;

         return true;
}

function checkUser() {

		var errors = [];
	
		//$('error_msg').innerHTML = 'Are you aggry with terms and conditions?';
		
	 	 
		if($('#condition').checked == false){
			errors[errors.length] = 'Are you agree with terms and conditions?';
		}
		if(jQuery.trim($('#name').val())==''){
			errors[errors.length] = 'First name required';
			$('#name').css("border-color", '#CC0000');
		}
		else{
			$('#house_no').css("border-color", '');
			}
		if(jQuery.trim($('#house_no').val())==''){
			errors[errors.length] = 'House number required';
			$('#house_no').css("border-color", '#CC0000');
		}
		else{
			$('#house_no').css("border-color", '');
			}
		if(jQuery.trim($('#street').val())==''){
			errors[errors.length] = 'Street required';
			$('#street').css("border-color", '#CC0000');
		}
		else{
			$('#street').css("border-color", '');
			}
		if(jQuery.trim($('#city').val())==''){
			errors[errors.length] = 'City required';
			$('#city').css("border-color", '#CC0000');
		}
		else{
			$('#city').css("border-color", '');
			}
		if(jQuery.trim($('#zip').val())==''){
			errors[errors.length] = 'Zip required';
			$('#zip').css("border-color", '#CC0000');
		}
		else{
			$('#zip').css("border-color", '');
			}
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var emailaddress = $('#email').val();
		
		if(jQuery.trim($('#email').val())==''){
			errors[errors.length] = 'Please enter email';
			$('#email').css("border-color", '#CC0000');
			
		}else if(reg.test(emailaddress) == false) {
			   errors[errors.length] = 'Please enter correct email';
			   $('#email').css("border-color", '#CC0000');
		      
	    }else if(jQuery.trim($('#reemail').val())==''){
			errors[errors.length] = 'Please conform email';
			$('#reemail').css("border-color", '#CC0000');
			
		}else if($('#email').val()!=$('#reemail').val()){
			errors[errors.length] = 'Please enter same email';
			$('#reemail').css("border-color", '#CC0000');	
		} 	
		if(jQuery.trim($('#userid').val())==''){
			errors[errors.length] = 'User Id required';
			$('#userid').css("border-color", '#CC0000');
			//
		}
		else{
			$('#userid').css("border-color", '');
			}
		if(jQuery.trim($('#password').val())==''){
			errors[errors.length] = 'Please enter password';
			$('#password').css("border-color", '#CC0000');
			
		}else{
			$('#password').css("border-color", '');
			}
		if($('#password').val()!=$('#repassword').val()){
			errors[errors.length] = 'Re-enter password should be same';
			$('#repassword').css("border-color", '#CC0000');
			
			}else{
				
				$('#repassword').css("border-color", '');
			}
		

		if(jQuery.trim($('#repassword').val())==''){
			errors[errors.length] = 'Please re-enter password';
			$('#repassword').css("border-color", '#CC0000');
			
		}else{
			
			$('#repassword').css("border-color", '');
		}
		
		

		if (errors.length > 0) {
			  reportErrors(errors);
			  return false;
		}
	}

	function reportErrors(errors){ 
	 	var msg = "<b>Please Enter Valid Data</b>";
		for (var i = 0; i<errors.length; i++) {
		var numError = i + 1;
		msg += "<br>" + numError + ". " + errors[i];
		}$('#error_msg').show();
		
		$('#error_msg').html(msg);
		return false;
	}
/*-----------------------------------------Admin add member form validation-------------------------------*/	
	function check() { 
		var errors = [];
		if(jQuery.trim($('#firstname').val())==''){
			errors[errors.length] = 'First name required';
			$('#firstname').css("border-color", '#CC0000');
		}
		else{
			
			$('#firstname').css("border-color", '');
		}

		
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var emailaddress = $('#email').val();
		
		if(jQuery.trim($('#email').val())==''){
			errors[errors.length] = 'Please enter email';
			$('#email').css("border-color", '#CC0000');
			
		}else if(reg.test(emailaddress) == false) {
			   errors[errors.length] = 'Please enter correct email';
			   $('#email').css("border-color", '#CC0000');
		      
	    } 
		

		if(jQuery.trim($('#userid').val())==''){
			errors[errors.length] = 'User Id required';
			$('#userid').css("border-color", '#CC0000');
			//
		}
		else{
			
			$('#userid').css("border-color", '');
		}

			
		
		if(jQuery.trim($('#password').val())==''){
			errors[errors.length] = 'Please enter password';
			$('#password').css("border-color", '#CC0000');
			
		}else{
			
			$('#password').css("border-color", '');
		}

		if (errors.length > 0) {  
				reportErrors(errors);				
			  return false;
		} 
	}
	
function savegift() 
{

 var errors = [];
	 var regex = /^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i;
     var check = regex.test($('#admin_email').val()); 
	 var check2 = regex.test($('#contact_email').val()); 
	 var check3 = regex.test($('#merchant_email').val()); 
	 $('#error_msg').hide();
	
	 if(jQuery.trim($('#admin_email').val())!="" && check==false)
	 {
	   errors[errors.length] = 'Please enter valid admin email.';
	 }
	 if(jQuery.trim($('#contact_email').val())!="" && check2==false)
	 {
	    errors[errors.length] = 'Please enter valid contact email.';
	 }
	 if(jQuery.trim($('#merchant_email').val())!="" && check3==false)
	 {
	    errors[errors.length] = 'Please enter valid merchant email.';
	 }
	var specialChars = "*|,'.,\":<>[]{}`\;()@&$!#%^?abcdefghijklmnopqrstuvwxyz";
		var auctionduration=$('#auction_duration').val();
		var flg=0;
		for (var i = 0; i < auctionduration.length; i++) 
		{
			if (specialChars.indexOf(auctionduration.charAt(i)) != -1)
			{
				flg=1; break;
			}
		} 
	if(flg==1)
		{
		errors[errors.length] = 'Please enter vaild auction duration.';
		}
		var specialChars = "*|,',\":<>[]{}`\;()@&$!#%^?abcdefghijklmnopqrstuvwxyz";
		var auctionpercent=$('#auction_percent').val();
		var flg2=0;
		for (var i = 0; i < auctionpercent.length; i++) 
		{
			if (specialChars.indexOf(auctionpercent.charAt(i)) != -1)
			{
				flg2=1; break;
			}
		}
	if(flg2==1)
		{
		errors[errors.length] = 'Please enter vaild auction percent.';
		}
		var specialChars = "*|,',\":<>[]{}`\;()@&$!#%^?abcdefghijklmnopqrstuvwxyz";
		var auctionsold=$('#auction_sold').val();
		var flg3=0;
		for (var i = 0; i < auctionsold.length; i++) 
		{
			if (specialChars.indexOf(auctionsold.charAt(i)) != -1)
			{
				flg3=1; break;
			}
		}
	if(flg3==1)
		{
		errors[errors.length] = 'Please enter vaild auction sold.';
		}
		var specialChars = "*|,',\":<>[]{}`\;()@&$!#%^?abcdefghijklmnopqrstuvwxyz";
		var scoutpercent=$('#scout_percent').val();
		var flg4=0;
		for (var i = 0; i < scoutpercent.length; i++) 
		{
			if (specialChars.indexOf(scoutpercent.charAt(i)) != -1)
			{
				flg4=1; break;
			}
		}
	if(flg4==1)
		{
		errors[errors.length] = 'Please enter vaild scout percent.';

		}
		
		if (errors.length > 0) {
			  reportErrors(errors);
			  return false;
		}
}
	
	
<?php
class Application_Model_ArtInbox   
{ 
	protected $_name = 'art_inbox';
    protected $_dbTable;
    
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    } 
    
    public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_ArtInbox');
        }
        return $this->_dbTable;
    }
    
	public function myInbox($uId)
	{
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select  =  "SELECT *,i.id as msg_id FROM art_inbox as i LEFT JOIN art_user as u ON i.sender_id =u.id WHERE i.folder_id = 0 AND i.delete != 1 AND i.receiver_id='".$uId."' order by receive_date desc";
		
		$result  =  $db->fetchAll($select);
		
		return  $result;
	}
	
	public function myInboxLimit($uId,$to)
	{
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select	 =	'';
		$select  =  "SELECT *,i.id as msg_id FROM art_inbox as i LEFT JOIN art_user as u ON i.sender_id =u.id WHERE i.delete != 1 AND i.receiver_id=$uId";
		//$select	 .= "order by msg_date desc";
		$select	 .= " $to ";
		$result  =  $db->fetchAll($select);
		
		
		return $rec	= $result;
	}	// My Inbox and Sent
	
	public function readMessage($mId) 
	{
		
		 $data = array(
            'read_status'   => 1,
        );
		$where	=	$this->getDbTable()->getAdapter()->quoteInto('id = ?', $mId); 
		return $allrec	=	$this->getDbTable()->update($data, $where);
	}
	
	public function myIndmessage($mId) 
	{
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select  =  "SELECT * FROM art_inbox where id =$mId ";
		$result  =  $db->fetchAll($select);
		return  $result;
	}
	
	public function savemsg($request)
	{ 
		$namespace 	= new Zend_Session_Namespace(); 
		$db			=	$this->getDbTable();
		if(isset($request['old_msg']))
		{
		$data	= 	array('sender_id'=>$namespace->userid,
						'receiver_id'=>$request['receiver_id'],
						'receive_date'=>date("Y-m-d H:i:s"),
						'subject'=>$request['subject'],
						'message'=>stripslashes($request['editor']).stripslashes($request['old_msg'])
					);
		}
		else
		{
		$data	= 	array('sender_id'=>$namespace->userid,
						'receiver_id'=>$request['receiver_id'],
						'receive_date'=>date("Y-m-d H:i:s"),
						'subject'=>$request['subject'],
						'message'=>stripslashes($request['editor'])
					);
		}
		
		return $insRec	=	$db->insert($data); 
		
	} 
	
	public function mySent($uId)
	{
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select  =  "SELECT *,i.id as msg_id FROM art_inbox as i LEFT JOIN art_user as u ON i.receiver_id =u.id WHERE i.delete != 1 AND i.sender_id=$uId order by receive_date desc";
		$result  =  $db->fetchAll($select);
		
		return  $result;
	}
	
	public function mySentLimit($uId,$to)
	{
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select	 =	'';
		$select  =  "SELECT *,i.id as msg_id FROM art_inbox as i LEFT JOIN art_user as u ON i.receiver_id =u.id WHERE i.delete != 1 AND i.sender_id=$uId ";
		$select	 .= " $to ";
		$result  =  $db->fetchAll($select);
		return $rec	= $result;
	}
	
	public function deleteMessages($val)
	{
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('id = ?', $val); 
		return $allrec	=	$db->delete($where);
	}
	
	public function restoreMessages($mId)
	{
		$data = array(
            'delete'   => 0,
        );
		$where	=	$this->getDbTable()->getAdapter()->quoteInto('id = ?', $mId); 
		return $allrec	=	$this->getDbTable()->update($data, $where);
	}
	public function trashMessage($mId) 
	{
		 $data = array(
            'delete'   => 1,
        );
		$where	=	$this->getDbTable()->getAdapter()->quoteInto('id = ?', $mId); 
		return $allrec	=	$this->getDbTable()->update($data, $where);
	}
	
	public function getTrashMessafes($uId)
	{
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		
		$select  =  "SELECT *,i.id as msg_id FROM art_inbox as i LEFT JOIN art_user as u ON i.sender_id =u.id WHERE i.delete = 1 AND i.receiver_id=$uId order by receive_date desc";
		
		$result  =  $db->fetchAll($select);
		
		return  $result;
	}
	
	public function getTrashMessafesLimit($uId,$to)
	{
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select	 =	'';
		$select  =  "SELECT *,i.id as msg_id FROM art_inbox as i LEFT JOIN art_user as u  ON  i.sender_id =u.id WHERE i.delete = 1 AND i.receiver_id=$uId";
		//$select	 .= "order by msg_date desc";
		$select	 .= " $to ";
		$result  =  $db->fetchAll($select);
		
		
		return $rec	= $result;
	}	// My Inbox and Sent
	///////////////////Added by reeta 24-02-2012//////////////////////////////////////////////////////
	public function getFolderMessafes($uId,$fId)
	{
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		
		 $select  =  "SELECT *,i.id as msg_id FROM art_inbox as i LEFT JOIN art_user as u ON i.sender_id =u.id WHERE i.folder_id = $fId AND i.receiver_id=$uId AND i.delete != 1 order by receive_date desc";
		
		$result  =  $db->fetchAll($select);
		
		return  $result;
	} 
	
	///////////////Move messages/////////////
	public function moveMessage($mId, $folderId) 
	{
		 $data = array(
            'folder_id'   => $folderId,
        );
		$where	=	$this->getDbTable()->getAdapter()->quoteInto('id = ?', $mId); 
		return $allrec	=	$this->getDbTable()->update($data, $where);
	}
	//////////////ADMIN SECTION//////////////////////
	 public function getMessage()
    {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select  =  "SELECT * FROM art_inbox ";
		$result  =  $db->fetchAll($select);
		return  $result;
	}
	public function deletemessagelist($id)
    { 
	   //echo'hi'. $id;die;
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('id IN (?)', $id); 
		return $db->delete($where);
    }
    
/*
@	Add by : Abhishek
@   Details: Count number of inbox msg in login user account
@	On: 02-03-2012
**/ 
    public function countInboxMsg($uid) 
    {//'.$uid.'
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		if($uid!='') {
		 $select  =  'SELECT count(receiver_id) AS msg FROM art_inbox where receiver_id = '.$uid.' and read_status=0 and art_inbox.delete=0 GROUP BY receiver_id'; 
		$result = $db->fetchAll($select); }
		/*echo '<pre>';
		print_r($result);die;*/
		return  $result;
    }
}

<?php
class Application_Model_ArtGroup   
{ 
	protected $_name = 'art_user';
    protected $_dbTable;
    
	public function setDbTable($dbTable)
    { 
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    } 
    
    public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Artuser');
        }
        return $this->_dbTable;
    }
	public function getUsers() {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt = $db->query('SELECT * FROM art_group');
        $row = $stmt->fetchAll();
        return $row;
	}
	
	public function SetPermission($gup_id){
		
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt = $db->query('SELECT * FROM art_group WHERE group_id="'.$gup_id.'"');
        $row = $stmt->fetch();
       	return $row;
	}
		
	public function addgroup($dbField,$id) {
				
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$db->update('art_group',$dbField,'group_id = "'.$id.'"');
		return true;
	}
	
	public function insertgroup($dbField) {
				
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$where = array('group_id' => $id);
		$db->insert('art_group',$dbField);
		return true;
	}
	
	public function getMember($searchelement,$sortBy) {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
				
		$sql.='SELECT * FROM art_user where 1';
		if($searchelement!='')	
		$sql.=' and first_name LIKE "%'.$searchelement.'%" or last_name LIKE "%'.$searchelement.'%" or email LIKE "%'.$searchelement.'%"';
		if($sortBy!='')	
		$sql.=' ORDER BY '.$sortBy.'';
		else
		$sql.=' ORDER BY id DESC';
		$stmt = $db->query($sql);
        $row = $stmt->fetchAll();
       // echo '<pre>';print_r($row);die;
        return $row;
	}
	public function getMemberallName($value){ 
	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt = $db->query("SELECT 	GROUP_CONCAT(first_name) AS rname, GROUP_CONCAT(email) AS email FROM art_user WHERE id IN ('".$value."')");
        $row = $stmt->fetchAll();
       // echo '<pre>';print_r($row);die;
        return $row;
	}
	public function getMemberName() {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt = $db->query('SELECT first_name as receipients, email,id as options FROM art_user ORDER BY id');
        $row = $stmt->fetchAll();
       // echo '<pre>';print_r($row);die;
        return $row;
	}
	
	public function getMemberID($email) {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();

		$stmt = $db->query('SELECT au.id,au.user_id,au.first_name,au.last_name,au.email,billing_address,billing_city,billing_state,billing_country,billing_zip, 
	shipping_address, shipping_city, shipping_state, shipping_country, shipping_zip, shipping_contact_number, credit_card,credit_card_number,exp_month,exp_year,cvv2 FROM art_user au LEFT JOIN art_user_details aud ON au.id = aud.user_id WHERE au.email="'.$email.'"');
		
        $row = $stmt->fetchAll();
        return $row;
	}
	public function getMemberdetailsByID($userid) {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt = $db->query('SELECT au.id,au.user_id,au.first_name,au.last_name,au.email,billing_address,billing_city,billing_state,billing_country,billing_zip, 
	shipping_address, shipping_city, shipping_state, shipping_country, shipping_zip, shipping_contact_number, credit_card,credit_card_number,exp_month,exp_year,cvv2 FROM art_user au LEFT JOIN art_user_details aud ON au.id = aud.user_id WHERE au.user_id="'.$userid.'"');
		
        $row = $stmt->fetchAll();
       // echo '<pre>';print_r($row);die;
        return $row;
	}
	public function deletemember($id)
    { 	
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt1 = $db->query('Delete from art_user_details where user_id IN ('.$id.') ');
		$stmt = $db->query('Delete from art_user where id IN ('.$id.') ');

		return $stmt;
    }
	
	public function updatememberstatus($id,$status)
    {  
	$db = Zend_Db_Table_Abstract::getDefaultAdapter();

        $data = array(
            'active'		=> $status
			
        );
		$db->update('art_user',$data,'id IN ('.$id.') '); 
		//$db->update('art_group',$data,'reg_id = "'.$id.'"');

    }
	
}
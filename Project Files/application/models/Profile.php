<?php

class Application_Model_DbTable_Profile extends Zend_Db_Table_Abstract
{ 	
	 
	/////////////find user details////////////////
	public function getUserdetails($email,$password=null) {
		
		 $makeQue = 'SELECT aud.*, au.id, au.first_name, au.last_name, au.email, au.user_id as userid, au.group_id, au.password
					FROM art_user au 
					LEFT JOIN art_user_details aud  ON au.id = aud.user_id
					WHERE au.email="'.$email.'" AND  password = "'.$password.'"';
		
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
	 	$stmt = $db->query($makeQue);
        $result = $stmt->fetchAll();
         
		return $result;
    }
	
	//////////////////////////// Update user //////////////////////////////////
	public function updateuser($userArray,$detailsArray,$userid)
    { 
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->update('art_user', $userArray, "id = '".$userid."'");
        $db->update('art_user_details', $detailsArray, "user_id = '".$userid."'");
    }
    
	//////////////////////////// Update user Details //////////////////////////////////
	public function updateuserDetails($detailsArray,$userid)
    { 
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->update('art_user_details', $detailsArray, "user_id = '".$userid."'");
    }
    
    public function getUsertype() {
    	
    }
    
    public function getUserProfile($user_id) {
    	$makeQue = 'SELECT au.id, au.first_name, au.last_name, au.create_date, aud.dob, aud.profile_image, aud.biography, acu.country, COUNT(af.user_id) AS follower, COUNT(af.au_id) AS following, ag.group_type
					FROM art_user AS au
					LEFT JOIN art_user_details AS aud ON aud.user_id = au.id
					LEFT JOIN art_followed AS af ON af.user_id = au.id
					LEFT JOIN art_country AS acu ON acu.id = aud.country
					LEFT JOIN art_group AS ag ON ag.group_id = au.group_id
					WHERE au.id = '.$user_id.' GROUP BY au.id'; 
		
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
	 	$stmt = $db->query($makeQue);
        $result = $stmt->fetchAll();
        return $result;
    }
    
	public function getUserArt($user_id) {
    	$makeQue = 'SELECT aw.id, awim.image
					FROM art_artworks AS aw
					INNER JOIN art_artworks_images AS awim ON awim.artwork_id = aw.id
					WHERE aw.owner_id = '.$user_id.' GROUP BY aw.id ORDER BY 1 desc';
		
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
	 	$stmt = $db->query($makeQue);
        $result = $stmt->fetchAll();
       
        return $result;
    }
    
    public function checkUser($user_id){
    	
    	$makeQue = 'SELECT id AS userid
					FROM art_user					 
					WHERE md5(id) = "'.$user_id.'"';
    	
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
	 	$stmt = $db->query($makeQue);
        $result = $stmt->fetchAll();
        /*echo '<pre>';
        echo count($result[0]);
        print_r($result);die;*/
        return $result;
    }
}
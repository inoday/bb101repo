<?php
class Application_Model_Artuser  extends Zend_Db_Table_Abstract
{
	protected $_name = 'art_user';
	protected $_dbTable;
			
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
		
        $this->_dbTable = $dbTable;
		
        return $this;
    }
			
	public function getDbTable()
	{
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_Artuser');
		}
		return $this->_dbTable;
	}

// *******************uUser data ***********	

	public function getUserData($data)
    {
		if( $data=='')
		{
			return false;
		}
		$db = $this->getDbTable();
		$select = $db->select()->where('email = ?', $data);
		$result = $db->fetchAll($select);
		//echo '<pre>'; print_r($result);die;
		return $rec	= $result->toArray();
    }
      public function getUsers()
    {
		$db = $this->getDbTable();
		$result = $db->fetchAll();
		return $rec	= $result->toArray();
    }
	public function chkAval_user($data)
    {
		//echo $data;die;
		$db = $this->getDbTable();
		$select = $db->select()->where('email = ?', $data);
		$result = $db->fetchAll($select);
		$row=$result->toArray();
		//echo '<pre>'; print_r($result->toArray());
		return $result->count(); 
    }
    
	public function adduser($users)
	{
		$data = array(
		 	 
            'first_name'   => $users['first_name'],
			'last_name'   => $users['last_name'],
            'email' => $users['email'],
			'password' => $users['password'],
        );
		
		return $allrec	=	$this->getDbTable()->insert($data);
			 
	}
	
	public function chkLogin($email,$pass)
    { 
		$db = $this->getDbTable();
		$select = $db->select()->where('email = ?', $email)->where('password = ?', $pass)->where('active = ?', '1');
		$result = $db->fetchAll($select)->count();
		 
		if($result==1)
		{
			return $result ;
		}
		
    }
    
    public function selestuser($id) {
    	
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select = $this->select()->where('id = ?', $id);
		$result = $this->fetchAll($select);
		
		return $result;
    }
    
	public function updateuser($dbField,$id) {
				
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$this->update($dbField,'id = "'.$id.'"');
		return true;
	}
	
	public function getUserdetails($id) {
		
		$makeQue = 'SELECT aud.*, au.id, au.user_id, au.first_name, au.last_name, au.email, au.password, au.active, au.scoutcode 
					FROM art_user au 
					LEFT JOIN art_user_details aud  ON au.id = aud.user_id
					WHERE au.id="'.$id.'"';
		
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
	 	$stmt = $db->query($makeQue);
        $result = $stmt->fetchAll();
         
		return $result;
    }
	
	public function userRec($data)
    {
		
		$db = $this->getDbTable();
		$select = $db->select()->where('id = ?', $data);
		
		$result = $db->fetchAll($select);
		return $rec	= $result->toArray();
	}
/************************** Function used in forget password module (07-03-2012) **********************/
		
	public function updateForgetPassword($dbField,$email) {
				
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$this->update($dbField,'email = "'.$email.'"');
		return true;
	}
/************************** END (07-03-2012) **********************/

	//**************** Profile User class *****************************	
	public function bloguserAll()
	{     
		//echo'<pre>';print_r($this->getDbTable());die('hiiiii');
		return $this->getDbTable()->fetchAll();
	}
		
}
?>
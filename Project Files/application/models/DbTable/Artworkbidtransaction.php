<?php
 
 
class Application_Model_DbTable_Artworkbidtransaction extends Zend_Db_Table_Abstract
{ 
	protected $_name = 'art_artworks_listing_transaction';
    protected $_dbTable;
	
 	public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Artworkbidtransaction');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
	    /**
   @	Added By : Reeta verma
   @	Added On :	24-05-2012	
   @	Input: void
   @	Return: reture the value from thr table.
   @
   **/	
   
    ####---------------INSERT Data in  Table----------------####
    
	public function addartworkbidtransaction($dbField)
    {   $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $dbField; 
		//$db = $this->getDbTable();	
	
		$artwork_id = $this->insert($dbField);

		
		
    }
    
####-------------------------------END---------------------------------------####
public function getartworkBP($lpID)
{   
	$db = $this->getDbTable();
	  $select = $db->select()->where('listing_pay_id = ?', $lpID)->where('type = ?','buyer_premium')->where('status = ?','1');
	
	$result = $db->fetchAll($select);
	return $rec	= $result->toArray();
}

####-------------------------------END---------------------------------------####	
public function getartworkArtP($lpID)
{   
	$db = $this->getDbTable();
	 $select = $db->select()->where('listing_pay_id = ?', $lpID)->where('type = ?','seller')->where('status = ?','1');
	
	$result = $db->fetchAll($select);
	return $rec	= $result->toArray();
}

####-------------------------------END---------------------------------------####	
public function getartworkAP($lpID)
{   
	$db = $this->getDbTable();
	 $select = $db->select()->where('listing_pay_id = ?', $lpID)->where('type = ?','artex_percentage')->where('status = ?','1');
	
	$result = $db->fetchAll($select);
	return $rec	= $result->toArray();
}



####-------------------------------END---------------------------------------####	 
public function getartworkSP($lpID)
{   
	$db = $this->getDbTable();
	 $select = $db->select()->where('listing_pay_id = ?', $lpID)->where('type = ?','scout_percentage')->where('status = ?','1');
	
	$result = $db->fetchAll($select);
	return $rec	= $result->toArray();
}



####-------------------------------END---------------------------------------####	
}
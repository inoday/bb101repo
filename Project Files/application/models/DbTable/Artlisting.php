<?php 
class Application_Model_DbTable_Artlisting extends Zend_Db_Table_Abstract
{ 
	public function endsoon() {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		/*********** Find bid duration date ********************/
		$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
		//echo '<pre>';print_r($duration);
	    /*********** END Find bid duration date ********************/
		$a = 'SELECT aw.id, aw.title, aw.price, aw.descrption, awim.image, aw.owner_id,aw.buy_it_now, u.first_name, u.last_name FROM art_artworks aw 
							INNER JOIN art_artworks_images awim ON awim.artwork_id = aw.id 
							INNER JOIN art_user u ON u.id = aw.owner_id 
							WHERE aw.payment_status = 1 AND aw.delete = 0 
							AND approval_status = 1
							AND  ADDDATE(bid_start_date, INTERVAL auction_duration DAY) > NOW() 
							AND  NOW()>=aw.bid_start_date
							GROUP BY aw.id  ORDER BY aw.bid_start_date ASC LIMIT 0,4';//die();
		$stmt = $db->query($a);
        $row = $stmt->fetchAll();
        return $row;
	}
	
	public function recentlyadded() {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		/*********** Find bid duration date ********************/
		$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
	    /*********** END Find bid duration date ********************/
		$stmt = $db->query('SELECT aw.id, aw.title, aw.price, aw.descrption, aw.owner_id, awim.image,aw.buy_it_now, u.first_name, u.last_name FROM art_artworks aw 
							INNER JOIN art_artworks_images awim ON awim.artwork_id = aw.id 
							INNER JOIN art_user u ON u.id = aw.owner_id 
							WHERE aw.payment_status = 1 AND aw.delete = 0 
							AND approval_status = 1
							AND  ADDDATE(bid_start_date, INTERVAL auction_duration DAY) > NOW() 
							AND  NOW()>=aw.bid_start_date 
							GROUP BY aw.id  ORDER BY aw.bid_start_date DESC LIMIT 0,4');
        $row = $stmt->fetchAll();
        return $row;
	}
	
	public function toprated() {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		/*********** Find bid duration date ********************/
		$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
	    /*********** END Find bid duration date ********************/
		$stmt = $db->query('SELECT aw.id, aw.title, aw.price, aw.descrption, awim.image,aw.buy_it_now, aw.owner_id, u.first_name, u.last_name FROM art_artworks aw 
							INNER JOIN art_artworks_images awim ON awim.artwork_id = aw.id  
							INNER JOIN art_user u ON u.id = aw.owner_id 
							WHERE aw.payment_status = 1  AND aw.delete = 0
							AND approval_status = 1
							AND  ADDDATE(bid_start_date, INTERVAL auction_duration DAY) > NOW() 
							AND  NOW()>=aw.bid_start_date
							GROUP BY aw.id ORDER BY aw.rate_count DESC LIMIT 0,4');
        $row = $stmt->fetchAll();
        return $row;
	}
	
	public function topartists() {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		/*********** Find bid duration date ********************/
		$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
	    /*********** END Find bid duration date ********************/
		$stmt = $db->query('SELECT aw.id, aw.title, aw.price, aw.descrption, awim.image, aw.owner_id,aw.buy_it_now, u.first_name, u.last_name FROM art_artworks aw 
							INNER JOIN art_artworks_images awim ON awim.artwork_id = aw.id 
							INNER JOIN art_user u ON u.id = aw.owner_id 
							WHERE aw.payment_status = 1  AND aw.delete = 0
							AND approval_status = 1
							AND  ADDDATE(bid_start_date, INTERVAL auction_duration DAY) > NOW()  
							AND  NOW()>=aw.bid_start_date
							GROUP BY aw.id  LIMIT 0,4');
        $row = $stmt->fetchAll();
        return $row;
	}

}
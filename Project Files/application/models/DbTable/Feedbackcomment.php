<?php

class Application_Model_DbTable_ArtUserFeedbackComments extends Zend_Db_Table_Abstract
{ 	
	protected $_name = 'art_user_feedbacks_comments';
	protected $_dbTable;
	
	public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Profilecomment');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
	public function addUserFeedbackComment($dbField)
    {   $db = Zend_Db_Table_Abstract::getDefaultAdapter();
    //echo '<pre>';
    //print_r($dbField);
    //die;
    
    	$db->insert($dbField);
		//$feedback_id = $this->insert($dbField);
    }
}
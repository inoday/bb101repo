<?php
 
 
class Application_Model_DbTable_Artworkuploadpayment extends Zend_Db_Table_Abstract
{ 
	protected $_name = 'art_artworks_listing_payments';
    protected $_dbTable;
	
 	public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Artworkpayment');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
	    /**
   @	Added By : Reeta verma
   @	Added On :	18-02-2012	
   @	Input: void
   @	Return: reture the value from thr table.
   @
   **/	
   
    ####---------------INSERT Data in  Table----------------####
    
	public function addartworkpayment($dbField)
    {   
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $dbField;
		$artwork_id = $this->insert($dbField);
    }
    
    ####-------------------------------END---------------------------------------####
	 
	public function updatePaymentListing($id,$amount) {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		
		$transaction_id = $id.':'.rand();
		$date = date('Y-m-d G:i:s');
			
        $data = array(
            'artwork_id' => $id, 
            'transaction_date' => $date, 
            'transaction_id' => $transaction_id, 
            'amount' => $amount, 
            'status' => 1,
        	'payment_type' => 3
        );
        $this->insert($data);
	}
	
}
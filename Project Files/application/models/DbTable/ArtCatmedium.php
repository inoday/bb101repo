<?php
class Application_Model_DbTable_ArtCatmedium extends Zend_Db_Table_Abstract
{


    protected $_name = 'art_artwork_cat_medium';
    protected $_dbTable;
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
    public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_ArtCatmedium');
        }
        return $this->_dbTable;
    }	
public function getcatmediumName($id)
	{
		$db = $this->getDbTable();
        $row = $this->fetchAll($this->select()->where('cat_id = ?', $id));
       // echo '<pre>';print_r($row);die;
        return $row->toArray();
	}
public function getcatmediumlistName($id) 
    { 
        $id = (int)$id;
		
		
			$row = $this->fetchAll($this->select()->where('id = ?', $id)); 
        if (!$row) {
            throw new Exception("Count not find row $id");
		}
        return $row->toArray();    
    }
}
?>

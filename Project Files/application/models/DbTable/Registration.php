<?php
 
 
class Application_Model_DbTable_Registration extends Zend_Db_Table_Abstract
{ 
	protected $_name = 'art_user';
    protected $_dbTable;
	
 	public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Artuser');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
	
    ####---------------INSERT Data in art_user Table----------------####
    
	public function adduser($dbField,$dbField_details)
    {   $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $dbField;
		//$db = $this->getDbTable();		
		$id = $this->insert($dbField);
		 $useridarr=array('user_id' => $id);
		$db = $this->getDbTable();
		$merg_arr=array_merge((array)$dbField_details, (array)$useridarr); //print_r($merg_arr); die();
		$lastinsertID = $db->insert($merg_arr);
		return $lastinsertID;
		
    }
    
    ####-------------------------------END---------------------------------------####
	
	///////////for Login//////////////////////////////////////
	public function chkLogin($userid,$pass)
    { 
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select = $this->select()->where('user_id = ?', trim($userid))->where('password = ?', $pass)->where('active = 1');
		$result = $this->fetchAll($select)->count();
		$details = $this->fetchAll($select);
		$loginArray[0] = $result;
		$loginArray[1] = $details;
		
		if($loginArray[0]==1)
		{
			return $loginArray ;
		}
		
    }
	///////////for Login by email//////////////////////////////////////
	public function chkLoginemail($email,$pass)
    { 	
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select = $this->select()->where('email = ?', trim($email))->where('password = ?', $pass)->where('active = 1');
		$result = $this->fetchAll($select)->count();
		$details = $this->fetchAll($select);
		$loginArray[0] = $result;
		$loginArray[1] = $details;
	
		if($loginArray[0]==1)
		{
			return $loginArray ;
		}
		
    }
	/////////////find user name////////////////
	public function usrename($email,$password=null) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		if($pass==null)
		$select = $this->select()->where('email = ?', $email);
		else
		$select = $this->select()->where('email = ?', $email)->where('password = ?', $pass);

		$result = $this->fetchAll($select);
		return $result->toArray();
    }
	
	//////////////////////////////////////////////////////////////////
	public function adduser_details($dbField_details,$id)
	{ //print_r($dbField_details);echo $id;die();
		$db = $this->getDbTable();
		$db->update($dbField_details,'user_id = '. (int)$id);

	}
		//////////////////////////////////////////////////
	public function chkAval_user($data)
	{
		//echo $data;
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select = $db->select()->where('email = ?', $data);
		$result = $db->fetchAll($select);
		return $result->count();
	}
			////////////////////////////////////////////////////
	public function getUserData($data)
	{
		if( $data=='')
		{
			return false;
		}
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select = $this->select()->where('email = ?', $data);
		$result = $this->fetchAll($select);
		 
		return $rec	= $result->toArray();
	}
	
	public function checkEmail($data) {
		if( $data=='')
		{
			return false;
		}
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select = $this->select()->where('email = ?', $data);
		$result = $this->fetchAll($select);
		$count =  $result->count();
		//echo '<pre>';print_r($result);die;
		return $count;
	}
	
	public function checkUserId($data) {
		if( $data=='')
		{
			return false;
		}
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select = $this->select()->where('user_id = ?', $data);
		$result = $this->fetchAll($select);
		$count =  $result->count();
		//echo '<pre>';print_r($result);die;
		return $count;
	}
	public function checkEmailedit($data,$id) {
		if( $data=='')
		{
			return false;
		}
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select = $this->select()->where('email = ?', $data)->where('id <> ?', $id);
		$result = $this->fetchAll($select);
		$count =  $result->count();
		//echo '<pre>';print_r($result);die;
		return $count;
	}
	
	public function checkUserIdedit($data,$id) {
		if( $data=='')
		{
			return false;
		}
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select = $this->select()->where('user_id = ?', $data)->where('id <> ?', $id);
		$result = $this->fetchAll($select);
		$count =  $result->count();
		//echo '<pre>';print_r($result);die;
		return $count;
	}
}

<?php

class Application_Model_DbTable_ArtworkBannerpages extends Zend_Db_Table_Abstract
{
    protected $_name = 'art_banner_pagename';
	protected $_dbTable;
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
    public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_ArtCountry');
        }
        return $this->_dbTable;
    }	
public function getBannerPageName()
	{
		$db = $this->getDbTable();
        $row = $this->fetchAll();
       // echo '<pre>';print_r($row);die;
        return $row;
	}
}
?>
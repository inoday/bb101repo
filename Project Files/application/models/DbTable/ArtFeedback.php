<?php
   
class Application_Model_DbTable_ArtFeedback extends Zend_Db_Table_Abstract
{

    protected $_name = 'art_feedbacks_on_site';
    protected $_dbTable;

    /**
   @	Add Faq
   @	Added By : Anil Rawat
   @	Added On :	20-10-2011	
   @	Input: void
   @	Return: reture the value from thr table.
   @
   **/	
    public function getfeedbacklist($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Count not find row $id");
        }
		//echo'<pre>';print_r( $row);die;
        return $row->toArray();    
    }
	
   /**
   @	Add Faq
   @	Added By : Anil Rawat
   @	Added On :	20-10-2011	
   @	Input:
	@			question( String)
	@			answer (String)
	@			addDate (date)
	@	Return: void
	@
   **/ 
    
	public function addfeedbacklist($title,$description,$display_start_date,$display_end_date,$addDate)
    {
        $data = array(
		'title'		=> $title,
		'description'=>$description,
            'display_start_date'		=> $display_start_date,
			'display_end_date'   	=> $display_end_date,
			'create_date'  	=> $addDate,

        );
        $this->insert($data);
    }
    
    public function updatefeedbacklist($id,$title,$description,$display_start_date,$display_end_date,$modify_date)
    {
        $data = array(
		'title'		=> $title,
		'description'=>$description,
            'display_start_date'		=> $display_start_date,
			'display_end_date'   	=> $display_end_date,
			'modify_date'=>$modify_date
        );
        $this->update($data,'id = '. (int)$id);
    }
	
public function updatefeedbackliststatus($id,$status)
    {
        $data = array(
            'publish'		=> $status
			
        );
        $this->update($data,'id = '. (int)$id);
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
    public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_Categorylist');
        }
        return $this->_dbTable;
    }

   public function deletefeedbacklist($id)
    { 
	   //echo'hi'. $id;die;
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('id IN ('.$id.') '); 
		return $this->delete($where);
    }


}


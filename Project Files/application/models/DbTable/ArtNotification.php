<?php
   
class Application_Model_DbTable_ArtNotification extends Zend_Db_Table_Abstract 
{

    protected $_name = 'art_email_notifications';
    protected $_dbTable;

    /**
   @	Add Faq
   @	Added By : Anil Rawat
   @	Added On :	20-10-2011	
   @	Input: void 
   @	Return: reture the value from thr table.
   @
   **/	
    public function getnotificationlist($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Count not find row $id");
        }
		//echo'<pre>';print_r( $row);die;
        return $row->toArray();    
    }
	
   /**
   @	Add Faq
   @	Added By : Anil Rawat
   @	Added On :	20-10-2011	
   @	Input:
	@			question( String)
	@			answer (String)
	@			addDate (date)
	@	Return: void
	@
   **/ 
    
	public function addnotificationlist($subject,$message,$send_date,$receipients,$addDate)
    {
        $data = array(
		'subject'   =>$subject,
				'message'     =>$message,
				'send_date'     =>$send_date,
				'receipients'     =>$receipients,
			'create_date'  	=> $addDate,

        );
        $this->insert($data);
    }
    
    public function updatenotificationlist($id,$subject,$message,$send_date,$receipients,$modify_date)
    {
        $data = array(
		'subject'   =>$subject,
		'message'     =>$message,
		'send_date'     =>$send_date,
		'receipients'     =>$receipients,
			'modify_date'=>$modify_date
        );
        $this->update($data,'id = '. (int)$id);
    }
	
public function updatenotificationliststatus($id,$status)
    {
        $data = array(
            'publish'		=> $status
			
        );
        $this->update($data,'id = '. (int)$id);
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
    public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_Categorylist');
        }
        return $this->_dbTable;
    }

   public function deletenotificationlist($id)
    { 
	   //echo'hi'. $id;die;
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('id = ?', $id); 
		return $this->delete($where);
    }


}


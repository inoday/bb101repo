<?php
   
class Application_Model_DbTable_ArtNewsfeed extends Zend_Db_Table_Abstract
{

    protected $_name = 'art_newsfeed';
    protected $_dbTable;

    /**

   @	Added By : Reeta
   
   @
   **/	
    public function getnewslist($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Count not find row $id");
        }
		//echo'<pre>';print_r( $row);die;
        return $row->toArray();    
    }
	public function getnewslistany() 
    {
		$where	=	$this->select()->where('publish = 1')->limit('1'); 
        $row = $this->fetchAll($where);
        if (!$row) {
            throw new Exception("Count not find row $id");
        }
		//echo'<pre>';print_r( $row);die;
        return $row->toArray();    
    }
   /**
 @	Added By : Reeta
	@
   **/ 
    
	public function addnewslist($title,$url,$addDate)
    {
        $data = array(
		'title'		=> $title,
		'url'=>$url,
			'create_date'  	=> $addDate,

        );//print_r($data);die;
        $this->insert($data);
    }
    
    public function updatenewslist($id,$title,$url,$modify_date)
    {
        $data = array(
		'title'		=> $title,
		'url'=>$url,
			'modify_date'=>$modify_date
        );
        $this->update($data,'id = '. (int)$id);
    }
	
public function updatenewsliststatus($id,$status)
    {
        $data = array(
            'publish'		=> $status
			
        );
        $this->update($data,'id IN ('.$id.') ');
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
    public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_Categorylist');
        }
        return $this->_dbTable;
    }

   public function deletenewslist($id)
    { 
	   //echo'hi'. $id;die;
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('id IN ('.$id.') '); 
		return $this->delete($where);
    }


}


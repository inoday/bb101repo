<?php 
   
class Application_Model_DbTable_Subcategorylist extends Zend_Db_Table_Abstract
{

    protected $_name = 'art_subcategory';
    protected $_dbTable;

    /**
   @	Add Faq
   @	Added By : Anil Rawat
   @	Added On :	20-10-2011	
   @	Input: void
   @	Return: reture the value from thr table.
   @ 
   **/	
    public function getSubcategorylist($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Count not find row $id");
        }
		//echo'<pre>';print_r( $row);die;
        return $row->toArray();    
    }
	
   /**
   @	Add Faq
   @	Added By : Anil Rawat
   @	Added On :	20-10-2011	
   @	Input:
	@			question( String)
	@			answer (String)
	@			addDate (date)
	@	Return: void
	@
   **/ 
    
	public function addSubcategorylist($section_id,$category_id,$subcategory_name,$subcategory_description,$addDate)
    {
        $data = array(
		'section_id'		=> $section_id,
		'category_id'		=> $category_id,
		'subcategory_name'		=> $subcategory_name,
		'subcategory_description'   	=> $subcategory_description,
		'add_date'  	=> $addDate,
			
        );
        $this->insert($data);
    }
    
    public function updateSubcategorylist($id,$section_id,$category_id,$subcategory_name,$subcategory_description)
    {
        $data = array(
		'section_id'		=> $section_id,
		'category_id'		=> $category_id,
		'subcategory_name'		=> $subcategory_name,
		'subcategory_description'   	=> $subcategory_description
			
        );
        $this->update($data,'id = '. (int)$id);
    }

public function updateSubcategoryliststatus($id,$status)
    {
        $data = array(
            'status'		=> $status
			
        );
        $this->update($data,'id IN ('.$id.') ');
    }

	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
    public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_Subcategorylist');
        }
        return $this->_dbTable;
    }

   public function deletesubcategorylist($id)
    { 
	   //echo'hi'. $id;die;
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('id IN ('.$id.') '); 
		return $this->getDbTable()->delete($where);
    }


}


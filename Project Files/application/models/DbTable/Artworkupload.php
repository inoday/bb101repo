<?php
 
 
class Application_Model_DbTable_Artworkupload extends Zend_Db_Table_Abstract
{ 
	protected $_name = 'art_artworks';
    protected $_dbTable;
	
 	public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Artworkuploadimages');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
	    /**
   @	Added By : Reeta verma
   @	Added On :	16-02-2012	
   @	Input: void
   @	Return: reture the value from thr table.
   @
   **/	
    public function getArtworkupload($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Count not find row $id");
        }
		//echo'<pre>';print_r( $row);die;
        return $row->toArray();    
    }
   /**
   @	Added By : Reeta verma
   @	Added On :	16-02-2012	
   @	Input: void
   @	Return: reture the value from thr table.
   @
   **/	
    public function getArtworkuploadbyname($name) 
    {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();

		$stmt = $db->query('SELECT * FROM art_artworks aw WHERE aw.title  = "'.$name.'" ');

        $result = $stmt->fetchAll();
		 
        return $result;    
    }

    ####---------------INSERT Data in  Table----------------####
    
	public function addartwork($dbField,$image)
    {   $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $dbField;
		//$db = $this->getDbTable();	
	
		$artwork_id = $this->insert($dbField);

		$db = $this->getDbTable();
		for($i=0;$i<count($image);$i++)
		{
		if($image[$i]!='')
		{$merg_arr=array('artwork_id'=>$artwork_id, 'image'=>$image[$i],'create_date'=>date("Y-m-d H:i:s")); //print_r($merg_arr); die();
		$db->insert($merg_arr);}
		}
		return $artwork_id;
    }
    
    ####-------------------------------END---------------------------------------####
	    ####---------------UPDATE Data in  Table----------------####
    
	public function updateartwork($id,$dbField,$image)
    {   $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $dbField; 
		//$db = $this->getDbTable();	
	
		$artwork_id = $this->update($dbField,'id = '. (int)$id);

		$db = $this->getDbTable();
		for($i=0;$i<count($image);$i++)
		{
			if($image[$i]!='')
			{
			$where	=	$db->select()->from(array('art_artworks_images'),array('id'=>'id'))->where('artwork_id = ' . $id)->where('image = "' . $image[$i].'"');  
        	$row = $db->fetchAll($where);
			$rec	= $row->toArray();
				if(count($rec)<=0)
				{
				$merg_arr=array('artwork_id'=>$id, 'image'=>$image[$i],'create_date'=>date("Y-m-d H:i:s")); //print_r($merg_arr); die();
				$db->insert($merg_arr);
				}
			}
		}
		
    }
    
    ####-------------------------------END---------------------------------------####
	public function updateartworkprice($id,$dbField)
    {// print_r($dbField); die();
	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $this->update($dbField,'id = '. (int)$id);
    }
####---------------Update Data in art_artworks Table----------------####
	public function updateartworkpaystatus($id,$data)
    { //echo $id;print_r($data);
	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        
        $this->update($data,'id = '. (int)$id);
		
    }
 ####-------------------------------END---------------------------------------####
 ####---------------Update Data in art_artworks Table----------------####
public function updateartworksoldstatus($id,$status)
    {
	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $data = array(
            'sold_status'		=> $status
			
        );
        $this->update($data,'id = '. (int)$id);
    }
 ####-------------------------------END---------------------------------------####
	    ####---------------Update Data in art_artworks Table----------------####
public function updateArtworkuploadliststatus($id,$status)
    {
	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $data = array(
            'approval_status'		=> $status,
		'bid_start_date'		=> date("Y-m-d H:i:s")	
        );
        $this->update($data,'id IN ('.$id.') ' );
    }
 ####-------------------------------END---------------------------------------####
	    ####---------------Delete Data in art_artworks and art_artworks_images Table----------------####	
	
   public function deleteartworkuploadlist($id)
    { 
	   //echo'hi'. $id;die;
	   $db = Zend_Db_Table_Abstract::getDefaultAdapter();
		//$db = $this->getDbTable();
		$stmt = $db->query('DELETE FROM art_artworks_images WHERE artwork_id="'.$id.'"');
		 $where	=	$this->getAdapter()->quoteInto('id = ?', $id);
		return $this->delete($where);
    }
	    ####---------------temp Delete Data in art_artworks and art_artworks_images Table----------------####	
	
   public function deleteartworkuploadtemplist($id)
    { 
	   $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $data = array(
            'delete'		=> '1'
			
        );
        $this->update($data,'id IN ('.$id.') ' );
    }
 ####-------------------------------END---------------------------------------####
 
 public function userartwork($owner_id) {
		
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt = $db->query('SELECT DISTINCT(aw.id) as wid,aw.*,awim.*,DATE_FORMAT(aw.bid_start_date,"%D %b %Y")  as b_date FROM art_artworks aw INNER JOIN art_artworks_images awim ON aw.id = awim.artwork_id WHERE owner_id="'.$owner_id.'"    AND aw.delete = 0 GROUP BY aw.id  ORDER BY awim.id');
        $result = $stmt->fetchAll();
        
		
		return $result;
	}
 ####-------------------------------get artwork for adminD---------------------------------------####
 
 public function getartworkadmin($searchelement,$sortBy,$category,$owner_id) {
		
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$sql='SELECT * FROM art_artworks aw WHERE aw.delete = 0 ';
		if($searchelement!='')
		$sql.='  and title LIKE "%'.$searchelement.'%"';
		if($category!='')
		$sql.='  and category ='.$category.'';
		if($owner_id!='')
		$sql.='  and owner_id ='.$owner_id.'';
		if($sortBy!='')
		$sql.='  ORDER BY '.$sortBy.' ';
		else
		$sql.='  ORDER BY id DESC'; 
		$stmt = $db->query($sql);
        $result = $stmt->fetchAll();
        
		
		return $result;
	}
	
 ####-------------------------------END---------------------------------------####
 
 public function deleteimageartwork($id) {
		
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('id = ?', $id); 
		return $this->getDbTable()->delete($where);
		
		

	}
####-------------------------------END---------------------------------------####
 
 public function deleteimageartworkByName($image) {
		
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('image = ?', $image); 
		return $this->getDbTable()->delete($where);
		
		

	}
 ####-------------------------------GET a user all images---------------------------------------####
	public function getImagesAOwner($owner_id,$artwork_id)
	{
	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
	$stmt = $db->query('SELECT b.image, a.owner_id,a.id as id FROM art_artworks a LEFT JOIN art_artworks_images b ON a.id=b.artwork_id WHERE owner_id="'.$owner_id.'" AND payment_status=1 AND publish=1  AND a.delete = 0  AND CURDATE()>=a.bid_start_date AND a.id<>"'.$artwork_id.'" group by a.id');
	$result = $stmt->fetchAll();
        
		
		return $result;
	}
####-------------------------------get artwork title---------------------------------------#### 
public function getArtworkTitlelist($id) 
    {
        $id = (int)$id;
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$where	=	$this->select()->from(array('art_artworks'),array('title' => 'title'))->where('id = ' . $id);  
        $row = $this->fetchAll($where);
        return $row->toArray();  
    }
	
	/////////////find user name////////////////
	public function checkArtwork($id){
    	$id = $id;
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$where	=	$this->select()->from(array('art_artworks'),array('id' => 'id'))->where('id = ' . $id);  
    	 $row = $this->fetchAll($where);
    	
	 	

        return $row;
    }
	 public function getArtwork()
    {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$result = $this->fetchAll();
		return $rec	= $result->toArray();
    }
    
	public function updatePayment($id) {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $data = array(
            'payment_status'		=> 1
			
        );
        $this->update($data,'id IN ('.$id.') ' );
	}
	
	public function updateipn($key)
    {
	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
	$stmt = $db->query("INSERT INTO `art_ipn` (`key`)VALUES ('".$key."');");
    }
}

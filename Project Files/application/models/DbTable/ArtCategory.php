<?php
   
class Application_Model_DbTable_ArtCategory extends Zend_Db_Table_Abstract
{

    protected $_name = 'art_artwork_categories';
    protected $_dbTable;

    /**

   @	Added By : Reeta
   @	Added On :	20-12-2011	

   @
   **/	
    public function getCategorylist($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Count not find row $id");
        }
        return $row->toArray();    
    }
	 /**
   @	Added By :Reeta
   @	Added On :	03-02-2012	
    @
   **/	
    public function getCategorylistName($id,$cat_name) 
    { 
        $id = (int)$id;
		if($id>0 && $cat_name!='')
		{ 
		$db = $this->getDbTable();
		$where[] = $db->getAdapter()->quoteInto('id <> ?', $id);
		$where[] = $db->getAdapter()->quoteInto('cat_name = ?', $cat_name);
		$row = $this->fetchAll($where);
		}
		else if($cat_name!='')
		{ $row = $this->fetchAll($this->select()->where('cat_name = ?', $cat_name)); }
		else if($id>0)
		{
			$row = $this->fetchAll($this->select()->where('id = ?', $id)); }
        if (!$row) {
            throw new Exception("Count not find row $id");
        }
        return $row->toArray();    
    }
   /**

   @	Added By : Reeta
   @	Added On :	20-10-2011	
	@
   **/ 
    
	public function addCategorylist($cat_name,$addDate)
    {
        $data = array(
		'cat_name'		=> $cat_name,
           			'create_date'  	=> $addDate,

        ); 
        $this->insert($data);
    }
    
    public function updateCategorylist($id,$cat_name,$modify_date)
    {
        $data = array(
		'cat_name'		=> $cat_name,
            
			'modify_date'=>$modify_date
        );
        $this->update($data,'id = '. (int)$id);
    }
	
public function updateCategoryliststatus($id,$status)
    { 
        $data = array(
            'publish'		=> $status
			
        );  
        $this->update($data,'id IN ('.$id.') ' ); 
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
    public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_Categorylist');
        }
        return $this->_dbTable;
    }

   public function deletecategorylist($id)
    { 
	  
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('id IN ('.$id.') '); 
		return $this->delete($where);
    }

	public function getcategoryName() {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt = $db->query('SELECT cat_name as cat_name, id as id FROM art_artwork_categories WHERE publish=1 ORDER BY 1');
        $row = $stmt->fetchAll();
        return $row;
	}
	public function getcategoryNameById($id) {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt = $db->query('SELECT cat_name as cat_name, id as id FROM art_artwork_categories where id= "'.$id.'" ORDER BY 1');
        $row = $stmt->fetchAll(); 
        return $row;
	}
	/******************* Fetch art category (06-03-2012) ****************/
	public function categoryList(){
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt = $db->query('SELECT cat_name, id as id FROM art_artwork_categories WHERE publish = 1 ORDER BY cat_name DESC');
        $row = $stmt->fetchAll();
        return $row;
	}
}


<?php 
   
class Application_Model_DbTable_Artwinnerstatus extends Zend_Db_Table_Abstract
{

    protected $_name = 'art_winner_remainder_status';
    protected $_dbTable;
 	public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Artwinnerstatus');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    /**
   @	Add Faq
   @	Added By : Reeta Verma
   @	Added On :	20-03-2012	
   @	Input: void
   @	Return: reture the value from thr table.
   @ 
   **/	
    public function getWinnerRemainderStatus($id,$artwork_id) 
    {
       
		$db = $this->getDbTable();
		$where	=	$db->select()->from(array('art_winner_remainder_status'),array('*'))->where('user_id = ' . $id)->where('artwork_id = ' . $artwork_id); 
        $row = $this->fetchAll($where); 
        return $row->toArray();    
    }
	  ####---------------INSERT Data in  Table----------------####
    
	public function addWinnerRemainderStatus($dbField)
    {   $db = Zend_Db_Table_Abstract::getDefaultAdapter();
       //print_r($dbField);
		//$db = $this->getDbTable();	
	
		$artwork_id = $this->insert($dbField);
		
		
    }
public function updateWinnerRemainderStatus($id,$dbField)
    {   $db = Zend_Db_Table_Abstract::getDefaultAdapter();
       
		//$db = $this->getDbTable();	
	
		$this->update($dbField,'id = '. (int)$id);
		
		
    }
}


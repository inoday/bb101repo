<?php
 
 
class Application_Model_DbTable_Artworkfeedbacks extends Zend_Db_Table_Abstract
{ 
	protected $_name = 'art_artworks_feedbacks';
	protected $_dbTable;
	
 	public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Artworkfeedbacks');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
	    /**
   @	Added By : Reeta verma
   @	Added On :	21-02-2012	
   @	Return: reture all feadback of a artwork.
   @
   **/	
    public function getArtworkfeedbacks($id) 
    {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
		 $stmt = $db->query('SELECT *,u.first_name as uname,f.id fid, fc.id fcid, TIME_FORMAT(TIMEDIFF(NOW(),fc.create_date),"%H") feedtime, DATEDIFF(NOW(),fc.create_date) feedday , DATE_FORMAT(fc.create_date,"%D %b %Y") cdate FROM art_artworks_feedbacks f LEFT JOIN art_artworks_feedbacks_comments fc ON f.id=fc.feedback_id LEFT JOIN art_user u ON u.id=fc.user_id WHERE fc.artwork_id="'.$id.'"  AND f.publish=1 order by  fc.create_date ' ); 
	$result = $stmt->fetchAll();
		
		return $result;   
    }
    ####-------------------------------END---------------------------------------####	
	public function getArtworkfeedbacks_user($id,$limit) 
    {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt = $db->query('SELECT `art_artworks_feedbacks`.*,art_user.first_name username, TIME_FORMAT(TIMEDIFF(NOW(),art_artworks_feedbacks.create_date),"%H") AS `feedtime`, DATEDIFF(NOW(),art_artworks_feedbacks.create_date) AS `feedday`, DATE_FORMAT(art_artworks_feedbacks.create_date,"%D %b %Y") AS `cdate` FROM art_artworks_feedbacks , art_user  WHERE art_user.id=art_artworks_feedbacks.user_id AND art_artworks_feedbacks.artwork_id="'.$id.'" AND art_artworks_feedbacks.publish=1 ORDER BY art_artworks_feedbacks.create_date DESC limit '.$limit.', 4' );
	$result = $stmt->fetchAll(); 
	
	
	 
		
		return $result;   
    }
	
    ####---------------INSERT Data in  Table----------------####
    
	public function addartworkfeedbacks($dbField)
    {   $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $dbField;
		//$db = $this->getDbTable();	
	
		$artwork_id = $this->insert($dbField);
		
		
    }
    
    ####-------------------------------END---------------------------------------####
####-------------------------------No of main comment on a artwork---------------------------------------#### 
public function getCommentCountlist($id) 
    {
        $id = (int)$id;
		$db = $this->getDbTable();
		$where	=	$db->select()->from(array('art_artworks_feedbacks'),array('countcomment' => 'COUNT(*)'))->where('artwork_id = ' . $id)->group('artwork_id');  
        $row = $this->fetchAll($where);
        return $row->toArray();  
    }

####-------------------------------END---------------------------------------####
####-------------------------------delete comment on artwork---------------------------------------####

   public function deletecommentlist($id)
    { 
	   //echo'hi'. $id;die;
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('id = ?', $id); 
		return $this->delete($where);
    }

####-------------------------------update status of comment on artwork---------------------------------------####	
public function updatecommentliststatus($id,$status)
    {
        $data = array(
            'publish'		=> $status
			
        );
        $this->update($data,'id = '. (int)$id);
    }
####-------------------------------get main feedback and sub comment---------------------------------------####	
	public function getArtworkfeedbackscomment($id) 
    {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
		 $stmt = $db->query('SELECT *,u.first_name as uname,f.id fid, fc.id fcid, TIME_FORMAT(TIMEDIFF(NOW(),fc.create_date),"%H") feedtime, DATEDIFF(NOW(),fc.create_date) feedday , DATE_FORMAT(fc.create_date,"%D %b %Y") cdate FROM art_artworks_feedbacks f LEFT JOIN art_artworks_feedbacks_comments fc ON f.id=fc.feedback_id LEFT JOIN art_user u ON u.id=fc.user_id WHERE fc.id="'.$id.'"  order by  fc.create_date ' ); 
	$result = $stmt->fetchAll();
     
		
		return $result;   
    }
####-------------------------------get sun comment ---------------------------------------####	
	public function getArtworkfeedbackssubcomment($id) 
    {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
		 $stmt = $db->query('SELECT *,u.first_name as uname,f.id fid, fc.id fcid, TIME_FORMAT(TIMEDIFF(NOW(),fc.create_date),"%H") feedtime, DATEDIFF(NOW(),fc.create_date) feedday , DATE_FORMAT(fc.create_date,"%D %b %Y") cdate FROM art_artworks_feedbacks f LEFT JOIN art_artworks_feedbacks_comments fc ON f.id=fc.feedback_id LEFT JOIN art_user u ON u.id=fc.user_id WHERE fc.feedback_id="'.$id.'"  order by  fc.create_date ' ); 
	$result = $stmt->fetchAll();
     
		
		return $result;   
    }
####-------------------------------get main feedback by id---------------------------------------####	
	public function getArtworkfeedbacksbyIdcomment($id) 
    {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
		 $stmt = $db->query('SELECT * FROM art_artworks_feedbacks WHERE id="'.$id.'"  ' ); 
	$result = $stmt->fetchAll();
     
		
		return $result;   
    }
}
<?php
/**
@	Manage blog catagory 
@	Created by: Abhishek
@	Created On: 13-01-2012
@	Funtions / Methods:							
**/
class Application_Model_DbTable_ArtGlobelinfo extends Zend_Db_Table_Abstract
{   
	protected $_name = 'art_globelinfo'; 
    protected $_dbTable;
	
	public function getDbTable()
    {
		if (null === $this->_dbTable) 
		{
            $this->setDbTable('Application_Model_DbTable_ArtGlobelinfo');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) 
		{
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
	
	/************* Get all global values in array form *********************/
	public function getglobelinfoAll()
	{	
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$global = 'SELECT * FROM art_globelinfo';
	    $stmt = $db->query($global);
	    $globalinfo = $stmt->fetchAll();
	    $arr = $globalinfo; //unserialize($globalinfo[0]['value']);
	    //print_r($arr);die;
		return $arr;
	}
		
    /******************** Insert and update global info in 'art_globelinfo' table ********************/
 	public function addglobleinfo($dbField)
    {  
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$global = 'SELECT * FROM art_globelinfo';
	    $stmt = $db->query($global);
	    $result = $stmt->fetchAll();
		
		if(count($result)>0)
		{
		  $rec	=	$db->update('art_globelinfo',$dbField);
		  return 'err'; 
		}
		else
		{ 
		  $this->insert($dbField);
		}
    }
}

<?php
 /**
 @	Manage blog catagory
 @	Project: Findbuddy
 @	Created by: Anil Rawat
 @	Created On: 14-11-2011
 @	Funtions / Methods:
 							
 **/
 
class Application_Model_DbTable_ArtBlogcat extends Zend_Db_Table_Abstract
{   
	 
	
    protected $_name = 'art_blogcat';
    protected $_dbTable;
	////////edit gift list//////////
	 public function getblogcate($blogcat_id) 
    {
	
		
        $blogcat_id = (int)$blogcat_id;
		
		if($blogcat_id == 0 || is_null($blogcat_id))
		{
			return false;
		}
		
        $row = $this->getDbTable()->fetchRow('blogcat_id = ' . $blogcat_id);
		
		
		
        if (!$row) {
            throw new Exception("Count not find row $blogcat_id");
        }
		
		
		
        return $row->toArray();    
    }
	
	public function getCategory()
	{     
	      //echo'<pre>';print_r($this->getDbTable());die('hiiiii');
		  $tr	= $this->getDbTable()->fetchAll();
		  return $tr->toArray();
	}
	////////edit gift list end//////////
	
	/////////for edit///////////////////
	public function updateblog($dbField)
    {   
	    
		   $db = $this->getDbTable();
		   $this->update($dbField, 'blogcat_id ='.(int)$dbField['blogcat_id']);
		   return 'edit'; 
       
    }
	////////////////end  edit////////////////
	
	public function blogcatAll()
	{     
	      //echo'<pre>';print_r($this->getDbTable());die('hiiiii');
		  return $this->getDbTable()->fetchAll();
	}
	
	 public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_ArtBlogcat');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
   
	public function addblogcat($dbField)
    {   
	    
        $dbField;
		$squestion = trim($dbField['blogcat_title']);
		$db = $this->getDbTable();
		if(!empty($squestion)){
		$where	=	$db->select()->where("blogcat_title Like '$squestion' ");
		}
		$result =  $this->getDbTable()->fetchAll($where);
		$members = $result->toArray();
		if(count($members)>0)
		{ 
		   return 'err'; 
		}
		else
		{
		  $this->insert($dbField);
		}
		
    }
    
	public function deleteblogcate($blogcat_id)
    { 
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('blogcat_id = ?', $blogcat_id); 
		return $this->getDbTable()->delete($where);
    }
	
}

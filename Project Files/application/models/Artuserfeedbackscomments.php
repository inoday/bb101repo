<?php
class Application_Model_Artuserfeedbackscomments   
{ 
	
	protected $_dbTable;
	
	public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Artuserfeedbackscomments');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
	public function addUserFeedbackComment($dbField)
    {   
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
       	$this->getDbTable()->insert($dbField);
    }
}
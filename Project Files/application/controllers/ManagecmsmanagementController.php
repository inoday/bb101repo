<?php
/*
@	controller for 'Manage Cms management' module
@	Add by : Abhishek 
@		On: 13-1-2012
**/   

class ManagecmsmanagementController extends Zend_Controller_Action
{    
     
     public function init()
    {   
	    
      	$namespace = new Zend_Session_Namespace();
		 
		if($namespace->adminemail =='') 
		{  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
    }


    
/*
@	show cmslist from the table
@	Add by : Abhishek
@		On: 20-10-2011
**/
		public function cmslistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
			
				
			$cmslist_model   = new Application_Model_DbTable_Cmslist();
			$cmslist_model_result = $cmslist_model->fetchAll();	
			$this->view->cmslist_model_result = $cmslist_model_result;
			
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}
/*
@	add & edit cmslist from the table
@	Add by : Abhishek
@		On: 20-10-2011
**/		
	public function addcmslistAction(){
		
		$this->_helper->layout->setLayout('adminlayout');
		$this->view->bodyCopy = "<p >Please fill out this form.</p>";
		
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		
		$this->view->adminemail = $adminemail;
		//if admin session is logout it's go to index page
		if($adminemail == ''){
			$this->_helper->redirector('index');
		}
		
		$id = $this->_getParam('id', 0);
	    $form = new Application_Form_Cmslist();
		
        $this->view->form = $form;
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getParams();
			 
            if ($form->isValid($formData)) {
             	
				$cms_page_name   = $form->getValue('cms_page_name');
				$cms_heading     = $form->getValue('cms_heading');
				$cms_description     = $form->getValue('cms_description');
				$addDate 	= date("Y-m-d H:i:s");
				$tutorial   = new Application_Model_DbTable_Cmslist();
			
				if($id==0){
					//add the 'cmslist'
					$tutorial->addcmslist($cms_page_name,$cms_heading,$cms_description,$addDate);
					$msg	=	'Content added successfully.';
					$this->_helper->redirector->gotosimple('cmslist','Managecmsmanagement',true,array('msg'=>$msg)) ;
				}
				else {
				    //edit the 'cmslist'
				  $tutorial->updatecmslist($id,$cms_page_name,$cms_heading,$cms_description,$addDate);
				  $msg	=	'Content update successfully.';
					$this->_helper->redirector->gotosimple('cmslist','Managecmsmanagement',true,array('msg'=>$msg)) ;
				}
				
            } else {
                $form->populate($formData);
            }
        }
		
		$id = $this->_getParam('id', 0);
				
		if ($id > 0) {
			$cmslist = new Application_Model_DbTable_Cmslist();
			
			//getting the 'cmslist' for edit	
			foreach ($cmslist->getcmslist($id) as $key => $value) {
				$record[$key]	=	stripslashes($value);
			}
			$form->populate($record);
		}
	}
/*
@	Delete cmslist from the table
@	Add by : Abhishek
@		On: 20-10-2011
**/		
	public function  deletecmslistAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		$id			=	$request['id'];
		
		$user			=	new Application_Model_DbTable_Cmslist();
		$deletecmslist		=	$user->deletecmslist($id);
		$this->view->deletecmslist	=	$deletecmslist;
		
		$msg	=	'Content deleted successfully.';
		$this->_helper->redirector->gotosimple('cmslist','Managecmsmanagement',true,array('msg'=>$msg)) ;

	}

	
	
    
/*
@	show sectionlist from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/
		public function sectionlistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
			
				
			$sectionlist_model   = new Application_Model_DbTable_Sectionlist();
			$sectionlist_model_result = $sectionlist_model->fetchAll();	
			//$this->view->sectionlist_model_result = $sectionlist_model_result;
			
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}
			$page = $this->_getParam('page',1);
			//send the page no.
			$this->view->page = $page;			
			//////////////////////////////////////////////////////////////////////////////////////	
			//added by reeta (06-03-2012)
			////////////////////////////////////////////////////////////////////////////
		$this->view->sortBy	=	$request['sortBy'];
		if($request['searchelement']!='')
		{
			//serching all element with the table
			$searchelement	=	$request['searchelement'];
			//show the search element on the search box
			$namespace->psquestion = $request['searchelement'];
			$this->view->psquestion = $namespace->psquestion;	
			
			if($request['sortBy']!='')
			{
			$sectionlist_model_result = $sectionlist_model->fetchAll($sectionlist_model->select()->where('section_name Like ?', '%'.$searchelement.'%')->order($request['sortBy']));
			}
			else
			$sectionlist_model_result = $sectionlist_model->fetchAll($sectionlist_model->select()->where('section_name Like ?', '%'.$searchelement.'%'));	
		}
		else if($request['sortBy']!='')
			{
			$sectionlist_model_result = $sectionlist_model->fetchAll($sectionlist_model->select()->where('section_name Like ?', '%'.$searchelement.'%')->order($request['sortBy']));
			}

			
		$this->view->sectionlist_model_result = $sectionlist_model_result;
		//end added by reeta (06-03-2012)
			/////////////////////////////////////////////////////////////////////////////
			

			$page = $this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($sectionlist_model_result);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			$this->view->paginator = $paginator;
		}

////////////////////////////////////////////////////////////////////////
		/*
@	add & edit sectionlist from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/		
	public function addsectionlistAction(){
		
		$this->_helper->layout->setLayout('adminlayout');
		
		
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		
		$this->view->adminemail = $adminemail;
		//if admin session is logout it's go to index page
		if($adminemail == ''){
			$this->_helper->redirector('index');
		}
		
		$id = $this->_getParam('id', 0);
		if($id==0)
		$this->view->bodyCopy = "Add Section";
		else
		$this->view->bodyCopy = "Edit Section";
	    $form = new Application_Form_Sectionlist();
		
        $this->view->form = $form;
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getParams();
			 
            if ($form->isValid($formData)) {
             	
				$section_name   = $form->getValue('section_name');
				$section_description     = $form->getValue('section_description');
				$addDate 	= date("Y-m-d H:i:s");
				$tutorial   = new Application_Model_DbTable_Sectionlist();
			
				if($id==0){
				
					//add the 'sectionlist'
					$tutorial->addSectionlist($section_name,$section_description,$addDate);
					$msg	=	'Content added successfully.';
					$this->_helper->redirector->gotosimple('sectionlist','Managecmsmanagement',true,array('msg'=>$msg)) ;
				}
				else {
				
				    //edit the 'sectionlist'
				  $tutorial->updateSectionlist($id,$section_name,$section_description);
				  $msg	=	'Content update successfully.';
					$this->_helper->redirector->gotosimple('sectionlist','Managecmsmanagement',true,array('msg'=>$msg)) ;
				}
				
            } else {
                $form->populate($formData);
            }
        }
		
		$id = $this->_getParam('id', 0);
				
		if ($id > 0) {
			$sectionlist = new Application_Model_DbTable_Sectionlist();
			
			//getting the 'sectionlist' for edit	
			foreach ($sectionlist->getSectionlist($id) as $key => $value) {
				$record[$key]	=	stripslashes($value);
			}
			$form->populate($record);
		}
	}
	
	/*
@	Delete sectionlist from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/		
	public function  deletesectionlistAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		
		$user			=	new Application_Model_DbTable_Sectionlist();
		$deletesectionlist		=	$user->deletesectionlist($id);
		$this->view->deletesectionlist	=	$deletesectionlist;

		$msg	=	'Content deleted successfully.';
		$this->_helper->redirector->gotosimple('sectionlist','Managecmsmanagement',true,array('msg'=>$msg)) ;

	}
	
	/*
@	
@	Add by : Reeta Verma
@		On: 21-01-2012
**/		
	public function  updatesectionliststatusAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		$status			=	$request['status'];
		$user			=	new Application_Model_DbTable_Sectionlist();
		$updateSectionliststatus		=	$user->updateSectionliststatus($id,$status);
		$this->view->updateSectionliststatus	=	$updateSectionliststatus;
	
		if($status=='1')
		$msg	=	'Status published successfully.';
		else
		$msg	=	'Status unpublished successfully.';
		$this->_helper->redirector->gotosimple('sectionlist','Managecmsmanagement',true,array('msg'=>$msg)) ;

	}
/*
@	show sectionlist from the table
@	Add by : Reeta Verma
@		On: 23-01-2012
**/
		public function viewsectionlistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
			
			$id = $this->_getParam('id', 0);	
			$sectionlist_model   = new Application_Model_DbTable_Sectionlist();
			$sectionlist_model_result = $sectionlist_model->fetchAll($sectionlist_model->select()->where('id = ?', $id));	
			$this->view->sectionlist_model_result = $sectionlist_model_result;
			
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////CATEGORY//////////////////////////
/*
@	show categorylist from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/
		public function categorylistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
			
			$id = $this->_getParam('sec', 0);	
			$categorylist_model   = new Application_Model_DbTable_Categorylist();
			$Sql=$categorylist_model->select()->where('section_id = ?', $id);
			$categorylist_model_result = $categorylist_model->fetchAll($categorylist_model->select()->where('section_id = ?', $id));
			/////////////get section name through sectionid////////////////////////////////////////
			$sectionlist_model   = new Application_Model_DbTable_Sectionlist(); 
			$sql=$sectionlist_model->select()->where('id = ?', $id);
			$sectionlist_model_result = $sectionlist_model->fetchAll($sectionlist_model->select()->where('id = ?', $id));	
			$this->view->sectionlist_model_result = $sectionlist_model_result;
			////////////////////////////////////////////////////////////////////////////////////////
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			$page = $this->_getParam('page',1);
			//send the page no.
			$this->view->page = $page;		
			//////////////////////////////////////////////////////////////////////////////////////	
			//added by reeta (06-03-2012)
			////////////////////////////////////////////////////////////////////////////
			$this->view->sortBy	=	$request['sortBy'];
		if($request['searchelement']!='')
		{
			//serching all element with the table
			$searchelement	=	$request['searchelement'];
			//show the search element on the search box
			$namespace->psquestion = $request['searchelement'];
			$this->view->psquestion = $namespace->psquestion;	

		if($request['sortBy']!='')
			{
			$categorylist_model_result = $categorylist_model->fetchAll($categorylist_model->select()->where('section_id = ?', $id)->where('category_name Like ?', '%'.$searchelement.'%')->order($request['sortBy']));
			}
			else
			$categorylist_model_result = $categorylist_model->fetchAll($categorylist_model->select()->where('section_id = ?', $id)->where('category_name Like ?', '%'.$searchelement.'%'));
		}
		else if($request['sortBy']!='')
			{
			$categorylist_model_result = $categorylist_model->fetchAll($categorylist_model->select()->where('section_id = ?', $id)->where('category_name Like ?', '%'.$searchelement.'%')->order($request['sortBy']));
			}


			
		$this->view->categorylist_model_result = $categorylist_model_result; 
		//end added by reeta (06-03-2012)
			/////////////////////////////////////////////////////////////////////////////

			$page = $this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($categorylist_model_result);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			$this->view->paginator = $paginator;
		}
		/*
@	add & edit categorylist from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/		
	public function addcategorylistAction(){
		
		$this->_helper->layout->setLayout('adminlayout');
	
		
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		
		$this->view->adminemail = $adminemail;
		//if admin session is logout it's go to index page
		if($adminemail == ''){
			$this->_helper->redirector('index');
		}
			/////////////get section name through sectionid////////////////////////////////////////
			$sec = $this->_getParam('sec', 0);	
			$sectionlist_model   = new Application_Model_DbTable_Sectionlist();
			$sectionlist_model_result = $sectionlist_model->fetchAll($sectionlist_model->select()->where('id = ?', $sec));	
			$this->view->sectionlist_model_result = $sectionlist_model_result;
			////////////////////////////////////////////////////////////////////////////////////////

		$id = $this->_getParam('id', 0);
			if($id==0)
		$this->view->bodyCopy = "Add Category";
		else
		$this->view->bodyCopy = "Edit Category";
	    $form = new Application_Form_Categorylist();
		
        $this->view->form = $form;
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getParams();
			 
            if ($form->isValid($formData)) {
             	
				$category_name   = $form->getValue('category_name');
				$category_description     = $form->getValue('category_description');
				$section_id     = $sec;
				$addDate 	= date("Y-m-d H:i:s");
				$tutorial   = new Application_Model_DbTable_Categorylist();
			
				if($id==0){
					//add the 'categorylist'
					$tutorial->addCategorylist($section_id,$category_name,$category_description,$addDate);
					$msg	=	'Content added successfully.';
					$this->_helper->redirector->gotosimple('categorylist','Managecmsmanagement',true,array('msg'=>$msg,'sec'=>$section_id)) ;
				}
				else {
				    //edit the 'categorylist'
				  $tutorial->updateCategorylist($id,$section_id,$category_name,$category_description);
				  $msg	=	'Content update successfully.';
					$this->_helper->redirector->gotosimple('categorylist','Managecmsmanagement',true,array('msg'=>$msg,'sec'=>$section_id)) ;
				}
				
            } else {
                $form->populate($formData);
            }
        }
		
		$id = $this->_getParam('id', 0);
				
		if ($id > 0) {
			$categorylist = new Application_Model_DbTable_Categorylist();
			
			//getting the 'categorylist' for edit	
			foreach ($categorylist->getCategorylist($id) as $key => $value) {
				$record[$key]	=	stripslashes($value);
			}
			$form->populate($record);
		}
	}
	
	/*
@	Delete categorylist from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/		
	public function  deletecategorylistAction()
	{ 	
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		$section_id = $this->_getParam('sec', 0);
		$user			=	new Application_Model_DbTable_Categorylist();
		$deletecategorylist		=	$user->deletecategorylist($id);
		$this->view->deletecategorylist	=	$deletecategorylist;
	
		$msg	=	'Content deleted successfully.';
		$this->_helper->redirector->gotosimple('categorylist','Managecmsmanagement',true,array('msg'=>$msg,'sec'=>$section_id)) ;

	}	
/*
@	
@	Add by : Reeta Verma
@		On: 21-01-2012
**/		
	public function  updatecategoryliststatusAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		$status			=	$request['status'];
		$section_id = $this->_getParam('sec', 0);
		$user			=	new Application_Model_DbTable_Categorylist();
		$updateCategoryliststatus		=	$user->updateCategoryliststatus($id,$status);
		$this->view->updateCategoryliststatus	=	$updateCategoryliststatus;

		if($status=='1')
		$msg	=	'Status published successfully.';
		else
		$msg	=	'Status unpublished successfully.';
		$this->_helper->redirector->gotosimple('categorylist','Managecmsmanagement',true,array('msg'=>$msg,'sec'=>$section_id)) ;

	}	
/*
@	
@	Add by : Reeta Verma
@		On: 23-01-2012
**/		
	public function viewcategorylistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
			$sec = $this->_getParam('sec', 0);	
			/////////////get section name through sectionid////////////////////////////////////////
			$sectionlist_model   = new Application_Model_DbTable_Sectionlist();
			$sectionlist_model_result = $sectionlist_model->fetchAll($sectionlist_model->select()->where('id = ?', $sec));	
			$this->view->sectionlist_model_result = $sectionlist_model_result;
			////////////////////////////////////////////////////////////////////////////////////////
			$id = $this->_getParam('id', 0);	
			$categorylist_model   = new Application_Model_DbTable_Categorylist();
			$categorylist_model_result = $categorylist_model->fetchAll($categorylist_model->select()->where('id = ?', $id));	
			$this->view->categorylist_model_result = $categorylist_model_result;
			
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////SUB CATEGORY//////////////////////////
/*
@	show subcategory from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/
		public function subcategorylistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
			
			$id = $this->_getParam('sec', 0);	
			$cat= $this->_getParam('cat', 0);	
			$subcategorylist_model   = new Application_Model_DbTable_Subcategorylist();
			$subcategorylist_model_result = $subcategorylist_model->fetchAll($subcategorylist_model->select()->where('category_id = ?', $cat));	
			//$this->view->subcategorylist_model_result = $subcategorylist_model_result;
			/////////////get section name through sectionid////////////////////////////////////////
			$sectionlist_model   = new Application_Model_DbTable_Sectionlist();
			$sectionlist_model_result = $sectionlist_model->fetchAll($sectionlist_model->select()->where('id = ?', $id));	
			$this->view->sectionlist_model_result = $sectionlist_model_result;
			////////////////////////////////////////////////////////////////////////////////////////
			/////////////get category name through sectionid////////////////////////////////////////
			$Categorylist_model   = new Application_Model_DbTable_Categorylist();
			$Categorylist_model_result = $Categorylist_model->fetchAll($Categorylist_model->select()->where('id = ?', $cat));	
			$this->view->Categorylist_model_result = $Categorylist_model_result;
			////////////////////////////////////////////////////////////////////////////////////////
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			$page = $this->_getParam('page',1);
			//send the page no.
			$this->view->page = $page;			
			
				////////////////////////////////////////////////////////////////////////////
				$this->view->sortBy	=	$request['sortBy'];
		if($request['searchelement']!='')
		{
			//serching all element with the table
			$searchelement	=	$request['searchelement'];
			//show the search element on the search box
			$namespace->psquestion = $request['searchelement'];
			$this->view->psquestion = $namespace->psquestion;	
	
				if($request['sortBy']!='')
			{
			$subcategorylist_model_result = $subcategorylist_model->fetchAll($subcategorylist_model->select()->where('category_id = ?', $cat)->where('subcategory_name Like ?', '%'.$searchelement.'%')->order($request['sortBy']));	
			}
			else {
			$subcategorylist_model_result = $subcategorylist_model->fetchAll($subcategorylist_model->select()->where('category_id = ?', $cat)->where('subcategory_name Like ?', '%'.$searchelement.'%'));	}
		}
		else if($request['sortBy']!='')
			{
			$subcategorylist_model_result = $subcategorylist_model->fetchAll($subcategorylist_model->select()->where('category_id = ?', $cat)->where('subcategory_name Like ?', '%'.$searchelement.'%')->order($request['sortBy']));	
			}


			
		$this->view->subcategorylist_model_result = $subcategorylist_model_result;
		//end added by reeta (06-03-2012)
			/////////////////////////////////////////////////////////////////////////////

			$page = $this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($subcategorylist_model_result);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			$this->view->paginator = $paginator;
		}
		/*
@	add & edit subcategory from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/		
	public function addsubcategorylistAction(){
		
		$this->_helper->layout->setLayout('adminlayout');
		
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		
		$this->view->adminemail = $adminemail;
		//if admin session is logout it's go to index page
		if($adminemail == ''){
			$this->_helper->redirector('index');
		}
			/////////////get section name through sectionid////////////////////////////////////////
			$sec = $this->_getParam('sec', 0);	
			$sectionlist_model   = new Application_Model_DbTable_Sectionlist();
			$sectionlist_model_result = $sectionlist_model->fetchAll($sectionlist_model->select()->where('id = ?', $sec));	
			$this->view->sectionlist_model_result = $sectionlist_model_result;
			////////////////////////////////////////////////////////////////////////////////////////
			/////////////get category name through categoryid////////////////////////////////////////
			$cat = $this->_getParam('cat', 0);	
			$categorylist_model   = new Application_Model_DbTable_Categorylist();
			$categorylist_model_result = $categorylist_model->fetchAll($categorylist_model->select()->where('id = ?', $cat));	
			$this->view->categorylist_model_result = $categorylist_model_result;
			////////////////////////////////////////////////////////////////////////////////////////

		$id = $this->_getParam('id', 0);
			if($id==0)
		$this->view->bodyCopy = "Add Sub-category";
		else
		$this->view->bodyCopy = "Edit Sub-category";
	    $form = new Application_Form_Subcategorylist();
		
        $this->view->form = $form;
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getParams();
			 
            if ($form->isValid($formData)) {
				$subcategory_name   = $form->getValue('subcategory_name');
				$subcategory_description     = $form->getValue('subcategory_description');
				$section_id     = $form->getValue('section_id');
				$category_id     = $form->getValue('category_id');
				$addDate 	= date("Y-m-d H:i:s");
				$tutorial   = new Application_Model_DbTable_Subcategorylist();
			
				if($id==0){
					//add the 'subcategory'
					$tutorial->addSubcategorylist($section_id,$category_id,$subcategory_name,$subcategory_description,$addDate);
					$msg	=	'Content added successfully.';
					$this->_helper->redirector->gotosimple('subcategorylist','Managecmsmanagement',true,array('msg'=>$msg,'sec'=>$section_id,'cat'=>$category_id)) ;
				}
				else {
				    //edit the 'subcategory'
				  $tutorial->updateSubcategorylist($id,$section_id,$category_id,$subcategory_name,$subcategory_description);
				  $msg	=	'Content update successfully.';
					$this->_helper->redirector->gotosimple('subcategorylist','Managecmsmanagement',true,array('msg'=>$msg,'sec'=>$section_id,'cat'=>$category_id)) ;
				}
				
            } else {
                $form->populate($formData);
            }
        }
		
		$id = $this->_getParam('id', 0);
				
		if ($id > 0) {
			$subcategorylist = new Application_Model_DbTable_Subcategorylist();
			
			//getting the 'subcategory' for edit	
			foreach ($subcategorylist->getSubcategorylist($id) as $key => $value) {
				$record[$key]	=	stripslashes($value);
			}
			$form->populate($record);
		}
	}
	
	/*
@	Delete subcategory from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/		
	public function  deletesubcategorylistAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		$section_id = $this->_getParam('sec', 0);
		$category_id = $this->_getParam('cat', 0);
		$user			=	new Application_Model_DbTable_Subcategorylist();
		$deletesubcategorylist		=	$user->deletesubcategorylist($id);
		$this->view->deletesubcategorylist	=	$deletesubcategorylist;

		$msg	=	'Content deleted successfully.';
		$this->_helper->redirector->gotosimple('subcategorylist','Managecmsmanagement',true,array('msg'=>$msg,'sec'=>$section_id,'cat'=>$category_id)) ;

	}
	
	/*
@	
@	Add by : Reeta Verma
@		On: 21-01-2012
**/		
	public function  updatesubcategoryliststatusAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		$status			=	$request['status'];
		$section_id = $this->_getParam('sec', 0);
		$category_id = $this->_getParam('cat', 0);
		$user			=	new Application_Model_DbTable_Subcategorylist();
		$updateSubcategoryliststatus		=	$user->updateSubcategoryliststatus($id,$status);
		$this->view->updateSubcategoryliststatus	=	$updateSubcategoryliststatus;

		if($status=='1')
		$msg	=	'Status published successfully';
		else
		$msg	=	'Status unpublished successfully';
		$this->_helper->redirector->gotosimple('subcategorylist','Managecmsmanagement',true,array('msg'=>$msg,'sec'=>$section_id,'cat'=>$category_id)) ;

	}
	
	/*
@	show subcategory from the table
@	Add by : Reeta Verma
@		On: 23-01-2012
**/
		public function viewsubcategorylistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
			$id = $this->_getParam('id', 0);
			$sec = $this->_getParam('sec', 0);	
			$cat= $this->_getParam('cat', 0);	
			$subcategorylist_model   = new Application_Model_DbTable_Subcategorylist();
			$subcategorylist_model_result = $subcategorylist_model->fetchAll($subcategorylist_model->select()->where('id = ?', $id));	
			$this->view->subcategorylist_model_result = $subcategorylist_model_result;
			/////////////get section name through sectionid////////////////////////////////////////
			$sectionlist_model   = new Application_Model_DbTable_Sectionlist();
			$sectionlist_model_result = $sectionlist_model->fetchAll($sectionlist_model->select()->where('id = ?', $sec));	
			$this->view->sectionlist_model_result = $sectionlist_model_result;
			////////////////////////////////////////////////////////////////////////////////////////
			/////////////get category name through sectionid////////////////////////////////////////
			$Categorylist_model   = new Application_Model_DbTable_Categorylist();
			$Categorylist_model_result = $Categorylist_model->fetchAll($Categorylist_model->select()->where('id = ?', $cat));	
			$this->view->Categorylist_model_result = $Categorylist_model_result;
			////////////////////////////////////////////////////////////////////////////////////////
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}	
}
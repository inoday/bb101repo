<?php
/*
@	controller for 'Manage blog management' module
@	Add by : Anil Rawat
@		On: 07-11-2011
**/

class adminmanagecommentController extends Zend_Controller_Action
{    
     
    public function init()
    {   
	    
      	$namespace = new Zend_Session_Namespace();
		 
		if($namespace->adminemail =='') 
		{  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
    }
   ///////////////////////////comment//////////////////////////
/*
@	Add by : Reeta Verma
@		On: 01-03-2012
**/ 
		public function artworkcommentlistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
			
			$artwork_id = $this->_getParam('id', 0);
			$commentlist_model   = new Application_Model_DbTable_Artworkfeedbacks();
			$commentlist_model_result = $commentlist_model->fetchAll($commentlist_model->select()->where('artwork_id = ' . $artwork_id));	
			$this->view->commentlist_model_result = $commentlist_model_result;//echo "<pre>";print_r($commentlist_model_result); die();
			
			////////////////////////////////////////////////////////////////////////////////////////
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			$page = $this->_getParam('page',1);
			//send the page no.
			$this->view->page = $page;			
			
			

			$page = $this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($commentlist_model_result);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			$this->view->paginator = $paginator;
		}

	
	public function  deletecommentlistAction()
	{ 
	$chk=$_POST['chk']; 
	if(count($chk)>0)
		{
		for($i=0;$i<count($chk);$i++)
		{
		$request 		=	$this->getRequest()->getParams();
		$did			=	$chk[$i];
		$id			=	$request['id'];
		
		$subcommentlist			=	new Application_Model_DbTable_Artworkfeedbackscomment();
		$deletesubcommentlist		=	$subcommentlist->deletecommentlist($did);
		
		$commentlist			=	new Application_Model_DbTable_Artworkfeedbacks();
		$deletecommentlist		=	$commentlist->deletecommentlist($did);
		$this->view->deletecommentlist	=	$deletecommentlist;
		}
		}
		else
		{
		$request 		=	$this->getRequest()->getParams();
		$did			=	$request['did'];
		$id			=	$request['id'];
		
		$subcommentlist			=	new Application_Model_DbTable_Artworkfeedbackscomment();
		$deletesubcommentlist		=	$subcommentlist->deletecommentlist($did);
		
		$commentlist			=	new Application_Model_DbTable_Artworkfeedbacks();
		$deletecommentlist		=	$commentlist->deletecommentlist($did);
		$this->view->deletecommentlist	=	$deletecommentlist;
		}
		$msg	=	'Comment deleted successfully.';
		$this->_helper->redirector->gotosimple('artworkcommentlist','Adminmanagecomment',true,array('msg'=>$msg,'id'=>$id)) ;

	}	
	
	public function  deletesubcommentlistAction()
	{ 
	
		$request 		=	$this->getRequest()->getParams();
		$did			=	$request['did'];
		$id			=	$request['id'];
		$subcommentlist			=	new Application_Model_DbTable_Artworkfeedbackscomment();
		$deletesubcommentlist		=	$subcommentlist->deletesubcommentlist($did);
		$this->view->deletesubcommentlist	=	$deletesubcommentlist;

		$msg	=	'Comment deleted successfully.';
		$this->_helper->redirector->gotosimple('viewcommentlist','Adminmanagecomment',true,array('msg'=>$msg,'id'=>$id)) ;

	}	
	
	public function  updatecommentliststatusAction()
	{ 
		$chk=$_POST['chk']; 
		if(count($chk)>0)
		{
		for($i=0;$i<count($chk);$i++)
		{
		$request 		=	$this->getRequest()->getParams();
		$did			=	$chk[$i];
		 $status			=	$request['status'];
		 $id			=	$request['id'];

		$user			=	new Application_Model_DbTable_Artworkfeedbacks();
		$updatecommentliststatus		=	$user->updatecommentliststatus($did,$status);
		$this->view->updatecommentliststatus	=	$updatecommentliststatus;
		}
		}
		else
		{
		$request 		=	$this->getRequest()->getParams();
		$did			=	$request['did'];
		 $status			=	$request['status'];
		$id			=	$request['id'];
		$user			=	new Application_Model_DbTable_Artworkfeedbacks();
		$updatecommentliststatus		=	$user->updatecommentliststatus($did,$status);
		$this->view->updatecommentliststatus	=	$updatecommentliststatus;
		}
		if($status=='1')
		$msg	=	'Published successfully.';
		else
		$msg	=	'Unpublished successfully.';
		$this->_helper->redirector->gotosimple('artworkcommentlist','Adminmanagecomment',true,array('msg'=>$msg,'id'=>$id)) ;

	}	

	public function viewcommentlistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
	
			////////////////////////////////////////////////////////////////////////////////////////
			$id = $this->_getParam('id', 0);	
			$commentlist_model   = new Application_Model_DbTable_Artworkfeedbacks();
			//$commentlist_model_result = $commentlist_model->getArtworkfeedbackscomment($id);
			$commentlist_model_result = $commentlist_model->fetchAll($commentlist_model->select()->where('id = ' . $id));	
	
			$this->view->commentlist_model_result = $commentlist_model_result;
			
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}	
/////////////////////////////////////////////////////////////////////////////////////////////////////////
		
}



 
?>
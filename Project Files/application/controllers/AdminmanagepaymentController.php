<?php
/*

**/

class AdminmanagepaymentController extends Zend_Controller_Action
{    
     
    public function init()
    {   
	    
      	$namespace = new Zend_Session_Namespace();
		 
		if($namespace->adminemail =='') 
		{  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
    }
   ///////////////////////////BID//////////////////////////
/*
@	Add by : Reeta Verma
@		On: 19-03-2012
**/ 
		public function paymentlistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
			$request  =	$this->getRequest()->getParams();
			 $this->view->sortBy	=	$request['sortBy']; 
			if($request['sortBy']=='')
			$sortBy='1';
			else
			$sortBy=$request['sortBy'];
			$paymentlist_model   = new Application_Model_DbTable_Artworkpayment();
			$paymentlist_model_result = $paymentlist_model->fetchAll($paymentlist_model->select()->from(array('art_artworks_listing_bid_payment'),array('countbid' => 'COUNT(*)','artwork_id'))->group('artwork_id')->order($sortBy));	
			if($request['bidder_id']!='')
			$paymentlist_model_result = $paymentlist_model->fetchAll($paymentlist_model->select()->from(array('art_artworks_listing_bid_payment'),array('countbid' => 'COUNT(*)','artwork_id'))->where('bidder_id = ?',$request['bidder_id'])->group('artwork_id')->order($sortBy));	
			if($request['artwork_id']!='')
			$paymentlist_model_result = $paymentlist_model->fetchAll($paymentlist_model->select()->from(array('art_artworks_listing_bid_payment'),array('countbid' => 'COUNT(*)','artwork_id'))->where('artwork_id = ?',$request['artwork_id'])->group('artwork_id')->order($sortBy));
			if($request['artwork_id']!='' && $request['bidder_id']!='')
			$paymentlist_model_result = $paymentlist_model->fetchAll($paymentlist_model->select()->from(array('art_artworks_listing_bid_payment'),array('countbid' => 'COUNT(*)','artwork_id'))->where('bidder_id = ?',$request['bidder_id'])->where('artwork_id = ?',$request['artwork_id'])->group('artwork_id')->order($sortBy));
			////////////////////////////////////////////////////////////////////////////////////////
			
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			$page = $this->_getParam('page',1);
			//send the page no.
			$this->view->page = $page;			
						
			
			$this->view->bidder_id = $request['bidder_id'];
			$this->view->artwork_id = $request['artwork_id'];
			$this->view->paymentlist_model_result = $paymentlist_model_result;

			$page = $this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($paymentlist_model_result);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			$this->view->paginator = $paginator;
		}

	
		public function viewpaymentlistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
	
			////////////////////////////////////////////////////////////////////////////////////////
			$id = $this->_getParam('auid', 0);	
			$paymentlist_model   = new Application_Model_DbTable_Artworkpayment();
			$paymentlist_model_result = $paymentlist_model->getPaymentlist($id);	//print_r($paymentlist_model_result);die();
			$this->view->paymentlist_model_result = $paymentlist_model_result;
			$this->view->id = $id;	
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}

	
/////////////////////////////////////////////////////////////////////////////////////////////////////////
			public function payforscoutAction()
			{
				$this->_helper->layout->setLayout('adminlayout');
				$request  =	$this->getRequest()->getParams();
			////////////////PAY INFO FROM GLOBAL TABLE (below paypal info value will be replace with scout payment info)///////////////////////////
			$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
			$rec1 =	$allgloble->getglobelinfoAll();
			
			$merchant_id		=	$rec1[0]['username'];
			$password			=	$rec1[0]['password'];
			$signature			=	$rec1[0]['signature'];
			$creditcardtype	    =	$rec1[0]['creditcardtype'];
			$creditcardnumber	=	$rec1[0]['creditcardnumber'];
			$exp_date_month	    =	$rec1[0]['exp_date_month'];
			$exp_date_year	    =	$rec1[0]['exp_date_year'];
			$cvv_number	   		=	$rec1[0]['cvv_number'];
			$firstname	    	=	$rec1[0]['C_first_name'];
			$lastName			=	$rec1[0]['C_last_name'];
			$address1    		=	$rec1[0]['street'];
			$city	    		=	$rec1[0]['city'];
			$state	   			=	$rec1[0]['state'];
			$zip	    		=	$rec1[0]['zip'];
			$countryid	   		=	$rec1[0]['country'];
			$paymentType		=	urlencode('Authorization');
			$currencyID 		= 	urlencode('GBP');	

			$id=$request['id'];
			$amount=$request['scout_amount'];
			
			 $nvpStr_ =	"&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditcardtype&ACCT=$creditcardnumber".
					"&EXPDATE=$exp_date_month$exp_date_year&CVV2=$cvv_number&FIRSTNAME=$firstname&LASTNAME=$lastName".
					"&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$countryid&CURRENCYCODE=$currencyID";
			
			
			global $environment;
 			$environment = 'sandbox';	// or 'beta-sandbox' or 'live'
		// Set up your API credentials, PayPal end point, and API version.
		 $API_UserName = urlencode($merchant_id);
		 $API_Password = urlencode($password);
		 $API_Signature = urlencode($signature);
		 $API_Endpoint = "https://api-3t.paypal.com/nvp";
		if("sandbox" === $environment || "beta-sandbox" === $environment) {
			$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
		}
		$version = urlencode('51.0');
		// Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		// Set the API operation, version, and API signature in the request.
		 $nvpreq = "METHOD=DoDirectPayment&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
		// Get response from the server.
	 $httpResponse = curl_exec($ch);
	//echo "<pre>";print_r(curl_getinfo($ch)); die();
		if(!$httpResponse) {
			exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
		}
		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);
	
		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i => $value) {
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}
		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
			exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
		}
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
//////////////////////Get mail content set by admin///////////////////////
	/*$getmailcontent	=	new Application_Model_ArtMailFormats();
$mailcontent		=	$getmailcontent->getMailContentById(23);
$formated_message=$mailcontent['content'];
$formated_subject=$mailcontent['subject'];
//////////////////////////////////////////////////////////////////////////////	

$name   = $firstname." ".$last_name;
$user_details   = new Application_Model_Artuser();
$userdetails=$user_details->getUserdetails($bidder_id); //print_r($userdetails);
$to=$userdetails[0]['email'];
		$message = sprintf($formated_message,$name,$title,$frame,$medium,$edition,$size);  
											$subject=$formated_subject;
										$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
											Zend_Mail::setDefaultTransport($tr);
											
											$mail = new Zend_Mail();
											$mail->setBodyHtml($message);
											$mail->setFrom('admin@artex.com', 'www.artex.com');
											$mail->addTo($to, $name);
											$mail->addCc($rec1[0]['contact_email'], 'Aministrator');
											$mail->setSubject($subject);
											$mail->send($tr);
			
////////////////////For purchase//////////////////////////////////////////////////////////////////////////////////////////	
/////////////////////////INSERT IN table art_inbox//////////////////////////////////////
$dbField = array('receiver_id' => $bidder_id,'sender_id' => '0', 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $subject,'message'=> $message,'folder_id'=> '0');
$inbox_res   	 = new Application_Model_DbTable_Artworkinbox();	
$rec = $inbox_res->addartworkinbox($dbField);
///////////////////////////////////////////////////////////////////////////////////////////
//add art_artworks_listing_transaction table
///////////////////////////////////////////////////////////////////////////////////*/
			 $status=1;
		 
			$paymentlist_model   = new Application_Model_DbTable_Artworkpayment();
			$paymentlist_model_result = $paymentlist_model->getPaymentlist($id);
			$res=$paymentlist_model_result[0]['id'];
				
		$dbFieldpay = array('artwork_id' => $id,'listing_pay_id' => $res,'paymentby_id' => '0','type'=>'scout_percentage', 'transaction_id' => $httpParsedResponseAr["TRANSACTIONID"], 'transaction_date' =>   date("Y-m-d H:i:s"), 'amount' => $amount, 'status' => $status,'credit_card' => $creditcardtype, 	'credit_card_number' => $creditcardnumber, 	'exp_month' => $exp_date_month, 	'exp_year' => $exp_date_year, 	'cvv2' => $cvv_number,'payment_type'=>'2' );
	
			$transaction   = new Application_Model_DbTable_Artworkbidtransaction();
			$res_transaction=$transaction->addartworkbidtransaction($dbFieldpay);
//////////////////////////////////////////////////////////////////////////////////////////////	
//update payment status art_artworks table
//////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////MAIL for successful payment//////////////////////////////////////////////////////////
		$this->_helper->redirector->gotosimple('viewpaymentlist','Adminmanagepayment',true,array('auid'=>$id,'msg'=>'Thank you for your credit card payment')) ;

			}
			else
		{
			//////////////////////Get mail content set by admin///////////////////////
			$getmailcontent	=	new Application_Model_ArtMailFormats();
			$mailcontent		=	$getmailcontent->getMailContentById(24);
			$formated_message=$mailcontent['content'];
			$formated_subject=$mailcontent['subject'];
			//////////////////////////////////////////////////////////////////////////////	
////////////////////////////////////////////////////////////////////// 159627
$name   = $firstname." ".$last_name;
$user_details   = new Application_Model_Artuser();
$userdetails=$user_details->getUserdetails($bidder_id); //print_r($userdetails);
$to=$userdetails[0]['email'];
$message = sprintf($formated_message,$name,$title,$frame,$medium,$edition,$size);  
											$subject=$formated_subject;
		/*									$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
											Zend_Mail::setDefaultTransport($tr);
											
											$mail = new Zend_Mail();
											$mail->setBodyHtml($message);
											$mail->setFrom('admin@artex.com', 'www.artex.com');
											$mail->addTo($to, $name);
											$mail->addCc($rec1[0]['contact_email'], 'Aministrator');
											$mail->setSubject($subject);
											$mail->send($tr);*/
			
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//add art_artworks_listing_transaction table
///////////////////////////////////////////////////////////////////////////////////
			 $status=0;
			 
			$paymentlist_model   = new Application_Model_DbTable_Artworkpayment();
			$paymentlist_model_result = $paymentlist_model->getPaymentlist($id);
			$res=$paymentlist_model_result[0]['id'];
			
		$dbFieldpay = array('artwork_id' => $id,'listing_pay_id' => $res,'paymentby_id' => '0','type'=>'scout_percentage', 'transaction_id' => $httpParsedResponseAr["TRANSACTIONID"], 'transaction_date' =>   date("Y-m-d H:i:s"), 'amount' => $amount, 'status' => $status,'credit_card' => $creditcardtype, 	'credit_card_number' => $creditcardnumber, 	'exp_month' => $exp_date_month, 	'exp_year' => $exp_date_year, 	'cvv2' => $cvv_number,'payment_type'=>'2' );
	
			$transaction   = new Application_Model_DbTable_Artworkbidtransaction();
			$res_transaction=$transaction->addartworkbidtransaction($dbFieldpay);
//////////////////////////////////////////////////////////////////////////////////////////////		

	 		$this->_helper->redirector->gotosimple('viewpaymentlist','Adminmanagepayment',true,array('auid'=>$id,'msg'=>'For Scout Percentage credit card payment failed.Please try again')) ;
		}
					die('g');
			}
			
			
}



 
?>
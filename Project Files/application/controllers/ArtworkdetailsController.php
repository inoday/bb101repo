<?php
class ArtworkdetailsController extends Zend_Controller_Action
{
 	public function init(){
 		   
    	/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			if($namespace->email)
			{
			$row=$user->getMemberID($namespace->email);
			$user_id=$row[0]['id'];
			$namespace->userid= $row[0]['id'];
			}
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
    }
    
    public function indexAction() {

    	$this->_helper->layout->disableLayout();
		 $this->_helper->layout->setLayout('layout');	
		  $request  =	$this->getRequest()->getParams();
			$id = $this->_getParam('id', 0);	
/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			if($namespace->email)
			{
			$row=$user->getMemberID($namespace->email);
			$user_id=$row[0]['id'];
			$namespace->userid= $row[0]['id'];
			}
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
///////////////// INFO FROM GLOBAL TABLE////////////////////////////////
 $allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
$rec =	$allgloble->getglobelinfoAll();
$this->view->rec=$rec;

// echo'<pre>';print_r($rec);//print_r($_POST);
/////////////////////////////////////////////////////////////////////////////////////->where('ADDDATE(bid_start_date, INTERVAL ? MINUTE) > NOW()', $rec[0]['auction_duration'])
			$today=date('Y-m-d');
			$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
			
			$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->from(array('art_artworks' => 'art_artworks'),array('*','count' => 'ADDDATE(bid_start_date, INTERVAL  auction_duration DAY)','ending'=>'TIMEDIFF(ADDDATE(bid_start_date, INTERVAL auction_duration DAY),NOW()-INTERVAL 0 MINUTE)'))->where('id = ?', $id)->where('payment_status = 1'));				
			$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result; 
			if(count($artworkuploadlist_model_result)<=0)
			{  $namespace->userid; 
				if($namespace->userid!='' || $request['mod']==1)
				{ 
					$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->from(array('art_artworks' => 'art_artworks'),array('*','count' => 'ADDDATE(bid_start_date, INTERVAL auction_duration DAY)','ending'=>'TIMEDIFF(ADDDATE(bid_start_date, INTERVAL auction_duration DAY),NOW()-INTERVAL 0 MINUTE)'))->where('id = ?', $id)->where('sold_status = 0')->where('payment_status = 1'));	 
			$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
			if(count($artworkuploadlist_model_result)<=0)
			{ 
			$this->_helper->redirector->gotosimple('index','index',true) ;
			}
			else {
					foreach($artworkuploadlist_model_result as $event){
						$cat_id= $event['category'];
						$owner_id= $event['owner_id'];
						 $artwork_id=$event['id'];
					}
					$artworkuploadmaxbid_model   = new Application_Model_DbTable_Artworkbid();
					$artworkuploadmaxbid_model_result = $artworkuploadmaxbid_model->getBidMaxlist($artwork_id);	 
					if($artworkuploadmaxbid_model_result[0]['bidder_id']==$namespace->userid)
					{
					//$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('id'=>$artwork_id)) ;
					}
				}
				}
				else
				{ 
				$this->_helper->redirector->gotosimple('index','index',true) ;
				}
			}
			foreach($artworkuploadlist_model_result as $event){
				$cat_id= $event['category'];
				$owner_id= $event['owner_id'];
				$artwork_id=$event['id'];
			}
			///////////////////Find category name///////////////////////////////////////
			$Categorylist_model   = new Application_Model_DbTable_ArtCategory();
			$Categorylist_model_result = $Categorylist_model->getCategorylistName($cat_id,'');	
			$catarr=$this->view->Categorylist_model_result = $Categorylist_model_result;
			///////////////////////////Listing Images////////////////////////////////////
			$artworkuploadimageslist_model   = new Application_Model_DbTable_Artworkuploadimages();
			$artworkuploadimageslist_model_result = $artworkuploadimageslist_model->fetchAll($artworkuploadimageslist_model->select()->where('artwork_id = ?', $id));	
			$this->view->artworkuploadimageslist_model_result = $artworkuploadimageslist_model_result;
			////////////////////////////////////////////////////////////////////////////////////////
			///////////////////////////User's Listing Other Images////////////////////////////////////
			$artworkuploadimageslist_model_all   = new Application_Model_DbTable_Artworkupload();
			$artworkuploadimageslist_model_result_all = $artworkuploadimageslist_model_all->getImagesAOwner($owner_id,$artwork_id);	

			$this->view->artworkuploadimageslist_model_result_all = $artworkuploadimageslist_model_result_all;
			////////////////////////////////////////////////////////////////////////////////////////
			///////////////////////////Artwork's rate////////////////////////////////////
			if($namespace->email)
			{
			$artworkuploadrate_model   = new Application_Model_DbTable_Artworkrating();
			$artworkuploadrate_model_result = $artworkuploadrate_model->getArtworkrate_user($artwork_id,$user_id);	 
			$this->view->artworkuploadrate_model_result = $artworkuploadrate_model_result;
			}
			///////////////////////////Artwork's feedback////////////////////////////////////
			
			$artworkuploadfeedbacks_model   = new Application_Model_DbTable_Artworkfeedbacks();
			$limit=0;
			$artworkuploadfeedbacks_model1 = $artworkuploadfeedbacks_model->fetchAll($artworkuploadfeedbacks_model->select()->from(array('art_artworks_feedbacks' => 'art_artworks_feedbacks'),array('user_id'))->where('artwork_id = ?', $artwork_id));
			$mpage= count($artworkuploadfeedbacks_model1)/4;
			$this->view->mpage = $mpage;
			$artworkuploadfeedbacks_model_result = $artworkuploadfeedbacks_model->getArtworkfeedbacks_user($artwork_id,$limit);	
			$this->view->artworkuploadfeedbacks_model_result = $artworkuploadfeedbacks_model_result;
			///////////////////////////Artwork's feedback comments////////////////////////////////////
				
			$artworkuploadfeedbacks_comments_model   = new Application_Model_DbTable_Artworkfeedbacks();
			$artworkuploadfeedbacks_comments_model_result = $artworkuploadfeedbacks_comments_model->getArtworkfeedbacks($artwork_id);	 

			$this->view->artworkuploadfeedbacks_comments_model_result = $artworkuploadfeedbacks_comments_model_result;
			
			///////////////////////////Artwork's bids count////////////////////////////////////
				
			$artworkuploadbidcount_model   = new Application_Model_DbTable_Artworkbid();
			$artworkuploadbidcount_model_result = $artworkuploadbidcount_model->getBidlist_count($artwork_id);	 
			$this->view->artworkuploadbidcount_model_result = $artworkuploadbidcount_model_result;
			///////////////////////////Artwork's next min bids////////////////////////////////////
				
			$artworkuploadminbid_model   = new Application_Model_DbTable_Artworkbid();
			$artworkuploadminbid_model_result = $artworkuploadminbid_model->getBidMinilist($artwork_id);	 
			$this->view->artworkuploadminbid_model_result = $artworkuploadminbid_model_result;
			///////////////////////////Artwork's max  bids////////////////////////////////////
				
			$artworkuploadmaxbid_model   = new Application_Model_DbTable_Artworkbid();
			$artworkuploadmaxbid_model_result = $artworkuploadmaxbid_model->getBidMaxlist($artwork_id);	 
			$this->view->artworkuploadmaxbid_model_result = $artworkuploadmaxbid_model_result; 
			///////////////////////////Artwork's  bids////////////////////////////////////
				
			$artworkuploadbid_model   = new Application_Model_DbTable_Artworkbid();
			$artworkuploadbid_model_result = $artworkuploadbid_model->getBidlist($artwork_id);	 
			$this->view->artworkuploadbid_model_result = $artworkuploadbid_model_result;
			
			
    }
/////////////////////////////loadmore////////////////////////////////////////////
public function loadmoreAction()
{
	$this->_helper->layout->disableLayout();

		$request	=	$this->getRequest()->getParams();
			 $limit=$request['lastmsg'];
			 $artwork_id=$request['artwork'];
			 $artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
			 $artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->from(array('art_artworks' => 'art_artworks'),array('owner_id'))->where('id = ?', $artwork_id));	 
			$this->view->artworkuploadlisteventowner_model_result = $artworkuploadlist_model_result;
			 
			 $nlimit=$limit+4;
			 
			 
			//echo $limit=1;
			$arraymerg=array();
			$limitar=array('limit'=>$nlimit); //print_r($limitar);
			$artworkuploadfeedbacks_model   = new Application_Model_DbTable_Artworkfeedbacks();
			
			$artworkuploadfeedbacks_model_result = $artworkuploadfeedbacks_model->getArtworkfeedbacks_user($artwork_id,$limit);	echo "<pre>";//print_r($artworkuploadfeedbacks_model_result);
			$this->view->arraymerg = $artworkuploadfeedbacks_model_result;
			$this->view->limit = $nlimit;
			//print_r($arraymerg);
	
			///////////////////////////Artwork's feedback comments////////////////////////////////////
				
			$artworkuploadfeedbacks_comments_model   = new Application_Model_DbTable_Artworkfeedbacks();
			$artworkuploadfeedbacks_comments_model_result = $artworkuploadfeedbacks_comments_model->getArtworkfeedbacks($artwork_id);	 

			$this->view->artworkuploadfeedbacks_comments_model_result = $artworkuploadfeedbacks_comments_model_result;
		

}
//////////////////////////////END/////////////////////////////////////////////////////
/////////////////////////////////////ADD RATE///////////////////////////////////////
	public function ratingAction()
	{ 
	$namespace = new Zend_Session_Namespace();
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
		$request	=	$this->getRequest()->getParams();
	/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			 $owner_id=$row[0]['id'];
			 $owner_name=$row[0]['first_name'].' '.$row[0]['last_name'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
 
 ////////////////////////////////Find user already rated or not////////////////////////////////////////////////////
 			$artworkuploadrating_model   = new Application_Model_DbTable_Artworkrating();
			$artworkuploadrating_model_result = $artworkuploadrating_model->fetchAll($artworkuploadrating_model->select()->where('artwork_id = ?', $request['id'])->where('user_id = ?', $owner_id));	
			$this->view->artworkuploadrating_model_result = $artworkuploadrating_model_result;
			foreach($artworkuploadrating_model_result as $event){
			$count=count($event); 
			}
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
$trate=$request['rate_count']+$request['rating']; 
	if($count<1)
	{
	$dbField = array('artwork_id' => $request['id'], 'user_id' => $owner_id, 'rating' =>  $request['rating'],'rate_date'=>date("Y-m-d H:i:s"));
	$rating_res   	 = new Application_Model_DbTable_Artworkrating();	
	$rec = $rating_res->addartworkrating($dbField);
	////////////////////////UPDATE RATE COUNT///////////////////////////////////////
	$id=$request['id'];
	$trate=$request['rate_count']+$request['rating']; 
	$dbField = array('rate_count'=> $trate); 
	$image = array();
	$rating_res   	 = new Application_Model_DbTable_Artworkupload();	
	$rec = $rating_res->updateartwork($id,$dbField,$image);
	//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
	$art_name=$rating_res->getArtworkupload($id);
	$base_Url  = $this->getRequest()->getbaseUrl(); 
		$msg= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$owner_id.'">'.$owner_name.'</a> rated <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/artworkdetails/index/id/'.$request['id'].'">'.$art_name['title'].'</a>'; 

	$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
	$dbField = array('user_id' => $owner_id, 'activity_msg' => $msg, 'create_date'=>date("Y-m-d H:i:s"));

	$rec_insert_activity = $insert_activity->addactivity($dbField);
	///////////////////////////////////////////////////////////////////////////////////
	echo $errmsg = "Rating successfully"; 
	}
	else
	{
	echo $errmsg = "Allready rated to this artwork";
	}
die();

	}
///////////////////////////////////////////END/////////////////////////////////////
/////////////////////////////////////ADD ratingandreview///////////////////////////////////////
	public function ratingandreviewAction()
	{ 
	$namespace = new Zend_Session_Namespace();
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
		$request	=	$this->getRequest()->getParams();
	/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			 $owner_id=$row[0]['id'];
			 $owner_name=$row[0]['first_name'].' '.$row[0]['last_name'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
 
 ////////////////////////////////Find user already rated or not////////////////////////////////////////////////////
 			$artworkuploadrating_model   = new Application_Model_DbTable_Artworkrating();
			$artworkuploadrating_model_result = $artworkuploadrating_model->fetchAll($artworkuploadrating_model->select()->where('artwork_id = ?', $request['id'])->where('user_id = ?', $owner_id));	
			$this->view->artworkuploadrating_model_result = $artworkuploadrating_model_result;
			foreach($artworkuploadrating_model_result as $event){
			$count=count($event); 
			}
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
$trate=$request['artwork_ratecount']+$request['feed_rate']; 
	if($count<1)
	{
	$dbField = array('artwork_id' => $request['id'], 'user_id' => $owner_id, 'rating' =>  $request['feed_rate'],'rate_date'=>date("Y-m-d H:i:s"));
	$rating_res   	 = new Application_Model_DbTable_Artworkrating();	
	$rec = $rating_res->addartworkrating($dbField);
	////////////////////////UPDATE RATE COUNT///////////////////////////////////////
	$id=$request['id'];
	$trate=$request['artwork_ratecount']+$request['feed_rate']; 
	$dbField = array('rate_count'=> $trate); 
	$image = array();
	$rating_res   	 = new Application_Model_DbTable_Artworkupload();	
	$rec = $rating_res->updateartwork($id,$dbField,$image);
	//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
	$art_name=$rating_res->getArtworkupload($id);
	$base_Url  = $this->getRequest()->getbaseUrl(); 
		$msg= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$owner_id.'">'.$owner_name.'</a> has rate and review on <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/artworkdetails/index/id/'.$request['id'].'">'.$art_name['title'].'</a>'; 

	$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
	$dbField = array('user_id' => $owner_id, 'activity_msg' => $msg, 'create_date'=>date("Y-m-d H:i:s"));

	$rec_insert_activity = $insert_activity->addactivity($dbField);
	///////////////////////////////add main comment//////////////////////////////////
	 $dbField = array('artwork_id' => $request['id'], 'user_id' => $owner_id,'feedback'=> $request['maincomment'],'feedback_rate'=>$request['feed_rate'],'create_date'=>date("Y-m-d H:i:s"));
	$rating_res   	 = new Application_Model_DbTable_Artworkfeedbacks();	
	$rec = $rating_res->addartworkfeedbacks($dbField);
	
	$namespace->msg	= "Rating and review successfully"; 
	$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('id'=>$request['id'])) ;
	}
	else
	{
	$namespace->msg	= "Allready rated and review to this artwork";
	$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('id'=>$request['id'])) ;
	}
die();

	}
///////////////////////////////////////////END/////////////////////////////////////
/////////////////////////////////////ADD watchlist///////////////////////////////////////
	public function watchlistAction()
	{ 
	$namespace = new Zend_Session_Namespace();
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
		$request	=	$this->getRequest()->getParams();
	/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			$owner_id=$row[0]['id'];
			$owner_name=$row[0]['first_name'].' '.$row[0]['last_name'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
 
 ////////////////////////////////Find user already watch or not////////////////////////////////////////////////////
 			$watchlist_model   = new Application_Model_DbTable_Artworkwatchlist();
			$watchlist_model_result = $watchlist_model->fetchAll($watchlist_model->select()->where('au_id = ?', $request['id'])->where('user_id = ?', $owner_id));	
			$this->view->watchlist_model_result = $watchlist_model_result;
			foreach($watchlist_model_result as $event){
			$count=count($event); 
			}
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if($count<1)
	{
	$dbField = array('au_id' => $request['id'], 'user_id' => $owner_id, 'create_date'=>date("Y-m-d H:i:s"));
	$watchlist_res   	 = new Application_Model_DbTable_Artworkwatchlist();	
	$rec = $watchlist_res->addartworkwatchlist($dbField);
	
	//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
	$artwork_res   	 = new Application_Model_DbTable_Artworkupload();
	$art_name=$artwork_res->getArtworkupload($request['id']);
$base_Url  = $this->getRequest()->getbaseUrl(); 
	$msg= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$owner_id.'">'.$owner_name.'</a> has watch on <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/artworkdetails/index/id/'.$request['id'].'">'.$art_name['title'].'</a>'; 

	$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
	$dbField = array('user_id' => $owner_id, 'activity_msg' => $msg, 'create_date'=>date("Y-m-d H:i:s"));

	$rec_insert_activity = $insert_activity->addactivity($dbField);
	///////////////////////////////////////////////////////////////////////////////////
	echo $errmsg = "Added successfully";
	}
	else
	{
	echo $errmsg = "Allready added to your watchlist";
	}
	 die();

	}
///////////////////////////////////////////END/////////////////////////////////////
/////////////////////////////////////ADD follow///////////////////////////////////////
	public function followAction()
	{ 
	$namespace = new Zend_Session_Namespace();
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
		$request	=	$this->getRequest()->getParams(); 
	/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			 $owner_id=$row[0]['id'];
			 $owner_name=$row[0]['first_name'].' '.$row[0]['last_name'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
 
 ////////////////////////////////Find user already followed or not////////////////////////////////////////////////////
 			$watchlist_model   = new Application_Model_DbTable_Artworkfollow();
			$watchlist_model_result = $watchlist_model->fetchAll($watchlist_model->select()->where('au_id = ?', $request['id'])->where('user_id = ?', $owner_id));	
			$this->view->watchlist_model_result = $watchlist_model_result;
			foreach($watchlist_model_result as $event){
				 $count=count($event); 
			}
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if($count<1)
	{
	$dbField = array('au_id' => $request['id'], 'user_id' => $owner_id, 'create_date'=>date("Y-m-d H:i:s"));
	$watchlist_res   	 = new Application_Model_DbTable_Artworkfollow();	
	$rec = $watchlist_res->addartworkfollow($dbField);
	//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
	$artwork_res   	 = new Application_Model_DbTable_Artworkupload();
	$art_name=$artwork_res->getArtworkupload($request['id']);
	$base_Url  = $this->getRequest()->getbaseUrl(); 
	$msg= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$owner_id.'">'.$owner_name.'</a> has follow on <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/artworkdetails/index/id/'.$request['id'].'">'.$art_name['title'].'</a>'; 

	$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
	$dbField = array('user_id' => $owner_id, 'activity_msg' => $msg, 'create_date'=>date("Y-m-d H:i:s"));

	$rec_insert_activity = $insert_activity->addactivity($dbField);
	///////////////////////////////////////////////////////////////////////////////////
	echo $errmsg = "Added successfully";
	}
	else
	{
	echo $errmsg = "Allready followed";
	}
	 die();

	}
///////////////////////////////////////////END/////////////////////////////////////
	public function commentAction()
	{ 
	$namespace = new Zend_Session_Namespace();
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
		$request	=	$this->getRequest()->getParams(); //print_r($request); die('g');
	/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			$owner_id=$row[0]['id'];
			$owner_name=$row[0]['first_name'].' '.$row[0]['last_name'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
  ////////////////////////////////Find user already reviewed or not////////////////////////////////////////////////////
 			$artwork_res   = new Application_Model_DbTable_Artworkfeedbacks();
			$artwork_res_result = $artwork_res->fetchAll($artwork_res->select()->where('artwork_id = ?', $request['id'])->where('user_id = ?', $owner_id));	
			$this->view->artwork_res_result = $artwork_res_result;
			foreach($artwork_res_result as $event){
			$count=count($event); 
			}
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 if($count<1)
	{
if($request['maincomment']!='')
{
	$dbField = array('artwork_id' => $request['id'], 'user_id' => $owner_id,'feedback'=> $request['maincomment'],'feedback_rate'=>$request['feed_rate'],'create_date'=>date("Y-m-d H:i:s"));
	$rating_res   	 = new Application_Model_DbTable_Artworkfeedbacks();	
	$rec = $rating_res->addartworkfeedbacks($dbField);
	//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
	$artwork_res   	 = new Application_Model_DbTable_Artworkupload();
	$art_name=$artwork_res->getArtworkupload($request['id']);
	$base_Url  = $this->getRequest()->getbaseUrl(); 
	$msg= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$owner_id.'">'.$owner_name.'</a> has comment on <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/artworkdetails/index/id/'.$request['id'].'">'.$art_name['title'].'</a>'; 

	$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
	$dbField = array('user_id' => $owner_id, 'activity_msg' => $msg, 'create_date'=>date("Y-m-d H:i:s"));

	$rec_insert_activity = $insert_activity->addactivity($dbField);
	///////////////////////////////////////////////////////////////////////////////////
	$namespace->msg	=	'Comment added successfully.';
	}
			$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('id'=>$request['id'])) ;
	}
	else
	{
	 $namespace->msg = "Allready added your review on this artwork.";
	$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('id'=>$request['id'])) ;
	}
}
public function helpreviewAction()
{
		$namespace = new Zend_Session_Namespace();
		if($namespace->email =='') 
		$this->_helper->redirector->gotosimple('index','index',true);	
		$request	=	$this->getRequest()->getParams();
		/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			$owner_id=$row[0]['id'];
			$owner_name=$row[0]['first_name'].' '.$row[0]['last_name'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
 $dbField = array('artwork_id' => $request['artwork_id'], 'user_id' => $owner_id,'feedback_id'=> $request['feedback_id'],'feedback_review'=>$request['review'],'create_date'=>date("Y-m-d H:i:s"));
	$review_res   	 = new Application_Model_DbTable_Artfeedbackreview();	
	$review = $review_res->addreview($dbField);
	echo $msg	=	'Thank you for your review.';die;
	//$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('id'=>$request['id'])) ;
}
/////////////////////////////////////////////////////////////////////////////////////
	public function feedcommentAction()
	{ 
	$namespace = new Zend_Session_Namespace();
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
		$request	=	$this->getRequest()->getParams(); //print_r($request); die('g');
	/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			$owner_id=$row[0]['id'];
			$owner_name=$row[0]['first_name'].' '.$row[0]['last_name'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
 
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if($request['subcomment']!='')
{
if($request['id']=='') $artwork_id=$request['artwork_id']; else $artwork_id=$request['id'];

	$dbField = array('artwork_id' => $artwork_id,'feedback_id' => $request['feedid'], 'user_id' => $owner_id,'comment'=> $request['subcomment'],'create_date'=>date("Y-m-d H:i:s"));
	$rating_res   	 = new Application_Model_DbTable_Artworkfeedbackscomment();	
	$rec = $rating_res->addartworksubfeedbacks($dbField);
	//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
	$artwork_res   	 = new Application_Model_DbTable_Artworkupload();
	$art_name=$artwork_res->getArtworkupload($request['id']);
	$base_Url  = $this->getRequest()->getbaseUrl(); 
	$msg= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$owner_id.'">'.$owner_name.'</a> has comment on <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/artworkdetails/index/id/'.$request['id'].'">'.$art_name['title'].'</a>'; 

	$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
	$dbField = array('user_id' => $owner_id, 'activity_msg' => $msg, 'create_date'=>date("Y-m-d H:i:s"));

	$rec_insert_activity = $insert_activity->addactivity($dbField);
	///////////////////////////////////////////////////////////////////////////////////
	$msg	=	'Comment added successfully.';
	}
			$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('msg'=>$msg,'id'=>$artwork_id)) ;
	}
///////////////////////////////////////////////////////////////////////////////////////
	public function recommendAction()
	{	$request	=	$this->getRequest()->getParams();
	$base_Url  = $this->getRequest()->getbaseUrl(); 
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$namespace 	= new Zend_Session_Namespace();
		
		$get_user	=	new Application_Model_DbTable_Registration();
 
		$getuser = $get_user->usrename($namespace->email,$namespace->passowrd);

		$id  = $request['id']; 
		$to  = $request['to']; 
		$message = $request['message'];
		$subject = $request['subject'];
		$senderName = $getuser[0]['first_name'].' '. $getuser[0]['last_name'];
 	//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
	$artwork_res   	 = new Application_Model_DbTable_Artworkupload();
	$art_name=$artwork_res->getArtworkupload($request['id']);
	$base_Url  = $this->getRequest()->getbaseUrl(); 
	$msg= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$getuser[0]['id'].'">'.$senderName.'</a> has recommeded <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/artworkdetails/index/id/'.$request['id'].'">'.$art_name['title'].'</a>'; 

	$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
	$dbField = array('user_id' => $getuser[0]['id'], 'activity_msg' => $msg, 'create_date'=>date("Y-m-d H:i:s"));

	$rec_insert_activity = $insert_activity->addactivity($dbField); 
	///////////////////////////////////////////////////////////////////////////////////
	//////////////////////Get mail content set by admin///////////////////////
	$getmailcontent	=	new Application_Model_ArtMailFormats();
	$mailcontent		=	$getmailcontent->getMailContentById(4);
	$formated_message=$mailcontent['content'];
	$formated_subject=$mailcontent['subject'];
	////////////////////////BID mail to current bidder////////////////////////////////
		 $baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$base_Url;
		
		 $message = sprintf($formated_message,$senderName,$login_user,$baseUrl,$id); 
		$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
		Zend_Mail::setDefaultTransport($tr);
		
		$mail = new Zend_Mail();
	 	$mail->setBodyHtml($message);
    	$mail->setFrom('admin@artex.com', 'www.artex.com');
    	$mail->addTo($to, $senderName);
    	$mail->setSubject($subject);
    	$mail->send($tr);
		$namespace->msg	=	'Your recommendation has been sent successfully.';

	$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('id'=>$request['id'])) ;
	}
///////////////////////////////////////////END/////////////////////////////////////
	public function viewscountAction()
	{
	$namespace = new Zend_Session_Namespace();
		
		$request	=	$this->getRequest()->getParams(); //print_r($request); die('g');
 
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	$id=$request['id'];
	$dbField = array('user_views'=> $request['user_views']+1); 
	$image = array();
	$rating_res   	 = new Application_Model_DbTable_Artworkupload();	
	$rec = $rating_res->updateartwork($id,$dbField,$image);
	$msg	=	'Comment added successfully.';
	$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('msg'=>$msg,'id'=>$request['id'])) ;
	}	
///////////////////////////////////////////////////////////////////////////////////////
	public function inboxAction()
	{
	$request	=	$this->getRequest()->getParams(); //print_r($request); die('g');
	$base_Url  = $this->getRequest()->getbaseUrl(); 
		/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			$owner_id=$row[0]['id'];
			$owner_name=$row[0]['first_name'].' '.$row[0]['last_name'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
 		/////////////GET email from user id ////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_Artuser();
			$rid=$request['receiver_id'];
			$user_result = $user->selestuser($rid);	 
			$this->view->user_result = $user_result;
			foreach($user_result as $event){
			$email= $event['email'];
			$reciever_name= $event['first_name'].' '.$event['last_name'];
			}
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
	/////////////////////////INSERT IN table art_inbox//////////////////////////////////////
	$dbField = array('receiver_id' => $request['receiver_id'],'sender_id' => $owner_id, 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $request['msubject'],'message'=> $request['mmessage'],'folder_id'=> '0');
	$inbox_res   	 = new Application_Model_DbTable_Artworkinbox();	
	$rec = $inbox_res->addartworkinbox($dbField);
		//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
	$artwork_res   	 = new Application_Model_DbTable_Artworkupload();
	$art_name=$artwork_res->getArtworkupload($request['id']);
	$base_Url  = $this->getRequest()->getbaseUrl(); 
	$msg= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$owner_id.'">'.$owner_name.'</a> has send message to '.$reciever_name; 
	$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
	$dbField = array('user_id' => $owner_id, 'activity_msg' => $msg, 'create_date'=>date("Y-m-d H:i:s"));

	$rec_insert_activity = $insert_activity->addactivity($dbField);
	///////////////////////////////////////////////////////////////////////////////////
	//////////////////////Get mail content set by admin///////////////////////
	$getmailcontent	=	new Application_Model_ArtMailFormats();
	$mailcontent		=	$getmailcontent->getMailContentById(3);
	$formated_message=$mailcontent['content'];
	$formated_subject=$mailcontent['subject'];
	////////////////////////BID mail to current bidder////////////////////////////////
	
	$to  = $email; 
	$subject="New message alert";
	//-------------------------Send Mail----------------------------------//
			 	 
		 $baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$base_Url;
		$message = sprintf($formated_message,ucfirst($reciever_name),ucfirst($owner_name),$baseUrl);
		
		$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com');
		Zend_Mail::setDefaultTransport($tr);
		
		$mail = new Zend_Mail();
	 	$mail->setBodyHtml($message);
    	$mail->setFrom('admin@artex.com', 'www.artex.com');
    	$mail->addCc('reeta.inoday@gmail.com', 'Aministrator');
    	$mail->addTo($to, 'Some Recipient');
    	$mail->setSubject($subject);
    	$mail->send($tr);
		$namespace->msg	=	'Message send successfully.';
	
		$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('id'=>$request['id'])) ;
	}	
/////////////////////////////Add bidding///////////////////////////////////	
	public function bidnowAction()
	{
		$request	=	$this->getRequest()->getParams();//print_r($request); die;
		$bid   			 = new Application_Model_DbTable_Artworkbid();
		$namespace 		 = new Zend_Session_Namespace();
		$user   	 	 = new Application_Model_ArtGroup();
		$artwork_res   	 = new Application_Model_DbTable_Artworkupload();
		$insert_activity = new Application_Model_DbTable_Artuseractivity();	
		$watchlist   	 = new Application_Model_DbTable_Artworkwatchlist();
		$user_details	 = new Application_Model_Artuser();
		$getmailcontent	 = new Application_Model_ArtMailFormats();
		$inbox_res   	 = new Application_Model_DbTable_Artworkinbox();

		if($request['cur_bid']!='')
		{ 
			/////////////////////OWNER INFO///////////////////////////////////
			$artworkuploadlist_model_result = $artwork_res->fetchAll($artwork_res->select()->where('id = ?', $request['id']));				
			$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
			foreach($artworkuploadlist_model_result as $event){
				$title= $event['title'];
				$owner_id= $event['owner_id'];
			}
			//////////////////////////////////////////////////////////////////////////////////
			
			$bid_result = $bid->fetchAll($bid->select()->where('au_id = ?',$request['id'])->where('bidder_id !=?',$owner_id)->group('bidder_id')); 
			$bid_row = $bid->fetchAll($bid->select()->where('au_id = ?',$request['id']))->count();// check first bid
			if($bid_row>0)
			{
				
				if($request['cur_bid']>$request['maximum_bid'])
				{
					if($request['maximum_biddder']==$bidder_id) 
					{
					 $cur_bid=$request['prev_bid'];
					 $request['maximum_bid']=$request['cur_bid'];
					} else {
					 $cur_bid=$request['maximum_bid']+$request['bid_interval'];
					 $request['maximum_bid']=$request['cur_bid'];
					}
					if($cur_bid>$request['maximum_bid'])
					 $cur_bid=$request['maximum_bid'];
					$bidder_id=$namespace->userid;
					$dbField = array('au_id' => $request['id'],'bidder_id' => $bidder_id, 'bid_date'=>date("Y-m-d H:i:s"),'current_bid'=> $cur_bid,'maximum_bid'=> $request['maximum_bid']);
					//print_r($dbField); die;
					$rec = $bid->addartworkbid($dbField);
					$max_bidder=$bidder_id;
					
					
					//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
					$row=$user->getMemberID($namespace->email); 
					$bidder_id=$row[0]['id'];
					$bidder_name=$row[0]['first_name'].' '.$row[0]['last_name'];
					$artwork_res   	 = new Application_Model_DbTable_Artworkupload();
					$art_name=$artwork_res->getArtworkupload($request['id']);
					$base_Url  = $this->getRequest()->getbaseUrl(); 
					$msg1= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$bidder_id.'">'.$bidder_name.'</a> has bid on <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/artworkdetails/index/id/'.$request['id'].'">'.$art_name['title'].'</a>'; 
					$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
					$dbField = array('user_id' => $owner_id, 'activity_msg' => $msg1, 'create_date'=>date("Y-m-d H:i:s"));
					$rec_insert_activity = $insert_activity->addactivity($dbField); 
					///////////////////////////////////////////////////////////////////////////////////
				}
				else
				{
					 $request['cur_bid']=$request['cur_bid'];
					 $request['maximum_bid']=$request['maximum_bid']; 
					$bidder_id=$namespace->userid;
					$bid_maxid = $bid->fetchAll($bid->select()->where('au_id = ?',$request['id'])->where('maximum_bid =?',$request['maximum_bid'])->order('id')->limit('1'));  
					
					$max_bidder=$bid_maxid[0]['bidder_id'];


					if($request['cur_bid']!=$request['maximum_bid']) {
					$dbField = array('au_id' => $request['id'],'bidder_id' => $bidder_id, 'bid_date'=>date("Y-m-d H:i:s"),'current_bid'=> $request['cur_bid'],'maximum_bid'=> $request['maximum_bid']);
			
					$rec = $bid->addartworkbid($dbField);
					
					//insert
					
					//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
					$row=$user->getMemberID($namespace->email);  //print_r($row);
					$bidder_id=$row[0]['id'];
					$bidder_name=$row[0]['first_name'].' '.$row[0]['last_name'];
					$artwork_res   	 = new Application_Model_DbTable_Artworkupload();
					$art_name=$artwork_res->getArtworkupload($request['id']);
					$base_Url  = $this->getRequest()->getbaseUrl(); 
					$msg1= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$bidder_id.'">'.$bidder_name.'</a> has bid on <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/artworkdetails/index/id/'.$request['id'].'">'.$art_name['title'].'</a>'; 
					$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
					$dbField = array('user_id' => $bidder_id, 'activity_msg' => $msg1, 'create_date'=>date("Y-m-d H:i:s")); //print_r($dbField);die('g');
					$rec_insert_activity = $insert_activity->addactivity($dbField); 
					///////////////////////////////////////////////////////////////////////////////////
					
					} 
					
					if($max_bidder!=$namespace->userid)
					{
					if($request['cur_bid']==$request['maximum_bid'])
					$request['cur_bid']=$request['cur_bid'];
					else
					$request['cur_bid']=$request['cur_bid']+$request['bid_interval'];
					//////////if currnt user's bid set equal to other user max bid
					if($request['cur_bid']==$request['maximum_bid'])
					{
                                        $bidder_id=$namespace->userid;
                                        $dbField = array('au_id' => $request['id'],'bidder_id' => $bidder_id, 'bid_date'=>date("Y-m-d H:i:s"),'current_bid'=> $request['cur_bid'],'maximum_bid'=> $request['maximum_bid']);

					$rec = $bid->addartworkbid($dbField);

					//insert

					//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
					$row=$user->getMemberID($namespace->email);  //print_r($row);
					$bidder_id=$row[0]['id'];
					$bidder_name=$row[0]['first_name'].' '.$row[0]['last_name'];
					$artwork_res   	 = new Application_Model_DbTable_Artworkupload();
					$art_name=$artwork_res->getArtworkupload($request['id']);
					$base_Url  = $this->getRequest()->getbaseUrl();
					$msg1= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$bidder_id.'">'.$bidder_name.'</a> has bid on <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/artworkdetails/index/id/'.$request['id'].'">'.$art_name['title'].'</a>';
					$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();
					$dbField = array('user_id' => $bidder_id, 'activity_msg' => $msg1, 'create_date'=>date("Y-m-d H:i:s")); //print_r($dbField);die('g');
					$rec_insert_activity = $insert_activity->addactivity($dbField);
					}
					///////////////////////////////////////////////////////////////////////////////////




					$request['maximum_bid']=$request['maximum_bid'];
					$dbField = array('au_id' => $request['id'],'bidder_id' => $max_bidder, 'bid_date'=>date("Y-m-d H:i:s"),'current_bid'=> $request['cur_bid'],'maximum_bid'=> $request['maximum_bid']);
					//print_r($dbField); die('g');
					$rec = $bid->addartworkbid($dbField);
					
					
					//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
					$row=$user->getMemberID($max_bidder); 
					$bidder_id=$row[0]['id'];
					$bidder_name=$row[0]['first_name'].' '.$row[0]['last_name'];
					$artwork_res   	 = new Application_Model_DbTable_Artworkupload();
					$art_name=$artwork_res->getArtworkupload($request['id']);
					$base_Url  = $this->getRequest()->getbaseUrl(); 
					$msg1= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$bidder_id.'">'.$bidder_name.'</a> has bid on <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/artworkdetails/index/id/'.$request['id'].'">'.$art_name['title'].'</a>'; 
					$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
					$dbField = array('user_id' => $owner_id, 'activity_msg' => $msg1, 'create_date'=>date("Y-m-d H:i:s"));
					$rec_insert_activity = $insert_activity->addactivity($dbField); 
					///////////////////////////////////////////////////////////////////////////////////
					
					}
					else
					{
						$max_bidder=$bidder_id;
					}
				}
				
		//////////////////////////////////////////////////////////////////////////////////////////////////
		#############################         CURRENT HIGHEST BIDDER           ##########################
		//////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////BIDDER INFO//////////////////////////////
			$user_details   	 = new Application_Model_Artuser();
			$bidder_id=$max_bidder;
			$user_result = $user_details->selestuser($max_bidder);	 
			$this->view->user_result = $user_result;
			foreach($user_result as $event){
			$bidder_name= $event['first_name'].' '.$event['last_name'];
			$email=$event['email'];
			}
			$artworkuploadimageslist_model   = new Application_Model_DbTable_Artworkuploadimages();
				$artworkuploadimageslist_model_result = $artworkuploadimageslist_model->fetchAll($artworkuploadimageslist_model->select()->where('artwork_id = ?', $request['id']));	
				
				foreach($artworkuploadimageslist_model_result as $artimage){
					$art_image=$artimage['image'];
				}
		//////////////////////////////////////////////////////////////////////////
		$mailcontent		=	$getmailcontent->getMailContentById(10);
		 $formated_message=$mailcontent['content'];
		$formated_subject=$mailcontent['subject'];
		////////////////////////BID mail to current bidder////////////////////////////////
		$base_Url  = $this->getRequest()->getbaseUrl(); 
		$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$base_Url; 
		$siteUrl='http://'.$_SERVER['SERVER_NAME'];
		 $message = sprintf($formated_message,ucwords($bidder_name),$siteUrl,$art_image,$baseUrl,$request['id'],''); 
		$subject=$formated_subject;
											$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
											Zend_Mail::setDefaultTransport($tr);
											
											$mail = new Zend_Mail();
											$mail->setBodyHtml($message);
											$mail->setFrom('admin@artex.com', 'www.artex.com');
											$mail->addTo($namespace->email, ucwords($bidder_name));
											//$mail->addCc('reeta.inoday@gmail.com', 'Aministrator');
											//$mail->addCc($art_owner_to, $art_name);//mail to artwork owner
											$mail->setSubject($subject);
											$mail->send($tr);
	////////////////////////////////////////////////////////////////////////////////
	/////////////////////////INSERT IN table art_inbox//////////////////////////////////////
					$dbField = array('receiver_id' => $max_bidder,'sender_id' => '0', 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $subject,'message'=> $message,'folder_id'=> '0'); 
					$inbox_res   	 = new Application_Model_DbTable_Artworkinbox();	
					$res = $inbox_res->addartworkinbox($dbField);
			///////////////////////////////////////////////////////////////////////////////////////////	

			////////////////////////BID mail to rest bidder////////////////////////////////
	
			$bid   = new Application_Model_DbTable_Artworkbid();
			$bid_result = $bid->fetchAll($bid->select()->where('au_id = ?',$request['id'])->where('bidder_id !=?',$max_bidder)->group('bidder_id')); 
			$bid_row = $bid->fetchAll($bid->select()->where('au_id = ?',$request['id'])->where('bidder_id !=?',$max_bidder))->count();// check first bid
	
			if($bid_row>0)
			{
				foreach($bid_result as $event){  
			
			/////////////GET email from user id ////////////////////////////////////////////////
			$user   	 = new Application_Model_Artuser();
			$rid=$event['bidder_id'];
			$user_result = $user->selestuser($rid);	 
			$this->view->user_result = $user_result;
			foreach($user_result as $event){
			$reciever_name= $event['first_name'].' '.$event['last_name'];
			 $email=$event['email'];
			}
			$artworkuploadimageslist_model   = new Application_Model_DbTable_Artworkuploadimages();
				$artworkuploadimageslist_model_result = $artworkuploadimageslist_model->fetchAll($artworkuploadimageslist_model->select()->where('artwork_id = ?', $request['id']));	
				
				foreach($artworkuploadimageslist_model_result as $artimage){
					$art_image=$artimage['image'];
				}
			//////////////////////Get mail content set by admin///////////////////////Outbid Mail
			$getmailcontent	=	new Application_Model_ArtMailFormats();
			$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$base_Url; 
			$siteUrl='http://'.$_SERVER['SERVER_NAME'];
			$mailcontent		=	$getmailcontent->getMailContentById(11);
			 $formated_message=$mailcontent['content'];
			$formated_subject=$mailcontent['subject'];
			///////////////////////////////////////////////////////////////////////////////////
			$base_Url  = $this->getRequest()->getbaseUrl(); 
			$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$base_Url;
			 $message = sprintf($formated_message,ucwords($reciever_name),$siteUrl,$art_image,$baseUrl,$request['id'],$baseUrl,$request['id']); 
			$subject=$formated_subject;								
										$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
											Zend_Mail::setDefaultTransport($tr);
											
											$mail = new Zend_Mail();
											$mail->setBodyHtml($message);
											$mail->setFrom('admin@artex.com', 'www.artex.com');
											$mail->addTo($email, ucwords($reciever_name));
											//$mail->addCc('reeta.inoday@gmail.com', 'Aministrator');
											//$mail->addCc($art_owner_to, $art_name);//mail to artwork owner
											$mail->setSubject($subject);
											$mail->send($tr);
				/////////////////////////INSERT IN table art_inbox//////////////////////////////////////
					$dbField = array('receiver_id' => $rid,'sender_id' => '0', 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $subject,'message'=> $message,'folder_id'=> '0');
					$inbox_res   	 = new Application_Model_DbTable_Artworkinbox();	
					$rec = $inbox_res->addartworkinbox($dbField);
					///////////////////////////////////////////////////////////////////////////////////////////											
			}
			}
			}
			else // first bid
			{
				////////////////////////////////////////////////////////////////////////////////
				$bidder_id=$namespace->userid;
				$dbField = array('au_id' => $request['id'],'bidder_id' => $bidder_id, 'bid_date'=>date("Y-m-d H:i:s"),'maximum_bid'=> $request['cur_bid'],'current_bid'=> $request['prev_bid']);
				
				$rec = $bid->addartworkbid($dbField);
				$data = array(
					'bid_start_date'		=> date("Y-m-d H:i:s")
				);
				
				$rec = $artwork_res->updateartworkpaystatus($request['id'],$data);	
				//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
					$row=$user->getMemberID($namespace->email); 
					$bidder_id=$row[0]['id'];
					$bidder_name=$row[0]['first_name'].' '.$row[0]['last_name'];
					$artwork_res   	 = new Application_Model_DbTable_Artworkupload();
					$art_name=$artwork_res->getArtworkupload($request['id']);
					$base_Url  = $this->getRequest()->getbaseUrl(); 
					$msg1= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$bidder_id.'">'.$bidder_name.'</a> has bid on <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/artworkdetails/index/id/'.$request['id'].'">'.$art_name['title'].'</a>'; 
					$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
					$dbField = array('user_id' => $bidder_id, 'activity_msg' => $msg1, 'create_date'=>date("Y-m-d H:i:s"));
					$rec_insert_activity = $insert_activity->addactivity($dbField); 
					///////////////////////////////////////////////////////////////////////////////////

				#############        MAIL TO OWNER  OPENING BID              #######################
				//////////////////////////////////////////////////////////////////////////////////
			/////////////GET email from user id ////////////////////////////////////////////////
			$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
			$recg =	$allgloble->getglobelinfoAll();
			
			$user   	 = new Application_Model_Artuser();
			$rid=$owner_id;
			$user_result = $user->selestuser($rid);	 
			$this->view->user_result = $user_result;
			foreach($user_result as $event){
			$reciever_name= $event['first_name'].' '.$event['last_name'];
			$email=$event['email'];
			}
				
				$mailcontent		=	$getmailcontent->getMailContentById(20);
				$formated_message=$mailcontent['content'];
				$formated_subject=$mailcontent['subject'];
				///////////////////////////////////////////////////////////////////////////////////
				$base_Url  = $this->getRequest()->getbaseUrl(); 
				$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$base_Url;
				$message = sprintf($formated_message,ucwords($reciever_name),$title,$baseUrl,$request['id'],$request['id_end_time']);
				$subject=$formated_subject;
				$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
				Zend_Mail::setDefaultTransport($tr);
					
					$mail = new Zend_Mail();
					$mail->setBodyHtml($message);
					$mail->setFrom('admin@artex.com', 'www.artex.com');
					$mail->addTo($email, ucwords($reciever_name));
					//$mail->addCc('reeta.inoday@gmail.com', 'Aministrator');
					//$mail->addCc($art_owner_to, $title);//mail to artwork owner
					$mail->setSubject($subject);
					$mail->send($tr);
				/////////////////////////INSERT IN table art_inbox//////////////////////////////////////
				$dbField = array('receiver_id' => $rid,'sender_id' => '0', 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $subject,'message'=> $message,'folder_id'=> '0');

				$rec = $inbox_res->addartworkinbox($dbField);
				///////////////////////////////////////////////////////////////////////////////////
				#############        END MAIL TO OWNER  OPENING BID             #######################
				//////////////////////////////////////////////////////////////////////////////////
				//FIND all watcher of this artwork
				$watchlist_result = $watchlist->fetchAll($watchlist->select()->where('au_id =?',$request['id'])->group('user_id')); 				
				foreach($watchlist_result as $event){  
			
					/////////////GET email from user id ////////////////////////////////////////////////
					
					$rid=$event['user_id'];
					$user_result = $user->selestuser($rid);	 
					$this->view->user_result = $user_result;
					foreach($user_result as $event){
					$reciever_name= $event['first_name'].' '.$event['last_name'];
					$email=$event['email'];
				}
				/////////////////Artwork detail//////////////////////////////////

				$artworkuploadlist_model_result = $artwork_res->fetchAll($artwork_res->select()->where('id = ?', $request['id']));				
				$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
				foreach($artworkuploadlist_model_result as $event){
					$title= $event['title'];
				}
				///////////////////////////Mail //////////////////////////////////////
				#######################  email to all watcher#############################
				//////////////////////Get mail content set by admin///////////////////////
				
				$mailcontent		=	$getmailcontent->getMailContentById(21);
				 $formated_message=$mailcontent['content'];
				$formated_subject=$mailcontent['subject'];
				///////////////////////////////////////////////////////////////////////////////////
				$base_Url  = $this->getRequest()->getbaseUrl(); 
				$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$base_Url;
				
				 $message = sprintf($formated_message,ucwords($reciever_name),$title,$baseUrl,$request['id'],$title,'',''); 
				$subject=$formated_subject; 
							$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
							Zend_Mail::setDefaultTransport($tr);
							
							$mail = new Zend_Mail();
							$mail->setBodyHtml($message);
							$mail->setFrom('admin@artex.com', 'www.artex.com');
							$mail->addTo($email, ucwords($reciever_name));
							$mail->addCc('reeta910@gmail.com', 'Aministrator');
							//$mail->addCc($art_owner_to, $art_name);//mail to artwork owner
							$mail->setSubject($subject);
							$mail->send($tr);
						/////////////////////////INSERT IN table art_inbox//////////////////////////////////////
					$dbField = array('receiver_id' => $rid,'sender_id' => '0', 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $subject,'message'=> $message,'folder_id'=> '0');
						
					$rec = $inbox_res->addartworkinbox($dbField);
					}
					///////////////////////////////////////////////////////////////////////////////////////////
					######################################MAIL TO BIDDER ##########################################
					/////////////////////////////////////////////////////////////////////////////////////////////////////
					/////////////GET user id from user email (store in session)////////////////////////////////////////////////
					
					$user   	 	 = new Application_Model_ArtGroup();
					$row=$user->getMemberID($namespace->email); 
					$owner_id=$row[0]['id'];
					$owner_name=$row[0]['first_name'].' '.$row[0]['last_name'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
 ////////////////artwork owner id//////////////////////////////////////////////////////

	/////////////////////////INSERT IN table art_inbox//////////////////////////////////////

	 $artworkuploadimageslist_model   = new Application_Model_DbTable_Artworkuploadimages();
				$artworkuploadimageslist_model_result = $artworkuploadimageslist_model->fetchAll($artworkuploadimageslist_model->select()->where('artwork_id = ?', $request['id']));	
				
				foreach($artworkuploadimageslist_model_result as $artimage){
					$art_image=$artimage['image'];
				}
	//////////////////////Get mail content set by admin///////////////////////
	
	$mailcontent		=	$getmailcontent->getMailContentById(10);
	 $formated_message=$mailcontent['content'];
	$formated_subject=$mailcontent['subject'];
	////////////////////////BID mail to current bidder////////////////////////////////
	$base_Url  = $this->getRequest()->getbaseUrl(); 
	$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$base_Url; 
	$siteUrl='http://'.$_SERVER['SERVER_NAME'];
	 $message = sprintf($formated_message,ucwords($bidder_name),$siteUrl,$art_image,$baseUrl,$request['id'],'');
	 
	$subject=$formated_subject;
											$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
											Zend_Mail::setDefaultTransport($tr);
											
											$mail = new Zend_Mail();
											$mail->setBodyHtml($message);
											$mail->setFrom('admin@artex.com', 'www.artex.com');
											$mail->addTo($namespace->email, ucwords($owner_name));
											//$mail->addCc($rec[0]['contact_email'], 'Aministrator');
											//$mail->addCc($art_owner_to, $art_name);//mail to artwork owner
											$mail->setSubject($subject);
											$mail->send($tr);
	////////////////////////////////////////////////////////////////////////////////
	/////////////////////////INSERT IN table art_inbox//////////////////////////////////////
					$dbField = array('receiver_id' => $owner_id,'sender_id' => '0', 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $subject,'message'=> $message,'folder_id'=> '0'); 
						
					$res = $inbox_res->addartworkinbox($dbField);
			/////////////////////////////////////////////////////////////////////////////////////////////////////
			###########################################END MAIL TO BIDDER ######################################
			/////////////////////////////////////////////////////////////////////////////////////////////////////
					
			
			//////////////////////////////////////Email sent to owner//////////////////////////////////////////////
				echo $msg	=	'Bid successfully.';
			}
			}
			else { echo $msg	=	'Try again for biddig.';
				}
		$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('msg'=>$msg,'id'=>$request['id'])) ;		
	}
	///////////////////////////////////////////////////////////////////////////
  public function checkartworkAction() {
  $request 		=	$this->getRequest()->getParams();
		$id = $request['id']; 
		 $userInfo=new Application_Model_DbTable_Artworkupload();
		 $check   = $userInfo->checkArtwork($id);
		 if($check[0]['id']!=0){
		 	$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('id'=>$check[0]['id'])) ;
		 }
		die;
		
	}
	
	public function  deletecommentAction()
	{ 
		$namespace 	= new Zend_Session_Namespace();
		$request 		=	$this->getRequest()->getParams();
		$did			=	$request['did'];
		$id			=	$request['id'];
		
		$subcommentlist			=	new Application_Model_DbTable_Artworkfeedbackscomment();
		$deletesubcommentlist		=	$subcommentlist->deletecommentlist($did);
		
		$commentlist			=	new Application_Model_DbTable_Artworkfeedbacks();
		$deletecommentlist		=	$commentlist->deletecommentlist($did);
		$this->view->deletecommentlist	=	$deletecommentlist;

		$namespace->msg	=	'Comment deleted successfully.';
		$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('msg'=>$msg,'id'=>$id)) ;

	}	
	
	public function  deletesubcommentAction()
	{ 
		$namespace 	= new Zend_Session_Namespace();
		$request 		=	$this->getRequest()->getParams();
		$did			=	$request['did'];
		$id			=	$request['id'];
		$subcommentlist			=	new Application_Model_DbTable_Artworkfeedbackscomment();
		$deletesubcommentlist		=	$subcommentlist->deletesubcommentlist($did);
		$this->view->deletesubcommentlist	=	$deletesubcommentlist;

		$namespace->msg	=	'Comment deleted successfully.';
		$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('msg'=>$msg,'id'=>$id)) ;

	}
	public function topdetailAction()
	{
	$this->_helper->layout->disableLayout();
	 $request  =	$this->getRequest()->getParams();
			$id = $this->_getParam('id', 0);	
/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			$user_id=$row[0]['id'];
			$namespace->userid= $row[0]['id'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
///////////////// INFO FROM GLOBAL TABLE////////////////////////////////
 $allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
$rec =	$allgloble->getglobelinfoAll();
$this->view->rec=$rec;
// echo'<pre>';print_r($rec);//print_r($_POST);
/////////////////////////////////////////////////////////////////////////////////////
			$today=date('Y-m-d');
			$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
			$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->from(array('art_artworks' => 'art_artworks'),array('*','count' => 'ADDDATE(bid_start_date, INTERVAL  auction_duration DAY)','ending'=>'TIMEDIFF(ADDDATE(bid_start_date, INTERVAL auction_duration DAY),NOW()-INTERVAL 0 MINUTE)'))->where('id = ?', $id));				
			$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
			foreach($artworkuploadlist_model_result as $event){
			$cat_id= $event['category'];
			$owner_id= $event['owner_id'];
			$artwork_id=$event['id'];
			}
			///////////////////Find category name///////////////////////////////////////
			$Categorylist_model   = new Application_Model_DbTable_ArtCategory();
			$Categorylist_model_result = $Categorylist_model->getCategorylistName($cat_id,$category_name);	
			$catarr=$this->view->Categorylist_model_result = $Categorylist_model_result;
			///////////////////////////Listing Images////////////////////////////////////
			$artworkuploadimageslist_model   = new Application_Model_DbTable_Artworkuploadimages();
			$artworkuploadimageslist_model_result = $artworkuploadimageslist_model->fetchAll($artworkuploadimageslist_model->select()->where('artwork_id = ?', $id));	
			$this->view->artworkuploadimageslist_model_result = $artworkuploadimageslist_model_result;
			////////////////////////////////////////////////////////////////////////////////////////
			///////////////////////////User's Listing Other Images////////////////////////////////////
			$artworkuploadimageslist_model_all   = new Application_Model_DbTable_Artworkupload();
			$artworkuploadimageslist_model_result_all = $artworkuploadimageslist_model_all->getImagesAOwner($owner_id,$artwork_id);	

			$this->view->artworkuploadimageslist_model_result_all = $artworkuploadimageslist_model_result_all;
			////////////////////////////////////////////////////////////////////////////////////////
			///////////////////////////Artwork's rate////////////////////////////////////
			
			if($namespace->email)
			{
			$artworkuploadrate_model   = new Application_Model_DbTable_Artworkrating();
			$artworkuploadrate_model_result = $artworkuploadrate_model->getArtworkrate_user($artwork_id,$user_id);	 
			$this->view->artworkuploadrate_model_result = $artworkuploadrate_model_result;
			}


			///////////////////////////Artwork's feedback////////////////////////////////////
			
			$artworkuploadfeedbacks_model   = new Application_Model_DbTable_Artworkfeedbacks();
			$limit=0;
			$artworkuploadfeedbacks_model_result = $artworkuploadfeedbacks_model->getArtworkfeedbacks_user($artwork_id,$limit);	 
			$this->view->artworkuploadfeedbacks_model_result = $artworkuploadfeedbacks_model_result;
			///////////////////////////Artwork's feedback comments////////////////////////////////////
				
			$artworkuploadfeedbacks_comments_model   = new Application_Model_DbTable_Artworkfeedbacks();
			$artworkuploadfeedbacks_comments_model_result = $artworkuploadfeedbacks_comments_model->getArtworkfeedbacks($artwork_id);	 

			$this->view->artworkuploadfeedbacks_comments_model_result = $artworkuploadfeedbacks_comments_model_result;
			
			///////////////////////////Artwork's bids count////////////////////////////////////
				
			$artworkuploadbidcount_model   = new Application_Model_DbTable_Artworkbid();
			$artworkuploadbidcount_model_result = $artworkuploadbidcount_model->getBidlist_count($artwork_id);	 
			$this->view->artworkuploadbidcount_model_result = $artworkuploadbidcount_model_result;
			///////////////////////////Artwork's next min bids////////////////////////////////////
				
			$artworkuploadminbid_model   = new Application_Model_DbTable_Artworkbid();
			$artworkuploadminbid_model_result = $artworkuploadminbid_model->getBidMinilist($artwork_id);	 
			$this->view->artworkuploadminbid_model_result = $artworkuploadminbid_model_result;
			///////////////////////////Artwork's max  bids////////////////////////////////////
				
			$artworkuploadmaxbid_model   = new Application_Model_DbTable_Artworkbid();
			$artworkuploadmaxbid_model_result = $artworkuploadmaxbid_model->getBidMaxlist($artwork_id);	 
			$this->view->artworkuploadmaxbid_model_result = $artworkuploadmaxbid_model_result;
			///////////////////////////Artwork's  bids////////////////////////////////////
				
			$artworkuploadbid_model   = new Application_Model_DbTable_Artworkbid();
			$artworkuploadbid_model_result = $artworkuploadbid_model->getBidlist($artwork_id);	 
			$this->view->artworkuploadbid_model_result = $artworkuploadbid_model_result;
	}
	//////////////////////////////////////////////////////////////////////////////////////////////
	public function bottomdetailAction()
	{
	$this->_helper->layout->disableLayout();
	 $request  =	$this->getRequest()->getParams();
			$id = $this->_getParam('id', 0);
			
			$today=date('Y-m-d');
			$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
			$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->from(array('art_artworks' => 'art_artworks'),array('user_views' => 'user_views','rate_count' => 'rate_count'))->where('id = ?', $id));	 
			$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
			
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function reviewpurchaseAction()
	{
		$request  =	$this->getRequest()->getParams();
		$id = $this->_getParam('id', 0);	
		$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
		$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->where('id = ?', $id));	
		$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
		$artworkuploadimageslist_model   = new Application_Model_DbTable_Artworkuploadimages();
		$artworkuploadimageslist_model_result = $artworkuploadimageslist_model->fetchAll($artworkuploadimageslist_model->select()->where('artwork_id = ?', $id));	
		$this->view->artworkuploadimageslist_model_result = $artworkuploadimageslist_model_result;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function checkauctionAction()
	{
		$this->_helper->layout->disableLayout();
		 $request  =	$this->getRequest()->getParams();
		 $winnerstatus_model   = new Application_Model_DbTable_Artwinnerstatus();
			//$id = $this->_getParam('id', 0);	
/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			$user_id=$row[0]['id'];
			$namespace->userid= $row[0]['id'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
///////////////// INFO FROM GLOBAL TABLE////////////////////////////////
$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
$rec =	$allgloble->getglobelinfoAll();
$this->view->rec=$rec;

//set by admin remainder day
echo "remainder_days".$remainder_days=$rec[0]['reminder'];
echo "<br>last_remainder_date=".$last_remainder_date=$rec[0]['claim'];
// echo'<pre>';print_r($rec);//print_r($_POST);
/////////////////////////////////////////////////////////////////////////////////////
			$today=date('Y-m-d');
			$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
			//$del='art_artworks.delete =0';
			$del1='art_artworks.relist_flag =0';
			$del2='art_artworks.sold_status =0';
					
			 $whe1='bid_start_date!="NULL"';
//echo $artworkuploadlist_model->select()->from(array('art_artworks' => 'art_artworks'),array('*','count' => 'ADDDATE(bid_start_date, INTERVAL  auction_duration DAY)','ending'=>'TIMEDIFF(ADDDATE(bid_start_date, INTERVAL auction_duration DAY),NOW()-INTERVAL 0 MINUTE)','maxending'=>'TIMEDIFF(ADDDATE(create_date, INTERVAL listing_duration DAY),NOW()-INTERVAL 0 MINUTE)'))->where($del)->where($del1)->where($del2)->where($whe1);
echo $artworkuploadlist_model->select()->from(array('art_artworks' => 'art_artworks'),array('*','count' => 'ADDDATE(bid_start_date, INTERVAL  auction_duration DAY)','ending'=>'TIMEDIFF(ADDDATE(bid_start_date, INTERVAL auction_duration DAY),NOW()-INTERVAL 0 MINUTE)','maxending'=>'TIMEDIFF(ADDDATE(create_date, INTERVAL listing_duration DAY),NOW()-INTERVAL 0 MINUTE)'))->where($del1)->where($del2)->where($whe1);
			$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->from(array('art_artworks' => 'art_artworks'),array('*','count' => 'ADDDATE(bid_start_date, INTERVAL  auction_duration DAY)','ending'=>'TIMEDIFF(ADDDATE(bid_start_date, INTERVAL auction_duration DAY),NOW()-INTERVAL 0 MINUTE)','maxending'=>'TIMEDIFF(ADDDATE(create_date, INTERVAL listing_duration DAY),NOW()-INTERVAL 0 MINUTE)'))->where($del1)->where($del2)->where($whe1));
					

			$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
			
			foreach($artworkuploadlist_model_result as $event){ //echo $event['bid_start_date'].'='.$event['auction_duration'].'='.$event['listing_duration']."=".$event['count'];
			$listing_duration_mail=$event['listing_duration'];
			$cat_id= $event['category'];
			$owner_id= $event['owner_id'];
			$title= $event['title'];
			$artwork_id= $event['id'];
			$count=$event['count']; 
			$location=$event['location'];
			$sold_status=$event['sold_status']; 
			$reserve_price=$event['reserve_price']; 
			$h=0;$h1=0; //echo $event['ending'];echo $event['maxending'];
			$atime=explode(':',$event['ending']);if(substr($atime[0], 0, 1)=='-') { $h=1;} else { $h=0;} 
			$maxatime=explode(':',$event['maxending']);if(substr($atime[0], 0, 1)=='-') { $h1=1;} else { $h1=0;} 
			
			//echo "<br>auction_duration=".$rec[0]['auction_duration'];
			$user_details   = new Application_Model_Artuser();
			$userdetails=$user_details->getUserdetails($owner_id); //print_r($userdetails);
			$name=$userdetails[0]['first_name'].' '.$userdetails[0]['last_name']; 
			$to=$userdetails[0]['email'];

			if($h>0)
				{ //echo $artwork_id;
					///////////////////////////Artwork's bids count////////////////////////////////////
					$artworkuploadbidcount_model   = new Application_Model_DbTable_Artworkbid();
					$artworkuploadbidcount_model_result = $artworkuploadbidcount_model->getBidlist_count($artwork_id);	 
					
					foreach($artworkuploadbidcount_model_result as $bid_event){
						
					if($bid_event==0)
						{ 
						//////////////////////Get mail content set by admin///////////////////////
						$getmailcontent		=	new Application_Model_ArtMailFormats();
						$mailcontent		=	$getmailcontent->getMailContentById(12);               
						$formated_message=$mailcontent['content'];
						$formated_subject=$mailcontent['subject']; 
						
						//////////////////////////////////////////////////////////////////////////////
							if($h1>0 && $event['delete']=='0')
								{//delete from system 
									if($event['relist_flag']=='0')
									{
									$artworkuploadlist			=	new Application_Model_DbTable_Artworkupload();
									$deleteartworkuploadlist	=	$artworkuploadlist->deleteartworkuploadtemplist($artwork_id);
									$this->view->deleteartworkuploadlist	=	$deleteartworkuploadlist;
									$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$this->baseUrl;
									$message = sprintf($formated_message,$name,$title,$baseUrl,''); 
									$subject = $formated_subject;
											$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
											Zend_Mail::setDefaultTransport($tr);
											
											$mail = new Zend_Mail();
											$mail->setBodyHtml($message);
											$mail->setFrom('admin@artex.com', 'www.artex.com');
											$mail->addTo($to, $name);
											$mail->addCc('reeta.inoday@gmail.com', 'Aministrator');
											$mail->setSubject($subject);
											$mail->send($tr);
										}
								}
								else
									{// bid status close 
											echo "<br>close bid status".$artwork_id;
											//send mail
						////////////////////////RELISTING AND REMOVE IF ANY OLD BID//////////////////
						
					$dbField 	= array('payment_status'=>'0','relist_flag' =>'1','sell_type'=>'','price'=>'','reserve_price'=>'','buy_it_now'=>'','location'=>'','service'=>'','postage_cost'=>'','dispatch_time'=>'','modify_date'=>date("Y-m-d H:i:s"));
					$image		= array();
					$artworkuploadlist = new Application_Model_DbTable_Artworkupload();
					$rec = $artworkuploadlist->updateartworkprice($artwork_id,$dbField);
					
					$artworkbid = new Application_Model_DbTable_Artworkbid();
					$delbid = $artworkbid->deleteBidOfArtwork($artwork_id);
						//////////////////////Get mail content set by admin///////////////////////
						$getmailcontent		=	new Application_Model_ArtMailFormats();
						$mailcontent		=	$getmailcontent->getMailContentById(13);
						$formated_message=$mailcontent['content'];
						$formated_subject=$mailcontent['subject'];
						//////////////////////////////////////////////////////////////////////////////
											$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$this->baseUrl;
											$message = sprintf($formated_message,$name,$title,$listing_duration_mail,$baseUrl); 
											$subject=$formated_subject;
											$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com');   
											Zend_Mail::setDefaultTransport($tr);
											
											$mail = new Zend_Mail();
											$mail->setBodyHtml($message);
											$mail->setFrom('admin@artex.com', 'www.artex.com');
											$mail->addTo($to, $name);
											$mail->addCc('reeta.inoday@gmail.com', 'Aministrator');
											$mail->setSubject($subject);
											$mail->send($tr);
									}
						}
					else if($event['delete']=='0')
						{ 
					////////////////////////CHECK RESERVE MET WITH BID AND REMOVE IF ANY OLD BID//////////////////	
					$artworkuploadmaxbid_model   = new Application_Model_DbTable_Artworkbid();
					$artworkuploadmaxbid_model_result = $artworkuploadmaxbid_model->getBidMaxlistlocation($artwork_id,$location);	 // max bidder and their country is matched with delivery country set by artwork owner
					$this->view->artworkuploadmaxbid_model_result = $artworkuploadmaxbid_model_result;
					$maxbid=$artworkuploadmaxbid_model_result[0]['maxbid'];
					if($maxbid<$reserve_price)
					{ //echo 'C'.$maxbid.'<'.$reserve_price;
						$dbField = array('payment_status'=>'0','relist_flag' =>'1','sell_type'=>'','price'=>'','reserve_price'=>'','buy_it_now'=>'','location'=>'','service'=>'','postage_cost'=>'','dispatch_time'=>'','modify_date'=>date("Y-m-d H:i:s"));
						$image=array();
						$artworkuploadlist = new Application_Model_DbTable_Artworkupload();
						$rec = $artworkuploadlist->updateartworkprice($artwork_id,$dbField);
						
						$artworkbid = new Application_Model_DbTable_Artworkbid();
						$delbid = $artworkbid->deleteBidOfArtwork($artwork_id);	
					}
					else
					{ //echo $sold_status; //die;
					if($sold_status==0)// check payment status recieved or not
						{
							$base_Url  = $this->getRequest()->getbaseUrl(); 
							$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$base_Url;	
							$artworkuploadmaxbid_model   = new Application_Model_DbTable_Artworkbid();
							$artworkuploadmaxbid_model_result = $artworkuploadmaxbid_model->getBidMaxlistlocation($artwork_id,$location);	 // max bidder and their country is matched with delivery country set by artwork owner
							$this->view->artworkuploadmaxbid_model_result = $artworkuploadmaxbid_model_result;
							//print_r($artworkuploadmaxbid_model_result);
							$bidder_id=$artworkuploadmaxbid_model_result[0]['bidder_id'];
							$maxbid=$artworkuploadmaxbid_model_result[0]['maxbid'];
							$user_details   = new Application_Model_Artuser();
							$userdetails=$user_details->getUserdetails($bidder_id); //print_r($userdetails);
							$name=$userdetails[0]['first_name'].' '.$userdetails[0]['last_name']; 
			  				$to=$userdetails[0]['email'];
				 
				 //check winner status
				 
				 $winnerstatus_model_result = $winnerstatus_model->getWinnerRemainderStatus($bidder_id,$artwork_id);	
				 if(count($winnerstatus_model_result)>0)//remainder mail
						{
						//print_r($winnerstatus_model_result);
						 $remainder_days; echo "<br>";
						  $maildate=$winnerstatus_model_result[0]['remainder_date'];echo "<br>";
						$last_paymentdate=date('Y-m-d'); //echo date("d",strtotime($maildate))+$remainder_days."<br>";
						  $next_remainder = date('Y-m-d',mktime(0, 0, 0, date("m",strtotime($maildate)), date("d",strtotime($maildate))+$remainder_days,   date("Y",strtotime($maildate)))); echo "<br>";
						 $last_remainder = date('Y-m-d',mktime(0, 0, 0, date("m",strtotime($maildate)), date("d",strtotime($maildate))+$last_remainder_date,   date("Y",strtotime($maildate)))); echo "<br>";
						
						//echo "<br>".$last_paymentdate."  ".$next_remainder."  ".$last_remainder."===".$artwork_id
						;
							if($last_paymentdate<=$last_remainder)
							{ 
								if($last_paymentdate==$next_remainder)// // payment not recieved  remainder mail
								{ 
						//////////////////////Get mail content set by admin///////////////////////
						$getmailcontent	=	new Application_Model_ArtMailFormats();
						$mailcontent		=	$getmailcontent->getMailContentById(15);
						$formated_message=$mailcontent['content'];
						$formated_subject=$mailcontent['subject'];
						//////////////////////////////////////////////////////////////////////////////									
						//remainder for payment
									$base_Url  = $this->getRequest()->getbaseUrl(); 
									$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$base_Url;
									$message = sprintf($formated_message,$name,$remainder_days,$baseUrl,$artwork_id,$title);
									$subject=$formated_subject;
									$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
									Zend_Mail::setDefaultTransport($tr);
									
									$mail = new Zend_Mail();
									$mail->setBodyHtml($message);
									$mail->setFrom('admin@artex.com', 'www.artex.com');
									$mail->addTo($to, $name);
									$mail->addCc('reeta.inoday@gmail.com', 'Aministrator');
									//$mail->addCc($rec[0]['contact_email'], 'Aministrator');
									$mail->setSubject($subject);
									$mail->send($tr);
									
									//////////////////update record art_winner_remainder_status///////////////////////////	
									$ws_id=$winnerstatus_model_result[0]['id']; //die();
									$dbField=array('user_id'=>$bidder_id,'artwork_id'=>$artwork_id,'remainder_date'=>date('Y-m-d H:i:s'));
									$winnerstatus_model_result = $winnerstatus_model->updateWinnerRemainderStatus($ws_id,$dbField);	
									//////////////////update record art_winner_remainder_status///////////////////////////	
								}
							}
						}
						else // first time mail to winner
						{
					//echo 'Check who is winner. and set sold status and send mail';
						//////////////////////Get mail content set by admin///////////////////////
						$getmailcontent	=	new Application_Model_ArtMailFormats();
						$mailcontent		=	$getmailcontent->getMailContentById(14);
						$formated_message=$mailcontent['content'];
						$formated_subject=$mailcontent['subject'];
						//////////////////////////////////////////////////////////////////////////////									
						$artworkuploadimageslist_model   = new Application_Model_DbTable_Artworkuploadimages();
				$artworkuploadimageslist_model_result = $artworkuploadimageslist_model->fetchAll($artworkuploadimageslist_model->select()->where('artwork_id = ?', $artwork_id));	
				
				foreach($artworkuploadimageslist_model_result as $artimage){
					$art_image=$artimage['image'];
				}
					///////////////////////////Artwork's max  bids////////////////////////////////////
						$base_Url  = $this->getRequest()->getbaseUrl(); 
						$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$base_Url;
						$siteUrl='http://'.$_SERVER['SERVER_NAME'];
						 $message = sprintf($formated_message,$name,$siteUrl,$art_image,$baseUrl,$artwork_id,$baseUrl,$artwork_id,$title,''); 
						$subject=$formated_subject;
											$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
											Zend_Mail::setDefaultTransport($tr);
											
											$mail = new Zend_Mail();
											$mail->setBodyHtml($message);
											$mail->setFrom('admin@artex.com', 'www.artex.com');
											$mail->addTo($to, $name);
											$mail->addCc('reeta.inoday@gmail.com', 'Aministrator');
											//$mail->addCc($rec[0]['contact_email'], 'Aministrator');
											$mail->setSubject($subject);
											$mail->send($tr);
											//////////////////add record art_winner_remainder_status///////////////////////////	
							$dbField=array('user_id'=>$bidder_id,'artwork_id'=>$artwork_id,'remainder_date'=>date('Y-m-d H:i:s'));
							$winnerstatus_model_result = $winnerstatus_model->addWinnerRemainderStatus($dbField);	
							//////////////////add record art_winner_remainder_status///////////////////////////		
											}
							}//sold status
					}// reserve met
						}//if
					}
					}
			}
	}
	
	/////////////////////////WINNER payment by directpay paypal///////////////////////////////////////////////////
	public function directpayAction()
	{
		$namespace = new Zend_Session_Namespace();
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		}
		$this->_helper->layout->disableLayout();
		 $this->_helper->layout->setLayout('layout');
	}
	/////////////////////////WINNER payment by credit card///////////////////////////////////////////////////
	public function creditpayAction()
	{
		$namespace = new Zend_Session_Namespace();
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		}
		$this->_helper->layout->disableLayout();
		 $this->_helper->layout->setLayout('layout');	
		 $form = new Application_Form_Artworkcreditpay();
         $this->view->form = $form;
		$request  =	$this->getRequest()->getParams();
		$id = $this->_getParam('id', 0);
		$this->view->id=$id;
		/////////////////Artwork detail///////////////////////////////////////////////////////////////////
		$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
			$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->where('id = ?', $id));				
				$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
					foreach($artworkuploadlist_model_result as $event){
						$cat_id= $event['category'];
						$owner_id= $event['owner_id'];
						$frame= $event['frame'];
						$medium= $event['medium'];
						if($frame==0) $frame='No'; else $frame='Yes'; 
						$title= $event['title'];
						$auction_sold=$event['auction_comm'];
						$scout_percent=$event['scout_percent'];
						$buyers_premium=$event['buyers_premium'];
						$postage_cost=$event['postage_cost'];
						 $artwork_id=$event['id'];
						$edition= $event['edition'];
						$size= $event['size'];
					}
		
					$this->view->title=$title;
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email); 
			$this->view->firstname	=	$row[0]['first_name'];
			$this->view->email	=	$row[0]['email'];
			$this->view->user_id	=	$row[0]['id'];
			$bidder_id				=	$row[0]['id'];
			$record['address1']	=	$row[0]['billing_address'];
			$record['city']	=	$row[0]['billing_city'];
			$record['state']	=	$row[0]['billing_state'];
			$record['zip']	=	$row[0]['billing_zip'];
			$record['country']	=	$row[0]['billing_country'];
			$record['saddress1']	=	$row[0]['shipping_address'];
			$record['scity']	=	$row[0]['shipping_city'];
			$record['sstate']	=	$row[0]['shipping_state'];
			$record['szip']	=	$row[0]['shipping_zip'];
			$record['scountry']	=	$row[0]['shipping_country'];
			$record['creditcardtype']   = $row[0]['credit_card'];
			$record['creditcardnumber']   = $row[0]['credit_card_number'];
			$record['exp_date_month']   = $row[0]['exp_month'];
			$record['exp_date_year']   = $row[0]['exp_year'];
			$record['cvv_number']   = $row[0]['cvv2'];
			
			$form->populate($record);
		///////////////////////////Artwork's max  bids///////////////////////////////////////( [maxbid] => 9099.00 [bidder_id] => 1 [current_bid] => 9099.00 ) 
		if($request['P']=='')
		{
		$artworkuploadmaxbid_model   = new Application_Model_DbTable_Artworkbid();
		$artworkuploadmaxbid_model_result = $artworkuploadmaxbid_model->getBidMaxlist($id);	 
		
		$product_price=$artworkuploadmaxbid_model_result[0]['maxbid']+$postage_cost;
		}
		else
		{ $product_price=$request['P'];}
		$this->view->product_price=$product_price;
		/////////////////PAY INFO FROM GLOBAL TABLE////////////////////////////////
			$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
			$rec1 =	$allgloble->getglobelinfoAll();
			
		/*	$merchant_id=$rec1[0]['username'];
			$password=$rec1[0]['password'];
			$signature=$rec1[0]['signature'];	*/
			
		$this->view->buyers_premium=$buyers_premium;
		
		$buyers_premium_price=number_format($product_price*($buyers_premium/100), 2, '.', '');
		$this->view->buyers_premium_price=$buyers_premium_price;
		
		
		 if ($this->getRequest()->isPost()) {
 $paymentType=urlencode('Authorization');
$creditcardtype   = $request['creditcardtype'];
$creditcardnumber   = $request['creditcardnumber'];
$exp_date_month   = $request['exp_date_month'];
$exp_date_year   = $request['exp_date_year'];
$cvv_number   = $request['cvv_number'];

$firstname   = $request['firstname'];
$lastName   = $row[0]['last_name'];
$user_id   = $request['user_id'];
$amount   = $request['amount'];
$address1 = urlencode($request['address1']);
$address2 = urlencode($request['address2']);
$city = urlencode($request['city']);
$state = urlencode($request['state']);
$zip = urlencode($request['zip']);
$countryid = urlencode($request['country']);				// US or other valid country code
$currencyID = urlencode('GBP');							// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
$country_res = new Application_Model_DbTable_ArtCountry();	
$country_ar=$country_res->getcountryCode($countryid);
$country=$country_ar['code'];
////////////////////////Artex amount////////////////////////////
		
		//$artex_amount=number_format($amount*($auction_sold/100), 2, '.', '');
		////////////////////////Scout amount////////////////////////////
		//$scout_amount=number_format($amount*($scout_percent/100), 2, '.', '');
		// die();
		/////////////////////////////////Owner paypal details//////////////////////////////////
		$user_rec = new Application_Model_Artuser();
		$user_res=$user_rec->getUserdetails($owner_id);//print_r($user_res);
		 $paypal_username=  $user_res[0]['paypal_username'];
		 $paypal_password=  $user_res[0]['paypal_password'];
		 $paypal_signature= $user_res[0]['paypal_signature'];
		
		/////////////////////////////////////////////////////////////////////////////
$nvpStr_ =	"&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditcardtype&ACCT=$creditcardnumber".
					"&EXPDATE=$exp_date_month$exp_date_year&CVV2=$cvv_number&FIRSTNAME=$firstname&LASTNAME=$lastName".
					"&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$countryid&CURRENCYCODE=$currencyID";
 ////////////////////////////////////////////////////////////////////////////
 global $environment;
 $environment = 'sandbox';	// or 'beta-sandbox' or 'live'
		// Set up your API credentials, PayPal end point, and API version.
		 $API_UserName = urlencode($paypal_username);
		 $API_Password = urlencode($paypal_password);
		 $API_Signature = urlencode($paypal_signature);
		//$API_UserName = urlencode('deepak_1324646632_biz_api1.inoday.com');
		//$API_Password = urlencode('1324646670');
		//$API_Signature = urlencode('An5ns1Kso7MWUdW4ErQKJJJ4qi4-Az1utoWx0MLHX5Xe77eTOq6J3URQ');
		$API_Endpoint = "https://api-3t.paypal.com/nvp";
		if("sandbox" === $environment || "beta-sandbox" === $environment) {
			$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
		}
		$version = urlencode('51.0');
		// Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		// Set the API operation, version, and API signature in the request.
		 $nvpreq = "METHOD=DoDirectPayment&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
		// echo $nvpreq = "METHOD=DoDirectPayment&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
		// Set the request as a POST FIELD for curl.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
		// Get response from the server.
	 $httpResponse = curl_exec($ch);
	//echo "<pre>";print_r(curl_getinfo($ch)); die();
		if(!$httpResponse) {
			exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
		}
		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);
	
		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i => $value) {
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}
		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
			exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
		}
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
//////////////////////Get mail content set by admin///////////////////////
$getmailcontent	=	new Application_Model_ArtMailFormats();
$mailcontent		=	$getmailcontent->getMailContentById(16);
$formated_message=$mailcontent['content'];
$formated_subject=$mailcontent['subject'];
//////////////////////////////////////////////////////////////////////////////	

$name   = $firstname." ".$last_name;
$user_details   = new Application_Model_Artuser();
$userdetails=$user_details->getUserdetails($bidder_id); //print_r($userdetails);
$to=$userdetails[0]['email'];
$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$this->baseUrl;
		$message = sprintf($formated_message,$name,$baseUrl,$id,$title,$amount,'');  
											$subject=$formated_subject;
											$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
											Zend_Mail::setDefaultTransport($tr);
											
											$mail = new Zend_Mail();
											$mail->setBodyHtml($message);
											$mail->setFrom('admin@artex.com', 'www.artex.com');
											$mail->addTo($to, $name);
											$mail->addCc($rec1[0]['contact_email'], 'Aministrator');
											$mail->setSubject($subject);
											$mail->send($tr);
			
////////////////////For purchase//////////////////////////////////////////////////////////////////////////////////////////	
/////////////////////////INSERT IN table art_inbox//////////////////////////////////////
$dbField = array('receiver_id' => $bidder_id,'sender_id' => '0', 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $subject,'message'=> $message,'folder_id'=> '0');
$inbox_res   	 = new Application_Model_DbTable_Artworkinbox();	
$rec = $inbox_res->addartworkinbox($dbField);
///////////////////////////////////////////////////////////////////////////////////////////
////////////////////For sold owner account/////////////////////////////////////////////////////////////////////	
/////////////////////////INSERT IN table art_inbox//////////////////////////////////////
$formated_subject='Item sold';
$formated_message='One item sold from your Artwork and payment will transfer in your given account. 
After recieving the payment, send this artwork to buyer and set this atrwork staus as send from your sold item list.
And pay artex commsion for this artwork.';
$dbField = array('receiver_id' => $owner_id,'sender_id' => '0', 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $formated_subject,'message'=> $formated_message,'folder_id'=> '0');
$inbox_res   	 = new Application_Model_DbTable_Artworkinbox();	
$rec = $inbox_res->addartworkinbox($dbField);
///////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

//add art_artworks_listing_payments table
///////////////////////////////////////////////////////////////////////////////////
		$paymentlist_model   = new Application_Model_DbTable_Artworkpayment();
		$paymentlist_model_result = $paymentlist_model->getPaymentlist($id);
		$res=$paymentlist_model_result[0]['id'];
		
		if($res=='')
		{
		$dbFieldpay = array('artwork_id' => $id,'bidder_id' => $bidder_id,'amount' => $amount );
	
			$payment   = new Application_Model_DbTable_Artworkbidpayment();
			$res=$payment->addartworkbidpayment($dbFieldpay);
		}
//////////////////////////////////////////////////////////////////////////////////////////////	

//add art_artworks_listing_transaction table
///////////////////////////////////////////////////////////////////////////////////
			 $status=1;
			
			
		$dbFieldpay = array('artwork_id' => $id,'listing_pay_id' => $res,'paymentby_id' => $bidder_id,'type'=>'seller', 'transaction_id' => $httpParsedResponseAr["TRANSACTIONID"], 'transaction_date' =>   date("Y-m-d H:i:s"), 'amount' => $amount, 'status' => $status,'credit_card' => $creditcardtype, 	'credit_card_number' => $creditcardnumber, 	'exp_month' => $exp_date_month, 	'exp_year' => $exp_date_year, 	'cvv2' => $cvv_number,'payment_type'=>'2' );
	
			$transaction   = new Application_Model_DbTable_Artworkbidtransaction();
			$res_transaction=$transaction->addartworkbidtransaction($dbFieldpay);
//////////////////////////////////////////////////////////////////////////////////////////////	
//update payment status art_artworks table
//////////////////////////////////////////////////////////////////////////////////////////////
		$statusupdate   = new Application_Model_DbTable_Artworkupload();
		
		$rec = $statusupdate->updateartworksoldstatus($id,$status);
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//*************************************                             ****************************************************//

//								After successfull seller payment do buyers premium payment 								//

//*************************************                             ****************************************************//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////PAY INFO FROM GLOBAL TABLE////////////////////////////////

			$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
			$rec1 =	$allgloble->getglobelinfoAll();
			 $merchant_id=$rec1[0]['username'];
			 $password=$rec1[0]['password'];
			 $signature=$rec1[0]['signature'];	
			
$buyers_premium_amount=number_format($amount*($buyers_premium/100), 2, '.', '');
$nvpStr_ =	"&PAYMENTACTION=$paymentType&AMT=$buyers_premium_amount&CREDITCARDTYPE=$creditcardtype&ACCT=$creditcardnumber".
					"&EXPDATE=$exp_date_month$exp_date_year&CVV2=$cvv_number&FIRSTNAME=$firstname&LASTNAME=$lastName".
					"&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$countryid&CURRENCYCODE=$currencyID";
 ////////////////////////////////////////////////////////////////////////////
 global $environment;
 $environment = 'sandbox';	// or 'beta-sandbox' or 'live'
		// Set up your API credentials, PayPal end point, and API version.
		 $API_UserName = urlencode($merchant_id);
		 $API_Password = urlencode($password);
		 $API_Signature = urlencode($signature);
		//$API_UserName = urlencode('deepak_1324646632_biz_api1.inoday.com');
		//$API_Password = urlencode('1324646670');
		//$API_Signature = urlencode('An5ns1Kso7MWUdW4ErQKJJJ4qi4-Az1utoWx0MLHX5Xe77eTOq6J3URQ');
		$API_Endpoint = "https://api-3t.paypal.com/nvp";
		if("sandbox" === $environment || "beta-sandbox" === $environment) {
			$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
		}
		$version = urlencode('51.0');
		// Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		// Set the API operation, version, and API signature in the request.
		$nvpreq = "METHOD=DoDirectPayment&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
		// Set the request as a POST FIELD for curl.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
		// Get response from the server.
	 $httpResponse = curl_exec($ch);
	//echo "<pre>";print_r(curl_getinfo($ch)); die();
		if(!$httpResponse) {
			exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
		}
		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);
	
		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i => $value) {
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}
		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
			exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
		}
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
//////////////////////Get mail content set by admin///////////////////////
$getmailcontent	=	new Application_Model_ArtMailFormats();
$mailcontent		=	$getmailcontent->getMailContentById(23);
$formated_message=$mailcontent['content'];
$formated_subject=$mailcontent['subject'];
//////////////////////////////////////////////////////////////////////////////	

$name   = $firstname." ".$last_name;
$user_details   = new Application_Model_Artuser();
$userdetails=$user_details->getUserdetails($bidder_id); //print_r($userdetails);
$to=$userdetails[0]['email'];
		$message = sprintf($formated_message,$name,$title,$frame,$medium,$edition,$size);  
											$subject=$formated_subject;
											$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
											Zend_Mail::setDefaultTransport($tr);
											
											$mail = new Zend_Mail();
											$mail->setBodyHtml($message);
											$mail->setFrom('admin@artex.com', 'www.artex.com');
											$mail->addTo($to, $name);
											$mail->addCc($rec1[0]['contact_email'], 'Aministrator');
											$mail->setSubject($subject);
											$mail->send($tr);
			
////////////////////For purchase//////////////////////////////////////////////////////////////////////////////////////////	
/////////////////////////INSERT IN table art_inbox//////////////////////////////////////


$dbField = array('receiver_id' => $bidder_id,'sender_id' => '0', 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $subject,'message'=> $message,'folder_id'=> '0');
$inbox_res   	 = new Application_Model_DbTable_Artworkinbox();	
$rec = $inbox_res->addartworkinbox($dbField);
///////////////////////////////////////////////////////////////////////////////////////////
//add art_artworks_listing_transaction table
///////////////////////////////////////////////////////////////////////////////////
			 $status=1;
			
		$dbFieldpay = array('artwork_id' => $id,'listing_pay_id' => $res,'paymentby_id' => $bidder_id,'type'=>'buyer_premium', 'transaction_id' => $httpParsedResponseAr["TRANSACTIONID"], 'transaction_date' =>   date("Y-m-d H:i:s"), 'amount' => $buyers_premium_amount, 'status' => $status,'credit_card' => $creditcardtype, 	'credit_card_number' => $creditcardnumber, 	'exp_month' => $exp_date_month, 	'exp_year' => $exp_date_year, 	'cvv2' => $cvv_number,'payment_type'=>'2' );
	
			$transaction   = new Application_Model_DbTable_Artworkbidtransaction();
			$res_transaction=$transaction->addartworkbidtransaction($dbFieldpay);
//////////////////////////////////////////////////////////////////////////////////////////////	
//update payment status art_artworks table
//////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////MAIL for successful payment//////////////////////////////////////////////////////////
		$this->_helper->redirector->gotosimple('welcome','Profile',true,array('msg'=>'Thank you for your credit card payment')) ;

			}
		else
		{
			//////////////////////Get mail content set by admin///////////////////////
			$getmailcontent	=	new Application_Model_ArtMailFormats();
			$mailcontent		=	$getmailcontent->getMailContentById(24);
			$formated_message=$mailcontent['content'];
			$formated_subject=$mailcontent['subject'];
			//////////////////////////////////////////////////////////////////////////////	
////////////////////////////////////////////////////////////////////// 159627
$name   = $firstname." ".$last_name;
$user_details   = new Application_Model_Artuser();
$userdetails=$user_details->getUserdetails($bidder_id); //print_r($userdetails);
$to=$userdetails[0]['email'];
$message = sprintf($formated_message,$name,$title,$frame,$medium,$edition,$size);  
											$subject=$formated_subject;
											$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
											Zend_Mail::setDefaultTransport($tr);
											
											$mail = new Zend_Mail();
											$mail->setBodyHtml($message);
											$mail->setFrom('admin@artex.com', 'www.artex.com');
											$mail->addTo($to, $name);
											$mail->addCc($rec1[0]['contact_email'], 'Aministrator');
											$mail->setSubject($subject);
											$mail->send($tr);
			
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//add art_artworks_listing_transaction table
///////////////////////////////////////////////////////////////////////////////////
			 $status=0;
			
		$dbFieldpay = array('artwork_id' => $id,'listing_pay_id' => $res,'paymentby_id' => $bidder_id,'type'=>'buyer_premium', 'transaction_id' => $httpParsedResponseAr["TRANSACTIONID"], 'transaction_date' =>   date("Y-m-d H:i:s"), 'amount' => $buyers_premium_amount, 'status' => $status,'credit_card' => $creditcardtype, 	'credit_card_number' => $creditcardnumber, 	'exp_month' => $exp_date_month, 	'exp_year' => $exp_date_year, 	'cvv2' => $cvv_number,'payment_type'=>'2' );
		
			$transaction   = new Application_Model_DbTable_Artworkbidtransaction();
			$res_transaction=$transaction->addartworkbidtransaction($dbFieldpay);
//////////////////////////////////////////////////////////////////////////////////////////////		

	 		$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('id'=>$id,'msg'=>'For Buyers Premium credit card payment failed.Please try again')) ;
		}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//*************************************                             ****************************************************//

//										END OF  buyers premium payment  												//

//*************************************                             ****************************************************//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////MAIL for successfull payment//////////////////////////////////////////////////////////
	$this->_helper->redirector->gotosimple('welcome','Profile',true,array('msg'=>'Thank you for your credit card payment')) ;

			}
		else
		{ 
			//////////////////////Get mail content set by admin///////////////////////
			$getmailcontent	=	new Application_Model_ArtMailFormats();
			$mailcontent		=	$getmailcontent->getMailContentById(17);
			$formated_message=$mailcontent['content'];
			$formated_subject=$mailcontent['subject'];
			//////////////////////////////////////////////////////////////////////////////	
//////////////////////////////////////////////////////////////////////
$name   = $firstname." ".$last_name;
$user_details   = new Application_Model_Artuser();
$userdetails=$user_details->getUserdetails($bidder_id); //print_r($userdetails);
$to=$userdetails[0]['email'];
$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$this->baseUrl;
		$message = sprintf($formated_message,$name,$baseUrl,$id,$title,$amount,$baseUrl,'');  

					$subject=$formated_subject;
					$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
					Zend_Mail::setDefaultTransport($tr);
					
					$mail = new Zend_Mail();
					$mail->setBodyHtml($message);
					$mail->setFrom('admin@artex.com', 'www.artex.com');
					$mail->addTo($to, $name);
					$mail->addCc($rec1[0]['contact_email'], 'Aministrator');
					$mail->setSubject($subject);
					$mail->send($tr);
			
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//////////////////////////////////////////////////////////////////////////////////////////////

//add art_artworks_listing_payments table
///////////////////////////////////////////////////////////////////////////////////
			 $status=0;
			
		
			
		$paymentlist_model   = new Application_Model_DbTable_Artworkpayment();
		$paymentlist_model_result = $paymentlist_model->getPaymentlist($id);
		$res=$paymentlist_model_result[0]['id'];
		
		if($res=='')
		{
		$dbFieldpay = array('artwork_id' => $id,'bidder_id' => $bidder_id,'amount' => $amount );
	
			$payment   = new Application_Model_DbTable_Artworkbidpayment();
			$res=$payment->addartworkbidpayment($dbFieldpay);
		}
//////////////////////////////////////////////////////////////////////////////////////////////	

//add art_artworks_listing_transaction table
///////////////////////////////////////////////////////////////////////////////////
			 $status=0;
			
		
		$dbFieldpay = array('artwork_id' => $id,'listing_pay_id' => $res,'paymentby_id' => $bidder_id,'type'=>'seller', 'transaction_id' => $httpParsedResponseAr["TRANSACTIONID"], 'transaction_date' =>   date("Y-m-d H:i:s"), 'amount' => $amount, 'status' => $status,'credit_card' => $creditcardtype, 	'credit_card_number' => $creditcardnumber, 	'exp_month' => $exp_date_month, 	'exp_year' => $exp_date_year, 	'cvv2' => $cvv_number,'payment_type'=>'2' );

			$transaction   = new Application_Model_DbTable_Artworkbidtransaction();
			$res_transaction=$transaction->addartworkbidtransaction($dbFieldpay);
//////////////////////////////////////////////////////////////////////////////////////////////	
						//update payment status art_artworks table
//////////////////////////////////////////////////////////////////////////////////////////////
		$statusupdate   = new Application_Model_DbTable_Artworkupload();
		
		$rec = $statusupdate->updateartworksoldstatus($id,$status);
	 		$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('id'=>$id,'msg'=>'Credit card payment failed.Please try again')) ;
		}
} 
}
///////////////////////////////REPORT ABUSE/////////////////////////////////////
	public function reportabuseAction()
	{
	$request	=	$this->getRequest()->getParams(); 
	$abuse   	 = new Application_Model_DbTable_Artreportabuse();	
	$base_Url  = $this->getRequest()->getbaseUrl(); 
		/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			$owner_id=$row[0]['id'];
			$owner_name=$row[0]['first_name'].' '.$row[0]['last_name'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
 ////////////////////////////////Find user already abuse or not////////////////////////////////////////////////////

			$abuse_result = $abuse->fetchAll($abuse->select()->where('feedback_id = ?', $request['report_comment_id'])->where('user_id = ?', $owner_id));	

			foreach($abuse_result as $event){
			$count=count($event); 
			}
			
			$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
			$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->where('id = ?', $request['id']));				
			$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
			foreach($artworkuploadlist_model_result as $event){
				$title= $event['title'];
				$art_ownerid= $event['owner_id'];
			}
			//art owner details
			$user   	 = new Application_Model_Artuser();
			$user_result = $user->selestuser($art_ownerid);	 
			$this->view->user_result = $user_result;
			foreach($user_result as $art_owner){
			$art_owner_name= $art_owner['first_name'].' '.$art_owner['last_name'];
			$art_owneremail=$art_owner['email'];
			}
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////'artwork_id', 


	if($count<1)
	{
		$ip = $this->getRequest()->getServer('REMOTE_ADDR');
/////////////////////////INSERT IN table art_feedbacks_reportabuse//////////////////////////////////////
	$dbField = array('artwork_id'=>$request['id'],'feedback_id'=> $request['report_comment_id'],'user_id' => $owner_id,'ip'=> $ip,'message' => $request['mmessage1'], 'category'=> 'artwork','create_date'=> date("Y-m-d H:i:s"));
	
	$insertabuse = $abuse->addreprotabuse($dbField);		
	

	///////////////// INFO FROM GLOBAL TABLE////////////////////////////////
 		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
		$rec =	$allgloble->getglobelinfoAll(); 
	//////////////////////Get mail content set by admin///////////////////////
	$getmailcontent	=	new Application_Model_ArtMailFormats();
	$mailcontent		=	$getmailcontent->getMailContentById(22);
	 $formated_message=$mailcontent['content'];
	$formated_subject=$mailcontent['subject'];
	////////////////////////BID mail to current bidder////////////////////////////////
	

//-------------------------Send Mail----------------------------------//
			 	 
		$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$base_Url;
		 $message = sprintf($formated_message,$art_owner_name,$title,$baseUrl,$baseUrl);
		
		$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com');
		Zend_Mail::setDefaultTransport($tr);
		
		$mail = new Zend_Mail();
	 	$mail->setBodyHtml($message);
    	$mail->setFrom('admin@artex.com', 'www.artex.com');
    	$mail->addTo($art_owneremail, $art_owner_name);
    	$mail->addCc($rec[0]['contact_email'], 'Aministrator');
    	$mail->addCc('reeta.inoday@gmail.com', 'Aministrator');
    	$mail->setSubject($formated_subject);
    	$mail->send($tr);

		
	$namespace->msg = "Reported successfully";
	$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('msg'=>$msg,'id'=>$request['id'])) ;
	}
	else
	{
	$namespace->msg = "Allready reported to this comment";
	$this->_helper->redirector->gotosimple('index','artworkdetails',true,array('msg'=>$msg,'id'=>$request['id'])) ;
	}
	}
	
	
}



?>

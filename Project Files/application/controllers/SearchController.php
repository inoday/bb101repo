<?php
class SearchController extends Zend_Controller_Action
{
 	public function init(){
    	$namespace = new Zend_Session_Namespace();
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$this->_helper->ajaxContext->addActionContext('search', 'html')->initContext();
		$this->request  = $this->getRequest()->getParams();
		$this->view->ind=$this->request['ind'];
 	}
    
    public function searchAction() {
		$namespace = new Zend_Session_Namespace();
		
    	///////////////// INFO FROM GLOBAL TABLE ///////////////////////
		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
		$rec =	$allgloble->getglobelinfoAll();
		$this->view->rec=$rec;
    	$request = $this->getRequest()->getParams(); 
    	if($namespace->perpage!='')
		$request['per_page']=$namespace->perpage;

		
		$searchStr = isset($request['searchbox']) ? $request['searchbox'] : '';
		$sortBy = isset($request['sortBy']) ? $request['sortBy'] : 'Ending soonest';
		$per_page = isset($request['per_page']) ? $request['per_page'] : '10'; 
		$heading = isset($request['heading']) ? $request['heading'] : 'Categories';
		$filter_heading = isset($request['heading']) ? $request['heading'] : 'Art Network';
		$filter	= new Application_Model_Searchlist();
		$subcatID = isset($request['subcat']) ? $request['subcat'] : '';
		$catIds = array();
		$catId	= isset($request['cat_id']) ? $request['cat_id'] : 0;
		
		if($searchStr !='' )
		{
			$catIds = $filter->getTopsearch($searchStr);
		}
		elseif( $catId > 0 ){
		
			$catIds[] = $catId;
		}
		
	 	$this->view->catidshow = $catIds;
		$this->view->subcatID=$subcatID;
	 	$this->view->heading = $heading;
		$artworks = $filter->getProduct($catIds,$subcatID, $sortBy,$searchStr);
		
	 	$this->view->sortBy = $sortBy;
		$this->view->perpage = $per_page;
		$this->view->artworks = $artworks;
		
		$this->view->catPreview = $searchStr;
		
		$page = $this->_getParam('page',1);
		//send the page no.
		$this->view->page = $page;			
		$namespace->perpage= $per_page;
		$this->view->current_view = $namespace->current_view;
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($artworks);
		$paginator->setItemCountPerPage($per_page);
		$paginator->setCurrentPageNumber($page);
		
		$this->view->list = 'none';
		$this->view->paginator = $paginator;
    }

	public function ajaxsearchAction(){
		$namespace = new Zend_Session_Namespace();

	///////////////// INFO FROM GLOBAL TABLE/////////////////////
		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
		$rec =	$allgloble->getglobelinfoAll();
		$this->view->rec=$rec;
			//print_r($this->getRequest()->getParams());
		if ($this->getRequest()->isXmlHttpRequest()) 
		{   
			$request = $this->getRequest()->getParams();
			
			$searchStr = isset($request['cat_ID']) ? $request['cat_ID'] : ''; 
			$keyword = isset($request['keyword']) ? $request['keyword'] : ''; 
			
			$sortBy = isset($request['sortBy']) ? $request['sortBy'] : ''; 
			$per_page = isset($request['per_page']) ? $request['per_page'] : ''; 
			$viewtype = isset($request['view']) ? $request['view'] : ''; 
			$subcatID = isset($request['subcat']) ? $request['subcat'] : '';
			
			$med_ID = isset($request['med_ID']) ? $request['med_ID'] : ''; 
			$sty_ID = isset($request['sty_ID']) ? $request['sty_ID'] : ''; 
			/////////////////////////////////////////////////////////////////////////////
			$Location = isset($request['location']) ? $request['location'] : '';
			$Heightfrom = isset($request['hfrom']) ? $request['hfrom'] : '';
			$Heightto = isset($request['hto']) ? $request['hto'] : '';
			$Widthfrom = isset($request['wfrom']) ? $request['wfrom'] : '';
			$Widthto = isset($request['wto']) ? $request['wto'] : '';
			$Depthfrom = isset($request['dfrom']) ? $request['dfrom'] : '';
			$Depthto = isset($request['dto']) ? $request['dto'] : '';
			
			/////////////////////////////////////////////////////////////////////////////
			if($viewtype == 'grid'){
				$grid = 'block';
				$list = 'none';
			}else{
				$grid = 'none';
				$list = 'block';
			}
			
			$this->view->list = $list;
			$this->view->grid = $grid;
			
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			 
			$filter	= new Application_Model_Searchlist();
			
			$artworks = $filter->getAjaxProduct($searchStr,$subcatID,$keyword,$sortBy,$Heightfrom,$Heightto,$Widthfrom,$Widthto,$Depthfrom,$Depthto,$Location,$med_ID,$sty_ID);
			$this->view->artworks = $artworks;
			$this->view->perpage = $per_page;
			$this->view->sortBy = $sortBy;
			$this->view->hfrom = $Location;
			$this->view->hfrom = $Heightfrom;
			$this->view->hto = $Heightto;
			$this->view->wfrom = $Widthfrom;
			$this->view->wto = $Widthto;
			$this->view->dfrom = $Depthfrom;
			$this->view->dto = $Depthto;
			$page = $this->_getParam('page',1);
			//send the page no.
			$this->view->page = $page;			
			$namespace->perpage= $per_page;
			$namespace->current_view= $request['view'];
			$this->view->current_view = $request['view'];
			$this->view->cat = 'abcdfegh';		

			$page = $this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($artworks);
			$paginator->setItemCountPerPage($per_page);
			$paginator->setCurrentPageNumber($page);
			$this->view->paginator = $paginator;
			return $this->render('ajaxsearch'); 	 
		}
    }
	
	public function sizesearchAction() {
    	///////////////// INFO FROM GLOBAL TABLE ///////////////////////
		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
		$rec =	$allgloble->getglobelinfoAll();
		$this->view->rec=$rec;
    	$request = $this->getRequest()->getParams();
    	$namespace = new Zend_Session_Namespace();
		if($request['per_page']=='')
		$request['per_page']=$namespace->perpage;
		$searchStr = isset($request['searchbox']) ? $request['searchbox'] : '';
		$sortBy = isset($request['sortBy']) ? $request['sortBy'] : 'Ending soonest';
		$per_page = isset($request['per_page']) ? $request['per_page'] : '10'; 
		$heading = isset($request['heading']) ? $request['heading'] : 'Categories';
		$filter	= new Application_Model_Searchlist();
		$subcatID = isset($request['subcat']) ? $request['subcat'] : '';
		$catIds = array();
		$catId	= isset($request['cat_id']) ? $request['cat_id'] : 0;
		
		if($searchStr !='' )
		{
			$catIds = $filter->getTopsearch($searchStr);
		}
		elseif( $catId > 0 ){
		
			$catIds[] = $catId;
		}
			/////////////////////////////////////////////////////////////////////////////
			$Heightfrom = isset($request['hfrom']) ? $request['hfrom'] : '';
			$Heightto = isset($request['hto']) ? $request['hto'] : '';
			$Widthfrom = isset($request['wfrom']) ? $request['wfrom'] : '';
			$Widthto = isset($request['wto']) ? $request['wto'] : '';
			$Depthfrom = isset($request['dfrom']) ? $request['dfrom'] : '';
			$Depthto = isset($request['dto']) ? $request['dto'] : '';
			$Location = isset($request['location']) ? $request['location'] : '';
		
			$this->view->hfrom = $Location;
			$this->view->hfrom = $Heightfrom;
			$this->view->hto = $Heightto;
			$this->view->wfrom = $Widthfrom;
			$this->view->wto = $Widthto;
			$this->view->dfrom = $Depthfrom;
			$this->view->dto = $Depthto;
			/////////////////////////////////////////////////////////////////////////////
	 	$this->view->catidshow = $catIds;
	 	$this->view->heading = $heading;
		$artworks = $filter->getProduct($catIds,$subcatID, $sortBy,$searchStr,$Heightfrom,$Heightto,$Widthfrom,$Widthto,$Depthfrom,$Depthto,$Location);
		
	 	$this->view->sortBy = $sortBy;
		$this->view->perpage = $per_page;
		$namespace = new Zend_Session_Namespace();
		$namespace->perpage = $per_page;
		echo $this->view->current_view = $namespace->current_view;
		$this->view->artworks = $artworks;
		
		$this->view->catPreview = $searchStr;
		
		$page = $this->_getParam('page',1);
		//send the page no.
		$this->view->page = $page;			
		
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($artworks);
		$paginator->setItemCountPerPage($per_page);
		$paginator->setCurrentPageNumber($page);
		
		$this->view->list = 'none';
		$this->view->paginator = $paginator;
    }
	
	public function locationsearchAction() {
		$namespace = new Zend_Session_Namespace();
    	///////////////// INFO FROM GLOBAL TABLE ///////////////////////
		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
		$rec =	$allgloble->getglobelinfoAll();
		$this->view->rec=$rec;
    	$request = $this->getRequest()->getParams();
    	if($request['per_page']=='')
		$request['per_page']=$namespace->perpage;
		$searchStr = isset($request['searchbox']) ? $request['searchbox'] : '';
		$sortBy = isset($request['sortBy']) ? $request['sortBy'] : 'Ending soonest';
		$per_page = isset($request['per_page']) ? $request['per_page'] : '10'; 
		$heading = isset($request['heading']) ? $request['heading'] : 'Categories';
		$filter	= new Application_Model_Searchlist();
		$subcatID = isset($request['subcat']) ? $request['subcat'] : '';
		$catIds = array();
		$catId	= isset($request['cat_id']) ? $request['cat_id'] : 0;
		
		
		if($searchStr !='' )
		{
			$catIds = $filter->getTopsearch($searchStr);
		}
		elseif( $catId > 0 ){
		
			$catIds[] = $catId;
		}
			/////////////////////////////////////////////////////////////////////////////
			$Location = isset($request['location']) ? $request['location'] : '';
		
			$this->view->hfrom = $Location;

			/////////////////////////////////////////////////////////////////////////////
	 	$this->view->catidshow = $catIds;
	 	$this->view->heading = $heading;
		$artworks = $filter->getProduct($catIds,$subcatID, $sortBy,$searchStr,$Location);
		
	 	$this->view->sortBy = $sortBy;
		$this->view->perpage = $per_page;
		$this->view->artworks = $artworks;
		$namespace = new Zend_Session_Namespace();
		$namespace->perpage = $per_page;
		$this->view->current_view = $namespace->current_view;
		$this->view->catPreview = $searchStr;
		
		$page = $this->_getParam('page',1);
		//send the page no.
		$this->view->page = $page;			
		
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($artworks); 
		$paginator->setItemCountPerPage($per_page);
		$paginator->setCurrentPageNumber($page);
		
		$this->view->list = 'none';
		$this->view->paginator = $paginator;
    }
}
?>
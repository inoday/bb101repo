<?php

class MessageController extends Zend_Controller_Action
{
	public function init()
    {   
    	$namespace = new Zend_Session_Namespace();
		$this->request  = $this->getRequest()->getParams(); 
		$this->view->action=$this->request['action'];
		$this->view->ind=$this->request['ind'];
    }
	
    public function indexAction()
     { 
	 $namespace 	= new Zend_Session_Namespace();
	 	if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
		 //@  
		$params 	= $this->getRequest()->getParams();
		
		$messages	= new Application_Model_ArtInbox();

		if(isset($params['del_chk_msg']))
		{
			$msg_del	= '';
			foreach($params['chk_del'] as $key => $val)
			{
				$msg_del	= $messages->trashMessage($val);
			}
			
			if(isset($msg_del))
			{
				$this->_helper->redirector->gotosimple('index','message',true,array('ijuh'=>"Message Deleted Success fully..",'ind'=>'3'))	;
			}
		}
		if(isset($params['move']))
		{
			$msg_del	= '';
			$fId=$params['folder_name'];
			foreach($params['chk_del'] as $key => $val)
			{
				$msg_del	= $messages->moveMessage($val,$fId);
			}
			
			if(isset($msg_del))	
			{
				$this->_helper->redirector->gotosimple('index','message',true,array('ijuh'=>"Message Moved Success fully..",'ind'=>'3'))	;
			}
		}
		// Deleting 
/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			$namespace->userid=$row[0]['id'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
 ///////////////////////GET FOLDER NAME//////////////////////////
	$folders	= new Application_Model_ArtFolder();
	$viewfolders	= $folders->myFolder($namespace->userid);
	$this->view->viewfolders=$viewfolders;
	//////////////////////////////////////////////////////////////////////
		$viewMe	= $messages->myInbox($namespace->userid);
		//echo '<pre>';print_r($viewMe);die;
//////////////////////////////////////////////////////////////////////////////////////////////////////		
		$this->view->totRecords	=	count($viewMe);
		$this->view->page_name	=	BASE_URL."message/index"; 
		
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($viewMe);
		$paginator->setItemCountPerPage(10);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
		
		
	
	} //@@ End of Index action.

	public function composeAction()
     { $namespace 	= new Zend_Session_Namespace(); //@ 
	 	if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
		$messages	= new Application_Model_ArtInbox();
		$params 	= $this->getRequest()->getParams();
		if(isset($params['id']))
		{
			$this->view->msgId	=	$params['id'];
			$this->view->db		=	$db;
			$msg_cont	= $messages->readMessage($params['id']);
		}
		
		/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			$namespace->userid=$row[0]['id'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
 ///////////////////////GET FOLDER NAME//////////////////////////
	$folders	= new Application_Model_ArtFolder();
	$viewfolders	= $folders->myFolder($namespace->userid);
	$this->view->viewfolders=$viewfolders;
	//////////////////////////////////////////////////////////////////////
	
	} //@@ End of Index action.
	
	public function trashAction()
     {
	 
		$namespace 	= new Zend_Session_Namespace(); //@ 
			if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
		$params 	= $this->getRequest()->getParams();
		
		$messages	= new Application_Model_ArtInbox();

		if(isset($params['del_chk_msg']))
		{
			$msg_del	= '';
			foreach($params['chk_del'] as $key => $val)
			{
				$msg_del	= $messages->deleteMessages($val);
			}
			
			if(isset($msg_del))	
			{
				$this->_helper->redirector->gotosimple('trash','message',true,array('ijuh'=>"Message Deleted Successfully..",'ind'=>'3'))	;
			}
		}
		
		
		if(isset($params['move']))
		{
			$msg_del	= '';
			foreach($params['chk_del'] as $key => $val)
			{
				$msg_del	= $messages->restoreMessages($val);
			}
			
			if(isset($msg_del))	
			{
				$this->_helper->redirector->gotosimple('trash','message',true,array('ijuh'=>"Message Restore Successfully..",'ind'=>'3'))	;
			}
		}
		// Deleting 
		/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			$namespace->userid=$row[0]['id'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
 ///////////////////////GET FOLDER NAME//////////////////////////
	$folders	= new Application_Model_ArtFolder();
	$viewfolders	= $folders->myFolder($namespace->userid);
	$this->view->viewfolders=$viewfolders;
	//////////////////////////////////////////////////////////////////////
		$viewMe	= $messages->getTrashMessafes($namespace->userid);
		$this->view->totRecords	=	count($viewMe);
		$this->view->page_name	=	BASE_URL."message/index"; 
		
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($viewMe);
		$paginator->setItemCountPerPage(10);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
			
	
	} //@@ End of Index action. 
	
	
	public function readAction()
     {$namespace 	= new Zend_Session_Namespace(); //@ 
	 	if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
	 	$messages	= new Application_Model_ArtInbox();
		$param	=	$this->getRequest()->getparams();
		if(isset($param['del_chk_msg']))
		{ 
			$msg_del	= '';
			$val=$param['del_id'];
			
				$msg_del	= $messages->trashMessage($val);
			
			
			if(isset($msg_del))	
			{
				$this->_helper->redirector->gotosimple('index','message',true,array('ijuh'=>"Message Deleted Successfully.",'ind'=>'3'))	;
			}
		}
		
		if(isset($param['reply']))
		{
			
			$val=$param['del_id'];
				$this->_helper->redirector->gotosimple('compose','message',true,array('id'=>$val))	;
			
		}
		$this->view->msgId	=	$param['id'];
		$this->view->db		=	$db;
		$this->view->Ctype	=	@$param['type'];
		
		$msg_cont	= $messages->readMessage($param['id']);
		$folders	= new Application_Model_ArtFolder();
		$viewfolders	= $folders->myFolder($namespace->userid);
		$this->view->viewfolders=$viewfolders;
		if(isset($param['rpl_messageBut']))
		{
			$msg_rpl	= $messages->saveMessages($param,'reply');
			if(isset($msg_rpl))
			{
				$this->_helper->redirector->gotosimple('index','message',true,array('ijuh'=>"Message Sent Successfully..",'ind'=>'3'))	;
			}
		}
	 
	 }
	 
	public function savemsgAction()
     {$namespace 	= new Zend_Session_Namespace(); //@ 
	 	if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
	 	$messages	= new Application_Model_ArtInbox();
		$param		=	$this->getRequest()->getparams();
	//	$param['editor'] 	= stripslashes($this->_request->getPost('editor'));
		
		//echo '<pre>';print_r($param);die;
		if(isset($param))
		{
			$msg_rpl	= $messages->savemsg($param);
			if(isset($msg_rpl))
			{
				$this->_helper->redirector->gotosimple('index','message',true,array('ijuh'=>"Message Sent Success fully..",'ind'=>'3'))	;
			}
		}
	 
	 }
	 
	public function mailfolderAction()
     {$namespace 	= new Zend_Session_Namespace(); //@ 

	 	if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
     	$messages	= new Application_Model_ArtFolder();
		$params 	= $this->getRequest()->getParams();
		/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			$namespace->userid=$row[0]['id'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
 ///////////////////////GET FOLDER NAME//////////////////////////
	$folders	= new Application_Model_ArtFolder();
	$viewfolders	= $folders->myFolder($namespace->userid);
	$this->view->viewfolders=$viewfolders;
	//////////////////////////////////////////////////////////////////////
		if(isset($params['create']))
		{
			$msg_rpl	= $messages->createfolder($params);
			if(isset($msg_rpl))
			{
				$this->_helper->redirector->gotosimple('mailfolder','message',true,array('ijuh'=>"Folder added successfully..",'ind'=>'3'))	;
			}
		}
		
	 }
	 	public function folderAction()
     {$namespace 	= new Zend_Session_Namespace(); //@ 
	 	if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
		
		$params 	= $this->getRequest()->getParams();
		
		$messages	= new Application_Model_ArtInbox();
		
		if(isset($params['del_chk_msg']))
		{
			$msg_del	= '';
			foreach($params['chk_del'] as $key => $val)
			{
				$msg_del	= $messages->deleteMessages($val);
			}
			
			if(isset($msg_del))	
			{
				$this->_helper->redirector->gotosimple('trash','message',true,array('ijuh'=>"Message Deleted Success fully..",'ind'=>'3'))	;
			}
		}
		if(isset($params['move']))
		{
			$msg_del	= '';
			$fId=$params['folder_name'];
			foreach($params['chk_del'] as $key => $val)
			{
				$msg_del	= $messages->moveMessage($val,$fId);
			}
			
			if(isset($msg_del))	
			{
			$foId=$params['fid'];
				$this->_helper->redirector->gotosimple('folder','message',true,array('ijuh'=>"Message Moved Success fully..",'fid'=>$foId,'ind'=>'3'))	;
			}
		}
		// Deleting 
		/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			$namespace->userid=$row[0]['id'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
 ///////////////////////GET FOLDER NAME//////////////////////////
	$folders	= new Application_Model_ArtFolder();
	$viewfolders	= $folders->myFolder($namespace->userid);
	$this->view->viewfolders=$viewfolders;
	//////////////////////////////////////////////////////////////////////
 		 $folder_id=$params['fid'];
		$viewMe	= $messages->getFolderMessafes($namespace->userid,$folder_id);
		$this->view->totRecords	=	count($viewMe);
		$this->view->page_name	=	BASE_URL."message/index"; 
		
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($viewMe);
		$paginator->setItemCountPerPage(10);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
			
	
	} //@@ End of Index action. 
	
	 public function sentAction()
     {$namespace 	= new Zend_Session_Namespace(); //@ 
	 	if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
		
		$messages	= new Application_Model_ArtInbox();
		$params 	= $this->getRequest()->getParams();
		
		if(isset($params['del_chk_msg']))
		{
			
			$msg_del	= '';
			foreach($params['chk_del'] as $key => $val)
			{
				$msg_del	= $messages->trashMessage($val);
			}
			
			if(isset($msg_del))
			{
				$this->_helper->redirector->gotosimple('sent','message',true,array('ijuh'=>"Message Deleted Success fully..",'ind'=>'3'))	;
			}
		}
		
		// Deleting
		/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email);
			$namespace->userid=$row[0]['id'];
 /////////////END GET user id from user email (store in session)////////////////////////////////////////////////
 ///////////////////////GET FOLDER NAME//////////////////////////
	$folders	= new Application_Model_ArtFolder();
	$viewfolders	= $folders->myFolder($namespace->userid);
	$this->view->viewfolders=$viewfolders;
	//////////////////////////////////////////////////////////////////////
		$viewMe	= $messages->mySent($namespace->userid);
		$this->view->totRecords	=	count($viewMe);
		$this->view->page_name	=	BASE_URL."message/index"; 
		
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($viewMe);
		$paginator->setItemCountPerPage(10);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
			
     }
	///////////////////////////ADMIN SECTION//////////////////////////
/*
@	Add by : Reeta Verma
@		On: 24-02-2012
**/ 
		public function messagelistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }

			$messages	= new Application_Model_ArtInbox();
			$params 	= $this->getRequest()->getParams();


			$messages_result = $messages->getMessage();	

			
			////////////////////////////////////////////////////////////////////////////////////////
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			$page = $this->_getParam('page',1);
			//send the page no.
			$this->view->page = $page;			
				//////////////////////////////////////////////////////////////////////////////////////	
			//added by reeta (06-03-2012)
			////////////////////////////////////////////////////////////////////////////
			$this->view->sortBy	=	$request['sortBy']; 
				$bootstrap = $this->getInvokeArg('bootstrap');
				$resource  = $bootstrap->getPluginResource('db');
				$db 	   = $resource->getDbAdapter();
				$sql='SELECT 	* FROM art_inbox Where 1';
				if($request['sender_id']!='')
				$sql.=' and sender_id = '.$request['sender_id'].'';
				if($request['receiver_id']!='')
				$sql.=' and receiver_id = '.$request['receiver_id'].'';
				if($request['searchelement']!='')
				$sql.=' and subject Like  "%'.$request['searchelement'].'%" OR message Like  "%'.$request['searchelement'].'%"';
				if($request['sortBy']!='')
				$sql.=' order by '.$request['sortBy'].'';
			
				$messages_result = $db->fetchAll($sql); 
			 $this->view->sortBy	=	$request['sortBy']; 

			$this->view->sender_id = $request['sender_id'];
			$this->view->receiver_id = $request['receiver_id'];
		$this->view->messages_result = $messages_result;
		//end added by reeta (06-03-2012)
			/////////////////////////////////////////////////////////////////////////////	
			

			$page = $this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($messages_result);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			$this->view->paginator = $paginator;
		}	
		
	public function  deletemessageAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		$id			=	$request['id'];
		$messagelist			=	new Application_Model_ArtInbox();
		$deletemessagelist		=	$messagelist->deletemessagelist($id);
		$this->view->deletemessagelist	=	$deletemessagelist;

		
		$msg	=	'Message deleted successfully.';
		$this->_helper->redirector->gotosimple('messagelist','message',true,array('msg'=>$msg)) ;

	}	
public function viewmessagelistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
	
			////////////////////////////////////////////////////////////////////////////////////////
			$id = $this->_getParam('id', 0);	
			$this->view->msgId	=	$this->_getParam('id', 0);
			$messagelist_model   = new Application_Model_ArtInbox();
			$messagelist_model_result = $messagelist_model->readMessage($id);	
			$this->view->messagelist_model_result = $messagelist_model_result;
			
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}
}

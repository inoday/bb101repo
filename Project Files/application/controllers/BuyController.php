<?php
     
class BuyController extends Zend_Controller_Action
{
	
	 public function init()
    {   
	    
      	$this->namespace= new Zend_Session_Namespace(); 
		 $this->userInfo	=	new Application_Model_DbTable_Profile();
		$this->message	=	new Application_Model_ArtInbox();
    }
    
     public function indexAction()
    {   
	    
      	
		 if($this->namespace->email!='')
		{ 
		 $this->view->getusertype = $this->userInfo->getUsertype();
		$getInfo = 	$this->userInfo->getUserdetails($this->namespace->email, $this->namespace->passowrd);
		$this->view->details = $getInfo;
		
		$this->view->countInbox = $this->message->countInboxMsg($this->namespace->userdetails[0]['id']);
		$this->view->bidding = $this->userInfo->bidding($this->namespace->userdetails[0]['id']); // Show bidding item
		$this->view->liveart = $this->userInfo->liveart($this->namespace->userdetails[0]['id']); // Show live artwork
		$this->view->watchlist = $this->userInfo->watchlist($this->namespace->userdetails[0]['id']); // Show watchlist
		$this->view->feedback = $this->userInfo->feedback($this->namespace->userdetails[0]['id']); // Show feedback count on artwork
	}
    }
}
?>

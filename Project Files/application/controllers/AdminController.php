<?php
    ini_set("display_errors",'off');

class AdminController extends Zend_Controller_Action
{
    public function init()
    {
        
    }
	
    public function autoselectAction(){
    	 
		$this->_helper->layout->disableLayout();
		extract($_POST);
		
		$data1 = $data."%";
				
		$users	=	new Application_Model_ArtUsers();
		$allrec		=	$users->getAutoUsers($data1);
		
		$this->view->allrec	=	$allrec;
	}

    public function indexAction(){
    	 
        $request = $this->getRequest()->getParams();
		$this->_helper->layout->setLayout('loginlayout');
		$this->view->form = $this->getForm();
		$_SESSION['add_gr']='';$_SESSION['msg']='';
		$this->_redirect('admin/process');
    }
	
	public function  getForm(){
		$namespace = new Zend_Session_Namespace(); 
		
		if($namespace->adminemail != '')
		{ 
			$this->_redirect('admin/home');
		}
		return new Application_Form_Login(array(
			//'action' => '../admin/process',
			'action' => 'admin/process',
			'method' => 'post',
		));
	}
	
	public function  processAction(){ 
	
		$namespace = new Zend_Session_Namespace(); 
		
		if($namespace->adminemail != '')
		{ 
			$this->_redirect('admin/home');
		} 
		$this->_helper->layout->setLayout('loginlayout');
		$request	=	$this->getRequest()->getParams();
		$users = new Application_Model_DbTable_Artuser();
        $form = new Application_Form_Login();
        $this->view->Login = $form;
       
        if($this->getRequest()->isPost()){ 
            if($form->isValid($_POST))
			{ 
                $data = $form->getValues();
                $auth = Zend_Auth::getInstance();
                $authAdapter = new Zend_Auth_Adapter_DbTable($users->getAdapter(),'art_admin');
                $authAdapter->setIdentityColumn('email')
                            ->setCredentialColumn('password');
                $authAdapter->setIdentity($data['email'])
                            ->setCredential($data['password']);
                $result = $auth->authenticate($authAdapter);
			 
                if($result->isValid())
				{
                	$namespace = new Zend_Session_Namespace();  
					$namespace->adminemail 	= $request['email'];
					 
                    $this->_redirect('admin/home');
                }else { 
                    $this->view->errorMessage = "Invalid username or password. Please try again.";
                }
            }
		}
    }
	
	public function  logoutAction(){
        Zend_Auth::getInstance()->clearIdentity();
		$namespace = new Zend_Session_Namespace();  
		$namespace->adminemail 	= null;
        $this->_helper->redirector(''); // back to login page
    }
	        /*  Manage Members module
			@	user list show 
			@	Add by : Abhishek
			@		 
			**/	
	
	public function  homeAction(){   
	    
      	$namespace = new Zend_Session_Namespace();
		
		if($namespace->adminemail ==''){
			  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
		
		$this->_helper->layout->setLayout('adminlayout');
		$this->view->title = "Admin Home Page";
	
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
	
		$this->view->adminemail = $adminemail;
		
		$this->view->homecontent = $this->details(); 
//echo '<pre>';print_r($this->details());die;
		
		if($adminemail = ''){
			$this->_helper->redirector('');
			
		}
		
    }
    public function  rehomeAction(){   
	    
      	$namespace = new Zend_Session_Namespace();
		
		if($namespace->adminemail ==''){
			  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
		$this->_helper->layout->disableLayout();
;
		$this->view->title = "Admin Home Page";
	
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
	
		$this->view->adminemail = $adminemail;
		
		$this->view->homecontent = $this->details(); 
//echo '<pre>';print_r($this->details());die;
		
		if($adminemail = ''){
			$this->_helper->redirector('');
			
		}
		
    }
	public function  adminAction(){ 
	
		$namespace = new Zend_Session_Namespace(); 
		
		if($namespace->adminemail != ''){
			 
			$this->_redirect('admin/home');
		}
		
		$this->_helper->layout->setLayout('loginlayout');
		$request	=	$this->getRequest()->getParams();
		$users = new Application_Model_DbTable_ArtUser();
        $form = new Application_Form_Login();
        $this->view->Login = $form;
        
        if($this->getRequest()->isPost()){
        	 
            if($form->isValid($_POST)){
            	 
                $data = $form->getValues();
                $auth = Zend_Auth::getInstance();
                $authAdapter = new Zend_Auth_Adapter_DbTable($users->getAdapter(),'art_admin');
                $authAdapter->setIdentityColumn('email')
                            ->setCredentialColumn('password');
                $authAdapter->setIdentity($data['email'])
                            ->setCredential($data['password']);
                $result = $auth->authenticate($authAdapter);
			 
                if($result->isValid())
				{
                	$namespace = new Zend_Session_Namespace();  
					$namespace->adminemail 	= $request['email'];
					 
                    $this->_redirect('admin/home');
                }else { 
                    $this->view->errorMessage = "Invalid username or password. Please try again.";
                }
            }
		}
    }
	
	public function  changepasswordAction(){
       	$namespace = new Zend_Session_Namespace(); 

		$this->_helper->layout->setLayout('adminlayout');
		$request	=	$this->getRequest()->getParams();
		$email=$namespace->adminemail;
		$adminpass=new Application_Model_DbTable_Admin(); 
		if(isset($request['submit']))
		{
		  	$adminpass_res=$adminpass->chkpass($email,$request['old_pass']); 
			if($adminpass_res=='1')
			{
				$adminchpass_res=$adminpass->updatepass($email,$request['new_pass']);
	
				$namespace->msg1=$msg='Password changed successfully';
				$this->_redirect('admin/changepassword/msg/'.$msg.'/page/'.$page);
			}
			else {
				$namespace->msg1=$msg='Enter correct current password';
				$this->_redirect('admin/changepassword/msg/'.$msg.'/page/'.$page);

			}
		}
    }
    
    private function details() {
    	$art_details = new Application_Model_DbTable_Admin();
    	$homecontent['top_art'] = $art_details->getTopArtwork();
    	$homecontent['latest_art'] = $art_details->getLatestArtwork();
    	$homecontent['latest_art_live'] = $art_details->getLatestArtworkLive();
    	$homecontent['top_seller'] = $art_details->getTopSeller();
    	$homecontent['top_buyer'] = $art_details->getTopBuyer();
    	$homecontent['latest_user'] = $art_details->getLatestUser();
    	return $homecontent;
    }
}
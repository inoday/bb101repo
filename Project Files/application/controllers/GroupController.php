<?php
     
/*
@	controller for 'Manage Group' module
@	Add by : Abhishek
@		On: 13-01-2012
**/

class GroupController extends Zend_Controller_Action
{
/*
@	Method for show group listing
@	Add by : Abhishek
@		On: 13-01-2012
**/
	public function  groupAction()
    {   
	    
      	$namespace = new Zend_Session_Namespace();
		 
		if($namespace->adminemail =='') 
		{  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
			
        $this->_helper->layout->setLayout('adminlayout');
		$this->view->title = "Admin Home Page";
		 
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		 
		$this->view->adminemail = $adminemail;
		 
		if($adminemail = ''){
			$this->_helper->redirector('');
		}
		
		$allgroup	=	new Application_Model_ArtGroup();
		
		$users		=	$allgroup->getUsers();
		
		$page = $this->_getParam('page',1);
		
		$this->view->page = $page;
	    $this->view->users = $users;
    }  	 
/*
@	Method for update Group permission module
@	Add by : Abhishek
@		On: 13-01-2012
**/
    public function grouppermissionAction(){
    	
    	$namespace = new Zend_Session_Namespace();
		 
		if($namespace->adminemail =='') 
		{  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
			
        $this->_helper->layout->setLayout('adminlayout');
		$this->view->title = "Admin Home Page";
		 
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		 
		$this->view->adminemail = $adminemail;
		 
		if($adminemail = ''){
			$this->_helper->redirector('');
		}
		$request 		=	$this->getRequest()->getParams();
		$gup_id			=	$request['gup_id'];
  		$permission	=	new Application_Model_ArtGroup();
  		$g_id		=	$permission->SetPermission($gup_id);
  		 
  		$this->view->g_id = $g_id;
    }
/*
@	Method for Insert Group permission module
@	Add by : Abhishek
@		On: 13-01-2012
**/
    function addgroupAction() {
    	
    	$namespace = new Zend_Session_Namespace();
		 
		if($namespace->adminemail =='') 
		{  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
			
        $this->_helper->layout->setLayout('adminlayout');
		$this->view->title = "Admin Home Page";
		 
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		 
		$this->view->adminemail = $adminemail;
		$request = $this->getRequest()->getParams();
	    if (isset($request['submit']))
		{ 
		    $dbField = array();
			$dbField = array('group_type' => $request['group_type']);
		   
		 	$insert   = new Application_Model_ArtGroup(); 
			     
			$ret = $insert->insertgroup($dbField);
			 
			$_SESSION['add_gr'] = 'Group added successfully';
			$this->view->msg = $msg;
			$this->_redirect('group/group/');	  
		}
		  
    }
    
/*
@	Method for update Group permission module
@	Add by : Abhishek
@		On: 13-01-2012
**/
    function addgrouppermissionAction() {
    	$request = $this->getRequest()->getParams();
    	if (isset($request['submit']))
		{     
		    $dbField = array();
			$dbField = array('my_wall' => $request['wall'],'auction' => $request['auction'],
							 'upload_aw' => $request['upload_art'],'collection' => $request['collection'],
							 'view_aw' => $request['view_art'],'manage_aw' => $request['manage_art'],
							 'watchlist' => $request['watchlist'],'message' => $request['message'],
							 'view_bidding' => $request['bidding'],'user_profile' => $request['profile'],
							 'aw_approval' => $request['approval'],'group_id' => $request['group_id']
							);
		   
		 	$update   = new Application_Model_ArtGroup(); 
			     
			$ret = $update->addgroup($dbField,$dbField['group_id']);
			$_SESSION['msg'] = 'Permission set successfully';
			$this->view->msg = $msg;
			$this->_redirect('group/group/msg/');			  
		}
    }
}
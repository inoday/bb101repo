<?php
     
class BannerController extends Zend_Controller_Action
{ 
	///////////////////////////CATEGORY//////////////////////////
/*
@	show categorylist from the table
@	Add by : Reeta Verma
@		On: 20-01-2012 
**/
		public function bannerlistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	//$this->_helper->redirector('index');
            	$this->_redirect('admin/process');
            }
			

			$bannerlist_model   = new Application_Model_DbTable_ArtBanner();
			$bannerlist_model_result = $bannerlist_model->getallbanner($searchelement,'');	
			
			
			////////////////////////////////////////////////////////////////////////////////////////
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			$page = $this->_getParam('page',1);
			//send the page no.
			$this->view->page = $page;			
			
								//////////////////////////////////////////////////////////////////////////////////////	
			//added by reeta (06-03-2012)
			////////////////////////////////////////////////////////////////////////////
			 $this->view->sortBy	=	$request['sortBy']; 
			if($request['searchelement']!='')
			{
				//serching all element with the table
				$searchelement	=	$request['searchelement'];
				//show the search element on the search box
				$namespace->psquestion = $request['searchelement'];
				$this->view->psquestion = $namespace->psquestion;	
				
			if($request['sortBy']!='')
			{
				$bannerlist_model_result = $bannerlist_model->getallbanner($searchelement,$request['sortBy']);		
			}
			else {
				$bannerlist_model_result = $bannerlist_model->getallbanner($searchelement,'');	
				}
				
				}	
		else if($request['sortBy']!='')
			{
	$bannerlist_model_result = $bannerlist_model->getallbanner($searchelement,$request['sortBy']);			
	}



			
		$this->view->bannerlist_model_result = $bannerlist_model_result;
		//end added by reeta (06-03-2012)
			/////////////////////////////////////////////////////////////////////////////	

			$page = $this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($bannerlist_model_result);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			$this->view->paginator = $paginator;
		}
		/*
@	add & edit Category from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/		
	public function addbannerlistAction(){
		
		$this->_helper->layout->setLayout('adminlayout');
	    $form = new Application_Form_ArtBanner();
		$Bannerlist_model   = new Application_Model_DbTable_ArtBanner();
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		$formData = $this->getRequest()->getParams();
		$this->view->adminemail = $adminemail;
		//if admin session is logout it's go to index page
		if($adminemail == ''){
			$this->_helper->redirector('index');
		}

		$id = $this->_getParam('id', 0);
			if($id==0)
		$this->view->bodyCopy = "Add Banner";
		else
		$this->view->bodyCopy = "Edit Banner";
        $this->view->form = $form;
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getParams();
			// print_r($formData); 
			 if($formData['banner_type'] == 'Image'){
			 ///////////////////////////////////////////////////////////////////////////

		
		$adapter = new Zend_File_Transfer();
	 	$path='../public/upload';
		$renameFilter = new Zend_Filter_File_Rename( $path );

		$adapter->setDestination('../public/images/banner');  // set path for upload folder
		$adapter->setOptions(array('ignoreNoFile' => true));	// remove validation to upload all files
		$files = $adapter->getFileInfo();//echo "<pre>";print_r($files);	 
		list($imageWidth, $imageHeight, $imageType, $imageAttr) = getimagesize($files['image1']['tmp_name']);
		//echo $imageWidth.",". $imageHeight;die;
		if($formData['location']=='Right') 
		{
			if($imageWidth>=124 && $imageHeight>=604)
			{ $width=124; $height=604;
			}
			else
			{
			$msg	=	'Please choose banner width minimun 124 and height 604 pixel.';
			$this->_helper->redirector->gotosimple('bannerlist','banner',true,array('msg'=>$msg,'sec'=>$section_id)) ;	
			}
		}
		if($formData['location']=='Bottom') 
		{
			if($imageWidth>=728 && $imageHeight>=90)
			{$width=728; $height=90;
			}
			else
			{
			$msg	=	'Please choose banner width minimun 728 and height 90 pixel.';
			$this->_helper->redirector->gotosimple('bannerlist','banner',true,array('msg'=>$msg,'sec'=>$section_id)) ;	
			}
		}
		//die();
		  $param = $this->getRequest()->getParams();
		  if($files['image1']['name']!='')
		  {
		  foreach($files as $fileID => $fileInfo)
			{
		  if(!$fileInfo['name']=='')
				{
					$renameFilter->addFile( array('source' => $fileInfo['tmp_name'], 'target' => $formData['location'].'_'.$formData['page_id'].$fileInfo['name'], 'overwrite' => true ) );// rename file on desination folder
					$image=$formData['location'].'_'.$formData['page_id'].$fileInfo['name'];
					$adapter->addFilter($renameFilter);

				}
				}
				$adapter->receive();
				require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "setup.php";

				require_once '../library/Zend/Filter/ImageSize.php';
				require_once '../library/Zend/Filter/ImageSize/Strategy/Crop.php';
				$filter = new Zend_Filter_ImageSize();
				$output_s = $filter->setHeight($height)
					->setWidth($width)
					->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
					->setThumnailDirectory('../public/images/banner/mid/')
					->setStrategy(new Zend_Filter_Imagesize_Strategy_Crop())
					->filter('../public/images/banner/'.$image);
			 ///////////////////////////////////////////////////////////////////////////
			 }
			 }
			 
     
             	
				$page_id   = $formData['page_id']; 
				$location    		 = $formData['location'];
				$banner_type    		 = $formData['banner_type'];
				if($banner_type=='Image')
				$banner_path=$image;
				else
				$banner_path    		 = $formData['banner_path'];
				$addDate 		 = date("Y-m-d H:i:s");
				if($banner_type=='Image' && $image==''){ 
				$dbField = array('page_id' => $page_id, 'location' => $location, 'banner_type' =>  $banner_type,'create_date'=>date("Y-m-d H:i:s"));
				} else if($banner_type=='Image' && $image!='') { 
				$dbField = array('page_id' => $page_id, 'location' => $location, 'banner_type' =>  $banner_type,'banner_path' =>  $banner_path,'create_date'=>date("Y-m-d H:i:s")); }
				else { 
				$dbField = array('page_id' => $page_id, 'location' => $location, 'banner_type' =>  $banner_type,'banner_path' =>  $banner_path,'create_date'=>date("Y-m-d H:i:s")); }

				///////////////////////////Check unique cat name//////////////////////////////////////

			
				if($id==0){
					//add the 'Category'
					$Bannerlist_model_result = $Bannerlist_model->addbannerlist($dbField);
					$msg	=	'Banner added successfully.';
					$this->_helper->redirector->gotosimple('bannerlist','banner',true,array('msg'=>$msg,'sec'=>$section_id)) ;
				}
				else {
				    //edit the 'Category'
				  $Bannerlist_model_result = $Bannerlist_model->updatebannerlist($id,$dbField);
				  $msg	=	'Banner update successfully.';
					$this->_helper->redirector->gotosimple('bannerlist','banner',true,array('msg'=>$msg,'sec'=>$section_id)) ;
				}
				
            } else {
               
            }
      
	
		$id = $this->_getParam('id', 0);
		$this->view->id	=	$id;		
		if ($id > 0) {
			$bannerlist = new Application_Model_DbTable_ArtBanner();
			
			//getting the 'Category' for edit	
			foreach ($bannerlist->getBannerlist($id) as $key => $value) {
				$record[$key]	=	stripslashes($value);
			}
			$this->view->record	=	$record;
			$form->populate($record);
			
		}
	}
	
	/*
@	Delete Category from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/		
	public function  deletebannerlistAction()
	{ 
			$request 		=	$this->getRequest()->getParams(); 
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		$bannerlist			=	new Application_Model_DbTable_ArtBanner();
		$deletebannerlist		=	$bannerlist->deletebannerlist($id);
		$this->view->deletebannerlist	=	$deletebannerlist;

		$msg	=	'Banner deleted successfully.';
		$this->_helper->redirector->gotosimple('bannerlist','banner',true,array('msg'=>$msg)) ;

	}	
/*
@	
@	Add by : Reeta Verma
@		On: 21-01-2012
**/		
	public function  updatebannerliststatusAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			 $id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		
		$status			=	$request['status'];

		$user			=	new Application_Model_DbTable_ArtBanner();
		$updateBannerliststatus		=	$user->updateBannerliststatus($id,$status);
		$this->view->updateBannerliststatus	=	$updateBannerliststatus;
	
		if($status=='1')
		$msg	=	'Published successfully.';
		else
		$msg	=	'Unpublished successfully.';
		$this->_helper->redirector->gotosimple('bannerlist','banner',true,array('msg'=>$msg)) ;

	}	
/*
@	
@	Add by : Reeta Verma
@		On: 27-02-2012
**/		
	public function viewbannerlistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
	
			////////////////////////////////////////////////////////////////////////////////////////
			$id = $this->_getParam('id', 0);	
			$bannerlist_model   = new Application_Model_DbTable_ArtBanner();
			$bannerlist_model_result = $bannerlist_model->getBannerlist($id);	
			$this->view->bannerlist_model_result = $bannerlist_model_result;
			
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
public function ajaxAction()
		{
			$this->_helper->layout->disableLayout();
			$request  =	$this->getRequest()->getParams();
			$this->view->segId = $request['segId'];
		}
}

<?php
/*
@	controller for 'Manage globle info' module
@	Add by : Reeta
@		
**/
class AdminmanageglobalinfoController extends Zend_Controller_Action
{      
    public function init()
    {   
      	$namespace = new Zend_Session_Namespace();
		
		if($namespace->adminemail =='') 
		{  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
    }   
    //*******edit packagelist from the table*******//	
	
	public function addglobalinfoAction()
	{
		$this->_helper->layout->setLayout('adminlayout');
		$this->view->bodyCopy = "<p>Please fill out this form.</p>";
		$request = $this->getRequest()->getParams();
		$this->view->error = @$request['ijuh'];	
		$param = $this->getRequest()->getParams();
		$page = $this->_getParam('page',1);
		$this->view->page   = $page;

		///**************** Edit Listing Action ********************//
	    $allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
		$rec = $allgloble->getglobelinfoAll(); 
       // echo'<pre>';print_r($rec);  die;
			
		if(!empty($rec[0]['meta_keywords'])){
			$this->view->title              =	$rec[0]['title'];
			$this->view->meta_keywords	    =	$rec[0]['meta_keywords'];
			$this->view->meta_description	=	$rec[0]['meta_description'];
			$this->view->status		    	=	$rec[0]['status'];
			$this->view->offline_message	=	$rec[0]['offline_message'];
			$this->view->admin_email	    =	$rec[0]['admin_email'];
			$this->view->contact_email      =	$rec[0]['contact_email'];
			$this->view->moderator_email    =	$rec[0]['moderator_email'];
			$this->view->join_us    		=	$rec[0]['join_us'];
			$this->view->follow_us    		=	$rec[0]['follow_us'];
			$this->view->image_small_width  =	$rec[0]['image_small_width'];
			$this->view->image_small_height =	$rec[0]['image_small_height'];
			$this->view->image_big_width    =	$rec[0]['image_big_width'];
			$this->view->image_big_height   =	$rec[0]['image_big_height'];
			$this->view->image_mid_width    =	$rec[0]['image_mid_width'];
			$this->view->image_mid_height   =	$rec[0]['image_mid_height'];
			$this->view->image_large_width  =	$rec[0]['image_large_width'];
			$this->view->image_large_height =	$rec[0]['image_large_height'];
			$this->view->artwork_size 		=	$rec[0]['artwork_size'];    
			$this->view->profile_size 		=	$rec[0]['profile_size'];
			$this->view->enable_ip			=	$rec[0]['enable_ip'];
			$this->view->image_type 		=	$rec[0]['image_type'];
			$this->view->ending_soon 		=	$rec[0]['ending_soon'];
			$this->view->app_id 			=	$rec[0]['app_id'];
			$this->view->secret 			=	$rec[0]['secret'];
			$this->view->domain 			=	$rec[0]['domain'];
			$this->view->auction_duration	=	$rec[0]['auction_duration'];
			$this->view->auction_reserve_price	=	$rec[0]['auction_reserve_price'];
			$this->view->advertising_auction_duration=	$rec[0]['advertising_auction_duration'];
			$this->view->max_auction_duration=	$rec[0]['max_auction_duration'];
			$this->view->auction_percent	=	$rec[0]['auction_percent'];
			$this->view->auction_sold		=	$rec[0]['auction_sold'];
			$this->view->buyers_premium		=	$rec[0]['buyers_premium'];
			$this->view->scout_percent	    =	$rec[0]['scout_percent'];
			$this->view->bid_interval	    =	$rec[0]['bid_interval'];
			$this->view->username           =	$rec[0]['username'];
			$this->view->password		    =	$rec[0]['password'];
			$this->view->reminder		    =	$rec[0]['reminder'];
			$this->view->claim		    	=	$rec[0]['claim'];
			$this->view->signature			=	$rec[0]['signature'];
			$this->view->merchant_email	    =	$rec[0]['merchant_email'];
			$this->view->creditcardtype	    =	$rec[0]['creditcardtype'];
			$this->view->creditcardnumber	=	$rec[0]['creditcardnumber'];
			$this->view->exp_date_month	    =	$rec[0]['exp_date_month'];
			$this->view->exp_date_year	    =	$rec[0]['exp_date_year'];
			$this->view->cvv_number	   		=	$rec[0]['cvv_number'];
			$this->view->C_first_name	    =	$rec[0]['C_first_name'];
			$this->view->C_last_name		=	$rec[0]['C_last_name'];
			$this->view->street	    		=	$rec[0]['street'];
			$this->view->city	    		=	$rec[0]['city'];
			$this->view->state	   			=	$rec[0]['state'];
			$this->view->zip	    		=	$rec[0]['zip'];
			$this->view->country	   		=	$rec[0]['country'];
		}
		//***************For Add Action ********************//
				
		if (isset($param['submit']))
		{   
		    $request = $this->getRequest()->getParams();
		    $str = serialize($request);
		    $getIp = $this->getRequest()->getServer('REMOTE_ADDR'); 
			             // $this->getRequest()->getServer('REMOTE_ADDR'); 
			$adddate = 	date("Y-m-d H:i:s");
		 	
		    $dbField = array();	
			$dbField = array('value' => $str,'modify_date' => $adddate,'ip' => $getIp);

			$tutorial = new Application_Model_DbTable_ArtGlobelinfo();
		    
			$tutorial->addglobleinfo($dbField);
			$msg = 'Global info updated successfully.';
			$this->_redirect('adminmanageglobalinfo/addglobalinfo/msg/'.$msg.'/page/'.$page);
        } 

		 
		if(isset($request['msg']))
		{
        	$request =	$this->getRequest()->getParams();
			$this->view->msg = $request['msg'];			
		} 
	}
}
?>
<?php
     
/*
@	controller for 'Manage Group' module
@	Add by : Abhishek
@		On: 13-01-2012
**/

class NewsController extends Zend_Controller_Action
{
	///////////////////////////CATEGORY//////////////////////////
/*
@	show sectionlist from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/ 
		public function newslistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
			

			$newslist_model   = new Application_Model_DbTable_ArtNews();
			$newslist_model_result = $newslist_model->fetchAll();	
			
			
			////////////////////////////////////////////////////////////////////////////////////////
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			$page = $this->_getParam('page',1);
			//send the page no.
			$this->view->page = $page;			
						////////////////////////////////////////////////////////////////////////
			//////////////////////////////////////////////////////////////////////////////////////	
			//added by reeta (06-03-2012)
			////////////////////////////////////////////////////////////////////////////
			 $this->view->sortBy	=	$request['sortBy']; 
			if($request['searchelement']!='')
			{
				//serching all element with the table
				$searchelement	=	$request['searchelement'];
				//show the search element on the search box
				$namespace->psquestion = $request['searchelement'];
				$this->view->psquestion = $namespace->psquestion;	
				if($request['sortBy']!='')
			{
				$newslist_model_result = $newslist_model->fetchAll($newslist_model->select()->where('title Like ?', '%'.$searchelement.'%')->orWhere('description Like ?', '%'.$searchelement.'%')->order($request['sortBy']));	
			}
			else
				$newslist_model_result = $newslist_model->fetchAll($newslist_model->select()->where('title Like ?', '%'.$searchelement.'%')->orWhere('description Like ?', '%'.$searchelement.'%'));	
		}
		else if(isset($request['sortBy']))
			{
				$newslist_model_result = $newslist_model->fetchAll($newslist_model->select()->where('title Like ?', '%'.$searchelement.'%')->orWhere('description Like ?', '%'.$searchelement.'%')->order($request['sortBy']));	
			}


			
		$this->view->newslist_model_result = $newslist_model_result;
		//end added by reeta (06-03-2012)
			/////////////////////////////////////////////////////////////////////////////
			

			$page = $this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($newslist_model_result);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			$this->view->paginator = $paginator;
		}
		/*
@	add & edit cmslist from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/		
	public function addnewslistAction(){
		
		$this->_helper->layout->setLayout('adminlayout');
	
		
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		
		$this->view->adminemail = $adminemail;
		//if admin session is logout it's go to index page
		if($adminemail == ''){
			$this->_helper->redirector('index');
		}

		$id = $this->_getParam('id', 0);
			if($id==0)
		$this->view->bodyCopy = "Add News";
		else
		$this->view->bodyCopy = "Edit News";
	    $form = new Application_Form_ArtNews();
		
        $this->view->form = $form;
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getParams();
			 
            if ($form->isValid($formData)) {
             	
				$title   = $form->getValue('title');
				$description     = $form->getValue('description');
				$display_start_date     = $form->getValue('display_start_date');
				$display_end_date     = $form->getValue('display_end_date');
				$addDate 	= date("Y-m-d H:i:s");
				$tutorial   = new Application_Model_DbTable_ArtNews();
			
				if($id==0){
					//add the 'cmslist'
					$tutorial->addnewslist($title,$description,$display_start_date,$display_end_date,$addDate);
					$msg	=	'News added successfully.';
					$this->_helper->redirector->gotosimple('newslist','news',true,array('msg'=>$msg,'sec'=>$section_id)) ;
				}
				else {
				    //edit the 'cmslist'
				  $tutorial->updatenewslist($id,$title,$description,$display_start_date,$display_end_date,$addDate);
				  $msg	=	'News update successfully.';
					$this->_helper->redirector->gotosimple('newslist','news',true,array('msg'=>$msg,'sec'=>$section_id)) ;
				}
				
            } else {
                $form->populate($formData);
            }
        }
		
		$id = $this->_getParam('id', 0);
				
		if ($id > 0) {
			$newslist = new Application_Model_DbTable_ArtNews();
			
			//getting the 'cmslist' for edit	
			foreach ($newslist->getnewslist($id) as $key => $value) {
				$record[$key]	=	stripslashes($value);
				if($key=='display_start_date' || $key=='display_end_date')
				{
				$date=explode(' ',$value);
				$record[$key]=$date[0];
				}
			}
			$form->populate($record);
		}
	}
	
	/*
@	Delete cmslist from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/		
	public function  deletenewslistAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
	
		$newslist			=	new Application_Model_DbTable_ArtNews();
		$deletenewslist		=	$newslist->deletenewslist($id);
		$this->view->deletenewslist	=	$deletenewslist;

		$msg	=	'News deleted successfully.';
		$this->_helper->redirector->gotosimple('newslist','news',true,array('msg'=>$msg)) ;

	}	
/*
@	
@	Add by : Reeta Verma
@		On: 21-01-2012
**/		
	public function  updatenewsliststatusAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		$status			=	$request['status'];

		$user			=	new Application_Model_DbTable_ArtNews();
		$updatenewsliststatus		=	$user->updatenewsliststatus($id,$status);
		$this->view->updatenewsliststatus	=	$updatenewsliststatus;
		
		if($status=='1')
		$msg	=	'Published successfully.';
		else
		$msg	=	'Unpublished successfully.';
		$this->_helper->redirector->gotosimple('newslist','news',true,array('msg'=>$msg)) ;

	}	
/*
@	
@	Add by : Reeta Verma
@		On: 23-01-2012
**/		
	public function viewnewslistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
	
			////////////////////////////////////////////////////////////////////////////////////////
			$id = $this->_getParam('id', 0);	
			$newslist_model   = new Application_Model_DbTable_ArtNews();
			$newslist_model_result = $newslist_model->fetchAll($newslist_model->select()->where('id = ?', $id));	
			$this->view->newslist_model_result = $newslist_model_result;
			
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function newsfeedlistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }

			$newslist_model   = new Application_Model_DbTable_ArtNewsfeed();
			$newslist_model_result = $newslist_model->fetchAll();	
			
			
			////////////////////////////////////////////////////////////////////////////////////////
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			$page = $this->_getParam('page',1);
			//send the page no.
			$this->view->page = $page;			
						////////////////////////////////////////////////////////////////////////
			//////////////////////////////////////////////////////////////////////////////////////	
			//added by reeta (06-03-2012)
			////////////////////////////////////////////////////////////////////////////
			 $this->view->sortBy	=	$request['sortBy']; 
			if($request['searchelement']!='')
			{
				//serching all element with the table
				$searchelement	=	$request['searchelement'];
				//show the search element on the search box
				$namespace->psquestion = $request['searchelement'];
				$this->view->psquestion = $namespace->psquestion;	
				if($request['sortBy']!='')
				{
					$newslist_model_result = $newslist_model->fetchAll($newslist_model->select()->where('title Like ?', '%'.$searchelement.'%')->orWhere('url Like ?', '%'.$searchelement.'%')->order($request['sortBy']));	
				}
			else
					$newslist_model_result = $newslist_model->fetchAll($newslist_model->select()->where('title Like ?', '%'.$searchelement.'%')->orWhere('url Like ?', '%'.$searchelement.'%'));	
		}
		else if(isset($request['sortBy']))
			{
				$newslist_model_result = $newslist_model->fetchAll($newslist_model->select()->where('title Like ?', '%'.$searchelement.'%')->orWhere('url Like ?', '%'.$searchelement.'%')->order($request['sortBy']));	
			}
			
		$this->view->newslist_model_result = $newslist_model_result;
		//end added by reeta (06-03-2012)
			/////////////////////////////////////////////////////////////////////////////
			

			$page = $this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($newslist_model_result);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			$this->view->paginator = $paginator;
		}
	public function addnewsfeedlistAction(){
		
		$this->_helper->layout->setLayout('adminlayout');
	
		
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		
		$this->view->adminemail = $adminemail;
		//if admin session is logout it's go to index page
		if($adminemail == ''){
			$this->_helper->redirector('index');
		}

		$id = $this->_getParam('id', 0);
			if($id==0)
		$this->view->bodyCopy = "Add News Feed";
		else
		$this->view->bodyCopy = "Edit News Feed";
	    $form = new Application_Form_ArtNewsfeed();
		
        $this->view->form = $form;
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getParams();
			 
            if ($form->isValid($formData)) {
             	
				$title   = $form->getValue('title');
				$url     = $form->getValue('url');
					$addDate 	= date("Y-m-d H:i:s");
				$tutorial   = new Application_Model_DbTable_ArtNewsfeed();
			
				if($id==0){
					//add the 'cmslist'
					$tutorial->addnewslist($title,$url,$addDate);
					$msg	=	'News added successfully.';
					$this->_helper->redirector->gotosimple('newsfeedlist','news',true,array('msg'=>$msg,'sec'=>$section_id)) ;
				}
				else {
				    //edit the 'cmslist'
				  $tutorial->updatenewslist($id,$title,$url,$addDate);
				  $msg	=	'News update successfully.';
					$this->_helper->redirector->gotosimple('newsfeedlist','news',true,array('msg'=>$msg,'sec'=>$section_id)) ;
				}
				
            } else {
                $form->populate($formData);
            }
        }
		
		$id = $this->_getParam('id', 0);
				
		if ($id > 0) {
			$newslist = new Application_Model_DbTable_ArtNewsfeed();
			
			//getting the 'cmslist' for edit	
			foreach ($newslist->getnewslist($id) as $key => $value) {
				$record[$key]	=	stripslashes($value);
			}
			$form->populate($record);
		}
	}
	
		public function  deletenewsfeedlistAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
	
		$newslist			=	new Application_Model_DbTable_ArtNewsfeed();
		$deletenewslist		=	$newslist->deletenewslist($id);
		$this->view->deletenewslist	=	$deletenewslist;

		$msg	=	'News deleted successfully.';
		$this->_helper->redirector->gotosimple('newsfeedlist','news',true,array('msg'=>$msg)) ;

	}
	public function  updatenewsfeedliststatusAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		$status			=	$request['status'];

		$user			=	new Application_Model_DbTable_ArtNewsfeed();
		$updatenewsliststatus		=	$user->updatenewsliststatus($id,$status);
		$this->view->updatenewsliststatus	=	$updatenewsliststatus;
		
		if($status=='1')
		$msg	=	'Published successfully.';
		else
		$msg	=	'Unpublished successfully.';
		$this->_helper->redirector->gotosimple('newsfeedlist','news',true,array('msg'=>$msg)) ;

	}	
		public function viewnewsfeedlistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
	
			////////////////////////////////////////////////////////////////////////////////////////
			$id = $this->_getParam('id', 0);	
			$newslist_model   = new Application_Model_DbTable_ArtNewsfeed();
			$newslist_model_result = $newslist_model->fetchAll($newslist_model->select()->where('id = ?', $id));	
			$this->view->newslist_model_result = $newslist_model_result;
			
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}	
}
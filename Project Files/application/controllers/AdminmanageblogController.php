<?php
/*
@	controller for 'Manage blog management' module
@	Add by : Anil Rawat
@		On: 07-11-2011
**/

class adminmanageblogController extends Zend_Controller_Action
{    
     
     public function init()
    { 
      	$namespace = new Zend_Session_Namespace(); 
		if($namespace->adminemail =='') 
		{  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
    }
	
	
	public function bloglistAction()
	{   //echo'hi asr';die;  
	    $this->_helper->layout->setLayout('adminlayout');
		$request  	=	$this->getRequest()->getParams();		
		$auth     	= Zend_Auth::getInstance();
		$identity 	= $auth->getIdentity();
		$adminemail = $identity;
		
		$this->view->adminemail = $adminemail;
		
		if($adminemail == '')
		{
		   $this->_helper->redirector('index');
		}
		/**********search blog list************/
		$request =	$this->getRequest()->getParams();
		$bootstrap = $this->getInvokeArg('bootstrap');
		$resource  = $bootstrap->getPluginResource('db');
		$db 	   = $resource->getDbAdapter();
		$Sql 		= "SELECT d.*,d.id as blogId,count( e.blog_id ) AS totalcommment,e.blog_id,e.user_id,e.comment,
						d.publish,d.create_date,d.modify_date FROM art_blog d LEFT JOIN art_blog_comments e ON e.blog_id = d.id where 1";	
		$this->view->sortBy	=	$request['sortBy']; 
		if($request['searchelement']!='')
		{
			//serching all element with the table
			$searchelement	=	$request['searchelement'];
			//show the search element on the search box
			$namespace->psquestion = $request['searchelement'];
			$this->view->psquestion = $namespace->psquestion;	
			$Sql .= " and title Like '%$searchelement%' ";
			
		}
		if($request['creator']!='')
			{
			$Sql .= " and creator= ".$request['creator']."";
			}
		$Sql .=	" GROUP BY (d.id)"; 
		if($request['sortBy']!='')
			{
			$Sql .= " ORDER BY ".$request['sortBy']." ";
			}
		$this->view->creator = $request['creator'];
		
		$Sqlresult = $db->fetchAll($Sql);	
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($Sqlresult);
		$paginator->setItemCountPerPage(10);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator; 
			if(isset($request['msg']))
			{
				$request =	$this->getRequest()->getParams();
				$this->view->msg	=	$request['msg'];
			}	
		
		$page = $this->_getParam('page',1);
		$this->view->page   = $page;

	}
	public function addblogAction()
	{ 
		$this->_helper->layout->setLayout('adminlayout');
		$blog_id = $this->_getParam('blog_id', 0);
			if($blog_id==0)
		$this->view->bodyCopy = "Add Blog";
		else
		$this->view->bodyCopy = "Edit Blog";
		$allblog	=	new Application_Model_DbTable_ArtBlog();
		$param = $this->getRequest()->getParams();
		if(isset($param['msg']))
			{
				$request =	$this->getRequest()->getParams();
				$this->view->msg	=	$request['msg'];
			}
		$page = $this->_getParam('page',1);
		$this->view->page   = $page; 
		     //////////////////for add//////////////////////////////////////
		        $request = $this->getRequest()->getParams();
	        if (isset($request['submit']))
			{if($request['bloguser_id']!='' && $request['title'] !='' )
			 {	     
			     $request = $this->getRequest()->getParams();
			     $id = $request['blog_id'];
			     $title = $request['title'];
			     $description = $request['description'];
				 $creator = $request['bloguser_id'];
			     $publish = $request['status'];
			     $create_date   = date("Y-m-d H:i:s");
				 $modify_date   = date("Y-m-d H:i:s");
			     $dbField = array();
				 $dbField = array('title' => $request['title'],'description'=> $request['description'],
				   'creator' => $request['bloguser_id'],'publish' => $request['status'],'create_date' => $create_date );
				  
				 $tutorial   = new Application_Model_DbTable_ArtBlog();
			     if($id =='')
				 {
				 $ret = $tutorial->addblogadmin($dbField);
				  $msg = 'Blog added successfully';
				 $this->_redirect('adminmanageblog/bloglist/msg/'.$msg.'/page/'.$page);
				  }
			 }
			 else
			 {
			     $msg = 'Please enter the value';
				 $this->_redirect('adminmanageblog/addblog/msg/'.$msg.'/page/'.$page);
			 }
	        } 
		    //////////////////for edit//////////////////////////////////////
			  if (isset($param['edit']))
			{   
			     $request = $this->getRequest()->getParams();
				 //echo'<pre>';print_r($request);die;
			     $blog_id = $request['blog_id'];
			     $blog_title = $request['title'];
			     $blog_description = $request['description'];
				 $bloguser_id = $request['bloguser_id'];
			     $blog_status = $request['status'];
			     $blog_adddate   = date("Y-m-d H:i:s");
				 $blog_modifydate   = date("Y-m-d H:i:s");
			     $dbField = array();
				 $dbField = array('id'=> $request['blog_id'],'title' => $request['title'],
				 'description'=> $request['description'],'creator' => $request['bloguser_id'],
				 'publish' => $request['status'],'modify_date' => $blog_modifydate );
				 $tutorial   = new Application_Model_DbTable_ArtBlog();
			     if($blog_id !='')
				 {
				 $ret = $tutorial->updateblog($dbField);
				 $msg = 'Blog edit successfully';
				 $this->_redirect('adminmanageblog/bloglist/msg/'.$msg.'/page/'.$page);
			     }
				
	        }
			    
				
				
		   //*************** Edit receive list Action ********************//	
			if(isset($param['blog_id']))
			{   
				$rec =	$allblog->getblog($param['blog_id']);
				$this->view->Cngblog_id      =	$param['id'];
				$this->view->title		    =	$rec['title'];
				$this->view->description	=	$rec['description'];
				$this->view->status		    =	$rec['publish'];
			}	
			//**********getting the blog category listing in addblog***************//	
			//$allblogCat		=	new Application_Model_DbTable_ArtBlogcat();
			//$blogcat		=	$allblogCat->blogcatAll();	  
			//$this->view->blogcat = $blogcat;
			//echo'<pre>';print_r($this->view->blogcat);die;

			//**********getting the owner listing in addblog***************//
			$allowner1		=	new Application_Model_Artuser();
			$allowner		=	$allowner1->bloguserAll();	  
			$this->view->allowner = $allowner;
			//echo'<pre>';print_r($this->view->allowner);die;
	
			
			$request =	$this->getRequest()->getParams();
			
			if(isset($request['blog_id'])){
				$this->view->bloguser_id 	= $request['bloguser_id'];
				$this->view->blog_id 		= @$request['blog_id'];
				
			}
			
			//print_r($request);die;
	
	
	
	}	
	

	
	public function  deleteblogAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$blog_id=implode(',',$request['chk']); 
		}
		else
		{
			$blog_id			=	$request['id'];
		}

		$page	=	$request['page'];
			$user			=	new Application_Model_DbTable_ArtBlog();
			$deleteblog		=	$user->deleteablog($blog_id);
			$this->view->deleteblog	=	$deleteblog;
	
		$msg	=	'Blog deleted successfully.';
		$this->_helper->redirector->gotosimple('bloglist','adminmanageblog',true,array('msg'=>$msg)) ;

	}	
		
	public function  commentlistAction()
        {   
		    $this->_helper->layout->setLayout('adminlayout');
		    $this->view->bodyCopy = "<p >Please fill out this form.</p>";
		    //echo'hi asr in comment';die;
		    $request =	$this->getRequest()->getParams();
			//echo'<pre>';print_r($request);die;
			//if(isset($request['blog_id']))
		
			@$blog_id1=$request['blog_id'];
			//echo'<pre>';print_r($blog_id1);die('hi asr');
			
				
				$bootstrap = $this->getInvokeArg('bootstrap');
				$resource  = $bootstrap->getPluginResource('db');
				$db 	   = $resource->getDbAdapter();
			    $Sql2 		= "SELECT e.id as idblog,e.blog_id,e.user_id as id,e.comment,d.title,e.create_date,e.publish,d.creator
				FROM art_blog_comments e LEFT JOIN art_blog d ON e.blog_id = d.id WHERE e.blog_id ='".$blog_id1."' ";
                //echo $Sql2;die;
			    $sqlResult	 = $db->fetchAll($Sql2);
			    //echo'<pre>';print_r($sqlResult);die;
			    $this->view->sqlResult = $sqlResult;
			   //echo'<pre>';print_r($this->view->sqlResult);die;
				$page = $this->_getParam('page',1);
		        $this->view->page   = $page;
				if(isset($request['msg']))
			    {
				$request =	$this->getRequest()->getParams();
				$this->view->msg	=	$request['msg'];
			    }
			
        } 		
		
	public function blockcommentAction()
	    {
		    $request = $this->getRequest()->getParams();
			//echo'<pre>';print_r($request);die;
			$id	=	$request['id'];
			$status	=	$request['value'];
		    $blog_id	=	$request['blog_id'];
			$bloguser_id	=	$request['bloguser_id'];
            $page	=	$request['page'];
			//$dbField = array();
			//$dbField = array('id'=> $request['id'],'status' => $request['value']);
			
			//echo'<pre>';print_r($value);die;
			$user	=	new Application_Model_DbTable_ArtComment();
			$blockcomment		=	$user->blockComment($id,$status);
			$this->view->blockcomment	=	$blockcomment;
			$msg = 'Edit status successfully';
			//$this->_redirect('adminmanagegiftmgm/commentlist/');
			$this->_redirect('adminmanageblog/commentlist/blog_id/'.$blog_id.'/bloguser_id/'.$bloguser_id.'/page/'.$page.'/msg/'.$msg);
			//adminmanageblog/commentlist/blog_id/1/bloguser_id/52/blogcat_id/6/page/1
		}
	
	public function  deletecommentAction()
		{   //echo'hi asr del';die;
			$request 		=	$this->getRequest()->getParams();
			$id	=	$request['id'];
			$status	=	$request['value'];
		    $blog_id	=	$request['blog_id'];
			$bloguser_id	=	$request['bloguser_id'];
			$blogcat_id	=	$request['blogcat_id'];
            $page	=	$request['page'];
			$blogcomment	=	new Application_Model_DbTable_ArtComment();
			$deletecomment		=	$blogcomment->deletecomment($id);
			$this->view->deleteblog	=	$deleteblog;
			$msg = 'Delete the comment successfully';
			$this->_redirect('adminmanageblog/commentlist/blog_id/'.$blog_id.'/bloguser_id/'.$bloguser_id.'/blogcat_id/'.$blogcat_id.'/page/'.$page.'/msg/'.$msg);
			
		}
    	public function viewbloglistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
	
			////////////////////////////////////////////////////////////////////////////////////////
			$id = $this->_getParam('id', 0);	
			$bannerlist_model   = new Application_Model_DbTable_ArtBlog();

				$bannerlist_model_result = $bannerlist_model->getblog($id);
				$this->view->bannerlist_model_result = $bannerlist_model_result;
		
			
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}  
		public function viewblogcommentlistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
	
			////////////////////////////////////////////////////////////////////////////////////////
			$id = $this->_getParam('id', 0);	
			$bannerlist_model   = new Application_Model_DbTable_ArtComment();

				$bannerlist_model_result = $bannerlist_model->getblogcomment($id);
				$this->view->bannerlist_model_result = $bannerlist_model_result;
		
			
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}   
		
		public function  updateblogliststatusAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}

		 $status			=	$request['status'];

		$user			=	new Application_Model_DbTable_ArtBlog();
		$updateBannerliststatus		=	$user->updateBlogliststatus($id,$status);
		$this->view->updateBannerliststatus	=	$updateBannerliststatus;
	
		if($status=='1')
		$msg	=	'Published successfully.';
		else
		$msg	=	'Unpublished successfully.';
		$this->_helper->redirector->gotosimple('bloglist','adminmanageblog',true,array('msg'=>$msg)) ;

	}	
		
		
}



 
?>
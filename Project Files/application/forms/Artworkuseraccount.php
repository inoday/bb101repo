<?php 
class Application_Form_Artworkuseraccount extends Zend_Form{
	
public $elementDecorators = array(
array('ViewHelper'),
 array('Label', array( )
	),
array('Errors'),
);
 
    public function init()
    {
				 
				 
		$set3=array('Visa'=>'Visa','MasterCard'=>'MasterCard','Discover'=>'Discover','Amex'=>'Amex')	; 
		$creditcardtype = $this->createElement('select','creditcardtype');
        $creditcardtype->setAttrib('id','creditcardtype')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set3);
				 
		$creditcardnumber = $this->createElement('text','creditcardnumber');
        $creditcardnumber->setAttrib('id','creditcardnumber')
                 ->setOptions(array('size' => '35','maxlength'=>'20','onkeypress'=>'return isNumberKey(event)'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input');
				 
		for ($c=1;$c<13;$c++) { 
			$set1[$c]=$c;
			}	
		$exp_date_month = $this->createElement('select','exp_date_month');
        $exp_date_month->setAttrib('id','exp_date_month')
                  ->setOptions(array('style' => 'width:120px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set1);
		for ($y=date('Y');$y<date('Y')+10;$y++) { 
			$set2[$y]=$y;
			}		 
		$exp_date_year = $this->createElement('select','exp_date_year');
        $exp_date_year->setAttrib('id','exp_date_year')
                  ->setOptions(array('style' => 'width:120px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set2);
		
      	$cvv_number = $this->createElement('text','cvv_number');
        $cvv_number->setAttrib('id','cvv_number')
                 ->setOptions(array('size' => '35','maxlength'=>'4','onkeypress'=>'return isNumberKey(event)'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
     	
		 $baddress = $this->createElement('text','baddress');
        $baddress->setAttrib('id','baddress')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
    
		 $bcity = $this->createElement('text','bcity');
        $bcity->setAttrib('id','bcity')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);     
		 $bstate = $this->createElement('text','bstate');
        $bstate->setAttrib('id','bstate')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);     
	 	$bzip = $this->createElement('text','bzip');
        $bzip->setAttrib('id','bzip')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;','maxlength'=>'6','onkeypress'=>'return isNumberKey(event)'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);     
	 $ballgroup	=	new Application_Model_DbTable_ArtCountry();
		    $brecord		=	$ballgroup->getcountryName();
			for ($c=0;$c<count($brecord);$c++) { 
			$bsetc[$brecord[$c]['id']]=$brecord[$c]['code'];
			}
        $bcountry = $this->createElement('select','bcountry');
        $bcountry->setAttrib('id','bcountry')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($bsetc);     
		$bphone = $this->createElement('text','bphone');
        $bphone->setAttrib('id','bphone')
                 ->setOptions(array('size' => '35','maxlength'=>'10','onkeypress'=>'return isNumberKey(event)'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);     		 
				 
	//////////////////////////////////////////////////////////////////////////////////////////////
	$saddress = $this->createElement('text','saddress');
        $saddress->setAttrib('id','saddress')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
   
		 $scity = $this->createElement('text','scity');
        $scity->setAttrib('id','scity')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);     
		 $sstate = $this->createElement('text','sstate');
        $sstate->setAttrib('id','sstate')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);     
	 $szip = $this->createElement('text','szip');
        $szip->setAttrib('id','szip')
                 ->setOptions(array('size' => '35','maxlength'=>'6','onkeypress'=>'return isNumberKey(event)'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);     
	 $sallgroup	=	new Application_Model_DbTable_ArtCountry();
		    $srecord		=	$sallgroup->getcountryName();
			for ($c=0;$c<count($srecord);$c++) { 
			$ssetc[$srecord[$c]['id']]=$srecord[$c]['code'];
			}
        $scountry = $this->createElement('select','scountry');
        $scountry->setAttrib('id','scountry')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($ssetc);
		$sphone = $this->createElement('text','sphone');
        $sphone->setAttrib('id','sphone')
                 ->setOptions(array('size' => '35','maxlength'=>'10','onkeypress'=>'return isNumberKey(event)'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true); 	 
				 
				 
				 
		$paypal_username = $this->createElement('text','paypal_username');
        $paypal_username->setAttrib('id','paypal_username')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
				 
		$paypal_password = $this->createElement('text','paypal_password');
        $paypal_password->setAttrib('id','paypal_password')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
				 
		$paypal_signature = $this->createElement('text','paypal_signature');
        $paypal_signature->setAttrib('id','paypal_signature')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
		$pemail = $this->createElement('text','pemail');
        $pemail->setAttrib('id','pemail')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
              //  
$this->addElements(array($creditcardtype,$creditcardnumber, $exp_date_month,$exp_date_year,$cvv_number,$baddress, $bcity,$bstate,$bzip,$bcountry,$bphone,$saddress, $scity,$sstate,$szip,$scountry,$sphone,$pemail,$paypal_username,$paypal_password,$paypal_signature));
    }
}
?>

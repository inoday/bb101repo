<?php 
class Application_Form_Artworkuploada extends Zend_Form{
	
public $elementDecorators = array(
array('ViewHelper'),
 array('Label', array( )
	),
array('Errors'),
);
 
    public function init()
    {
		
		   $cat_id=$_GET['segId']; 
		   $con_id=$_GET['conId'];	 
		   if($cat_id!='')
		   { 		
		  $allsubject	=	new Application_Model_DbTable_ArtCatsubject();
		    $record		=	$allsubject->getcatsubjectName($cat_id);
			for ($c=0;$c<count($record);$c++) { 
			$set[$record[$c]['id']]=$record[$c]['subject'];
			}
			
        $subject = $this->createElement('select','subject');
        $subject->setAttrib('id','subject')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set);
		
             $allmedium	=	new Application_Model_DbTable_ArtCatmedium();
		    $mrecord		=	$allmedium->getcatmediumName($cat_id);
			if(count($mrecord)==0)
				{
					$mset[0]	='NO RECORD';
				}
			else 
			   {
					for ($c=0;$c<count($mrecord);$c++) { 
					$mset[$mrecord[$c]['id']]=$mrecord[$c]['medium'];
						}
				}
			//print_r($mrecord); die();
        $medium = $this->createElement('select','medium');
        $medium->setAttrib('id','medium')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($mset);  
				 
		  $allstyle	=	new Application_Model_DbTable_ArtCatstyle();
		    $srecord		=	$allstyle->getcatstyleName($cat_id);
			for ($c=0;$c<count($srecord);$c++) { 
			$sset[$srecord[$c]['id']]=$srecord[$c]['style'];
			}
			
        $style = $this->createElement('select','style');
        $style->setAttrib('id','style')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($sset);  
			$this->addElements(array($subject,$medium,$style));
			}	
			
			
			if($con_id!='')
			{ 
			 $w=$_GET['width'];	 
			 $d=$_GET['depth'];	 
			 $h=$_GET['height'];	 
			 $cu=$_GET['cu'];  
			if($cu=='mm' && $con_id=='cm')
			{ $w=$w*.1; $d=$d*.1; $h=$h*.1; }
			if($cu=='mm' && $con_id=='m')
			{ $w=$w*0.001; $d=$d*0.001; $h=$h*0.001; }
			if($cu=='mm' && $con_id=='in')
			{ $w=$w*0.03937; $d=$d*0.03937; $h=$h*0.03937; }
			if($cu=='mm' && $con_id=='ft')
			{ $w=$w*0.003281; $d=$d*0.003281; $h=$h*0.003281; }
			if($cu=='mm' && $con_id=='km')
			{ $w=$w*0.000001; $d=$d*0.000001; $h=$h*0.000001; }
			if($cu=='mm' && $con_id=='mm')
			{$w=$w*1; $d=$d*1; $h=$h*1;}
			
			if($cu=='cm' && $con_id=='mm')
			{ $w=$w*10; $d=$d*10; $h=$h*10; }
			if($cu=='cm' && $con_id=='m')
			{ $w=$w*0.01; $d=$d*0.01; $h=$h*0.01; }
			if($cu=='cm' && $con_id=='in')
			{ $w=$w*0.393701; $d=$d*0.393701; $h=$h*0.393701; }
			if($cu=='cm' && $con_id=='ft')
			{ $w=$w*0.032808; $d=$d*0.032808; $h=$h*0.032808; }
			if($cu=='cm' && $con_id=='km')
			{ $w=$w*0.00001; $d=$d*0.00001; $h=$h*0.00001; }
			if($cu=='cm' && $con_id=='cm')
			{$w=$w*1; $d=$d*1; $h=$h*1;}
			
			if($cu=='m' && $con_id=='cm')
			{ $w=$w*100; $d=$d*100; $h=$h*100; }
			if($cu=='m' && $con_id=='mm')
			{ $w=$w*1000; $d=$d*1000; $h=$h*1000; }
			if($cu=='m' && $con_id=='in')
			{ $w=$w*39.370079; $d=$d*39.370079; $h=$h*39.370079; }
			if($cu=='m' && $con_id=='ft')
			{ $w=$w*3.280833; $d=$d*3.280833; $h=$h*3.280833; }
			if($cu=='m' && $con_id=='km')
			{ $w=$w*0.001; $d=$d*0.001; $h=$h*0.001; }
			if($cu=='m' && $con_id=='m')
			{$w=$w*1; $d=$d*1; $h=$h*1;}
			
			if($cu=='km' && $con_id=='cm')
			{ $w=$w*100000; $d=$d*100000; $h=$h*100000; }
			if($cu=='km' && $con_id=='m')
			{ $w=$w*1000; $d=$d*1000; $h=$h*1000; }
			if($cu=='km' && $con_id=='in')
			{ $w=$w*39370.07874; $d=$d*39370.07874; $h=$h*39370.07874; }
			if($cu=='km' && $con_id=='ft')
			{ $w=$w*3280.833333; $d=$d*3280.833333; $h=$h*3280.833333; }
			if($cu=='km' && $con_id=='mm')
			{ $w=$w*1000000; $d=$d*1000000; $h=$h*1000000; }
			if($cu=='km' && $con_id=='km')
			{$w=$w*1; $d=$d*1; $h=$h*1;}
			
			if($cu=='in' && $con_id=='cm')
			{ $w=$w*2.54; $d=$d*2.54; $h=$h*2.54; }
			if($cu=='in' && $con_id=='m')
			{ $w=$w*0.0254; $d=$d*0.0254; $h=$h*0.0254; }
			if($cu=='in' && $con_id=='mm')
			{ $w=$w*25.4; $d=$d*25.4; $h=$h*25.4; }
			if($cu=='in' && $con_id=='ft')
			{ $w=$w*0.083333; $d=$d*0.083333; $h=$h*0.083333; }
			if($cu=='in' && $con_id=='km')
			{ $w=$w*0.000025; $d=$d*0.000025; $h=$h*0.000025; }
			if($cu=='in' && $con_id=='in')
			{$w=$w*1; $d=$d*1; $h=$h*1;}
			
			if($cu=='ft' && $con_id=='cm')
			{ $w=$w*30.480061; $d=$d*30.480061; $h=$h*30.480061; }
			if($cu=='ft' && $con_id=='m')
			{ $w=$w*0.304801; $d=$d*0.304801; $h=$h*0.304801; }
			if($cu=='ft' && $con_id=='in')
			{ $w=$w*12.000024; $d=$d*12.000024; $h=$h*12.000024; }
			if($cu=='ft' && $con_id=='mm')
			{ $w=$w*304.80061; $d=$d*304.80061; $h=$h*304.80061; }
			if($cu=='ft' && $con_id=='km')
			{ $w=$w*0.000305; $d=$d*0.000305; $h=$h*0.000305; }
			if($cu=='ft' && $con_id=='ft')
			{$w=$w*1; $d=$d*1; $h=$h*1;}
			
			if($w==0) $w='';if($d==0) $d='';if($wh==0) $h='';
		  $width = $this->createElement('text','width');
        $width->setAttrib('id','width')
		 		 ->setOptions(array('value' => $w))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'wd80')
		->setOptions(array('maxlength'=>'6','onkeypress'=>'return isNumberKey(event)'))
                 ->setRequired(true);
				 
		$depth = $this->createElement('text','depth');
        $depth->setAttrib('id','depth')
		 		 ->setOptions(array('value' => $d))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'wd80')
		->setOptions(array('maxlength'=>'6','onkeypress'=>'return isNumberKey(event)'))
                 ->setRequired(true);
				 
		$height = $this->createElement('text','height');
        $height->setAttrib('id','height')
		 		 ->setOptions(array('value' => $h))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'wd80')
			->setOptions(array('maxlength'=>'6','onkeypress'=>'return isNumberKey(event)'))
                 ->setRequired(true);
              //  
		$this->addElements(array($width,$depth,$height));
	  
		}
    }
}
?>

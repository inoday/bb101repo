<?php 
class Application_Form_Artworkupload extends Zend_Form{
	
public $elementDecorators = array(
array('ViewHelper'),
 array('Label', array( )
	),
array('Errors'),
);
 
    public function init()
    {
    	 //$group1 = new Zend_Form_Element_Text('group');
        // $group1->setMultiOptions(array('artist' => 'Artist'));
                 
        /* $group2 = new Zend_Form_Element_Radio('group');
         $group2->setMultiOptions(array('Bbuyer' => 'Buyer'));
          
         $group3 = new Zend_Form_Element_Radio('group');
         $group3->setMultiOptions(array('dealer' => 'Dealer'));
          */
		  
			$setu=  array('0'=>'Admin');
			$allgroup	=	new Application_Model_ArtGroup();
		    $record		=	$allgroup->getMemberName();
			for ($c=0;$c<count($record);$c++) { 
			$setu[$record[$c]['options']]=$record[$c]['receipients'];
			}

			$owner_id = $this->createElement('select','owner_id');
        	$owner_id->setAttrib('id','owner_id')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
				 ->setMultiOptions($setu);
		 	$owner_idd = $this->createElement('select','owner_idd');
        	$owner_idd->setAttrib('id','owner_idd')
                ->setOptions(array('style' => 'width:240px;','disabled'=>'disabled'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
				 ->setMultiOptions($setu);
		  
		  $allgroup	=	new Application_Model_DbTable_ArtCategory();
		    $record		=	$allgroup->getcategoryName();
			for ($c=0;$c<count($record);$c++) { 
			$set[$record[$c]['id']]=$record[$c]['cat_name'];
			}
        $category = $this->createElement('select','category');
        $category->setAttrib('id','category')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set);
		
			//$category->setValue($set);
					          
	    $title = $this->createElement('text','title');
        $title->setAttrib('id','title')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
				 
        $price = $this->createElement('text','price');
        $price->setAttrib('id','price')
                 ->setOptions(array('size' => '45'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
				 ->setOptions(array('style' => 'width:240px;'))
                 ->setRequired(true);
		
		$size = $this->createElement('text','size');
        $size->setAttrib('id','size')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
				 
		$primary_colors = $this->createElement('text','primary_colors');
        $primary_colors->setAttrib('id','primary_colors')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input');
		
		
				
		$edition = $this->createElement('text','edition');
        $edition->setAttrib('id','edition')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
				 
		$set3=array('0'=>'No','1'=>'Yes')	; 
		$frame = $this->createElement('select','frame');
        $frame->setAttrib('id','frame')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set3);
				 
		$keywords = $this->createElement('text','keywords');
        $keywords->setAttrib('id','keywords')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input');
				 
		$medium = $this->createElement('text','medium');
        $medium->setAttrib('id','medium')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input');
		
		$style = $this->createElement('text','style');
        $style->setAttrib('id','style')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
		
		
		
		
		
      	$bid_start_date = $this->createElement('text','bid_start_date');
        $bid_start_date->setAttrib('id','bid_start_date')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
     	$location = $this->createElement('text','location');
        $location->setAttrib('id','location')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
		$set2=array('fixed'=>'fixed','variable'=>'variable')	; 
		$shipping_type = $this->createElement('select','shipping_type');
        $shipping_type->setAttrib('id','shipping_type')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set2);
		$frame_details = $this->createElement('textarea','frame_details');
        $frame_details->setAttrib('id','frame_details')
                 ->setOptions(array('rows' => '8','cols'=>'62'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
				 		 	 
     	$descrption = $this->createElement('textarea','descrption');
        $descrption->setAttrib('id','descrption')
                 ->setOptions(array('rows' => '8','cols'=>'62'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
		/////////////////global extesion value ////////////////////////////////
		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
		$rec =	$allgloble->getglobelinfoAll();
		$image_type=$rec[0]['image_type'];	
			 
   		$image1 = $this->createElement('file','image1');
        $image1->setAttrib('id','image1')
                 ->setOptions(array('style' => 'width:240px;'))
				 ->setDestination('../public/upload')
                 ->setRequired(false);
                
    	$image2 = $this->createElement('file','image2');
        $image2->setAttrib('id','image2')
                 ->setOptions(array('style' => 'width:240px;'))
				 ->setDestination('../public/upload')
				 ->setRequired(false);
				 
		$image3 = $this->createElement('file','image3');
        $image3->setAttrib('id','image3')
                 ->setOptions(array('style' => 'width:240px;'))
				 ->setDestination('../public/upload')
				 ->setRequired(false);
				 
		$image4 = $this->createElement('file','image4');
        $image4->setAttrib('id','image4')
                 ->setOptions(array('style' => 'width:240px;'))
				 ->setDestination('../public/upload')
				 ->setRequired(false);
				 
		$image5 = $this->createElement('file','image5');
        $image5->setAttrib('id','image5')
                 ->setOptions(array('style' => 'width:240px;'))
				 ->setDestination('../public/upload')
				 ->setRequired(false);
				 
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id','submit')
                 ->setOptions(array('size' => '35'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'sign_up_submit');
		 
                
              //  
$this->addElements(array($owner_id,$owner_idd,$category,$title,$price,$size,$edition,$frame,$keywords,$medium,$bid_start_date,$shipping_type,$frame_details,$descrption, $image1,$image2,$image3,$image4,$image5,$submit));
    }
}
?>
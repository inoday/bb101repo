<?php
class Application_Form_Categorylist extends Zend_Form
{
	public function __construct($options = null,$page_name = null)
    {
        parent::__construct($options);

        $id = new Zend_Form_Element_Hidden('id');
        $id->addFilter('Int');
		$this->addAttribs(array(
         'onSubmit' => 'return validate(this)', 
     ));
        $cms_page_name = new Zend_Form_Element_Text('category_name',array('style'=>'width:250px; height:20px;'));
        $cms_page_name->setLabel('Category Name:')
               ->setRequired(true)
               ->addFilter('StripTags')
               ->addFilter('StringTrim')
               ->addValidator('NotEmpty')
			   ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:110px;',
			  
        ));
		$a=explode('/',($_SERVER['REQUEST_URI'])); $b=count($a); $c=$a[$b-1];
       	 $sec = new Zend_Form_Element_Hidden('section_id');
        	$sec->setValue($c);
		
		$cms_description = new Zend_Form_Element_Textarea('category_description',array('style'=>'width:600px; height:300px;'));
		$cms_description->setLabel('Description:')
		->addFilter('StringTrim')
		->addDecorator('HtmlTag', array(
		'decorators' => $this->elementDecorators,
		'style'=>'padding-left:110px;',

		));
			
			 
		$submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id', 'submitbutton');
		$submit->setLabel('Submit')
              ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:13px;',
			
        ));
		$cancel = new Zend_Form_Element_Button('cancel');
        $cancel->setAttrib('id', 'submitbutton');
		$cancel->setAttrib('onclick', 'history.go(-1)');
		$cancel->setLabel('Cancel')
               ->addDecorator('HtmlTag', array(
                 'decorators' => $this->elementDecorators,
                 'style'=>'padding-left:13px;',
			
        ));
        $this->addElements(array($id,$cms_page_name,$sec,$cms_description,$submit,$cancel));
		
	
		/////////////for decoration/////////////////////

        $this->setElementDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element','style'=>'width:85%;')),
            array('Label', array('tag' => 'td')),
            array(array('row' => 'HtmlTag'), array('tag' => 'tr','valign'=>'top'))
        ));
		$sec->setDecorators(array('ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element')),
            array(array('emptyrow' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element', 'placement' => 'PREPEND')),
            array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
            ));
      $submit->setDecorators(array('ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element','style'=>'text-align:right;'))
            ));
      
        $cancel->setDecorators(array('ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element'))
            ));
		
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'width'=>'100%', 'cellpadding'=>'3', 'cellspacing'=>'8')),
            'Form'
        ));	
    }
}
?>
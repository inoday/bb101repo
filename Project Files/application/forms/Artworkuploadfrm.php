<?php 
class Application_Form_Artworkuploadfrm extends Zend_Form{
	
public $elementDecorators = array(
array('ViewHelper'),
 array('Label', array( )
	),
array('Errors'),
);
 
    public function init()
    {
$cat_id1=$_GET['segId'];

			$keywords = $this->createElement('text','keywords');
        $keywords->setAttrib('id','keywords')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input');
				 
		  $allgroup	=	new Application_Model_DbTable_ArtCategory();
		    $record		=	$allgroup->getcategoryName();
			for ($c=0;$c<count($record);$c++) { 
			if($c==0) $cat_id=$record[$c]['id'];
			 if($cat_id1>0) $cat_id=$cat_id1;
			$set[$record[$c]['id']]=$record[$c]['cat_name'];
			}
        $category = $this->createElement('select','category');
        $category->setAttrib('id','category')
                ->setOptions(array('style' => 'width:240px;','onchange'=>'changesub()'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set);

			//$category->setValue($set);
		////////////////////////////////////////////////////////////////////////

		  	  		
		  $allsubject	=	new Application_Model_DbTable_ArtCatsubject();
		    $record		=	$allsubject->getcatsubjectName($cat_id);
			for ($c=0;$c<count($record);$c++) { 
			$suset[$record[$c]['id']]=$record[$c]['subject'];
			}
			
        $subject = $this->createElement('select','subject');
        $subject->setAttrib('id','subject')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($suset);
		
             $allmedium	=	new Application_Model_DbTable_ArtCatmedium();
		    $mrecord		=	$allmedium->getcatmediumName($cat_id);
			for ($c=0;$c<count($mrecord);$c++) { 
			$mset[$mrecord[$c]['id']]=$mrecord[$c]['medium'];
			}
			
        $medium = $this->createElement('select','medium');
        $medium->setAttrib('id','medium')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($mset);  
				 
		  $allstyle	=	new Application_Model_DbTable_ArtCatstyle();
		    $srecord		=	$allstyle->getcatstyleName($cat_id);
			for ($c=0;$c<count($srecord);$c++) { 
			$sset[$srecord[$c]['id']]=$srecord[$c]['style'];
			}
			
        $style = $this->createElement('select','style');
        $style->setAttrib('id','style')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($sset);  
		//////////////////////////////////////////////////////////////////////////	
		
		$set3=array('mm'=>'millimeters','ft'=>'feet','in'=>'inches','km'=>'kilometers','m'=>'meters','cm'=>'centimeters')	; 
		$convert = $this->createElement('select','convert');
        $convert->setAttrib('id','convert')
                ->setOptions(array('style' => 'width:240px;','onchange'=>'unitconvert()'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set3);
		
				          
	    $width = $this->createElement('text','width');
        $width->setAttrib('id','width')
                 ->setDecorators($this->elementDecorators)
		->setAttrib('class', 'wd80')
		->setOptions(array('maxlength'=>'6','onkeypress'=>'return isNumberKey(event)'))
                 ->setRequired(true);
				 
		$depth = $this->createElement('text','depth');
        $depth->setAttrib('id','depth')
                 ->setDecorators($this->elementDecorators)
		->setOptions(array('maxlength'=>'6','onkeypress'=>'return isNumberKey(event)'))
				 ->setAttrib('class', 'wd80')
                 ->setRequired(true);
				 
		$height = $this->createElement('text','height');
        $height->setAttrib('id','height')
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'wd80')
		->setOptions(array('maxlength'=>'6','onkeypress'=>'return isNumberKey(event)'))
                 ->setRequired(true);
		
		$created_in = $this->createElement('text','created_in');
        $created_in->setAttrib('id','created_in')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
				 
		$date_completed = $this->createElement('text','date_completed');
        $date_completed->setAttrib('id','date_completed')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
		
		  $title = $this->createElement('text','title');
        $title->setAttrib('id','title')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
		
		$descrption = $this->createElement('textarea','descrption');
        $descrption->setAttrib('id','descrption')
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'wd770')
                 ->setRequired(true);
				 		 
        $presentation = $this->createElement('text','presentation');
        $presentation->setAttrib('id','presentation')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
				 
		$price = $this->createElement('text','price');
        $price->setAttrib('id','price')
                 ->setOptions(array('size' => '45','maxlength'=>'6','onkeypress'=>'return isNumberKey(event)'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
				 ->setOptions(array('style' => 'width:240px;'))
                 ->setRequired(true);
		$reserve_price = $this->createElement('text','reserve_price');
        $reserve_price->setAttrib('id','reserve_price')
                 ->setOptions(array('size' => '45','maxlength'=>'6','onkeypress'=>'return isNumberKey(event)'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
				 ->setOptions(array('style' => 'width:240px;'))
                 ->setRequired(true);
				 
		$buy_it_now = $this->createElement('text','buy_it_now');
        $buy_it_now->setAttrib('id','buy_it_now')
                 ->setOptions(array('size' => '45','maxlength'=>'6','onkeypress'=>'return isNumberKey(event)'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
				 ->setOptions(array('style' => 'margin-top:5px'))
                 ->setRequired(true);	
		
		 $ballgroup	=	new Application_Model_DbTable_ArtCountry();
		    $brecord		=	$ballgroup->getcountryName();
			for ($c=0;$c<count($brecord);$c++) { 
			$bsetc[$brecord[$c]['id']]=$brecord[$c]['country'];
			}
        $location = $this->createElement('select','location');
        $location->setAttrib('id','location')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($bsetc);     
		
		$servicetype=array('Collect in Person'=>'Collect in Person','Royal Mail Recorded Parcel'=>'Royal Mail Recorded Parcel','Parcelforce'=>'Parcelforce','Other parcel service'=>'Other parcel service','Courier'=>'Courier')	; 
		$service = $this->createElement('select','service');
        $service->setAttrib('id','service')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($servicetype);
		
		$postage_cost = $this->createElement('text','postage_cost');
        $postage_cost->setAttrib('id','postage_cost')
                 ->setOptions(array('size' => '45','maxlength'=>'6','onkeypress'=>'return isNumberKey(event)'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
				 ->setOptions(array('style' => 'width:240px;'))
                 ->setRequired(true);
				 
		$servicetype=array('Collect in Person'=>'Collect in Person','Royal Mail Recorded Parcel'=>'Royal Mail Recorded Parcel','Parcelforce'=>'Parcelforce','Other parcel service'=>'Other parcel service','Courier'=>'Courier')	; 
		$service = $this->createElement('select','service');
        $service->setAttrib('id','service')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($servicetype);
				 		 		 	 
		$set5=array('1'=>'1 working day','2'=>'2 working days','3'=>'3 working days','4'=>'4 working days','5'=>'5 working days','10'=>'10 working days','15'=>'15 working days','20'=>'20 working days','30'=>'30 working days'); 
		$dispatch_time = $this->createElement('select','dispatch_time');
        $dispatch_time->setAttrib('id','dispatch_time')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set5);
				 
		$shipping_fixed_price = $this->createElement('text','shipping_fixed_price');
        $shipping_fixed_price->setAttrib('id','shipping_fixed_price')
                 ->setOptions(array('size' => '45'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
				 ->setOptions(array('style' => 'width:240px;'))
                 ->setRequired(true);
		
 		$status = $this->createElement('radio','status');
        $status->setMultiOptions(array(
               '0'      => 'Online Auction',
             ))
			 ->setAttrib('id','status')
			 ->setRequired(true)
			 ->setOptions(array('style' => 'width:20px;height:20px;','onclick'=>'settype(1)','checked'=>'checked'))
			 ->setDecorators($this->elementDecorators);
		$status1 = $this->createElement('radio','status1');
        $status1->setMultiOptions(array(
               '1'    => 'Fixed'
             ))
			 ->setRequired(true)
			 ->setAttrib('id','status1')
			 ->setOptions(array('style' => 'width:20px;height:20px;','onclick'=>'settype(2)'))
			 ->setDecorators($this->elementDecorators);
				 		 
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id','submit')
                 ->setOptions(array('size' => '35'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'sign_up_submit');
		 
                
              //  
$this->addElements(array($keywords,$category,$subject,$medium,$style,$convert,$width,$depth,$height,$created_in,$date_completed,$title,$descrption,$presentation,$submit,$buy_it_now,$reserve_price,$price,$shipping_type,$shipping_fixed_price,$status,$status1,$location,$service,$postage_cost,$dispatch_time));
    }
}
?>

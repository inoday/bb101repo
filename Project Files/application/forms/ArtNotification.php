<?php
class Application_Form_ArtNotification extends Zend_Form
{
	public function __construct($options = null,$page_name = null)
    {
        parent::__construct($options);

        $id = new Zend_Form_Element_Hidden('id');
        $id->addFilter('Int');
/*		$this->addAttribs(array(
         'onSubmit' => 'return validate(this)', 
     ));*/
	  	$all = new Zend_Form_Element_Checkbox('all',array('checked'=>'checked'));
        $all->setLabel('Send to all:')
               ->addFilter('StripTags')
               ->addFilter('StringTrim')
			   ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:110px;',
			  
        ));
		
		
			$receipients = new Zend_Form_Element_Multiselect('receipients');
			$receipients->setLabel('Receipients:');

			
			$allgroup	=	new Application_Model_ArtGroup();
		    $record		=	$allgroup->getMemberName();
			for ($c=0;$c<count($record);$c++) { 
			$receipients->addMultiOption($record[$c]['options'], $record[$c]['receipients']);
			$set[]=$record[$c]['options'];
			}
			$receipients->setValue($set);
		//$arr=array('1'=>'abc','2'=>'vbn');$receipients->addMultiOptions($arr);
		
		
        $subject = new Zend_Form_Element_Text('subject',array('style'=>'width:250px; height:20px;'));
        $subject->setLabel('Subject:')
               ->setRequired(true)
               ->addFilter('StripTags')
               ->addFilter('StringTrim')
               ->addValidator('NotEmpty')
			   ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:110px;',
			  
        ));
		
		$message = new Zend_Form_Element_Textarea('message',array('style'=>'width:600px; height:300px;'));
		$message->setLabel('Message:')
		->setRequired(true)
		->addFilter('StringTrim')
		 ->addValidator('NotEmpty')
		->addDecorator('HtmlTag', array(
		'decorators' => $this->elementDecorators,
		'style'=>'padding-left:110px;',

		));
		
		
		
		$send_date = new Zend_Form_Element_Text('send_date',array('style'=>'width:250px; height:20px;' ,'id'=>'datepicker'));
		$send_date->setLabel('Send Date:')
               ->addFilter('StripTags')
               ->addFilter('StringTrim')
			   ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:110px;',
		));
		
	
			 
		$submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id', 'submitbutton');
		$submit->setLabel('Submit')
              ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:13px;',
			
        ));
		$cancel = new Zend_Form_Element_Button('cancel');
        $cancel->setAttrib('id', 'submitbutton');
		$cancel->setAttrib('onclick', 'history.go(-1)');
		$cancel->setLabel('Cancel')
               ->addDecorator('HtmlTag', array(
                 'decorators' => $this->elementDecorators,
                 'style'=>'padding-left:13px;',
			
        ));
        $this->addElements(array($id,$all,$receipients,$subject,$message,$send_date,$submit,$cancel));
		
	
		/////////////for decoration/////////////////////

        $this->setElementDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element','style'=>'width:85%;')),
            array('Label', array('tag' => 'td')),
            array(array('row' => 'HtmlTag'), array('tag' => 'tr','valign'=>'top'))
        ));
	
	  $all->setDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element','align'=>'left')),
            array('Label', array('tag' => 'td')),
            array(array('row' => 'HtmlTag'), array('tag' => 'tr','valign'=>'top'))
        ));
	
      $submit->setDecorators(array('ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element','style'=>'text-align:right;'))
            ));
      
        $cancel->setDecorators(array('ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element'))
            ));
		
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'width'=>'100%', 'cellpadding'=>'3', 'cellspacing'=>'8')),
            'Form'
        ));	
    }
}
?>
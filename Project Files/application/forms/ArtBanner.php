<?php
class Application_Form_ArtBanner extends Zend_Form
{
		
public $elementDecorators = array(
array('ViewHelper'),
 array('Label', array( )
	),
array('Errors'),
);
 
    public function init()
    {
		$allgroup	=	new Application_Model_DbTable_ArtworkBannerpages();
		    $record		=	$allgroup->getBannerPageName();
			for ($c=0;$c<count($record);$c++) { 
			$set[$record[$c]['id']]=$record[$c]['page_name'];
			}
        $page_id = $this->createElement('select','page_id');
        $page_id->setAttrib('id','page_id')
                ->setOptions(array('style' => 'width:240px;','onchange'=>'changesub()'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set);  
				 
		$set3=array('Right'=>'Right','Bottom'=>'Bottom'); 
		$location = $this->createElement('select','location');
        $location->setAttrib('id','creditcardtype')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set3);
				 
		 
		$location1 = $this->createElement('select','location');
        $location1->setAttrib('id','creditcardtype')
                ->setOptions(array('style' => 'width:240px;','disabled'=>'disabled'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set3);
		
		$set4=array('Image'=>'Image','HTML'=>'HTML')	; 
		$banner_type = $this->createElement('select','banner_type');
        $banner_type->setAttrib('id','banner_type')
                ->setOptions(array('style' => 'width:240px;', 'onchange'=>'return show_btype()'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set4);
				 		 
		$image1 = $this->createElement('file','image1');
        $image1->setAttrib('id','image1')
                 ->setOptions(array('style' => 'width:240px;'))
				 ->setDestination('../public/images/banner');
				 
		$banner_path = $this->createElement('textarea','banner_path');
        $banner_path->setAttrib('id','banner_path')
                 ->setOptions(array('rows' => '4','cols'=>'42'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input');		 
			 
		
	    
              //  
$this->addElements(array($page_id, $location,$location1,$banner_type,$image1,$banner_path));
    }
}
?>
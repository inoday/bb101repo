<?php 
class Application_Form_Artworkcreditpay extends Zend_Form{
	
public $elementDecorators = array(
array('ViewHelper'),
 array('Label', array( )
	),
array('Errors'),
);
 
    public function init()
    {
    	 //$group1 = new Zend_Form_Element_Text('group');
        // $group1->setMultiOptions(array('artist' => 'Artist'));
                 
        /* $group2 = new Zend_Form_Element_Radio('group');
         $group2->setMultiOptions(array('Bbuyer' => 'Buyer'));
          
         $group3 = new Zend_Form_Element_Radio('group');
         $group3->setMultiOptions(array('dealer' => 'Dealer'));
          */

				 
				 
		$set3=array('Visa'=>'Visa','MasterCard'=>'MasterCard','Discover'=>'Discover','Amex'=>'Amex')	; 
		$creditcardtype = $this->createElement('select','creditcardtype');
        $creditcardtype->setAttrib('id','creditcardtype')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set3);
				 
		$creditcardnumber = $this->createElement('text','creditcardnumber');
        $creditcardnumber->setAttrib('id','creditcardnumber')
                 ->setOptions(array('size' => '35','maxlength'=>'20','onkeypress'=>'return isNumberKey(event)'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input');
				 
		for ($c=1;$c<13;$c++) { 
			$set1[$c]=$c;
			}	
		$exp_date_month = $this->createElement('select','exp_date_month');
        $exp_date_month->setAttrib('id','exp_date_month')
                  ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set1);
		for ($y=date('Y');$y<date('Y')+10;$y++) { 
			$set2[$y]=$y;
			}		 
		$exp_date_year = $this->createElement('select','exp_date_year');
        $exp_date_year->setAttrib('id','exp_date_year')
                  ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($set2);
		
      	$cvv_number = $this->createElement('text','cvv_number');
        $cvv_number->setAttrib('id','cvv_number')
                 ->setOptions(array('size' => '35','maxlength'=>'4','onkeypress'=>'return isNumberKey(event)'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
     	
		 $address1 = $this->createElement('text','address1');
        $address1->setAttrib('id','address1')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
         $address2 = $this->createElement('text','address2');
        $address2->setAttrib('id','address2')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);     
		 $city = $this->createElement('text','city');
        $city->setAttrib('id','city')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);     
		 $state = $this->createElement('text','state');
        $state->setAttrib('id','state')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);     
	 $zip = $this->createElement('text','zip');
        $zip->setAttrib('id','zip')
                 ->setOptions(array('size' => '35','maxlength'=>'6','onkeypress'=>'return isNumberKey(event)'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);     
	 $allgroup	=	new Application_Model_DbTable_ArtCountry();
		    $record		=	$allgroup->getcountryName();
			for ($c=0;$c<count($record);$c++) { 
			$setc[$record[$c]['id']]=$record[$c]['code'];
			}
        $country = $this->createElement('select','country');
        $country->setAttrib('id','country')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($setc); 
		 $saddress1 = $this->createElement('text','saddress1');
        $saddress1->setAttrib('id','saddress1')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
         $saddress2 = $this->createElement('text','saddress2');
        $saddress2->setAttrib('id','saddress2')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);     
		 $scity = $this->createElement('text','scity');
        $scity->setAttrib('id','scity')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);     
		 $sstate = $this->createElement('text','sstate');
        $sstate->setAttrib('id','sstate')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);     
	 $szip = $this->createElement('text','szip');
        $szip->setAttrib('id','szip')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);     
	 $allgroup	=	new Application_Model_DbTable_ArtCountry();
		    $records		=	$allgroup->getcountryName();
			for ($c=0;$c<count($records);$c++) { 
			$setcs[$records[$c]['id']]=$records[$c]['code'];
			}
        $scountry = $this->createElement('select','scountry');
        $scountry->setAttrib('id','scountry')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true)
				 ->setMultiOptions($setcs);         
              //  
$this->addElements(array($creditcardtype,$creditcardnumber, $exp_date_month,$exp_date_year,$cvv_number,$address1,$address2, $city,$state,$zip,$country,$saddress1,$saddress2, $scity,$sstate,$szip,$scountry));
    }
}
?>

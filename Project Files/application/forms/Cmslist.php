<?php
class Application_Form_Cmslist extends Zend_Form
{
	public function __construct($options = null,$page_name = null)
    {
        parent::__construct($options);

        $id = new Zend_Form_Element_Hidden('id');
        $id->addFilter('Int');
		
        $cms_page_name = new Zend_Form_Element_Text('cms_page_name',array('style'=>'width:250px; height:20px;'));
        $cms_page_name->setLabel('CmsPageName:')
               ->setRequired(true)
               ->addFilter('StripTags')
               ->addFilter('StringTrim')
               ->addValidator('NotEmpty')
			   ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:110px;',
			  
        ));

        $cms_heading = new Zend_Form_Element_Textarea('cms_heading',array('style'=>'width:250px; height:20px;'));
        $cms_heading->setLabel('CmsHeading:')
              ->setRequired(true)
              ->addFilter('StringTrim')
              ->addValidator('NotEmpty')
			  ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:110px;',	
        ));
		
		$cms_description = new Zend_Form_Element_Textarea('cms_description',array('style'=>'width:400px; height:300px;'));
		$cms_description->setLabel('CmsDescription:')
		->setRequired(true)
		->addFilter('StringTrim')
		->addValidator('NotEmpty')
		->addDecorator('HtmlTag', array(
		'decorators' => $this->elementDecorators,
		'style'=>'padding-left:110px;',

		));
			
			 
		$submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id', 'submitbutton');
		$submit->setLabel('Submit')
              ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:13px;',
			
        ));
        $this->addElements(array($id,$cms_page_name,$cms_heading,$cms_description,$submit));
		
				
		/////////////for decoration/////////////////////
		
        $this->setElementDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element')),
            array('Label', array('tag' => 'td')),
            array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
        ));
        $submit->setDecorators(array('ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element')),
            array(array('emptyrow' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element', 'placement' => 'PREPEND')),
            array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
            ));
        
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table')),
            'Form'
        ));	
    }
}
?>
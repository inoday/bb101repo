<?php
class Zend_View_Helper_Banner extends Zend_View_Helper_Abstract
{
	/*
	 @ Fetch image of artwork from art_artworks_images table to show in banner
	 @ Add by : Abhishek
	 @ On: 15-02-2012
	**/
	
	public function banner() {
		
		$bannerlist = new Application_Model_Banner();
		
		$bannerlist_result = $bannerlist->bannerimage();	
		 
		return $bannerlist_result;
	}
	
	
}
?>
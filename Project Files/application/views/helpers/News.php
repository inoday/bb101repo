<?php
class Zend_View_Helper_News extends Zend_View_Helper_Abstract
{
	/*
	 @ Fetch news of artwork from art_news table to show
	 @ Add by : Abhishek
	 @ On: 24-02-2012
	**/
	
	public function news() {
		
		$newslist = new Application_Model_News();
		
		$newslist_result = $newslist->news();	
		 
		return $newslist_result;
	}
	
	
}
?>
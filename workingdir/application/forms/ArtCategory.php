<?php
class Application_Form_ArtCategory extends Zend_Form
{
	public function __construct($options = null,$page_name = null)
    {
        parent::__construct($options);

        $id = new Zend_Form_Element_Hidden('id');
        $id->addFilter('Int');
/*		$this->addAttribs(array(
         'onSubmit' => 'return validate(this)', 
     ));*/
        $cat_name = new Zend_Form_Element_Text('cat_name',array('style'=>'width:250px; height:20px;'));
        $cat_name->setLabel('Category Name:')
               ->setRequired(true)
               ->addFilter('StripTags')
               ->addFilter('StringTrim')
               ->addValidator('NotEmpty')
			   ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:110px;',
			  
        ));
		
		/*$cat_lft = new Zend_Form_Element_Text('lft',array('style'=>'width:250px; height:20px;'));
		$cat_lft->setLabel('LFT:')
               ->addFilter('StripTags')
               ->addFilter('StringTrim')
			   ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:110px;',
		));
		
		$cat_rgt = new Zend_Form_Element_Text('rgt',array('style'=>'width:250px; height:20px;'));
		$cat_rgt->setLabel('RGT:')
               ->addFilter('StripTags')
               ->addFilter('StringTrim')
			   ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:110px;',
		));*/
			
			 
		$submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id', 'submitbutton');
		$submit->setLabel('Submit')
              ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:13px;',
			
        ));
		$cancel = new Zend_Form_Element_Button('cancel');
        $cancel->setAttrib('id', 'submitbutton');
		$cancel->setAttrib('onclick', 'history.go(-1)');
		$cancel->setLabel('Cancel')
               ->addDecorator('HtmlTag', array(
                 'decorators' => $this->elementDecorators,
                 'style'=>'padding-left:13px;',
			
        ));
        $this->addElements(array($id,$cat_name,$submit,$cancel));
		
	
		/////////////for decoration/////////////////////

        $this->setElementDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element','style'=>'width:85%;')),
            array('Label', array('tag' => 'td')),
            array(array('row' => 'HtmlTag'), array('tag' => 'tr','valign'=>'top'))
        ));
	
      $submit->setDecorators(array('ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element','style'=>'text-align:right;'))
            ));
      
        $cancel->setDecorators(array('ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element'))
            ));
		
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'width'=>'100%', 'cellpadding'=>'3', 'cellspacing'=>'8')),
            'Form'
        ));	
    }
}
?>
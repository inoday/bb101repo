
          <?php
class Application_Form_Login extends Zend_Form
{   
public $formDecorators = array(
array('FormElements'),
    array('Form'),
);
public $elementDecorators = array(
array('ViewHelper'),
 array('Label', array( 'style' => 'display: block;font-size: 16px;font-weight: bold;
    margin-bottom: 5px;')
	),
array('Errors'),
);
public $buttonDecorators = array(
array('ViewHelper'),
array('HtmlTag', array('tag' => 'div'))
);
    public function init()
    {
	
        $username = $this->createElement('text','email');
        $username->setLabel('Email:')
		         ->setAttrib('id','suName')
                 ->setOptions(array('size' => '35'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'input')
                 ->setRequired(true);
				 
        $password = $this->createElement('password','password');
        $password->setLabel('Password:')
		         ->setAttrib('id','pwd')
                 ->setOptions(array('size' => '35'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'input')
                 ->setRequired(true);
				
		// create checkbox for newsletter subscription
      

        $signin = $this->createElement('submit','Login');
        $signin->setLabel('Login')
				 ->setAttrib('style', 'background-color: #555555;
    border: 2px solid #CCCCCC;
    border-radius: 10px 10px 10px 10px;
    color: #FFFFFF;
    cursor: pointer;
    display: block;
    font-weight: bold;
    margin: 10px auto 0;
    padding: 5px;
    width: 100px;')
                ->setIgnore(true);

        $this->addElements(array($username,$password,$signin));
    }
}
?>                                    
                             
                                    
                               

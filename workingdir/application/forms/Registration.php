<?php 
class Application_Form_Registration extends Zend_Form{
	
public $elementDecorators = array(
array('ViewHelper'),
 array('Label', array( )
	),
array('Errors'),
);
 
    public function init()
    {
    	 
	    $firstname = $this->createElement('text','firstname');
        $firstname->setAttrib('id','name')
                ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators)
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
				 
        $lastname = $this->createElement('text','lastname');
        $lastname->setAttrib('id','lastname')
                 ->setOptions(array('size' => '45'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
				 ->setOptions(array('style' => 'width:240px;'))
                 ->setRequired(true);
		
		$house_no = $this->createElement('text','house_no');
        $house_no->setAttrib('id','house_no')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input');
		
		$street = $this->createElement('text','street');
        $street->setAttrib('id','street')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input');
		
		$city = $this->createElement('text','city');
        $city->setAttrib('id','city')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
		
		
		$zip = $this->createElement('text','zip');
        $zip->setAttrib('id','zip')
                 ->setOptions(array('size' => '35','maxlength'=>'6','onkeypress'=>'return isNumberKey(event)'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
		
		
      	$scoutcode = $this->createElement('text','scoutcode');
        $scoutcode->setAttrib('id','scoutcode')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
     	$email = $this->createElement('text','email');
        $email->setAttrib('id','email')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('onblur' => 'checkEmail(this.value);'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
     	$reemail = $this->createElement('text','reemail');
        $reemail->setAttrib('id','reemail')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
   		$userid = $this->createElement('text','userid');
        $userid->setAttrib('id','userid')
                 ->setOptions(array('size' => '35'))
				 ->setOptions(array('onblur' => 'checkUserId(this.value);'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
                
                 
        $password = $this->createElement('password','password');
        $password->setAttrib('id','password')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;', 'onkeyup'=>'passwordStrength(this.value)'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
                 
        $repassword = $this->createElement('password','repassword');
        $repassword->setAttrib('id','repassword')
                 ->setOptions(array('size' => '35'))
                 ->setOptions(array('style' => 'width:240px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);

        $captcha = $this->addElement('captcha', 'captcha', array(
            'label'      => 'Please enter the 5 letters displayed below:',
            'required'   => true,
            'captcha'    => array(
                'captcha' => 'Figlet',
                'wordLen' => 5,
                'timeout' => 300
            )
        ));
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id','submit')
                 ->setOptions(array('size' => '35'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'sign_up_submit')
                 ->setRequired(true);
		 
                
                //$userid,
        $this->addElements(array($firstname,$lastname,$house_no,$street,$city,$zip,$scoutcode,$email,$reemail,$userid,$password,$repassword,$submit));
    }
}
?>

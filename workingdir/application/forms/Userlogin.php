<?php
class Application_Form_Userlogin extends Zend_Form
{

public $elementDecorators = array(
array('ViewHelper'),
 array('Label', array( 'style' => 'display: block;font-size: 16px;font-weight: bold;
    margin-bottom: 5px;')
	),
array('Errors'),
);

 public function init()
    {
    	     
   		$loginemail = $this->createElement('text','loginemail');
        $loginemail->setAttrib('id','loginemail')
                 ->setOptions(array('size' => '25'))
                 ->setOptions(array('style' => 'width:168px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
                
                 
        $pwd = $this->createElement('password','pwd');
        $pwd->setAttrib('id','pwd')
                 ->setOptions(array('size' => '25'))
                 ->setOptions(array('style' => 'width:168px;'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
                 
         
         
        $login = new Zend_Form_Element_Submit('login');
        $login->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'sign_up_submit')
                 ->setIgnore(true);     
                
        $this->addElements(array($loginemail,$pwd,$login));
    }
   
}
?>
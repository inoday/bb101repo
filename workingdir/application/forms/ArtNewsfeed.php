<?php
class Application_Form_ArtNewsfeed extends Zend_Form
{
	public function __construct($options = null,$page_name = null)
    {
        parent::__construct($options);

        $id = new Zend_Form_Element_Hidden('id');
        $id->addFilter('Int');
	/*	$this->addAttribs(array(
         'onSubmit' => 'return validate(this)', 
     ));*/
        $title = new Zend_Form_Element_Text('title',array('style'=>'width:250px; height:20px;'));
        $title->setLabel('Title:')
               ->setRequired(true)
               ->addFilter('StripTags')
               ->addFilter('StringTrim')
               ->addValidator('NotEmpty')
			   ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:110px;',
			  
        ));
		
		$url = new Zend_Form_Element_Text('url',array('style'=>'width:250px; height:20px;'));
        $url->setLabel('URL:')
               ->setRequired(true)
               ->addFilter('StripTags')
               ->addFilter('StringTrim')
               ->addValidator('NotEmpty')
			   ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:110px;',
			  
        ));
		
	 
		$submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id', 'submit_button');
		$submit->setLabel('Submit')
              ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:13px;',
			
        ));
		$cancel = new Zend_Form_Element_Button('cancel');
        $cancel->setAttrib('id', 'submit_button');
		$cancel->setAttrib('onclick', 'history.go(-1)');
		$cancel->setLabel('Cancel')
               ->addDecorator('HtmlTag', array(
                 'decorators' => $this->elementDecorators,
                 'style'=>'padding-left:13px;',
			
        ));
        $this->addElements(array($id,$title,$url,$submit,$cancel));
		
	
		/////////////for decoration/////////////////////

        $this->setElementDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element','style'=>'width:85%;')),
            array('Label', array('tag' => 'td')),
            array(array('row' => 'HtmlTag'), array('tag' => 'tr','valign'=>'top'))
        ));
	
      $submit->setDecorators(array('ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element','style'=>'text-align:right;'))
            ));
      
        $cancel->setDecorators(array('ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element'))
            ));
		
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'width'=>'100%', 'cellpadding'=>'3', 'cellspacing'=>'8')),
            'Form'
        ));	
    }
}
?>
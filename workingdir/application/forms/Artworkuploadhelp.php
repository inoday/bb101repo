<?php 
class Application_Form_Artworkuploadhelp extends Zend_Form{
	
public $elementDecorators = array(
array('ViewHelper'),
 array('Label', array( )
	),
array('Errors'),
);
 
    public function init()
    {
    	 //$group1 = new Zend_Form_Element_Text('group');
        // $group1->setMultiOptions(array('artist' => 'Artist'));
                 
        /* $group2 = new Zend_Form_Element_Radio('group');
         $group2->setMultiOptions(array('Bbuyer' => 'Buyer'));
          
         $group3 = new Zend_Form_Element_Radio('group');
         $group3->setMultiOptions(array('dealer' => 'Dealer'));
          */
		  
		

		$artwork_tag = $this->createElement('textarea','artwork_tag');
        $artwork_tag->setAttrib('id','artwork_tag')
                 ->setOptions(array('rows' => '4','cols'=>'32'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
				 
		$size = $this->createElement('textarea','size');
        $size->setAttrib('id','size')
                 ->setOptions(array('rows' => '4','cols'=>'32'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);
          
		$converter = $this->createElement('textarea','converter');
        $converter->setAttrib('id','converter')
                 ->setOptions(array('rows' => '4','cols'=>'32'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true); 
          
		$describe_artwork = $this->createElement('textarea','describe_artwork');
        $describe_artwork->setAttrib('id','describe_artwork')
                 ->setOptions(array('rows' => '4','cols'=>'32'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true); 
          
		$presentation = $this->createElement('textarea','presentation');
        $presentation->setAttrib('id','presentation')
                 ->setOptions(array('rows' => '4','cols'=>'32'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true); 
          
		$upload_photo = $this->createElement('textarea','upload_photo');
        $upload_photo->setAttrib('id','upload_photo')
                ->setOptions(array('rows' => '4','cols'=>'32'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);  
          
		$reserve_price = $this->createElement('textarea','reserve_price');
        $reserve_price->setAttrib('id','reserve_price')
                ->setOptions(array('rows' => '4','cols'=>'32'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);   
          
		$date_completed = $this->createElement('textarea','date_completed');
        $date_completed->setAttrib('id','date_completed')
                ->setOptions(array('rows' => '4','cols'=>'32'))
                 ->setDecorators($this->elementDecorators) 
				 ->setAttrib('class', 'signup_input')
                 ->setRequired(true);      
              //  
$this->addElements(array($artwork_tag,$size,$converter,$describe_artwork,$presentation,$upload_photo,$reserve_price,$date_completed));
    }
}
?>
<?php
class Application_Form_Admin extends Zend_Form
{
    public $elementDecorators = array(
        'ViewHelper',
         array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
         array('Label', array('tag' => 'td','style'=>'font-size:11px; font-weight:bold')),
         array(array('row' => 'HtmlTag'), array('tag' => 'tr', 'placement' => 'prepend')),
    );
	
	

	public $buttonDecorators = array(
       'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
	
    public function init()
    {
        $this->setName('user');

        $id = new Zend_Form_Element_Hidden('id');
        $id->addFilter('Int');
		$this->addElement('text', 'first_name',array(
            'decorators' => $this->elementDecorators,
            'label'       => 'FirstName:',
			'style'=>'width:200px;',
			'required' => true,
        ));
		
        $this->addElement('text', 'last_name', array(
            'decorators' => $this->elementDecorators,
            'label'       => 'Last Name:',
			'style'=>'width:200px;',
			'required' => true,
            'validators' => array(
                    'stringLength')
			
        ));
		
        
		
		$this->addElement('text', 'username', array(
		    'decorators' => $this->elementDecorators,
            'filters'    => array('StringTrim','StringToLower'),
            'validators' => array('Alnum',array('StringLength', false, array(3, 20)),),
            'required'   => true,
            'label'      => 'Username:',  
                
            ));
			
					
		$this->addElement('password', 'password', array(
		    
		    'decorators' => $this->elementDecorators,
            'filters'    => array('StringTrim'),
           
			'validators' => array('PasswordVerification' => array('StringLength', true, array(6, 20))),
            'required'   => true,
            'label'      => 'Password:',
        ));
		
		
		$this->addElement('text', 'email', array(
            'decorators' => $this->elementDecorators,
            'label'       => 'Email Address:',
			'style'=>'width:200px;',
        ));
		
		 $this->addElement('text', 'twitter_href', array(
            'decorators' => $this->elementDecorators,
            'label'       => 'Twitter Link:',
			'style'=>'width:200px;',
			
        ));
        
		$submit = $this->addElement('submit', 'save', array(
            'decorators' => $this->buttonDecorators,
            'label'       => 'Save',
			'id'       => 'submitbutton',
        ),array($id));
		
		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'dl', 'class' => 'zend_form')),
            array('Description', array('placement' => 'prepend')),
            'Form'
        ));

		
		$this->addElements(array($id, $captcha,$submit));
		
    }

    public function loadDefaultDecorators()
    {
        $this->setDecorators(array(
            'FormElements',
              array('HtmlTag', array('tag' => 'table')),
			'Form',
        ));
    }
}
?>
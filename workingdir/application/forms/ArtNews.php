<?php
class Application_Form_ArtNews extends Zend_Form
{
	public function __construct($options = null,$page_name = null)
    {
        parent::__construct($options);

        $id = new Zend_Form_Element_Hidden('id');
        $id->addFilter('Int');
	/*	$this->addAttribs(array(
         'onSubmit' => 'return validate(this)', 
     ));*/
        $title = new Zend_Form_Element_Text('title',array('style'=>'width:250px; height:20px;'));
        $title->setLabel('Title:')
               ->setRequired(true)
               ->addFilter('StripTags')
               ->addFilter('StringTrim')
               ->addValidator('NotEmpty')
			   ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:110px;',
			  
        ));
		
		$description = new Zend_Form_Element_Textarea('description',array('style'=>'width:600px; height:300px;'));
		$description->setLabel('Description:')
		->setRequired(true)
		->addFilter('StringTrim')
		 ->addValidator('NotEmpty')
		->addDecorator('HtmlTag', array(
		'decorators' => $this->elementDecorators,
		'style'=>'padding-left:110px;',

		));
		
		$display_start_date = new Zend_Form_Element_Text('display_start_date',array('style'=>'width:250px; height:20px;'));
		$display_start_date->setLabel('Display Start Date:')
               ->addFilter('StripTags')
               ->addFilter('StringTrim')
			   ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:110px;',
		));
		
		$display_end_date = new Zend_Form_Element_Text('display_end_date',array('style'=>'width:250px; height:20px;'));
		$display_end_date->setLabel('Display End Date:')
               ->addFilter('StripTags')
               ->addFilter('StringTrim')
			   ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:110px;',
		));
			
			 
		$submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id', 'submitbutton');
		$submit->setLabel('Submit')
              ->addDecorator('HtmlTag', array(
              'decorators' => $this->elementDecorators,
              'style'=>'padding-left:13px;',
			
        ));
		$cancel = new Zend_Form_Element_Button('cancel');
        $cancel->setAttrib('id', 'submitbutton');
		$cancel->setAttrib('onclick', 'history.go(-1)');
		$cancel->setLabel('Cancel')
               ->addDecorator('HtmlTag', array(
                 'decorators' => $this->elementDecorators,
                 'style'=>'padding-left:13px;',
			
        ));
        $this->addElements(array($id,$title,$description,$display_start_date,$display_end_date,$submit,$cancel));
		
	
		/////////////for decoration/////////////////////

        $this->setElementDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element','style'=>'width:85%;')),
            array('Label', array('tag' => 'td')),
            array(array('row' => 'HtmlTag'), array('tag' => 'tr','valign'=>'top'))
        ));
	
      $submit->setDecorators(array('ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element','style'=>'text-align:right;'))
            ));
      
        $cancel->setDecorators(array('ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' =>'td', 'class'=> 'element'))
            ));
		
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'width'=>'100%', 'cellpadding'=>'3', 'cellspacing'=>'8')),
            'Form'
        ));	
    }
}
?>
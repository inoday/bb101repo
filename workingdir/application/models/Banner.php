<?php
class Application_Model_Banner  extends Zend_Db_Table_Abstract
{
	protected $_name = 'art_artworks_images';
	protected $_dbTable;
			
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
		
        $this->_dbTable = $dbTable;
		
        return $this;
    }
	 
	public function getDbTable()
	{
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_Banner');
		}
		return $this->_dbTable;
	}
	
	public function bannerimage() {
	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt = $db->query('SELECT aw.id, aw.title, aw.price, aw.descrption, awim.image, aw.owner_id, u.first_name, u.last_name , aw.rate_count,DATEDIFF(DATE(NOW()),bid_start_date) as count,TIMEDIFF(ADDDATE(bid_start_date, INTERVAL auction_duration DAY),NOW()-INTERVAL 0 MINUTE) as ending
							FROM art_artworks aw 
							INNER JOIN art_artworks_images awim ON awim.artwork_id = aw.id 
							INNER JOIN art_user u ON u.id = aw.owner_id 
							WHERE aw.approval_status = 1 AND aw.payment_status = 1 
							AND  ADDDATE(bid_start_date, INTERVAL advertising_duration DAY) > NOW() 
							AND NOW()>=aw.bid_start_date group by aw.id');
        $result = $stmt->fetchAll();
        
		
		return $result;
	}
	

}
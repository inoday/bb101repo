<?php
class Application_Model_ArtMailFormats extends Zend_Db_Table_Abstract
{
	protected $_dbTable;
	
	public function setDbTable($dbTable)
    {
        
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_ArtMailFormats');
        }
        return $this->_dbTable;
    }
	

	public function getall()
    {
		$db = $this->getDbTable();
		$select = $db->select();
		$result =  $this->getDbTable()->fetchAll($select);
		$res	=	$result->toArray();
		return $result;
    }

	public function getMailContentById($id)
    {
		$db = $this->getDbTable();
		$select = $db->select()->where('id = ?', $id);
		$result =  $this->getDbTable()->fetchAll($select);
		$res	=	$result->toArray();
		return $res[0];
    }

	public function updateContent($id,$editor1,$subject)
    {  
	 
		$db = $this->getDbTable();
		$data = array(
			'content'      => $editor1,
			'subject'=>$subject
		);
		$where	=	$db->getAdapter()->quoteInto('id= ?', $id);  
		return $this->getDbTable()->update($data, $where);
    }
	

	
}


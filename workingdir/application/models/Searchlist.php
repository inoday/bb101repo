<?php 
class Application_Model_Searchlist {
	  
	public function setDbTable($dbTable){
		
		if (is_string($dbTable)){
			
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
    public function getDbTable(){
    	
		if (null === $this->_dbTable){
            $this->setDbTable('Application_Model_DbTable_Searchlist');
        }
        return $this->_dbTable;
    }

	public function getTopsearch($searchString){
		 
		$breakStr = explode(',', $searchString);
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		
		$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
	    
		if (in_array("All",$breakStr))
	  	{
	  		$makeQue = "SELECT id FROM art_artwork_categories";
	  		$stmt = $db->query($makeQue);
        	$rows = $stmt->fetchAll(); 
        	 
			$cat_id = array();
			
        	if (count($rows)>0) {
        		for($i=0;$i<count($rows);$i++){
					$cat_id[$i] = $rows[$i]['id'];
				}
				
			   	return $cat_id;
        	}
	  	}
		else
	 	{
	  		$implode = implode("','",$breakStr);
		    $makeQue = "SELECT id FROM art_artwork_categories WHERE cat_name IN('".$implode."')"; 
		    $stmt = $db->query($makeQue);
        	$rows = $stmt->fetchAll(); 
        	 
			$cat_id = array();
			
        	if (count($rows)>0) {
        		for($i=0;$i<count($rows);$i++){
					$cat_id[$i] = $rows[$i]['id'];
				}		
			   	return $cat_id;
        	}else {
        		 
			    $makeQue = 'SELECT category FROM art_artworks 
			    			WHERE approval_status = 1 AND 
							ADDDATE(bid_start_date, INTERVAL auction_duration DAY) > NOW() 
			    			AND (title like "%'.$searchString.'%" OR keywords like "%'.$searchString.'%")';
			    $stmt = $db->query($makeQue);
	        	$rows = $stmt->fetchAll(); 
	        	
        		for($i=0;$i<count($rows);$i++){
					$cat_id[$i] = $rows[$i]['category'];
				}	
				 	
			   	return $cat_id;
        	}
	  	}
	}

	public function getProduct($catId,$subcatID,$sortBy,$searchStr){ 
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		/*********** Find bid duration date ********************/
		$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
	    /*********** END Find bid duration date ********************/
	    
		$makeQue = 'SELECT aw.id, aw.title, aw.price, aw.owner_id,aw.buy_it_now, awim.image, u.first_name, u.last_name, DATEDIFF(DATE(NOW()),aw.bid_start_date) as cout,TIMEDIFF(ADDDATE(bid_start_date, INTERVAL auction_duration DAY),NOW()-INTERVAL 0 MINUTE) as ending, aw.date_completed, aw.rate_count, aw.medium
					FROM art_artworks aw 
					INNER JOIN art_artworks_images awim ON awim.artwork_id = aw.id 
					INNER JOIN art_user u ON u.id = aw.owner_id 
					WHERE  ';
		
		$where = array();
		
		$where[] = ' aw.payment_status = 1  AND aw.delete = 0';
		if ($subcatID!='') {
			$where[] = " aw.subject = ".$subcatID;
		}else{
			$where[] = count($catId) > 0  ? " aw.category IN('".implode("','", $catId)."') ":' (title like "%'.$searchStr.'%" OR keywords like "%'.$searchStr.'%") ';
		}
		 
		$where[] = ' ADDDATE(bid_start_date, INTERVAL auction_duration DAY) > NOW() ' ; 	
		$where[] = ' NOW()>=aw.bid_start_date'; 				 
		switch ($sortBy)
		{
			case 'Ending Soonest':
			  $orderBy = ' GROUP BY aw.id ORDER BY aw.bid_start_date ASC';
			  break;
			case 'Recently Added':
			  $orderBy = ' GROUP BY aw.id ORDER BY aw.bid_start_date DESC';
			  break;
			case 'Top Rated':
			  $orderBy = ' GROUP BY aw.id ORDER BY aw.rate_count DESC';
			  break;
			case 'Top Artists':
			  $orderBy = ' GROUP BY aw.id ORDER BY aw.create_date ASC';
			  break;
			default:
			  $orderBy = ' GROUP BY aw.id ORDER BY aw.create_date ASC';
		}
				
	 	$query = $makeQue. implode(" AND ", $where).$orderBy;
		 //echo $query;						 
		$stmt = $db->query($query);
        $row = $stmt->fetchAll();
        
        return $row;
	}
	
	public function getAjaxProduct($searchStr, $subcatID, $keyword, $sortBy = 'Ending Soonest',$Heightfrom,$Heightto,$Widthfrom,$Widthto,$Depthfrom,$Depthto,$location,$med_ID,$sty_ID){
		/*********** Find bid duration date ********************/
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
	    /*********** END Find bid duration date ********************/
		$pos = strpos($searchStr, 'All');
		$keyword = isset($keyword) ? $keyword : '';
		$makeQue = 'SELECT aw.id, aw.title, aw.price, awim.image, u.id userid, aw.owner_id,aw.buy_it_now,u.first_name, u.last_name, aw.rate_count, DATEDIFF(DATE(NOW()),aw.bid_start_date) as cout,shipping_type,TIMEDIFF(ADDDATE(bid_start_date, INTERVAL auction_duration DAY),NOW()-INTERVAL 0 MINUTE) as ending, aw.date_completed, aw.rate_count, aw.medium
					FROM art_artworks aw 
					INNER JOIN art_artworks_images awim ON awim.artwork_id = aw.id 
					INNER JOIN art_user u ON u.id = aw.owner_id 
					LEFT JOIN art_user_details ud ON u.id = ud.user_id
					WHERE ';
		
		$where = array();
		
		$where[] = ' aw.payment_status = 1  AND aw.delete = 0';
		$where[] = ' ADDDATE(bid_start_date, INTERVAL auction_duration DAY) > NOW() ' ; 	
		$where[] = ' NOW()>=aw.bid_start_date'; 	
		
		if ($subcatID!='' && ($med_ID=='' && $sty_ID =='')) {
				$where[] = " aw.subject = ".$subcatID;
		}
		else if($pos === false && $searchStr != '') {
			$catIds = explode(",", $searchStr);
			$catIds = array_diff($catIds, array(''));
		
	   		$where[] = count($catIds) > 0  ? " aw.category IN('".implode("','", $catIds)."') ":''; 
		}
		if( $med_ID !='' ) {
			$where[] = count($med_ID) > 0  ? " aw.medium IN(".$med_ID.") ":''; 
		}
		if( $sty_ID !='' ) {
			$where[] = count($sty_ID) > 0  ? " aw.style IN(".$sty_ID.") ":''; 
		}
		if( $keyword !='' ) {
	   		 $where[] = ' aw.keywords LIKE "%'.$keyword.'%" '; 
		}
		////////////////////////////////////////////////////////////////////////////////////
		if( $location !='' ) {
	   		 $where[] = ' ud.city LIKE "%'.$location.'%" '; 
		}
		if( $Heightfrom!='' && $Heightto!='') {
	   		 $orwhere[] = ' aw.height BETWEEN '.$Heightfrom.' AND '.$Heightto.''; 
		}
		if( $Widthfrom!='' && $Widthto!='') {
	   		 $orwhere[] = ' aw.width BETWEEN '.$Widthfrom.' AND '.$Widthto.''; 
		}
		if( $Depthfrom!='' && $Depthto!='') {
	   		 $orwhere[] = ' aw.depth BETWEEN '.$Depthfrom.' AND '.$Depthto.''; 
		}
		//////////////////////////////////////////////////////////////////////////////////// 		
		switch ($sortBy)
		{
			case 'Ending Soonest':
			  $orderBy = ' GROUP BY aw.id ORDER BY aw.bid_start_date ASC';
			  break;
			case 'Recently Added':
			  $orderBy = ' GROUP BY aw.id ORDER BY aw.bid_start_date DESC';
			  break;
			case 'Top Rated':
			  $orderBy = ' GROUP BY aw.id ORDER BY aw.rate_count DESC';
			  break;
			case 'Top Artists':
			  $orderBy = ' GROUP BY aw.id ORDER BY aw.create_date ASC';
			  break;
			default:
			  $orderBy = ' GROUP BY aw.id ORDER BY aw.create_date ASC';
		}
			if(	count($orwhere)>0)
		{ $query = $makeQue. implode(" AND ", $where)." AND " .implode(" OR ", $orwhere).$orderBy; }
		else
		{  $query = $makeQue. implode(" AND ", $where).$orderBy; }
		//echo $query;				
		$stmt = $db->query($query);
        $row = $stmt->fetchAll(); 
		 
        return $row;
	}
}
?>
<?php
 
 
class Application_Model_DbTable_Artworkupload extends Zend_Db_Table_Abstract
{ 
	protected $_name = 'art_artworks';
    protected $_dbTable;
	
 	public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Artworkuploadimages');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
	    /**
   @	Added By : Reeta verma
   @	Added On :	16-02-2012	
   @	Input: void
   @	Return: reture the value from thr table.
   @
   **/	
    public function getArtworkupload($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Count not find row $id");
        }
		//echo'<pre>';print_r( $row);die;
        return $row->toArray();    
    }

    ####---------------INSERT Data in  Table----------------####
    
	public function addartwork($dbField,$image)
    {   $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $dbField;
		//$db = $this->getDbTable();	
	
		$artwork_id = $this->insert($dbField);

		$db = $this->getDbTable();
		for($i=0;$i<count($image);$i++)
		{
		if($image[$i]!='')
		{$merg_arr=array('artwork_id'=>$artwork_id, 'image'=>$image[$i],'create_date'=>date("Y-m-d H:i:s")); //print_r($merg_arr); die();
		$db->insert($merg_arr);}
		}
		
    }
    
    ####-------------------------------END---------------------------------------####
	    ####---------------UPDATE Data in  Table----------------####
    
	public function updateartwork($id,$dbField,$image)
    {   $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $dbField;
		//$db = $this->getDbTable();	
	
		$artwork_id = $this->update($dbField,'id = '. (int)$id);

		$db = $this->getDbTable();
		for($i=0;$i<count($image);$i++)
		{
		if($image[$i]!='')
		{$merg_arr=array('artwork_id'=>$id, 'image'=>$image[$i],'create_date'=>date("Y-m-d H:i:s")); //print_r($merg_arr); die();
		$db->insert($merg_arr);}
		}
		
    }
    
    ####-------------------------------END---------------------------------------####
####---------------Update Data in art_artworks Table----------------####
public function updateartworkpaystatus($id,$status)
    {
	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $data = array(
            'payment_status'		=> $status
			
        );
        $this->update($data,'id = '. (int)$id);
    }
 ####-------------------------------END---------------------------------------####
	    ####---------------Update Data in art_artworks Table----------------####
public function updateArtworkuploadliststatus($id,$status)
    {
	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $data = array(
            'approval_status'		=> $status
			
        );
        $this->update($data,'id IN ('.$id.') ' );
    }
 ####-------------------------------END---------------------------------------####
	    ####---------------Delete Data in art_artworks and art_artworks_images Table----------------####	
	
   public function deleteartworkuploadlist($id)
    { 
	   //echo'hi'. $id;die;
	   $db = Zend_Db_Table_Abstract::getDefaultAdapter();
		//$db = $this->getDbTable();
		$stmt = $db->query('DELETE FROM art_artworks_images WHERE artwork_id="'.$id.'"');
		 $where	=	$this->getAdapter()->quoteInto('id = ?', $id);
		return $this->delete($where);
    }
 ####-------------------------------END---------------------------------------####
 
 public function userartwork($owner_id) {
		
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt = $db->query('SELECT DISTINCT(aw.id) as wid,aw.*,awim.*,DATE_FORMAT(aw.bid_start_date,"%D %b %Y")  as b_date FROM art_artworks aw INNER JOIN art_artworks_images awim ON aw.id = awim.artwork_id WHERE owner_id="'.$owner_id.'"   GROUP BY aw.id  ORDER BY awim.id');
        $result = $stmt->fetchAll();
        
		
		return $result;
	}
 ####-------------------------------END---------------------------------------####
 
 public function deleteimageartwork($id) {
		
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('id IN ('.$id.') ' ); 
		return $this->getDbTable()->delete($where);
		
		die();

	}
 ####-------------------------------GET a user all images---------------------------------------####
	public function getImagesAOwner($owner_id)
	{
	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
	$stmt = $db->query('SELECT b.image, a.owner_id,a.id as id FROM art_artworks a INNER JOIN art_artworks_images b ON a.id=b.artwork_id WHERE owner_id="'.$owner_id.'" AND payment_status=1 AND publish=1  AND CURDATE()>=a.bid_start_date');
	$result = $stmt->fetchAll();
        
		
		return $result;
	}
}
<?php
class Application_Model_ArtFolder   
{ 
	protected $_name = 'art_folder';
    protected $_dbTable;
    
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    } 
    
    public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_ArtFolder');
        }
        return $this->_dbTable;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
	public function myFolder($uId)
	{
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select  =  "SELECT i.id,i.folder_name FROM art_folder as i,art_user as u where i.creator =u.id AND i.delete != 1 ";
		
		$result  =  $db->fetchAll($select);
		
		return  $result;
	}
	
	///////////////////Added by reeta 24-02-2012//////////////////////////////////////////////////////
	public function createfolder($request)
	{ 
		$namespace 	= new Zend_Session_Namespace(); 
		$db			=	$this->getDbTable();
		
		$data	= 	array('creator'=>$namespace->userid,
						'create_date'=>date("Y-m-d H:i:s"),
						'folder_name'=>$request['folder']
					);

		return $insRec	=	$db->insert($data); 
		
	} 
}
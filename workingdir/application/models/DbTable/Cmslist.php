<?php

class Application_Model_DbTable_Cmslist extends Zend_Db_Table_Abstract
{

    protected $_name = 'art_section';
	protected $_name1 = 'art_category';
	protected $_name2 = 'art_subcategory';
    protected $_dbTable;

    /**
   @	Add Faq
   @	Added By : Anil Rawat
   @	Added On :	20-10-2011	
   @	Input: void
   @	Return: reture the value from thr table.
   @
   **/	
    public function getCmslist($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Count not find row $id");
        }
		//echo'<pre>';print_r( $row);die;
        return $row->toArray();    
    }
	
   /**
   @	Add Faq
   @	Added By : Anil Rawat
   @	Added On :	20-10-2011	
   @	Input:
	@			question( String)
	@			answer (String)
	@			addDate (date)
	@	Return: void
	@
   **/ 
    
	public function addCmslist($cms_page_name,$cms_heading,$cms_description,$addDate)
    {
        $data = array(
            'cms_page_name'		=> $cms_page_name,
			'cms_heading'   	=> $cms_heading,
			'cms_description'   => $cms_description,
			'created_date'  	=> $addDate,
			
        );
        $this->insert($data);
    }
    
    public function updateCmslist($id,$cms_page_name,$cms_heading,$cms_description,$addDate)
    {
        $data = array(
            'cms_page_name'		=> $cms_page_name,
			'cms_heading'   	=> $cms_heading,
			'cms_description'   => $cms_description,
			'created_date'  	=> $addDate,
			
        );
        $this->update($data,'id = '. (int)$id);
    }

	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
    public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_Cmslist');
        }
        return $this->_dbTable;
    }

   public function deletecmslist($id)
    { 
	   //echo'hi'. $id;die;
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('id = ?', $id); 
		return $this->getDbTable()->delete($where);
    }


}


<?php
 
 
class Application_Model_DbTable_Artworkpayment extends Zend_Db_Table_Abstract
{ 
	protected $_name = 'art_artworks_listing_bid_payment';
    protected $_dbTable;
	
 	public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Artworkpayment');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
	///////////////////////////////////////////////////payment//////////////////////////////////////////////////////////
	 public function getPaymentCurlist($id) 
    {
        $id = (int)$id;
		$db = $this->getDbTable();
		$where	=	$db->select()->from(array('art_artworks_listing_bid_payment'),array('*'))->where('artwork_id = ' . $id)->order('id desc')->limit('1'); 
        $row = $this->fetchAll($where);
        return $row->toArray();  
    } 
   
   ///////////////////////////////////////////////////payment//////////////////////////////////////////////////////////
	 public function getPaymentlist($id)  
    {
        $id = (int)$id;
		$db = $this->getDbTable();
		$where	=	$db->select()->from(array('art_artworks_listing_bid_payment'),array('*'))->where('artwork_id = ' . $id); 
        $row = $this->fetchAll($where);
        return $row->toArray();  
  
    }
   
}
<?php
class Application_Model_DbTable_ArtCountry extends Zend_Db_Table_Abstract
{


    protected $_name = 'art_country';
    protected $_dbTable;
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
    public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_ArtCountry');
        }
        return $this->_dbTable;
    }	
public function getcountryName()
	{
		$db = $this->getDbTable();
        //$row = $this->fetchAll();
        $row = $this->fetchAll($this->select()->from(array('art_country' => 'art_country'))->order('id asc'));
       // echo '<pre>';print_r($row);die;
        return $row;
	}
public function getcountryCode($id)
	{
		$id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Count not find row $blog_id");
        }
        return $row->toArray();    
	}

}
?>

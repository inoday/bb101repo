<?php
 
 
class Application_Model_DbTable_Artworkbid extends Zend_Db_Table_Abstract
{ 
	protected $_name = 'art_bidding';
    protected $_dbTable;
	
 	public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Artworkbid');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
	
    ####---------------INSERT Data in  Table----------------####
    
	public function addartworkbid($dbField)
    {   $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $dbField;
		//$db = $this->getDbTable();	
	
		$artwork_id = $this->insert($dbField);
		
		
    }
    
    ####-------------------------------END---------------------------------------####
  ####---------------GET bidiing of a artwork count----------------####	 
 public function getBidlist_count($id) 
    {
        $id = (int)$id;
		$db = $this->getDbTable();
		$where	=	$db->select()->from(array('art_bidding'),array('noofbid' => 'count(*)'))->where('au_id = ' . $id); 
        $row = $this->fetchRow($where);
        return $row->toArray();  
    }
   ####-------------------------------END---------------------------------------####
    ####---------------GET bidiing of a artwork count----------------####	 
 public function getBidMinilist($id) 
    {
        $id = (int)$id;
		$db = $this->getDbTable();
		$where	=	$db->select()->from(array('art_bidding'),array('minbid' => 'MAX(maximum_bid)','bidder_id', 'current_bid'))->where('au_id = ' . $id)->group('au_id'); 
        $row = $this->fetchAll($where);
        return $row->toArray();  
    }
   ####-------------------------------END---------------------------------------####
      ####---------------GET bidiing of a artwork count----------------####	 
 public function getBidMaxlist($id) 
    {
        $id = (int)$id;
		$db = $this->getDbTable();
		$where	=	$db->select()->from(array('art_bidding'),array('maxbid' => 'current_bid','bidder_id', 'current_bid'))->where('au_id = ' . $id)->order(array('current_bid desc', 'id desc'))->limit('1'); 
        $row = $this->fetchAll($where);
        return $row->toArray();  
    }
   ####-------------------------------END---------------------------------------####
   ####---------------------GET bidiing of a artwork count----------------------####	 
 public function getBidMaxlistlocation($id,$location) 
    {
        $id = (int)$id;
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		
		$stmt = $db->query('SELECT `ab`.`current_bid` AS `maxbid`, `ab`.`bidder_id`, `ab`.`current_bid`, aud.country FROM `art_bidding` ab LEFT JOIN art_user au ON au.id= ab.bidder_id LEFT JOIN art_user_details aud ON au.id= aud.user_id WHERE au_id = "'.$id.'" AND aud.country="'.$location.'" ORDER BY `current_bid` DESC LIMIT 1' ); 
	$result = $stmt->fetchAll();

        return $result;  
    }
   ####-------------------------------END---------------------------------------####
  ####---------------GET bidiing of a artwork----------------####	 
 public function getBidlist($id) 
    {
        $id = (int)$id;
		$db = $this->getDbTable();
		$where	=	$db->select()->where('au_id = ' . $id)->order(array('bid_date DESC')); 
        $row = $this->fetchAll($where);
        if (!$row) {
           return $row;
        }
		else {
        return $row->toArray();    }
    }
	//////////////////////////////current bid/////////////////////////////////////
	public function getBidCurlist($id) 
    {
	$id = (int)$id;
	$db = $this->getDbTable();
	$where	=	$db->select()->from(array('art_bidding'),array('*'))->where('au_id = ' . $id)->order('bid_date desc')->limit('1'); 
	$row = $this->fetchAll($where);
	return $row->toArray();  
    } 
public function deleteBidOfArtwork($artid)
	{
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('au_id = ?', $artid); 
		return $this->getDbTable()->delete($where);
	}
 public function getMaxBidofUser($uid,$au_id)
    {
	 $uid = (int)$uid;  $au_id = (int)$au_id;
		$db = $this->getDbTable();
		$where	=	$db->select()->from(array('art_bidding'),array('maxbid' => 'MAX(current_bid)','bidder_id', 'current_bid'))->where('bidder_id = ' . $uid)->where('au_id = ' . $au_id)->group('au_id');
        $row = $this->fetchAll($where);
        return $row->toArray();
    }
}

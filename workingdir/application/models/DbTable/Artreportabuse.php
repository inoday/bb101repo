<?php
class Application_Model_DbTable_Artreportabuse extends Zend_Db_Table_Abstract
{


    protected $_name = 'art_feedbacks_reportabuse';
    protected $_dbTable;
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
    public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_Artreportabuse');
        }
        return $this->_dbTable;
    }	
	public function addreprotabuse($data)
    {
	 $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $this->insert($data); 
    }
	//////////////ADMIN SECTION//////////////////////
	 public function getMessage()
    {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select  =  "SELECT * FROM art_feedbacks_reportabuse ";
		$result  =  $db->fetchAll($select);
		return  $result;
	}
	public function deletereprotabuselist($id)
    { 
	   //echo'hi'. $id;die;
		$db = $this->getDbTable();
		//echo $where	=	$db->getAdapter()->quoteInto('id IN (?)', $id); die;
		$where = $db->getAdapter()->quoteInto('id IN ('.$id.') ','');
		return $db->delete($where);
    }
	public function getabuseByID($id)
	{
		$id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Count not find row $id");
        }
		//echo'<pre>';print_r( $row);die;
        return $row->toArray();    
	}
	
}
?>

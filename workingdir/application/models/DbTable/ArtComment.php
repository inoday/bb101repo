<?php
 /**
 @	Manage blog catagory
 @	Project: Findbuddy
 @	Created by: Anil Rawat
 @	Created On: 14-11-2011
 @	Funtions / Methods:
 							
 **/
 
class Application_Model_DbTable_ArtComment extends Zend_Db_Table_Abstract
{   
	 
	
    protected $_name = 'art_blog_comments'; 
    protected $_dbTable;
	////////edit gift list//////////

	
	public function blogAll()
	{     
	      //echo'<pre>';print_r($this->getDbTable());die('hiiiii');
		  return $this->getDbTable()->fetchAll();
	}
	
	 public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_ArtComment');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
    public function blockComment($id,$status)
    {   
	    //echo $status;die;
		$db = $this->getDbTable();
		if($status =='1' or $status ==''){
			$status	='0';
		}else{
			$status	='1';
		}
		$data = array('publish'=> $status,);
		//print_r($data);die;
		$where	=	$db->getAdapter()->quoteInto('id = ?',$id); 
		return $this->getDbTable()->update($data,$where);
    }

    
	public function deletecomment($id)
    { 
	   //echo'hi'. $id;die;
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('id = ?', $id); 
		return $this->getDbTable()->delete($where);
    }
	
	 public function insertcomment($commentblog)
    {  
		   return $this->insert($commentblog);
  
    }
	public function getblogcomment($blog_id) 
    {   
        $blog_id = (int)$blog_id;
        $row = $this->fetchRow('id = ' . $blog_id);
        if (!$row) {
            throw new Exception("Count not find row $blog_id");
        }
        return $row->toArray();    
    }
}

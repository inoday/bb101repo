<?php
 /**
 @	Manage blog catagory
 @	Project: Findbuddy
 @	Created by: Anil Rawat 
 @	Created On: 14-11-2011
 @	Funtions / Methods:
 							
 **/
 
class Application_Model_DbTable_ArtBlog extends Zend_Db_Table_Abstract
{   
	 
	
    protected $_name = 'art_blog';
    protected $_dbTable;
	////////edit gift list//////////
	 public function getblogcate($blogcat_id) 
    {
        $blogcat_id = (int)$blogcat_id; 
        $row = $this->fetchRow('blogcat_id = ' . $blogcat_id);
        if (!$row) {
            throw new Exception("Count not find row $blogcat_id");
        }
        return $row->toArray();    
    }
	
	public function getCategory()
	{     
		  $tr	= $this->getDbTable()->fetchAll();
		  return $tr->toArray();
	}
	////////edit gift list end//////////
	public function getblog($blog_id) 
    {   
        $blog_id = (int)$blog_id;
        $row = $this->fetchRow('id = ' . $blog_id);
        if (!$row) {
            throw new Exception("Count not find row $blog_id");
        }
        return $row->toArray();    
    }
	/////////for edit///////////////////
	public function updateblog($dbField)
    {    
	     //echo'<pre>';print_r($dbField);die;
	     $db = $this->getDbTable();
		 $data = array('title' => $dbField['title'],
		 'description'=> $dbField['description'],'creator' => $dbField['creator'],'publish' => $dbField['publish'],'image' => $dbField['image'],'modify_date' => $dbField['modify_date'] );
		 $where = $this->getDbTable()->getAdapter()->quoteInto('id = ?',$dbField['id']);
		 $rec	=	$db->update($data,$where);
		
		   //$where = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $uId);
		  // return 'edit';  
    }
	////////////////end  edit////////////////
	public function blogAll()
	{     
		  return $this->getDbTable()->fetchAll();
	}
	 public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_ArtBlog');
        }
        return $this->_dbTable;
    }
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }  
    public function getAutoblog($squestion)
    {   
		$squestion = trim($squestion);
		$db = $this->getDbTable();
		if(!empty($squestion)){
		$where	=	$db->select()->where("blog_title Like '%$squestion%' "); 
		}else{
		$where	=	$db->select(); 
		}
		$result =  $this->getDbTable()->fetchAll($where);
		$members = $result->toArray();
		return $members;
    }
	public function addblog($dbField)
    {   	    
        $dbField; 
		$squestion = trim($dbField['blog_title']);
		$db = $this->getDbTable();
		if(!empty($squestion)){
		$where	=	$db->select()->where("blog_title Like '$squestion' "); 
		}
		$result =  $this->getDbTable()->fetchAll($where);
		$members = $result->toArray();
		if(count($members)>0)
		{ 
		  return 'err'; 
		}
		else
		{
		  $this->insert($dbField);
		}		
    }
    public function addblogadmin($dbField)
    {   	    
        $dbField; 
		
		  $this->insert($dbField);
				
    }
	public function deleteblog($blog_title)
    { 
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('title = ?', $blog_title); 
		return $this->getDbTable()->delete($where);
    }
	
	
	public function deleteablog($blog_id)
    { 
		$db = $this->getDbTable();
		echo $where	=	$db->getAdapter()->quoteInto('id IN (?)', $blog_id); //die();
		return $this->getDbTable()->delete($where);
    }
	/*
	   @	Created by: Anil Rawat
       @	Created On: 22-12-2011
	*/
	  public function getviewblog($blog_id) 
    {   
        $blog_id = (int)$blog_id;
        $row 		= $this->fetchRow('id = ' . $blog_id);
        if (!$row) {
            throw new Exception("Count not find row $blog_id");
        }
        return $row->toArray();    
    }
	
	 public function insertblog($dbField)
    {  
		   return $this->insert($dbField);
  
    }
	
	  public function getblogview($blog_id) 
    {   
        $blog_id = (int)$blog_id;
        $row 		= $this->fetchRow('id = ' . $blog_id);
        if (!$row) {
            throw new Exception("Count not find row $blog_id");
        }
        return $row->toArray();    
    }
	public function updateBlogliststatus($id,$status)
    {
        $data = array(
            'publish'		=> $status
			
        );
        $this->update($data,'id IN ('.$id.') ');
    }
}

<?php

class Application_Model_DbTable_ArtUserFeedbacksComments extends Zend_Db_Table_Abstract
{ 	
	protected $_name = 'art_user_feedbacks_comments';
	protected $_dbTable;
	
	public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Profilecomment');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
	 ####---------------INSERT Data in  Table----------------####
    
	public function addartworksubfeedbacks($dbField)
    {  
	
	 $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $dbField;
		//$db = $this->getDbTable();	
	
		$artwork_id = $this->insert($dbField);
		
		print_r($dbField); 
    }
    
    ####-------------------------------END---------------------------------------####
	####-------------------------------delete comment on artwork---------------------------------------####

   public function deletesubcommentlist($id)
    { 
	   //echo'hi'. $id;die;
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('id = ?', $id); 
		return $this->delete($where);
    } 

	####-------------------------------delete comment and scu comment on artwork---------------------------------------####

   public function deletecommentlist($id)
    { 
	   //echo'hi'. $id;die;
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('feedback_id = ?', $id); 
		return $this->delete($where);
    }
	
	/**
   @	Added By : Abhishek
   @	Added On : 05-03-2012	
   @	Return: reture all feadback of a User.
   @
   **/
   
	public function addUserFeedbackComment($dbField)
    {   
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$db->insert($dbField);
    }
}
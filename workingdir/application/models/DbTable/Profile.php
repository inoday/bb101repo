<?php

class Application_Model_DbTable_Profile extends Zend_Db_Table_Abstract
{ 	
	protected $_name = 'art_user_feedbacks';
	 
	protected $_dbTable;
	protected $_dbTable2;
	
 	public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Profile');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
	public function getDbTable2()
    {
		if (null === $this->_dbTable2) {
            $this->setDbTable('Application_Model_DbTable_Profilecomment');
        }
        return $this->_dbTable2;
    }
	
	public function setDbTable2($dbTable2)
    {
		if (is_string($dbTable2)) 
		{
			$dbTable2 = new $dbTable2();
		}
		if (!$dbTable2 instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable2 = $dbTable2;

		return $this;
    }
	/////////////find user details////////////////
	public function getUserdetails($email,$password=null) {
		
		$makeQue = 'SELECT aud.*, au.id, au.first_name, au.last_name, au.email, au.user_id as userid, au.group_id, au.password
					FROM art_user au 
					LEFT JOIN art_user_details aud  ON au.id = aud.user_id
					WHERE au.email="'.$email.'"';
		if($password!=null)
		   $makeQue .=' AND  password = "'.$password.'"';
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
	 	$stmt = $db->query($makeQue);
        $result = $stmt->fetchAll();
         
		return $result;
    }
	
	//////////////////////////// Update user //////////////////////////////////
	public function updateuser($userArray,$detailsArray,$userid)
    { 
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->update('art_user', $userArray, "id = '".$userid."'");
        $db->update('art_user_details', $detailsArray, "user_id = '".$userid."'");
    }
    
	//////////////////////////// Update user Details //////////////////////////////////
	public function updateuserDetails($detailsArray,$userid)
    { 
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->update('art_user_details', $detailsArray, "user_id = '".$userid."'");
    }
    
    public function getUsertype() {
    	
    	$makeQue = 'SELECT group_id, group_type	FROM art_group
					 
					WHERE group_id IN(3,4,5)'; 
		
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
	 	$stmt = $db->query($makeQue);
        $result = $stmt->fetchAll();
        return $result;    	
    }
    
    public function getUserProfile($user_id) {
    	$makeQue = 'SELECT au.id, au.first_name, au.last_name, au.email, au.create_date, aud.dob, aud.profile_image, aud.biography, aud.user_id, aud.city, acu.country as countr, COUNT(af.user_id) AS follower, COUNT(af.au_id) AS following, ag.group_type,aud.*
					FROM art_user AS au
					LEFT JOIN art_user_details AS aud ON aud.user_id = au.id
					LEFT JOIN art_followed AS af ON af.user_id = au.id
					LEFT JOIN art_country AS acu ON acu.id = aud.country
					LEFT JOIN art_group AS ag ON ag.group_id = au.group_id
					WHERE au.id = '.$user_id.' GROUP BY au.id'; 
		
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
	 	$stmt = $db->query($makeQue);
        $result = $stmt->fetchAll();
        return $result;
    }
    
	public function getUserArt($user_id) {
    	$makeQue = 'SELECT aw.id,aw.title, aw.price,awim.image
					FROM art_artworks AS aw
					INNER JOIN art_artworks_images AS awim ON awim.artwork_id = aw.id
					WHERE aw.owner_id = '.$user_id.' GROUP BY aw.id ORDER BY 1 desc';
		
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
	 	$stmt = $db->query($makeQue);
        $result = $stmt->fetchAll();
       
        return $result;
    }
    public function getUserArtmorderated($user_id) { 
    	$makeQue = 'SELECT count(*) as modrate_art FROM art_artworks 
					WHERE owner_id = '.$user_id.' AND approval_status!=2 ORDER BY 1 desc';
		
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
	 	$stmt = $db->query($makeQue);
        $result = $stmt->fetchAll();
        return $result;
    }
    public function checkUser($user_id){
    	
    	$makeQue = 'SELECT id AS userid
					FROM art_user					 
					WHERE md5(id) = "'.$user_id.'"';
    	
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
	 	$stmt = $db->query($makeQue);
        $result = $stmt->fetchAll();
        
        return $result;
    }
    
	public function addUserFeedback($dbField)
    {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
     	$feedback_id = $this->insert($dbField);
    }
    
   /**
   @	Added By : Abhishek
   @	Added On : 03-03-2012	
   @	Return: return all feadback of a User.
   @
   **/	
    public function getUserFeedbacks($id) 
    { 
        $que = 'SELECT u.id user, u.group_id, ud.profile_image, uf.from_user_id, uf.to_user_id, uf.id, uf.from_user_id, uf.from_user_id, uf.feedback, uf.create_date, u.first_name,
       		    uf.create_date feedtime       			
       		    FROM art_user_feedbacks uf 
       			LEFT JOIN art_user u ON u.id = uf.from_user_id
       			LEFT JOIN art_user_details ud ON ud.user_id = uf.from_user_id
				WHERE uf.to_user_id="'.$id.'" 
				ORDER BY uf.create_date DESC'; 
     
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
   
		return $result;   
    }
    
	public function getUserFeedbacks_user($id, $limit) 
    { 
        $que = 'SELECT u.id user, u.group_id, ud.profile_image, uf.from_user_id, uf.to_user_id, uf.id, uf.from_user_id, uf.from_user_id, uf.feedback, uf.create_date, u.first_name,
       			uf.create_date feedtime
       			FROM art_user_feedbacks uf 
       			LEFT JOIN art_user u ON u.id = uf.from_user_id
       			LEFT JOIN art_user_details ud ON ud.user_id = uf.from_user_id
				WHERE uf.to_user_id="'.$id.'" 
				ORDER BY uf.create_date DESC 
				LIMIT '.$limit.', 1';  
     
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
   
		return $result;   
    }
    
	public function getArtworkfeedbacksComments($id) 
    { 
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $que = 'SELECT fc.id fcid, fc.feedback_id, fc.comment, u.id userID,u.first_name,
       				fc.create_date cdate
       				FROM art_user_feedbacks_comments fc
       				LEFT JOIN art_user u ON u.id = fc.from_user_id
       				WHERE fc.to_user_id="'.$id.'" 
					ORDER BY fc.create_date DESC'; //die;
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
    
		return $result;   
    }
    
    public function deletecomment($id) {
    	$DB = Zend_Db_Table_Abstract::getDefaultAdapter();
		$DB->delete('art_user_feedbacks_comments', 'feedback_id = '.$id);
		$DB->delete('art_user_feedbacks', 'id = '.$id); 
    }
    /**
   @	Added By : Abhishek
   @	Added On : 12-03-2012	
   @	Return: return all following and follower of a User.
   @
   **/	
    public function following($id) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
       $que = 'SELECT count(user_id) following
       			FROM art_followed       				 
       			WHERE user_id="'.$id.'"'; 
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		return $result;
    }
    
	public function follower($id) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $que = 'SELECT count(user_id) follower   				 
       			FROM art_users_followers       				 
       			WHERE user_id="'.$id.'"';
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		return $result;
    }
	 public function tofollowing($id) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
       $que = 'SELECT count(user_id) following
       			FROM art_followed       				 
       			WHERE user_id="'.$id.'" AND CURDATE()=date(create_date)'; 
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		return $result;
    }
    
	public function tofollower($id) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $que = 'SELECT count(user_id) follower   				 
       			FROM art_users_followers       				 
       			WHERE user_id="'.$id.'" AND CURDATE()=date(add_date)';
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		return $result;
    }
   /**
   @	Added By : Abhishek
   @	Added On : 12-03-2012	
   @	Return: Get bidding details login User.
   @
   **/
    
    public function bidding($id){
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
     	$result = array();
	    $duration = $this->getAuctionDuration();
	   
	    $getID = 'SELECT DISTINCT au_id	 
				  FROM art_bidding        				 
       			  WHERE bidder_id="'.$id.'" AND CURDATE()=date(bid_date)';
             				 
		$stmtgetID = $db->query($getID);
		$resultID = $stmtgetID->fetchAll();
		if(count($resultID)){
			$endingSoon = $this->getEndingSoon($resultID, $duration['ending_soon']); 
		}else{
			$endingSoon['endingsoon'] = 0;
		}
	    
	   
	    //echo '<pre>';print_r($resultID);  die;
	   
        $que = 'SELECT count(DISTINCT au_id) bidding		 
				FROM art_bidding        				 
       			WHERE bidder_id="'.$id.'" AND CURDATE()=date(bid_date)'; //die;
             				 
		$stmt = $db->query($que);
		$getbidding = $stmt->fetchAll();
   		foreach ($getbidding as $value) {
	    		$bidding[0] = $value; 
	    }
	    $result[] = $bidding[0];
		$result[] = $endingSoon['endingsoon'];
		
		return $result;    
    }
    
	public function liveart($id) {
    	$duration = $this->getAuctionDuration();
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	if(!$id) return false;
    	 $queID = 'SELECT aw.id liveart	 
				FROM art_artworks aw    				 
       			WHERE aw.owner_id = '.$id.' AND 
       			payment_status = 1 AND
       			approval_status = 1 
				AND  ADDDATE(aw.bid_start_date, INTERVAL auction_duration DAY) > NOW()
				AND  NOW()>=aw.bid_start_date '; 
    	     				 
		$stmtID = $db->query($queID);
		$resultID = $stmtID->fetchAll();
		if(count($resultID)){
    		$endingSoon = $this->getEndingSoon($resultID, $duration['ending_soon']);
		}else{
			$endingSoon['endingsoon'] = 0;
		}
    	
    	 $que = 'SELECT count(aw.id) liveart	 
				FROM art_artworks aw    				 
       			WHERE aw.owner_id = '.$id.' AND 
       			payment_status = 1 AND
       			approval_status = 1 AND
       			DATEDIFF(CURDATE(),aw.bid_start_date) < auction_duration '; 
    	     				 
		$stmt = $db->query($que);
		$getArtLive = $stmt->fetchAll();
		foreach ($getArtLive as $value) {
	    		$getArt[0] = $value; 
	    }
	    $result[] = $getArt[0];
		$result[] = $endingSoon['endingsoon'];
		
		return $result;
    }
    
    public function watchlist($id) {
    	$maxduration = $this->getMaxAuctionDuration();
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
     	$result = array();
	    $duration = $this->getAuctionDuration();
	   
	    $getID = 'SELECT DISTINCT au_id	 
				  FROM art_watchlist        				 
       			  WHERE user_id="'.$id.'"'; //die;
             				 
		$stmtgetID = $db->query($getID);
		$resultID = $stmtgetID->fetchAll();
		
		if(count($resultID)){
			$endingSoon = $this->getEndingSoon($resultID,1);
    	}else{
			$endingSoon['endingsoon'] = 0;
		}
	    //echo '<pre>';print_r($resultID);  die;
	   
        $que = 'SELECT count(DISTINCT au_id) watchling		 
				FROM art_watchlist        				 
       			WHERE user_id="'.$id.'"'; //die;
             				 
		$stmt = $db->query($que);
		$getwatching = $stmt->fetchAll();
   		foreach ($getwatching as $value) {
	    		$watch[0] = $value; 
	    }
	    $result[] = $watch[0];
		$result[] = $endingSoon['endingsoon'];
		 
		return $result;  
    }
    
    public function getEndingSoon($resultID,$duration) {
 
	    foreach ($resultID as $idArray) {
	    	foreach ($idArray as $value) {
	    		$id[] = $value; 
	    	} 	 
		}
	    
   		$res_id = implode(',', $id);  
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$que = 'SELECT count(aw.bid_start_date) endingsoon	 
				FROM art_artworks aw    				 
       			WHERE aw.id IN('.$res_id.') AND 
       			DATEDIFF(NOW(),aw.bid_start_date) < '.$duration.' '; //die;
    	     				 
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		
		foreach ($result as $value) {
	    	$endingSoon = $value;
	    }
	    return $endingSoon;
		 
    }
    
    public function feedback($id) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	
    	$que = 'SELECT count(artwork_id) newfeedback		 
				FROM art_artworks_feedbacks        				 
       			WHERE DATEDIFF(CURDATE(),create_date) < 1 AND 
       			publish = 1 AND user_id="'.$id.'"';// die;
             				 
		$stmt = $db->query($que);
		$feedback = $stmt->fetchAll();
//echo '<pre>';print_r($feedback);  die;
		return $feedback;
    }
 /********** Get auction duration *********************/
    public function getAuctionDuration(){
    
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	/********** Get auction duration *********************/
		$getduration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
    	 
	    foreach ($getduration as $value) {
	    	$duration = $value;
	    }
	    return $duration;
    
    }
    
 	public function getMaxAuctionDuration(){
    
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	/********** Get auction duration *********************/
    	$getduration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
	    foreach ($getduration as $value) {
	    	$duration = $value;
	    }
	    return $duration;
    }
    
	public function getBids($user_id) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
    	$que = 'SELECT ab.bid_date, ab.current_bid, aw.frame, aw.title,aw.category,aw.bid_start_date, TIMEDIFF(ADDDATE(aw.bid_start_date, INTERVAL auction_duration DAY),NOW()-INTERVAL 0 MINUTE) as ending,
    			au.id, au.first_name, au.last_name, awim.image, ab.au_id
				FROM art_bidding ab  
				INNER JOIN art_artworks aw ON aw.id = ab.au_id 
				INNER JOIN art_user au ON au.id = ab.bidder_id 				
				INNER JOIN art_artworks_images awim ON awim.artwork_id = ab.au_id	 
       			WHERE ab.bidder_id="'.$user_id.'"
       			GROUP BY ab.id, ab.bidder_id 
       			ORDER BY ab.bid_date DESC'; //die;
             				 
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		  
		return $result;
    }
    
    public function getWatchlist($user_id) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
        $que = 'SELECT awh.id watch_id, awh.create_date, aw.edition, aw.frame, aw.title,aw.category, aw.price, au.id, au.first_name, au.last_name, awim.image, awh.au_id , awh.id watchID,aw.bid_start_date,
        		TIMEDIFF(ADDDATE(aw.bid_start_date, INTERVAL auction_duration DAY),NOW()-INTERVAL 0 MINUTE) as ending
    			FROM art_watchlist awh 
    			INNER JOIN art_artworks aw ON aw.id = awh.au_id 
    			INNER JOIN art_user au ON au.id = awh.user_id 
    			INNER JOIN art_artworks_images awim ON awim.artwork_id = awh.au_id 
    			WHERE awh.user_id="'.$user_id.'"
    			GROUP BY awh.id
       			ORDER BY awh.create_date DESC'; //die;
             				 //,ab.current_bid   LEFT JOIN art_bidding ab ON ab.au_id = awh.au_id
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		//echo '<pre>';print_r($result);  die;
		return $result;
    }
    
	public function getBoughtArt($user_id) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		//add INNER JOIN art_artworks_listing_transaction at ON at.listing_pay_id = awp.id  for changes in transaction  (23 may 2012)
    	$que = 'SELECT at.transaction_date, awp.amount, aw.frame, aw.title,aw.category, aw.price, au.id, au.first_name, au.last_name, awim.image,aw.id as wid,aw.subject
    			FROM art_artworks_listing_bid_payment awp 
				INNER JOIN art_artworks_listing_transaction at ON at.listing_pay_id = awp.id 
    			INNER JOIN art_artworks aw ON aw.id = awp.artwork_id 
    			INNER JOIN art_user au ON au.id = awp.bidder_id 
    			INNER JOIN art_artworks_images awim ON awim.artwork_id = awp.artwork_id 
    			WHERE awp.bidder_id ="'.$user_id.'"
				AND at.status=1
				AND type="seller"
    			GROUP BY awp.id
       			ORDER BY at.transaction_date DESC';//die;
    	$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		//echo '<pre>';print_r($result);  
		return $result;
    }
    
    public function bidderCount($art_id){
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$que = 'SELECT count( DISTINCT ab.bidder_id ) bidder 
				FROM art_bidding ab
				WHERE ab.au_id ='.$art_id;
    	$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		//echo '<pre>';print_r($result);  die;
		return $result[0]['bidder'];
    }
    public function bidCount($art_id){
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$que = 'SELECT count( * ) bidder 
				FROM art_bidding ab
				WHERE ab.au_id ='.$art_id;
    	$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		//echo '<pre>';print_r($result);  die;
		return $result[0]['bidder'];
    }
    public function maxBid($art_id) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$que = 'SELECT MAX(ab.current_bid) maxbid 
				FROM art_bidding ab
				WHERE ab.au_id ='.$art_id;
    	$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		
		return $result[0]['maxbid'];
    }
    ///////////////////////////////////////////
	public function maxBiddetails($art_id,$mbid) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$que = 'SELECT * 
				FROM art_bidding ab
				WHERE ab.au_id ='.$art_id.' and current_bid='.$mbid; 
    	$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		
		return $result;
    }
    ////////////////////////////////////////////
    public function aboutBuy($user_id) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
    	$que = 'SELECT MAX(ab.current_bid) mbid,ab.bid_date, ab.current_bid, aw.frame,aw.title,aw.category,aw.bid_start_date, TIMEDIFF(ADDDATE(aw.bid_start_date, INTERVAL auction_duration DAY),NOW()-INTERVAL 0 MINUTE) as ending,
    			au.id, au.first_name, au.last_name, awim.image, ab.au_id,aw.id as wid,aw.subject
				FROM art_bidding ab  
				INNER JOIN art_artworks aw ON aw.id = ab.au_id 
				INNER JOIN art_user au ON au.id = ab.bidder_id 				
				INNER JOIN art_artworks_images awim ON awim.artwork_id = ab.au_id	
       			WHERE ab.bidder_id="'.$user_id.'" 
				AND aw.sold_status=0
				AND  ADDDATE(aw.bid_start_date, INTERVAL auction_duration DAY) <= NOW() 
				AND  NOW()>=aw.bid_start_date
       			GROUP BY aw.id
       			ORDER BY ab.current_bid,ab.bid_date DESC';// die;
            				 
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		  
		return $result;
    }
    
	public function listed($user_id) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
    	$que = 'SELECT aw.frame, aw.create_date, aw.date_completed,ab.current_bid, aw.price, aw.title,aw.category, au.id, au.first_name, au.last_name,ab.bid_date, awim.image, aw.id as wid,aw.subject,aw.buy_it_now
				FROM art_artworks aw  
			 	INNER JOIN art_user au ON au.id = aw.owner_id 				
				INNER JOIN art_artworks_images awim ON awim.artwork_id = aw.id
				INNER JOIN art_bidding ab ON ab.au_id != aw.id 
				WHERE aw.approval_status = 1 AND aw.payment_status = 1 AND aw.owner_id = "'.$user_id.'" 
				AND  ADDDATE(aw.bid_start_date, INTERVAL auction_duration DAY) > NOW() 
				AND  NOW()>=aw.bid_start_date
				GROUP BY aw.id
       			ORDER BY aw.create_date DESC';// AND die;
             				 
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		
		return $result;
    }
    
	public function livebids($user_id) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
    	$que = 'SELECT aw.frame, aw.create_date, ab.current_bid,aw.edition, aw.price, aw.title,aw.category, au.id, au.first_name, au.last_name, ab.bid_date,aw.bid_start_date,awim.image,TIMEDIFF(ADDDATE(aw.bid_start_date, INTERVAL auction_duration DAY),NOW()-INTERVAL 0 MINUTE) as ending, aw.id as wid,aw.subject
				FROM art_artworks aw  
			 	INNER JOIN art_user au ON au.id = aw.owner_id 				
				INNER JOIN art_artworks_images awim ON awim.artwork_id = aw.id 
				INNER JOIN art_bidding ab ON ab.au_id = aw.id 
				WHERE aw.approval_status = 1 AND aw.payment_status = 1 AND aw.owner_id = "'.$user_id.'" 
				AND  ADDDATE(aw.bid_start_date, INTERVAL auction_duration DAY) > NOW() 
				AND  NOW()>=aw.bid_start_date
				GROUP BY aw.id
       			ORDER BY aw.create_date DESC';// AND die;
				
             				 
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		
		return $result;
    }
    
	public function moderated($user_id) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
    	$que = 'SELECT DISTINCT(aw.id) as wid,aw.frame, aw.date_completed, aw.edition, aw.price,aw.width,aw.height,aw.depth, aw.title,aw.category, au.id, au.first_name, au.last_name, awim.image,aw.payment_status,aw.approval_status,aw.buy_it_now,aw.subject
				FROM art_artworks aw  
			 	INNER JOIN art_user au ON au.id = aw.owner_id 				
				LEFT JOIN art_artworks_images awim ON awim.artwork_id = aw.id
				WHERE aw.payment_status = 0 AND aw.delete=0 AND aw.owner_id = "'.$user_id.'"
				GROUP BY aw.id
       			ORDER BY aw.create_date DESC'; //die;
             				 
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		
		return $result;
    }
    
    public function aboutsell($user_id) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
    	$que = 'SELECT MAX(ab.current_bid) mbid,ab.bid_date, ab.current_bid, aw.frame,aw.title,aw.category,aw.bid_start_date, 
		TIMEDIFF(ADDDATE(aw.bid_start_date, INTERVAL auction_duration DAY),NOW()-INTERVAL 0 MINUTE) as ending,
    			au.id, au.first_name, au.last_name, awim.image, ab.au_id,aw.id as wid,aw.subject
				FROM art_bidding ab  
				INNER JOIN art_artworks aw ON aw.id = ab.au_id 
				INNER JOIN art_user au ON au.id = ab.bidder_id 				
				INNER JOIN art_artworks_images awim ON awim.artwork_id = ab.au_id	
				WHERE aw.approval_status = 1 AND aw.payment_status = 1 AND aw.owner_id = "'.$user_id.'" 
				AND aw.sold_status=0
				AND  ADDDATE(aw.bid_start_date, INTERVAL auction_duration DAY) <= NOW() 
				AND  NOW()>=aw.bid_start_date
				GROUP BY aw.id
       			ORDER BY aw.create_date DESC';// die;
             				 
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		
		return $result;
    }
	
	public function sold($user_id) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
    //add INNER JOIN art_artworks_listing_transaction at ON at.listing_pay_id = awp.id  for changes in transaction (23 may 2012)
        $que = 'SELECT at.transaction_date, awp.amount, aw.frame, aw.title,aw.category, aw.send_status,aw.seller_payment_status,
				aw.price, au.id, au.first_name, au.last_name, awim.image,aw.id as wid,aw.subject
    			FROM art_artworks_listing_bid_payment awp 
				INNER JOIN art_artworks_listing_transaction at ON at.listing_pay_id = awp.id 
    			INNER JOIN art_artworks aw ON aw.id = awp.artwork_id 
    			INNER JOIN art_user au ON au.id = awp.bidder_id 
    			INNER JOIN art_artworks_images awim ON awim.artwork_id = awp.artwork_id 
    			WHERE aw.owner_id ="'.$user_id.'"
				AND at.status=1
				AND type="seller"
    			GROUP BY awp.id
       			ORDER BY at.transaction_date DESC';//die;     				 
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		
		return $result;
    }
    public function deleteWatchList($watch_id) {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$que = 'DELETE FROM art_watchlist WHERE art_watchlist.id ='.$watch_id;
    	$stmt = $db->query($que);
    }
	public function getfeedback($user_id)
	{
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
    //add INNER JOIN art_artworks_listing_transaction at ON at.listing_pay_id = awp.id  for changes in transaction (23 may 2012)
        $que = 'SELECT at.transaction_date, awp.amount, aw.frame, aw.title,aw.category, 
				aw.price, au.id, au.first_name, au.last_name, awim.image,aw.id as wid,
				aw.subject,awp.bidder_id
    			FROM art_artworks_listing_bid_payment awp 
				INNER JOIN art_artworks_listing_transaction at ON at.listing_pay_id = awp.id 
    			INNER JOIN art_artworks aw ON aw.id = awp.artwork_id 
    			INNER JOIN art_user au ON au.id = awp.bidder_id 
    			INNER JOIN art_artworks_images awim ON awim.artwork_id = awp.artwork_id 
    			WHERE (aw.owner_id ="'.$user_id.'"
				OR awp.bidder_id ="'.$user_id.'")
				AND at.status=1
				AND type="seller"
    			GROUP BY awp.id
       			ORDER BY at.transaction_date DESC';//die;     				 
		$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		
		return $result;
	}
	public function artworkposne($user_id)
	{
		$n=0;$p=0;$nu=0;
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
	//add INNER JOIN art_artworks_listing_transaction at ON at.listing_pay_id = awp.id  for changes in transaction  (23 may 2012)
		$que = 'SELECT aw.id as id,aw.title
    			FROM art_artworks_listing_bid_payment awp
				INNER JOIN art_artworks_listing_transaction at ON at.listing_pay_id = awp.id  
    			INNER JOIN art_artworks aw ON aw.id = awp.artwork_id 
    			INNER JOIN art_user au ON au.id = awp.bidder_id 
    			INNER JOIN art_artworks_images awim ON awim.artwork_id = awp.artwork_id 
    			WHERE (aw.owner_id ="'.$user_id.'"
				OR awp.bidder_id ="'.$user_id.'")
				AND at.status=1
				AND type="seller"
    			GROUP BY awp.id
       			ORDER BY at.transaction_date DESC';//die;  
    	$stmt = $db->query($que);
		$result = $stmt->fetchAll();
		//echo '<pre>';print_r($result); 
		foreach ($result as $value) {
	    		$aid=$value['id'];
				$quen = 'SELECT count(*) negative
				FROM art_artworks_feedbacks_review 
				WHERE artwork_id ='.$aid.' and  feedback_review=2';//die;
				$stmtn = $db->query($quen);
				$resultn = $stmtn->fetchAll(); //print_r($resultn);
				 $negative=$resultn[0]['negative'];
				$quep = 'SELECT count(*) positive
				FROM art_artworks_feedbacks_review 
				WHERE artwork_id ='.$aid.' and  feedback_review=1';//die;
				$stmtp = $db->query($quep);
				$resultp = $stmtp->fetchAll(); //print_r($resultp);
				 $positive=$resultp[0]['positive'];
				if($negative>$positive)
				$n++;
				else if($negative<$positive)
				$p++;
				else if($negative==$positive)
				$nu++;
	    } 
		$result1[]= $n.':'.$p.':'.$nu;
		return $result1;
    
	}
	public function artworkratestatus($aid)
	{
				$db = Zend_Db_Table_Abstract::getDefaultAdapter();
				$quen = 'SELECT count(*) negative
				FROM art_artworks_feedbacks_review 
				WHERE artwork_id ='.$aid.' and  feedback_review=2';//die;
				$stmtn = $db->query($quen);
				$resultn = $stmtn->fetchAll(); //print_r($resultn);
				$negative=$resultn[0]['negative'];
				$quep = 'SELECT count(*) positive
				FROM art_artworks_feedbacks_review 
				WHERE artwork_id ='.$aid.' and  feedback_review=1';//die;
				$stmtp = $db->query($quep);
				$resultp = $stmtp->fetchAll(); //print_r($resultp);
				 $positive=$resultp[0]['positive'];
				if($negative>$positive)
				return 'negativeIconSmall.png';
				else if($negative<$positive)
				return 'checkIconSmall.png';
				else if($negative==$positive)
				return 'okIconSmall.png';
	}
####-------------------------------END---------------------------------------####
 
 public function deleteimageprofileByName($image) {
		
		$userArray=array('profile_image'=>'');
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $db->update('art_user_details', $userArray, "profile_image = '".$image."'");
		

	}	
}

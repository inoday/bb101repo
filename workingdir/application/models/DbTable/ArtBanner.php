<?php 
   
class Application_Model_DbTable_ArtBanner extends Zend_Db_Table_Abstract
{

    protected $_name = 'art_banner';
    protected $_dbTable;

    /**

   @	Added By : Reeta
   @	Added On :	20-12-2011	

   @
   **/	
    public function getBannerlist($id)  
    {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt = $db->query('SELECT *,ab.id abid FROM art_banner ab, art_banner_pagename abp where abp.id=ab.page_id and ab.id="'.$id.'" ORDER BY 1');
        $row = $stmt->fetchAll();
		    return $row[0];  
       
  
    }
	
	 public function getBannerlistByPage($id)  
    {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$stmt = $db->query('SELECT *,ab.id abid FROM art_banner ab, art_banner_pagename abp where abp.id=ab.page_id and ab.page_id="'.$id.'" ORDER BY 1');
        $row = $stmt->fetchAll();
		    return $row[0];  
       
  
    }
	 /**
   @	Added By :Reeta
   @	Added On :	03-02-2012	
    @
   **/	
    public function getBannerlistName($id,$cat_name) 
    { 
        $id = (int)$id;
		if($id>0 && $cat_name!='')
		{ 
		$db = $this->getDbTable();
		$where[] = $db->getAdapter()->quoteInto('id <> ?', $id);
		$where[] = $db->getAdapter()->quoteInto('cat_name = ?', $cat_name);
		$row = $this->fetchAll($where);
		}
		else if($cat_name!='')
		{ $row = $this->fetchAll($this->select()->where('cat_name = ?', $cat_name)); }
		else if($id>0)
		{
			$row = $this->fetchAll($this->select()->where('id = ?', $id)); }
        if (!$row) {
            throw new Exception("Count not find row $id");
        }
        return $row->toArray();    
    }
   /**

   @	Added By : Reeta
   @	Added On :	20-10-2011	
	@
   **/ 
    
	public function addbannerlist($dbField)
    {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $dbField;
        $this->insert($dbField);
    }
    
    public function updatebannerlist($id,$dbField)
    {
       $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $dbField;
       $artwork_id = $this->update($dbField,'id = '. (int)$id);
    }
	
public function updateBannerliststatus($id,$status)
    {
        $data = array(
            'publish'		=> $status
			
        );
        $this->update($data,'id IN ('.$id.') ');
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
    public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_ArtBanner');
        }
        return $this->_dbTable;
    }

   public function deletebannerlist($id)
    { 
	   //echo'hi'. $id;die;
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('id in (?)', $id); 
		return $this->delete($where);
    }

	public function getallbanner($searchelement,$sortBy) {
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();

		if($searchelement=='')
		$sql='SELECT *,ab.id abid FROM art_banner ab, art_banner_pagename abp where abp.id=ab.page_id';
		if($searchelement!='')
		$sql.=' and page_name LIKE "%'.$searchelement.'%"';
		if($sortBy!='')
		$sql.=' ORDER BY '.$sortBy.'';

		$stmt = $db->query($sql);
        $row = $stmt->fetchAll();
        return $row;
	}
}


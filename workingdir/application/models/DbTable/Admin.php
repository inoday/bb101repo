<?php
class Application_Model_DbTable_Admin extends Zend_Db_Table_Abstract
{ 
	protected $_name = 'art_admin';
    protected $_dbTable;
	
 	public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Artuser');
        }
        return $this->_dbTable;
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
	
    ####---------------INSERT Data in art_user_registration Table----------------####
    
	public function updatepass($email,$newpass)
    {  
		//$db = $this->getDbTable();	
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select = $this->select()->where('email = ?', $email);
		$result = $this->fetchAll();
		$rec	= $result->toArray(); 
		$id=$rec[0]['id'];
	 	$data = array(
			'password'	 => $newpass,
			'modify_date'=>date('Y-m-d H:i:s')
        );
		$date=date('Y-m-d H:i:s');
		$res=$db->query('Update art_admin set password="'.$newpass.'" , modify_date="'.$date.'" where id="'.$id.'"'); 
    }
    
    ####-------------------------------END---------------------------------------####
	
	///////////for Login//////////////////////////////////////
	public function chkpass($email,$pass)
    { 	
		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select = $this->select()->where('email = ?', $email)->where('password = ?', $pass);
		$result = $this->fetchAll($select)->count();
		$loginArray[0] = $result;
		return $loginArray[0] ;
    }
    
    ####------------------------ Function used on admin home page ---------------------------####
	
    public function getTopArtwork() {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$makeQue = "SELECT id, title FROM art_artworks
    				WHERE art_artworks.delete = 0 AND sold_status = 0 AND approval_status = 1 AND payment_status = 1
    				ORDER BY user_views DESC, rate_count DESC LIMIT 0,10";
	  	$stmt = $db->query($makeQue);
        $rows = $stmt->fetchAll();
        return $rows;
    }
    
    public function getLatestArtwork() {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	 $makeQue = "SELECT id, title FROM art_artworks
    				WHERE art_artworks.delete = 0 AND sold_status = 0
    				ORDER BY create_date DESC LIMIT 0,10";//die;
	  	$stmt = $db->query($makeQue);
        $rows = $stmt->fetchAll();
        return $rows;
    }
    
	public function getLatestArtworkLive() {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$duration = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
    	   	
    	$makeQue = "SELECT id, title FROM art_artworks
    				WHERE art_artworks.delete = 0 AND sold_status = 0 AND approval_status = 1 AND payment_status = 1 AND 
    				DATEDIFF(CURDATE(),bid_start_date) < auction_duration 
    				ORDER BY create_date DESC LIMIT 0,10";//die;
	  	$stmt = $db->query($makeQue);
        $rows = $stmt->fetchAll();
        return $rows;
    }
    
	public function getTopSeller() {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$makeQue = "SELECT COUNT( P.artwork_id ), U.id, U.first_name, U.last_name 
		FROM art_artworks_listing_bid_payment P 
		INNER JOIN art_artworks AW ON AW.id = P.artwork_id 
		INNER JOIN art_user U ON AW.owner_id = U.id GROUP BY AW.owner_id ORDER BY 1 DESC LIMIT 0 , 10";//die;
	  	$stmt = $db->query($makeQue);
        $rows = $stmt->fetchAll();
        return $rows;
    }
    
    public function getTopBuyer() {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$makeQue = "SELECT COUNT(P.bidder_id), P.bidder_id, U.id, U.first_name
					FROM art_artworks_listing_bid_payment P 
					INNER JOIN art_user U ON U.id = P.bidder_id
					GROUP BY P.bidder_id 
					ORDER By 1 DESC LIMIT 0,10";
	  	$stmt = $db->query($makeQue);
        $rows = $stmt->fetchAll();
        return $rows;
    }
        
    public function getLatestUser() {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$makeQue = "SELECT id, first_name, last_name FROM art_user
    				ORDER BY create_date DESC LIMIT 0,10"; 
	  	$stmt = $db->query($makeQue);
        $rows = $stmt->fetchAll();
        return $rows;
    }
    ####-------------------------------END---------------------------------------####
}
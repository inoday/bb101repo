<?php
   
class Application_Model_DbTable_Categorylist extends Zend_Db_Table_Abstract
{

    protected $_name = 'art_category';
    protected $_dbTable;

    /**
   @	Add Faq
   @	Added By : Anil Rawat
   @	Added On :	20-10-2011	
   @	Input: void
   @	Return: reture the value from thr table.
   @
   **/	
    public function getCategorylist($id) 
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Count not find row $id");
        }
		//echo'<pre>';print_r( $row);die;
        return $row->toArray();    
    }
	 public function getCategorylistBysectionid($sid) 
    {
        $id = (int)$sid;
        $row = $this->fetchAll('section_id = ' . $id);
        if (!$row) {
            throw new Exception("Count not find row $id");
        }
		//echo'<pre>';print_r( $row->toArray());die;
        return $row->toArray();    
    }
   /**
   @	Add Faq
   @	Added By : Anil Rawat
   @	Added On :	20-10-2011	
   @	Input:
	@			question( String)
	@			answer (String)
	@			addDate (date)
	@	Return: void
	@
   **/ 
    
	public function addCategorylist($section_id,$category_name,$category_description,$addDate)
    {
        $data = array(
		'section_id'		=> $section_id,
            'category_name'		=> $category_name,
			'category_description'   	=> $category_description,
			'add_date'  	=> $addDate,
			
        );
        $this->insert($data);
    }
    
    public function updateCategorylist($id,$section_id,$category_name,$category_description)
    {
        $data = array(
		'section_id'		=> $section_id,
            'category_name'		=> $category_name,
			'category_description'   	=> $category_description
			
        );
        $this->update($data,'id = '. (int)$id);
    }
	
public function updateCategoryliststatus($id,$status)
    {
        $data = array(
            'status'		=> $status
			
        );
        $this->update($data,'id IN ('.$id.') ');
    }
	
	public function setDbTable($dbTable)
    {
		if (is_string($dbTable)) 
		{
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
		throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;

		return $this;
    }
    
    public function getDbTable()
    {
		if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_Categorylist');
        }
        return $this->_dbTable;
    }

   public function deletecategorylist($id)
   { 
		$db = $this->getDbTable();
		$where	=	$db->getAdapter()->quoteInto('id IN ('.$id.') '); 
		return $this->getDbTable()->delete($where);
   }
    
   public function getCatSubject($cat_id) {
    	$makeQue = 'SELECT id, cat_id, subject
					FROM art_artwork_cat_subject 
					WHERE cat_id="'.$cat_id.'"';
		
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
	 	$stmt = $db->query($makeQue);
        $result = $stmt->fetchAll();
		return $result;
   }
}


<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {   
    	$namespace = new Zend_Session_Namespace(); 
    	$this->userInfo	= new Application_Model_DbTable_Profile();
		$this->country	= new Application_Model_DbTable_ArtCountry();
		$this->message	= new Application_Model_ArtInbox();
		$namespace 		= new Zend_Session_Namespace();
		$user   	 	= new Application_Model_ArtGroup();
		
		if ($namespace->email){ 
			$row=$user->getMemberID($namespace->email);
			$user_id=$row[0]['id'];//die;
			$namespace->userid= $row[0]['id'];
		}

		//include('http://localhost/artex/public/artworkdetails/checkauction');
		
    }
	
    public function indexAction()
    { 
		$this->_helper->layout->setLayout('layout'); 
		$_SESSION['msg'] = '';
		$Loginform = new Application_Form_Userlogin();
        $this->view->Loginform = $Loginform;
   
        $ip = $this->getRequest()->getServer('REMOTE_ADDR');
        $check = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
        echo '<pre>';print_r($check);die;
        if ($check[0]['status']==0) {
			$ipArray = explode(',',$check[0]['enable_ip']);
			if (!in_array($ip, $ipArray)) {
		    	$this->_redirect('Index/undermaintanance');
			}
		} 
		
        /*
		 @ Call getartlist() function to show artwork on index page
		 @ Add by : Abhishek
		 @ On: 15-02-2012
		**/ 
        $this->view->getartlist = $this->getartlist(); 
		$bannerlist_model   = new Application_Model_DbTable_ArtBanner();
		$this->view->getbannerlist = $bannerlist_model->getBannerlistByPage(3);//die;
///////////////////////////Artwork's max  bids////////////////////////////////////		 
	}
	
	public function userauthenticationAction() {
		$namespace = new Zend_Session_Namespace();
		$request  = $this->getRequest()->getParams();
		$authcode = $request['authcode']; 
		$dbField = array();
		$dbField = array('active' => '1');
		$auth = new Application_Model_Authentication();
		$value = $auth->activateuser($dbField,$authcode);
		//print_r($value); die;
		
		// get user data
				$userData = $auth->getUserDataByauthcode($authcode);

				//$namespace->useremail 	= $userData[0]['email'];
				$namespace->email		= $userData[0]['email'];
				$namespace->fname 		= $userData[0]['firstname']; 
				$namespace->lname       = $userData[0]['lastname'];
				
				$namespace->useremail 	= $userData[0]['id'];
				$namespace->userdetails[0]['id'] = $userData[0]['id'];
				$this->_helper->redirector->gotosimple('welcome','Profile',true,array('authcode'=>$request['authcode'])) ;
				//$namespace->logouturl	= $logoutUrl;
	}
	
	public function thanksAction() {
	
		$namespace = new Zend_Session_Namespace();
		
	}
	
	public function facebookAction() {
		
	}
	
	/*
	 @ Fetch artwork from art_artworks table to show on index page
	 @ Add by : Abhishek
	 @ On: 15-02-2012
	**/
	
	private function getartlist() {
		$artarray = array();
		$artwork 	 = new Application_Model_DbTable_Artlisting();
		
		$artarray[0] = $artwork->endsoon();
		$artarray[1] = $artwork->recentlyadded();
		$artarray[2] = $artwork->toprated();
		$artarray[3] = $artwork->topartists();
		
		return $artarray;
	}

	public function reindexAction()
    	{ 
		$this->_helper->layout->disableLayout();
		$_SESSION['msg'] = '';
		$Loginform = new Application_Form_Userlogin();
        $this->view->Loginform = $Loginform;
        
        /*
		 @ Call getartlist() function to show artwork on index page
		 @ Add by : Abhishek
		 @ On: 15-02-2012
		**/
        $this->view->getartlist = $this->getartlist();
		$bannerlist_model   = new Application_Model_DbTable_ArtBanner();
		$this->view->getbannerlist = $bannerlist_model->getBannerlistByPage(3);
///////////////////////////Artwork's max  bids////////////////////////////////////		 
	}
	
	public function undermaintananceAction() {
		$this->_helper->layout->disableLayout();
		$check = Application_Model_DbTable_ArtGlobelinfo::getglobelinfoAll();
        
        if ($check[0]['status']==1) {
			 
		    	$this->_redirect('Index/index');
			 
		} 
		echo 'Our site is under maintenance. Please open after some time';
	}
	
	public function retopscrollAction()
	    { 
		$this->_helper->layout->disableLayout();
		$_SESSION['msg'] = '';
		$Loginform = new Application_Form_Userlogin();
        $this->view->Loginform = $Loginform;
        
        /*
		 @ Call getartlist() function to show artwork on index page
		 @ Add by : Abhishek
		 @ On: 15-02-2012
		**/
        $this->view->getartlist = $this->getartlist();
		$bannerlist_model   = new Application_Model_DbTable_ArtBanner();
		$this->view->getbannerlist = $bannerlist_model->getBannerlistByPage(3);
///////////////////////////Artwork's max  bids////////////////////////////////////
		 
		 
		}
	/////////////////////////////////////ADD Subscribe///////////////////////////////////////
	public function subscribeAction()
	{ 
		$request	=	$this->getRequest()->getParams(); 

 ////////////////////////////////Find user already followed or not////////////////////////////////////////////////////
 			$subscribe   = new Application_Model_DbTable_ArtSubscribe();
			$subscribe_result = $subscribe->fetchAll($subscribe->select()->where('email = ?', $request['subscribe_email']));	
			$this->view->watchlist_model_result = $subscribe_result;
			foreach($subscribe_result as $event){
				 $count=count($event); 
			}
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if($count<1)
	{
	$dbField = array( 'email' => $request['subscribe_email'], 'create_date'=>date("Y-m-d H:i:s"));
	$rec = $subscribe->addsubscribe($dbField);
	echo $errmsg = "Subscribed successfully.";
	}
	else
	{
	echo $errmsg = "Allready subscribed.";
	}
	 die();

	}
}

?>

<?php
     
class CategoryController extends Zend_Controller_Action
{
	///////////////////////////CATEGORY//////////////////////////
/*
@	show categorylist from the table
@	Add by : Reeta Verma
@		On: 20-01-2012 
**/
		public function categorylistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
			

			$categorylist_model   = new Application_Model_DbTable_ArtCategory();
			$categorylist_model_result = $categorylist_model->fetchAll();	
			
			
			////////////////////////////////////////////////////////////////////////////////////////
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			$page = $this->_getParam('page',1);
			//send the page no.
			$this->view->page = $page;			
			////////////////////////////////////////////////////////////////////////
			//////////////////////////////////////////////////////////////////////////////////////	
			//added by reeta (06-03-2012)
			////////////////////////////////////////////////////////////////////////////
			 $this->view->sortBy	=	$request['sortBy']; 
			if($request['searchelement']!='')
			{
				//serching all element with the table
				$searchelement	=	$request['searchelement'];
				//show the search element on the search box
				$namespace->psquestion = $request['searchelement'];
				$this->view->psquestion = $namespace->psquestion;	
				if($request['sortBy']!='')
			{
				$categorylist_model_result = $categorylist_model->fetchAll($categorylist_model->select()->where('cat_name Like ?', '%'.$searchelement.'%')->order($request['sortBy']));	
			}
			else
				$categorylist_model_result = $categorylist_model->fetchAll($categorylist_model->select()->where('cat_name Like ?', '%'.$searchelement.'%'));	
		}
		else if($request['sortBy']!='')
			{
				$categorylist_model_result = $categorylist_model->fetchAll($categorylist_model->select()->where('cat_name Like ?', '%'.$searchelement.'%')->order($request['sortBy']));	
			}


			
		$this->view->categorylist_model_result = $categorylist_model_result;
		//end added by reeta (06-03-2012)
			/////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////
			$page = $this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($categorylist_model_result);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			$this->view->paginator = $paginator;
		}
		/*
@	add & edit Category from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/		
	public function addcategorylistAction(){
		
		$this->_helper->layout->setLayout('adminlayout');
	
		
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		
		$this->view->adminemail = $adminemail;
		//if admin session is logout it's go to index page
		if($adminemail == ''){
			$this->_helper->redirector('index');
		}

		$id = $this->_getParam('id', 0);
			if($id==0)
		$this->view->bodyCopy = "Add Category";
		else
		$this->view->bodyCopy = "Edit Category";
	    $form = new Application_Form_ArtCategory();
		
        $this->view->form = $form;
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getParams();
			 
            if ($form->isValid($formData)) {
             	
				$category_name   = $form->getValue('cat_name'); 
				//$lft    		 = $form->getValue('lft');
				//$rgt    		 = $form->getValue('rgt');
				$addDate 		 = date("Y-m-d H:i:s");
				///////////////////////////Check unique cat name//////////////////////////////////////
				$Categorylist_model   = new Application_Model_DbTable_ArtCategory();
				$Categorylist_model_result = $Categorylist_model->getCategorylistName($id,$category_name);	
				$catarr=$this->view->Categorylist_model_result = $Categorylist_model_result;
				//echo "<pre>";print_r($catarr);echo count($catarr);die();
				if(count($catarr)>0)
				{
					$msg	=	'Category already added. Try again';
					$this->_helper->redirector->gotosimple('categorylist','category',true,array('msg'=>$msg,'sec'=>$section_id)) ;
				}
				////////////////////////////////////////////////////////////////////////////////////////////////
				$tutorial   	 = new Application_Model_DbTable_ArtCategory();
			
				if($id==0){
					//add the 'Category'
					$tutorial->addCategorylist($category_name,$addDate);
					$msg	=	'Category added successfully.';
					$this->_helper->redirector->gotosimple('categorylist','category',true,array('msg'=>$msg,'sec'=>$section_id)) ;
				}
				else {
				    //edit the 'Category'
				  $tutorial->updateCategorylist($id,$category_name,$addDate);
				  $msg	=	'Category update successfully.';
					$this->_helper->redirector->gotosimple('categorylist','category',true,array('msg'=>$msg,'sec'=>$section_id)) ;
				}
				
            } else {
                $form->populate($formData);
            }
        }
		
		$id = $this->_getParam('id', 0);
				
		if ($id > 0) {
			$categorylist = new Application_Model_DbTable_ArtCategory();
			
			//getting the 'Category' for edit	
			foreach ($categorylist->getCategorylist($id) as $key => $value) {
				$record[$key]	=	stripslashes($value);
			}
			$form->populate($record);
			
		}
	}
	
	/*
@	Delete Category from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/		
	public function  deletecategorylistAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		$categorylist			=	new Application_Model_DbTable_ArtCategory();
		$deletecategorylist		=	$categorylist->deletecategorylist($id);
		$this->view->deletecategorylist	=	$deletecategorylist;
		
		$msg	=	'Category deleted successfully.';
		$this->_helper->redirector->gotosimple('categorylist','category',true,array('msg'=>$msg)) ;

	}	
/*
@	
@	Add by : Reeta Verma
@		On: 21-01-2012
**/		
	public function  updatecategoryliststatusAction()
	{ 	
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		$status			=	$request['status'];
        
			$user			=	new Application_Model_DbTable_ArtCategory();
			$updateCategoryliststatus		=	$user->updateCategoryliststatus($id,$status);
			$this->view->updateCategoryliststatus	=	$updateCategoryliststatus;
			
			if($status=='1')
			$msg	=	'Published successfully.';
			else
			$msg	=	'Unpublished successfully.';
			$this->_helper->redirector->gotosimple('categorylist','category',true,array('msg'=>$msg)) ;

	}	
/*
@	
@	Add by : Reeta Verma
@		On: 23-01-2012
**/		
	public function viewcategorylistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
	
			////////////////////////////////////////////////////////////////////////////////////////
			$id = $this->_getParam('id', 0);	
			$categorylist_model   = new Application_Model_DbTable_ArtCategory();
			$categorylist_model_result = $categorylist_model->fetchAll($categorylist_model->select()->where('id = ?', $id));	
			$this->view->categorylist_model_result = $categorylist_model_result;
			
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
}
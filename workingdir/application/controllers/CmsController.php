<?php
/*
@	controller for 'Manage Cms management' module
@	Add by : Reeta 
@		On: 25-1-2012
**/   

class CmsController extends Zend_Controller_Action
{    
     
     public function init()
    {   
	    
      	$namespace = new Zend_Session_Namespace();
		 
		
    }


    
/* 
@	show cmslist from the table
@	AAdd by : Reeta 
@		On: 25-1-2012
**/    
		public function cmssectionAction()
		{ 
            $this->_helper->layout->setLayout('layout');
			
			
			$id = $this->_getParam('page', 0);		
			$cmslist_model   = new Application_Model_DbTable_Sectionlist();
			$cmslist_model_result = $cmslist_model->fetchAll($cmslist_model->select()->where('id = ?', $id)->where('status = ?', 1));	
			//echo "<pre>";print_r($cmslist_model_result); die('es');
			$this->view->cmslist_model_result = $cmslist_model_result;
			/////////////get category list through sectionid////////////////////////////////////////
			$cat = $this->_getParam('cat', 0);	
			$categorylist_model   = new Application_Model_DbTable_Categorylist();
			$categorylist_model_result = $categorylist_model->fetchAll($categorylist_model->select()->where('section_id = ?', $id)->where('status = ?', 1));	
			$this->view->categorylist_model_result = $categorylist_model_result;
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}

		public function cmscategoryAction()
		{ 
            $this->_helper->layout->setLayout('layout');
			
			

			$id = $this->_getParam('page', 0);	
			$sec = $this->_getParam('sec', 0);
			$categorylist_model   = new Application_Model_DbTable_Categorylist();
			$categorylist_model_result = $categorylist_model->fetchAll($categorylist_model->select()->where('id = ?', $id));	
			$this->view->categorylist_model_result = $categorylist_model_result;
				
			/////////////get section name through sectionid////////////////////////////////////////
			$sectionlist_model   = new Application_Model_DbTable_Sectionlist();
			$sectionlist_model_result = $sectionlist_model->fetchAll($sectionlist_model->select()->where('id = ?', $sec)->where('status = ?', 1));	
			$this->view->sectionlist_model_result = $sectionlist_model_result;
			/////////////get sub category list through categoryid////////////////////////////////////////
			$subcategorylist_model   = new Application_Model_DbTable_Subcategorylist();
			$subcategorylist_model_result = $subcategorylist_model->fetchAll($subcategorylist_model->select()->where('category_id = ?', $id)->where('status = ?', 1));	
			$this->view->subcategorylist_model_result = $subcategorylist_model_result;
			$request  =	$this->getRequest()->getParams();
			
			
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}
		
		public function cmssubcategoryAction()
		{ 
            $this->_helper->layout->setLayout('layout');
			
			

			$id = $this->_getParam('page', 0);	
			$cat = $this->_getParam('cat', 0);
			$sec = $this->_getParam('sec', 0);
			$sectionlist_model   = new Application_Model_DbTable_Sectionlist();
			$sectionlist_model_result = $sectionlist_model->fetchAll($sectionlist_model->select()->where('id = ?', $sec)->where('status = ?', 1));	
			$this->view->sectionlist_model_result = $sectionlist_model_result;
			////////////////////////////////////////////////////////////////////////////////////
			$categorylist_model   = new Application_Model_DbTable_Categorylist();
			$categorylist_model_result = $categorylist_model->fetchAll($categorylist_model->select()->where('id = ?', $cat)->where('status = ?', 1));	
			$this->view->categorylist_model_result = $categorylist_model_result;
			///////////////////////////////////////////////////////////////////////////////
			$subcategorylist_model   = new Application_Model_DbTable_Subcategorylist();
			$subcategorylist_model_result = $subcategorylist_model->fetchAll($subcategorylist_model->select()->where('id = ?', $id)->where('status = ?', 1));	
			$this->view->subcategorylist_model_result = $subcategorylist_model_result;
			$request  =	$this->getRequest()->getParams();
			
			
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}

}
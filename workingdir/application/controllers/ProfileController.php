<?php

class ProfileController extends Zend_Controller_Action
{	
    public function init()
    {   
    	$this->namespace= new Zend_Session_Namespace(); 
    	$this->request  = $this->getRequest()->getParams();
    	$this->userInfo	=	new Application_Model_DbTable_Profile();
		$this->country	=	new Application_Model_DbTable_ArtCountry();
		$this->baseUrl  = $this->getRequest()->getbaseUrl(); 
		$this->message	=	new Application_Model_ArtInbox();	
		if($this->request['action']=='userprofile' && $this->request['id']==$this->namespace->userdetails[0]['id'])
		$this->_redirect('Profile/welcome');
 		$this->view->action=$this->request['action'];
		$this->view->ind=$this->request['ind'];
    }
    
	################################ Fetch user info and Update profile By Abhishek ##############################
    public function indexAction()
    { 		
		if($this->namespace->email=='')
		{ 
			$this->_redirect('index/index');
		}

		$this->view->country = $this->country->getcountryName();
		
		$userArray = array();
		$detailsArray = array();
		
		################################ Update user profile By Abhishek ##############################
		if (isset($this->request['userType'])) {
			
			$userid 								= $this->request['user_id'];
			$userArray['first_name'] 				= $this->request['fname'];
			$userArray['last_name'] 				= $this->request['lname'];
			$userArray['email'] 					= $this->request['email'];
			$userArray['group_id'] 					= $this->request['userType'];
			if($this->request['password']!='') {
			$userArray['password'] 					= md5($this->request['password']);
			$userArray['original_password'] 			= $this->request['password'];
			$this->namespace->passowrd				= md5($this->request['password']);
			} 
			$detailsArray['street'] 				= $this->request['street'];
			$detailsArray['house_no'] 				= $this->request['house_no'];
			$detailsArray['country'] 				= $this->request['country'];
			$detailsArray['newsletter']				= isset($this->request['newsletter']) ? $this->request['newsletter'] : 'no';
			$detailsArray['city']					= $this->request['city'];
			$detailsArray['zip'] 					= $this->request['zip'];
			$detailsArray['dob'] 					= $this->request['dob'];
			$detailsArray['biography'] 				= $this->request['biography'];
			 
			############################# Upload image ##################################
			/*$path='../public/images/profile_image';
			require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "setup.php";
			$renameFilter = new Zend_Filter_File_Rename( $path );
			$adapter = new Zend_File_Transfer();
 					
			$adapter->setDestination($path);  // set path for upload folder
			$adapter->setOptions(array('ignoreNoFile' => true));	// remove validation to upload all files
			$files = $adapter->getFileInfo();
	  		$imageName = $userid.'_'.$userArray['first_name'].'_'.$files['profile_image']['name'];
			if ($files['profile_image']['name']!=''){
				$renameFilter->addFile( array('source' => $files['profile_image']['tmp_name'], 'target' => $imageName, 'overwrite' => true ) );// rename file on desination folder
				$adapter->addFilter($renameFilter);
				$adapter->receive();
				
				$filter   = new Zend_Filter_ImageSize();
				$output_s = $filter->setHeight(50)
						    ->setWidth(50)
						    ->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
						    ->setThumnailDirectory('../public/images/profile_image/small/') 
						    ->filter('../public/images/profile_image/'.$imageName);
						
				$output_l = $filter->setHeight(150)
						    ->setWidth(150)
						    ->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
						    ->setThumnailDirectory('../public/images/profile_image/large/')
						    ->filter('../public/images/profile_image/'.$imageName);
			
				$detailsArray['profile_image']	= $imageName;
			}*/
	  		 
			############################# END Upload image##################################
			 $detailsArray['profile_image']	= $this->namespace->filename[0];			 	 
			$this->userInfo->updateuser($userArray,$detailsArray,$userid);
			$this->view->msg = 'Your profile updated successfully.'; 
			$this->_helper->redirector->gotosimple('welcome','Profile',true,array('msg'=>$msg)) ;
		}
		################################ END Update user profile ##############################
		
		$this->view->getusertype = $this->userInfo->getUsertype();
		$getInfo = 	$this->userInfo->getUserdetails($this->namespace->email, $this->namespace->passowrd);
		$this->view->details = $getInfo;
		
		$this->view->countInbox = $this->message->countInboxMsg($this->namespace->userdetails[0]['id']);
		$this->view->bidding = $this->userInfo->bidding($this->namespace->userdetails[0]['id']); // Show bidding item
		$this->view->liveart = $this->userInfo->liveart($this->namespace->userdetails[0]['id']); // Show live artwork
		$this->view->watchlist = $this->userInfo->watchlist($this->namespace->userdetails[0]['id']); // Show watchlist
		$this->view->feedback = $this->userInfo->feedback($this->namespace->userdetails[0]['id']); // Show feedback count on artwork
		
	}
	################################ END Fetch user info and Update profile ##############################
	
	public function accountAction() {
		//print_r($_REQUEST);
		if($this->namespace->email=='')
		{ 
			$this->_redirect('index/index');
		}
		$form = new Application_Form_Artworkuseraccount();
        $this->view->form = $form; 
		$this->view->country = $this->country->getcountryName();
		if (isset($this->request['submit'])) {
			
			$userid 								= $this->namespace->userdetails[0]['id'];
						
			$detailsArray['billing_address'] 		= $this->request['baddress'];
			$detailsArray['billing_country'] 		= $this->request['bcountry'];
			$detailsArray['billing_city'] 			= $this->request['bcity'];
			$detailsArray['billing_state'] 			= $this->request['bstate'];
			$detailsArray['billing_zip'] 			= $this->request['bzip'];
			$detailsArray['billing_contact_number'] = $this->request['bphone'];
			$detailsArray['shipping_address'] 		= $this->request['saddress'];
			$detailsArray['shipping_country'] 		= $this->request['scountry'];
			$detailsArray['shipping_city'] 			= $this->request['scity'];
			$detailsArray['shipping_state']			= $this->request['sstate'];
			$detailsArray['shipping_zip'] 			= $this->request['szip'];
			$detailsArray['shipping_contact_number']= $this->request['sphone'];
			$detailsArray['paypal_username']		= $this->request['paypal_username'];
			$detailsArray['paypal_password']		= $this->request['paypal_password'];
			$detailsArray['paypal_signature']		= $this->request['paypal_signature'];
			$detailsArray['paypal_email']			= $this->request['pemail'];
			$detailsArray['credit_card']			=$this->request['creditcardtype'];
			$detailsArray['credit_card_number']		=$this->request['creditcardnumber'];
			$detailsArray['exp_month']				=$this->request['exp_date_month'] ;
			$detailsArray['exp_year']				=$this->request['exp_date_year'];
			$detailsArray['cvv2']					=$this->request['cvv_number'];
			$detailsArray['modified_date']			=date("Y-m-d H:i:s");
			
			$getInfo = 	$this->userInfo->updateuserDetails($detailsArray,$userid);
			$this->view->msg = 'Your account updated successfully.'; 
		}
		$getInfo = 	$this->userInfo->getUserdetails($this->namespace->email, $this->namespace->passowrd);//print_r($getInfo);die;
		$record['baddress']			=	$getInfo[0]['billing_address'];
		$record['bcity']			=	$getInfo[0]['billing_city'];
		$record['bstate']			=	$getInfo[0]['billing_state'];
		$record['bzip']				=	$getInfo[0]['billing_zip'];
		$record['bcountry']			=	$getInfo[0]['billing_country'];
		$record['bphone'] 			= 	$getInfo[0]['billing_contact_number'];
		$record['saddress'] 		= 	$getInfo[0]['shipping_address'];
		$record['scountry'] 		= 	$getInfo[0]['shipping_country'];
		$record['scity'] 			= 	$getInfo[0]['shipping_city'];
		$record['sstate']			= 	$getInfo[0]['shipping_state'];
		$record['szip'] 			= 	$getInfo[0]['shipping_zip'];
		$record['sphone']			= 	$getInfo[0]['shipping_contact_number'];
		$record['paypal_username']	= 	$getInfo[0]['paypal_username'];
		$record['paypal_password']	= 	$getInfo[0]['paypal_password'];
		$record['paypal_signature']	= 	$getInfo[0]['paypal_signature'];
		$record['pemail']			= 	$getInfo[0]['paypal_email'];
		$record['creditcardtype'] 	= 	$getInfo[0]['credit_card'];
		$record['creditcardnumber']	= 	$getInfo[0]['credit_card_number'];
		$record['exp_date_month'] 	= 	$getInfo[0]['exp_month'];
		$record['exp_date_year']	= 	$getInfo[0]['exp_year'];
		$record['cvv_number']		= 	$getInfo[0]['cvv2'];
		
		$form->populate($record);
		$this->view->countInbox = $this->message->countInboxMsg($this->namespace->userdetails[0]['id']);
		$this->view->bidding = $this->userInfo->bidding($this->namespace->userdetails[0]['id']); // Show bidding item
		$this->view->liveart = $this->userInfo->liveart($this->namespace->userdetails[0]['id']); // Show live artwork
		$this->view->watchlist = $this->userInfo->watchlist($this->namespace->userdetails[0]['id']); // Show watchlist
		$this->view->feedback = $this->userInfo->feedback($this->namespace->userdetails[0]['id']); // Show feedback count on artwork
		
	}
	
	public function userprofileAction() {
		

		$this->view->userInfom = $this->userInfo->getUserProfile($this->request['id']);
		$this->view->userArt = $this->userInfo->getUserArt($this->request['id']);
		$this->view->userid = $this->request['id'];
		$loginID = isset($this->namespace->userdetails[0]['id']) ? $this->namespace->userdetails[0]['id'] : '';
		$this->view->loginmember_id = $loginID;
		$this->view->msg = isset($this->request['msg']) ? $this->request['msg'] : '';
		$this->view->action_name = $this->getRequest()->getActionName();

		################################ Profile commenting ON 03-03-2012 ##############################
				 
		$comments = $this->userInfo->getUserFeedbacks($this->request['id']);	
		$following = $this->userInfo->following($this->request['id']);
		 
		$follower =  $this->userInfo->follower($this->request['id']);
		$this->view->comments = $comments;
		$this->view->following = $following;
		$this->view->follower = $follower;
		$feedbackscomments = $this->userInfo->getArtworkfeedbacksComments($this->request['id']);	
		$this->view->feedbackscomments  = $feedbackscomments;
		################################ END Profile commenting ##############################
		################################ Recent activity on profile 13-03-2012 (Reeta verma) ##############################
		$activitylist = new Application_Model_DbTable_Artuseractivity();
		
		$activitylist_result = $activitylist->Artuseractivity();
		$this->view->activitylist_result  = $activitylist_result;	
		
		//////////////////////total transaction///////////////////////
		$total_soldmoney=0;$total_boughtmoney=0;
		$watchlist = $this->userInfo->getBoughtArt($this->request['id']);	
		$total_bought=count($watchlist); 
		
		for($i=0;$i<$total_bought;$i++)
		$total_boughtmoney=$total_boughtmoney+$watchlist[$i]['amount'];
		
		$sold = $this->userInfo->sold($this->request['id']);
		$total_sold=count($sold); 
		
		for($i=0;$i<$total_sold;$i++)
		$total_soldmoney=$total_soldmoney+$sold[$i]['amount'];
		$this->view->total_trans_money=$total_soldmoney+$total_boughtmoney;
		
		$this->view->total_tans=$total_sold+$total_bought;
		/////////////////////////feedback rating %////////////////////////////////////////////////
		$artworkposne = $this->userInfo->artworkposne($this->request['id']);	
		$arr=explode(':',$artworkposne[0]);
		//$total_bought=count($watchlist);
		
		$artworkuser = $this->userInfo->getfeedback($this->request['id']);	
		@$feedbackper=($arr[1]/count($artworkuser))*100;
		$this->view->feedbackper = number_format($feedbackper, 1, '.', '');
		$bannerlist_model   = new Application_Model_DbTable_ArtBanner();
		$this->view->getbannerlist = $bannerlist_model->getBannerlistByPage(1);
	}
	public function biographyAction() {
		
		$this->_helper->layout->disableLayout();
		$this->view->userInfo = $this->userInfo->getUserProfile($this->request['id']);
		$this->view->userArt = $this->userInfo->getUserArt($this->request['id']);
		$this->view->userid = $this->request['id'];
		$loginID = isset($this->namespace->userdetails[0]['id']) ? $this->namespace->userdetails[0]['id'] : '';
		$this->view->loginmember_id = $loginID;
		$this->view->msg = isset($this->request['msg']) ? $this->request['msg'] : '';
		$this->view->action_name = $this->getRequest()->getActionName();

		################################ Profile commenting ON 03-03-2012 ##############################
				 
		$comments = $this->userInfo->getUserFeedbacks($this->request['id']);	
		$following = $this->userInfo->following($this->request['id']);
		 
		$follower =  $this->userInfo->follower($this->request['id']);
		$this->view->comments = $comments;
		$this->view->following = $following;
		$this->view->follower = $follower;
		$feedbackscomments = $this->userInfo->getArtworkfeedbacksComments($this->request['id']);	
		$this->view->feedbackscomments  = $feedbackscomments;
		################################ END Profile commenting ##############################
		################################ Recent activity on profile 13-03-2012 (Reeta verma) ##############################
		$activitylist = new Application_Model_DbTable_Artuseractivity();
		
		$activitylist_result = $activitylist->Artuseractivity();
		$this->view->activitylist_result  = $activitylist_result;	
	}
	public function profilerecommendAction()
	{	
		$namespace 	= new Zend_Session_Namespace();
		if($this->namespace->email=='')
		{ 
			$this->_redirect('index/index');
		}
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$get_user	=	new Application_Model_DbTable_Registration();
 
		$getuser = $get_user->usrename($this->namespace->email,$this->namespace->passowrd);
		$login_user = $this->request['profile_user'];
		$user_id  = md5($this->request['user_id']); 
		$to  = $this->request['to']; 
		$message = $this->request['message'];
		$subject = $this->request['subject'];
		$senderName = $getuser[0]['first_name'].' '. $getuser[0]['last_name'];
 		//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
			/////////////GET user id from user email (store in session)////////////////////////////////////////////////
		$user   	 = new Application_Model_ArtGroup();
		$row=$user->getMemberID($namespace->email);
		$owner_id=$row[0]['id']; 
		$owner_name=$row[0]['first_name'].' '.$row[0]['last_name'];
		/////////////GET email from user id ////////////////////////////////////////////////
		$user   	 = new Application_Model_Artuser();
		$rid=$this->request['user_id'];
		$user_result = $user->selestuser($rid);	 
		$this->view->user_result = $user_result;
		foreach($user_result as $event){
			$reciever_name= $event['first_name'].' '.$event['last_name'];
		}
			

		$base_Url  = $this->getRequest()->getbaseUrl(); 
		$msg= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$owner_id.'">'.$owner_name.'</a> recommeded to <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$rid.'">'.$reciever_name.'\'s</a> profile'; 

		$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
		$dbField = array('user_id' => $owner_id, 'activity_msg' => $msg, 'create_date'=>date("Y-m-d H:i:s"));
		
		$rec_insert_activity = $insert_activity->addactivity($dbField);
			///////////////////////////////////////////////////////////////////////////////////
		//////////////////////Get mail content set by admin///////////////////////
		$getmailcontent	=	new Application_Model_ArtMailFormats();
		$mailcontent		=	$getmailcontent->getMailContentById(5);
		$formated_message=$mailcontent['content'];
		$formated_subject=$mailcontent['subject'];
		////////////////////////////////////////////////////////	
		$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$this->baseUrl;
		
		$message = sprintf($formated_message,$senderName,$login_user,$baseUrl,$user_id); 
		$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com');
		Zend_Mail::setDefaultTransport($tr);
		
		$mail = new Zend_Mail();
	 	$mail->setBodyHtml($message);
    	$mail->setFrom('admin@artex.com', 'www.artex.com');
    	$mail->addTo($to, 'Some Recipient');
    	$mail->setSubject($subject);
    	$mail->send($tr);
			
	   	$this->_helper->redirector->gotosimple('userprofile','profile',true,array('id'=>$this->request['user_id'], 'msg' => 'Your recommendation has been sent successfully.')) ;
		 
	}
	
	public function checkuserAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$user_id = $this->request['user_id']; 
		$check   = $this->userInfo->checkUser($user_id);
		if($check[0]['userid']!=0){
			$this->_helper->redirector->gotosimple('userprofile','profile',true,array('id'=>$check[0]['userid'])) ;
		}	
	}
	
	public function inboxAction()
	{		 
			 
		if($this->namespace->email=='')
		{ 
			$this->_redirect('index/index');
		}
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);	  
		$to = $this->request['receiver_email']; 
		/////////////////////////INSERT IN table art_inbox////////////////////////////////////// 
		if ($this->request['receiver_id']!='' && $this->request['sender_id']!='')
		{
			$dbField = array('receiver_id' => $this->request['receiver_id'],'sender_id' => $this->request['sender_id'], 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $this->request['msubject'],'message'=> $this->request['mmessage']);
			$inbox_res   	 = new Application_Model_DbTable_Artworkinbox();	
			$rec = $inbox_res->addartworkinbox($dbField);
			$subject = $this->request['msubject'];
			$get_user	=	new Application_Model_DbTable_Registration();
 			$messageSend = '<p>'.$this->request['mmessage'].'</p>';	
			$getuser = $get_user->usrename($this->namespace->email,$this->namespace->passowrd);
			$senderName = $getuser[0]['first_name'].' '. $getuser[0]['last_name'];
			
			//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
			/////////////GET email from user id ////////////////////////////////////////////////
			$user   	 = new Application_Model_Artuser();
			$rid=$this->request['receiver_id'];
			$user_result = $user->selestuser($rid);	 
			$this->view->user_result = $user_result;
			foreach($user_result as $event){
			$reciever_name= $event['first_name'].' '.$event['last_name'];
			}
			

			$base_Url  = $this->getRequest()->getbaseUrl(); 
			 $msg= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$this->request['sender_id'].'">'.$senderName.'</a> has send message to <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$rid.'">'.$reciever_name.'</a>';
			$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
			$dbField = array('user_id' => $this->request['sender_id'], 'activity_msg' => $msg, 'create_date'=>date("Y-m-d H:i:s"));
			
			$rec_insert_activity = $insert_activity->addactivity($dbField);
			///////////////////////////////////////////////////////////////////////////////////
			//////////////////////Get mail content set by admin///////////////////////
			$getmailcontent	=	new Application_Model_ArtMailFormats();
			$mailcontent		=	$getmailcontent->getMailContentById(3);
			$formated_message=$mailcontent['content'];
			$formated_subject=$mailcontent['subject'];
			////////////////////////////////////////////////////////
			
			 $message = sprintf($formated_message,ucfirst($reciever_name),ucfirst($senderName),$base_Url); 			
			
			//-------------------------Send Mail----------------------------------//
			$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com');
			Zend_Mail::setDefaultTransport($tr);
			
			$mail = new Zend_Mail();
		 	$mail->setBodyHtml($message);
	    	$mail->setFrom('admin@artex.com', 'www.artex.com');
	    	$mail->addTo($to, 'Some Recipient');
	    	$mail->setSubject($subject);
	    	$mail->send($tr);
	    	
	    	$this->_helper->redirector->gotosimple('userprofile','profile',true,array('id'=>$this->request['receiver_id'], 'msg' => 'Your message has been sent successfully.')) ;
		}
	}
	
	public function followAction()
	{ 
		$namespace = new Zend_Session_Namespace();
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
		 
		/////////////GET user id from user email (store in session)///////////////////////////////////
		 
		$user   	 = new Application_Model_ArtGroup();
		 
		$row=$user->getMemberID($this->namespace->email);
	 	$owner_id=$row[0]['id'];
		$owner_name=$row[0]['first_name'].' '.$row[0]['last_name'];
 		/////////////END GET user id from user email (store in session)/////////////////////////////////
 
		////////////////////////////////Find user already followed or not///////////////////////////////////
 		$watchlist_model   = new Application_Model_DbTable_Artworkuserfollow();
		$watchlist_model_result = $watchlist_model->fetchAll($watchlist_model->select()->where('user_id = ?', $this->request['id'])->where('follower_id = ?', $owner_id));	
		/*echo '<pre>';
		print_r($watchlist_model_result);
		die;*/
		$this->view->watchlist_model_result = $watchlist_model_result;
		foreach($watchlist_model_result as $event){
			 $count=count($event); 
		}

		if($count<1)
		{
			$dbField = array('user_id' => $this->request['id'], 'follower_id' => $owner_id, 'add_date'=>date("Y-m-d H:i:s"));
			$watchlist_res   	 = new Application_Model_DbTable_Artworkuserfollow();	
			$rec = $watchlist_res->addartworkfollow($dbField);
			//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
			/////////////GET email from user id ////////////////////////////////////////////////
			$user   	 = new Application_Model_Artuser();
			$rid=$this->request['id'];
			$user_result = $user->selestuser($rid);	 
			$this->view->user_result = $user_result;
			foreach($user_result as $event){
			$profile_user_name= $event['first_name'].' '.$event['last_name']; 
			}
			
			$base_Url  = $this->getRequest()->getbaseUrl(); 
			$msg= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$owner_id.'">'.$owner_name.'</a> followed to <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$rid.'">'.$profile_user_name.'\'s</a> profile'; 

			$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
			$dbField = array('user_id' => $owner_id, 'activity_msg' => $msg, 'create_date'=>date("Y-m-d H:i:s"));
			
			$rec_insert_activity = $insert_activity->addactivity($dbField);
			///////////////////////////////////////////////////////////////////////////////////
			echo $errmsg = "added successfully";die;
		}
		else
		{
			echo $errmsg = "allready added";die;
		}
	}
	
	public function commentAction()
	{  
		if($this->namespace->email=='')
		{ 
			$this->_redirect('index/index');
		}
		$namespace = new Zend_Session_Namespace();
		if($this->namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
		
 		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if($this->request['maincomment']!='')
		{
			$dbField['to_user_id'] = $this->request['id'];
			$dbField['from_user_id'] = $this->namespace->userdetails[0]['id'];
			$dbField['feedback'] = $this->request['maincomment'];
			$dbField['create_date'] = date("Y-m-d H:i:s");
			//print_r($dbField);die;
			$rec = $this->userInfo->addUserFeedback($dbField);
			//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
			/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$user   	 = new Application_Model_ArtGroup();
			$row=$user->getMemberID($namespace->email);
			$owner_id=$row[0]['id'];
			$owner_name=$row[0]['first_name'].' '.$row[0]['last_name'];
			/////////////GET email from user id ////////////////////////////////////////////////
			$user   	 = new Application_Model_Artuser();
			$rid=$this->request['id'];
			$user_result = $user->selestuser($rid);	 
			$this->view->user_result = $user_result;
			foreach($user_result as $event){
			$profile_user_name= $event['first_name'].' '.$event['last_name']; 
			}
			
			$base_Url  = $this->getRequest()->getbaseUrl(); 
			$msg= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$owner_id.'">'.$owner_name.'</a> commented on <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$rid.'">'.$profile_user_name.'\'s</a> profile'; 
			$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
			$dbField = array('user_id' => $owner_id, 'activity_msg' => $msg, 'create_date'=>date("Y-m-d H:i:s"));
			
			$rec_insert_activity = $insert_activity->addactivity($dbField);
			///////////////////////////////////////////////////////////////////////////////////
		}
		
		if ($this->request['action_name']=='userprofile'){
			$this->_helper->redirector->gotosimple('userprofile','Profile',true,array('id'=>$this->request['id'])) ;
		}
		elseif($this->request['action_name']=='welcome'){
		 	$this->_helper->redirector->gotosimple('welcome','Profile') ;
		}
	}
	
	public function feedcommentAction()
	{ 	 
		$namespace = new Zend_Session_Namespace();
		if($this->namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
		
		if($this->request['subcomment']!='')
		{	 
			$dbField['from_user_id'] = $this->namespace->userdetails[0]['id'];
			$dbField['feedback_id']  = $this->request['feedid'];
			$dbField['to_user_id']   = $this->request['id'];
			$dbField['comment']     = $this->request['subcomment'];
			$dbField['create_date']  = date("Y-m-d H:i:s");
			$this->request['action_name'];
			 
			$rec = new Application_Model_Artuserfeedbackscomments();
			$rec->addUserFeedbackComment($dbField);
			
			//////////////////////////INSERT VALUE IN ACTIVITY TABLE////////////////////////////////////////////////
			/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$user   	 = new Application_Model_ArtGroup();
			$row=$user->getMemberID($namespace->email);
			$owner_id=$row[0]['id']; 
			$owner_name=$row[0]['first_name'].' '.$row[0]['last_name'];
			/////////////GET email from user id ////////////////////////////////////////////////
			$user   	 = new Application_Model_Artuser();
			$rid=$this->request['id'];
			$user_result = $user->selestuser($rid);	 
			$this->view->user_result = $user_result;
			foreach($user_result as $event){
			$profile_user_name= $event['first_name'].' '.$event['last_name']; 
			}
			$base_Url  = $this->getRequest()->getbaseUrl(); 
			$msg= '<a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$owner_id.'">'.$owner_name.'</a> commented on <a href="http://'. $_SERVER['HTTP_HOST'].$base_Url.'/Profile/userprofile/id/'.$rid.'">'.$profile_user_name.'\'s</a> profile'; 
			
			$insert_activity   	 = new Application_Model_DbTable_Artuseractivity();	
			$dbField = array('user_id' => $owner_id, 'activity_msg' => $msg, 'create_date'=>date("Y-m-d H:i:s"));
			
			$rec_insert_activity = $insert_activity->addactivity($dbField);
			///////////////////////////////////////////////////////////////////////////////////
		}
	if ($this->request['action_name']=='userprofile'){
			$this->_helper->redirector->gotosimple('userprofile','Profile',true,array('id'=>$this->request['id'])) ;
		}
		elseif($this->request['action_name']=='welcome'){
		 	$this->_helper->redirector->gotosimple('welcome','Profile');
		}
	}
	
	public function welcomeAction() 
	{
		$namespace = new Zend_Session_Namespace();
		if($namespace->email=='') 
		{ 
			$this->_redirect('index/index');
		}
		$bannerlist_model   = new Application_Model_DbTable_ArtBanner();
		$this->view->getbannerlist = $bannerlist_model->getBannerlistByPage(1);

		/****************** Get login user details ****************************/
		$user_details = $this->getuserdetails($namespace->userdetails[0]['id']);
		$this->view->user_details = $user_details;
		$this->view->userid = $namespace->userdetails[0]['id'];
		/***************************** END ******************************/
		$this->view->action_name = $this->getRequest()->getActionName();
		$request=$this->getRequest()->getParams();

		$this->view->authcode = $request['authcode'];
		################################ Profile commenting ON 03-03-2012 ##############################
				 
		$comments = $this->userInfo->getUserFeedbacks($namespace->userdetails[0]['id']);	
		 
		$this->view->comments = $comments;
		
		$feedbackscomments = $this->userInfo->getArtworkfeedbacksComments($namespace->userdetails[0]['id']);	
		$this->view->feedbackscomments  = $feedbackscomments;
		
		################################ END Profile commenting ##############################
		//////////////////////total transaction///////////////////////
		$total_soldmoney=0;$total_boughtmoney=0;
		$watchlist = $this->userInfo->getBoughtArt($namespace->userdetails[0]['id']);	
		$total_bought=count($watchlist); 
		
		for($i=0;$i<$total_bought;$i++)
		$total_boughtmoney=$total_boughtmoney+$watchlist[$i]['amount'];
		
		$sold = $this->userInfo->sold($namespace->userdetails[0]['id']);
		 $total_sold=count($sold);
		$this->view->total_sold=$total_sold;
		
		for($i=0;$i<$total_sold;$i++)
		 $total_soldmoney=$total_soldmoney+$sold[$i]['amount'];
		$this->view->total_trans_money=$total_soldmoney+$total_boughtmoney;
		
		$this->view->total_tans=$total_sold+$total_bought;
				/////////////////////////feedback rating %////////////////////////////////////////////////
		$artworkposne = $this->userInfo->artworkposne($namespace->userdetails[0]['id']);	
		$arr=explode(':',$artworkposne[0]);
		//$total_bought=count($watchlist);
		
		$artworkuser = $this->userInfo->getfeedback($namespace->userdetails[0]['id']);	
		@$feedbackper=($arr[1]/count($artworkuser))*100;
		$this->view->feedbackper = number_format($feedbackper, 1, '.', '');
	}
	
	public function deletecommentAction(){
		if($this->namespace->email=='')
		{ 
			$this->_redirect('index/index');
		}
		$this->request['comment_id']; 
		$this->userInfo->deletecomment($this->request['comment_id']);
		
		if ($this->request['action_name']=='userprofile'){
			$this->_helper->redirector->gotosimple('userprofile','Profile',true,array('id'=>$this->request['id'])) ;
		}
		elseif($this->request['action_name']=='welcome'){
		 	$this->_helper->redirector->gotosimple('welcome','Profile') ;
		}
	}
	
	public function loadmoreAction()
	{
		$this->_helper->layout->disableLayout();
		$request	=	$this->getRequest()->getParams();
	 
		$limit=$request['lastmsg'];
		$user_id=$request['userID'];
		$nlimit=$limit+1;
		 
		$arraymerg=array();
		$limitar=array('limit'=>$nlimit);
		 
		$userfeedbacks_model_result = $this->userInfo->getUserFeedbacks_user($user_id,$limit);	//print_r($artworkuploadfeedbacks_model_result);
		if(count($userfeedbacks_model_result)>0)
		{
			$arraymerg[]=array_merge((array)$userfeedbacks_model_result[0], (array)$limitar); 
		}
		$this->view->arraymerg = $arraymerg;
		 
		///////////////////////////Artwork's feedback comments////////////////////////////////////
		 
		$nameSpace = isset($user_id) ? $user_id : '';
		$this->view->action_name = $request['action_name'];
		$comments = $this->userInfo->getUserFeedbacks($nameSpace);	
		
		$this->view->comments = $comments;
		$this->view->userID = $request['id'];
		$feedbackscomments = $this->userInfo->getArtworkfeedbacksComments($nameSpace);	
		$this->view->feedbackscomments  = $feedbackscomments;
	}
	
	/******************** Show user bids list (22-03-2012)**************************************/
	
	public function bidsAction() {
		$namespace = new Zend_Session_Namespace();
		if($namespace->email=='') 
		{ 
			$this->_redirect('Index/index');
		}
		/****************** Get login user details ****************************/
		$NumRec= $this->_getParam('NumRec',10);
		$user_details = $this->getuserdetails($namespace->userdetails[0]['id']);
		$this->view->user_details = $user_details;
		//$this->view->actionName	= $user_details['display_sub_menu'];
		$bids = $this->userInfo->getBids($namespace->userdetails[0]['id']);	
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($bids);
		$paginator->setItemCountPerPage($NumRec);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
		$this->view->per_page = $NumRec;		
		/***************************** END ******************************/
	}
	
	public function watchlistAction() {
		$namespace = new Zend_Session_Namespace();
		if($namespace->email=='') 
		{ 
			$this->_redirect('Index/index');
		}
		$request = $this->getRequest()->getParams();
		$NumRec= $this->_getParam('NumRec',10);
		/****************** Get login user details ****************************/
		$user_details = $this->getuserdetails($namespace->userdetails[0]['id']);
		$this->view->user_details = $user_details;
		$watchlist = $this->userInfo->getWatchlist($namespace->userdetails[0]['id']);	
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($watchlist);
		$paginator->setItemCountPerPage($NumRec);
		$paginator->setCurrentPageNumber($page);
		//echo '<pre>';print_r($user_details);die;
		$this->view->msg = isset($request['msg']) ? 'Record deleted successfully':'';
		$this->view->paginator = $paginator;
		$this->view->page = $page;
		$this->view->per_page = $NumRec;
		//echo '<pre>';print_r($paginator);  die;
	}
	/***************************** END ******************************/
	////////////////////////////////////////////////////////////////////////////////
	public function rewatchlistAction() {
		$this->_helper->layout->disableLayout();
		$namespace = new Zend_Session_Namespace();
		if($namespace->email=='') 
		{ 
			$this->_redirect('Index/index');
		}
		$request = $this->getRequest()->getParams();
		$NumRec= $this->_getParam('NumRec',10);
		/****************** Get login user details ****************************/
		$user_details = $this->getuserdetails($namespace->userdetails[0]['id']);
		$this->view->user_details = $user_details;
		$watchlist = $this->userInfo->getWatchlist($namespace->userdetails[0]['id']);	
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($watchlist);
		$paginator->setItemCountPerPage($NumRec);
		$paginator->setCurrentPageNumber($page);
		//echo '<pre>';print_r($user_details);die;
		$this->view->msg = isset($request['msg']) ? 'Record deleted successfully':'';
		$this->view->paginator = $paginator;
		$this->view->page = $page;
		$this->view->per_page = $NumRec;
		//echo '<pre>';print_r($paginator);  die;
	}
	/***************************** END ******************************/
	public function boughtAction() {
		
		$namespace = new Zend_Session_Namespace();
		if($namespace->email=='') 
		{ 
			$this->_redirect('Index/index');
		}
		/****************** Get login user details ****************************/
		$NumRec= $this->_getParam('NumRec',10);
		$user_details = $this->getuserdetails($namespace->userdetails[0]['id']);
		$this->view->user_details = $user_details;
		$watchlist = $this->userInfo->getBoughtArt($namespace->userdetails[0]['id']);	
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($watchlist);
		$paginator->setItemCountPerPage($NumRec);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
		$this->view->page = $page;
		$this->view->per_page = $NumRec;
	}
	
	public function aboutbuyAction() {
		$namespace = new Zend_Session_Namespace();
		if($namespace->email=='') 
		{ 
			$this->_redirect('Index/index');
		}
		/****************** Get login user details ****************************/
		$NumRec= $this->_getParam('NumRec',10);
		$user_details = $this->getuserdetails($namespace->userdetails[0]['id']);
		$this->view->user_details = $user_details;
		$watchlist = $this->userInfo->aboutBuy($namespace->userdetails[0]['id']);	
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($watchlist);
		$paginator->setItemCountPerPage($NumRec);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
		$this->view->page = $page;
		$this->view->per_page = $NumRec;
	}
	
	public function deletewatchtistAction($watch_id) {
		$namespace = new Zend_Session_Namespace();
		if($namespace->email=='') 
		{ 
			$this->_redirect('Index/index');
		}
		$request = $this->getRequest()->getParams();
		/****************** Get login user details ****************************/
		$user_details = $this->getuserdetails($namespace->userdetails[0]['id']);
		$this->view->user_details = $user_details;
		$watchlist = $this->userInfo->deleteWatchList($request['id']);
 
		$this->_helper->redirector->gotosimple('watchlist','Profile',true,array('msg'=>'Record deleted successfully')) ;
 
	}
	
	public function listedAction() {
		$namespace = new Zend_Session_Namespace();
		$request = $this->getRequest()->getParams();
		if($namespace->email=='') 
		{ 
			$this->_redirect('Index/index');
		}
		if(isset($request['msg']))
		{
	        $request =	$this->getRequest()->getParams();
			$this->view->msg = $request['msg'];			
		}	
		
		/****************** Get login user details ****************************/
		$NumRec= $this->_getParam('NumRec',10);
		$user_details = $this->getuserdetails($namespace->userdetails[0]['id']);
		$this->view->user_details = $user_details;
		$listed = $this->userInfo->listed($namespace->userdetails[0]['id']);	
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($listed);
		$paginator->setItemCountPerPage($NumRec);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
		$this->view->page = $page;
		$this->view->per_page = $NumRec;
	}
	
	public function livebidsAction() {
		$namespace = new Zend_Session_Namespace();
		if($namespace->email=='') 
		{ 
			$this->_redirect('Index/index');
		}
		/****************** Get login user details ****************************/
		$NumRec= $this->_getParam('NumRec',10);
		$user_details = $this->getuserdetails($namespace->userdetails[0]['id']);
		$this->view->user_details = $user_details;
		$livebids = $this->userInfo->livebids($namespace->userdetails[0]['id']);	
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($livebids);
		$paginator->setItemCountPerPage($NumRec);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
		$this->view->page = $page;
		$this->view->per_page = $NumRec;
	}
	
	public function galleryAction() {
		 
	}
	
	public function moderatedAction() {
		$namespace = new Zend_Session_Namespace();
		$namespace->filename=null;
		if($namespace->email=='') 
		{ 
			$this->_redirect('Index/index');
		}
		$request  =	$this->getRequest()->getParams();
        if(isset($request['msg']))
		{
	        $request =	$this->getRequest()->getParams();
			$this->view->msg = $request['msg'];			
		}	
		/****************** Get login user details ****************************/
		$NumRec= $this->_getParam('NumRec',10);
		$user_details = $this->getuserdetails($namespace->userdetails[0]['id']);
		$this->view->user_details = $user_details;
		$moderated = $this->userInfo->moderated($namespace->userdetails[0]['id']);	
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($moderated);
		$paginator->setItemCountPerPage($NumRec);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
		$this->view->page = $page;
		$this->view->per_page = $NumRec;
	}
	
	public function aboutsellAction() {
		$namespace = new Zend_Session_Namespace();
		if($namespace->email=='') 
		{ 
			$this->_redirect('Index/index');
		}
		/****************** Get login user details ****************************/
		$NumRec= $this->_getParam('NumRec',10);
		$user_details = $this->getuserdetails($namespace->userdetails[0]['id']);
		$this->view->user_details = $user_details;
		$aboutsell = $this->userInfo->aboutsell($namespace->userdetails[0]['id']);	
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($aboutsell);
		$paginator->setItemCountPerPage($NumRec);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
		$this->view->page = $page;
		$this->view->per_page = $NumRec;
	}
	
	public function boughtnsoldAction()
	{
		$this->_helper->layout->disableLayout();
		$request  =	$this->getRequest()->getParams(); 
		$viewtype = isset($request['view']) ? $request['view'] : 'grid'; 
		$viewtab = isset($request['viewtab']) ? $request['viewtab'] : 'bought'; 
		$per_page= $this->_getParam('per_page',10);
		$userid= $this->_getParam('id',0);
		$user_details = $this->getuserdetails($userid);
		$this->view->user_details = $user_details;
		$watchlist = $this->userInfo->getBoughtArt($userid);	
		$total_bought=count($watchlist);
		
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($watchlist);
		$paginator->setItemCountPerPage($per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
		$this->view->page = $page;
		$this->view->per_page = $per_page;
		$this->view->userid = $userid;
		
		$this->view->grid = $viewtype;
			
		$this->view->bought = $viewtab;

		/////////////////////////////////////////////////////////////////////

		$sold = $this->userInfo->sold($userid);	
		$total_sold=count($sold);
		$page = $this->_getParam('page',1);
		$paginator1 = Zend_Paginator::factory($sold);
		$paginator1->setItemCountPerPage($per_page);
		$paginator1->setCurrentPageNumber($page);
		$this->view->paginator1 = $paginator1;
		$this->view->total_bought = $total_bought;
		$this->view->total_sold = $total_sold;
		$this->view->page = $page;
		$this->view->per_page = $per_page;
		
	}
	public function sendsoldAction()
	{
		$this->_helper->layout->disableLayout();
		$request  =	$this->getRequest()->getParams(); 
		$id=$request['id'];
		$data = array(
		'send_status'		=> '1'
		);	
		$art   = new Application_Model_DbTable_Artworkupload();
		$rec = $art->updateartworkpaystatus($id,$data);
		
	}
	public function paysoldAction()
	{
		
		$namespace = new Zend_Session_Namespace();
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		}
		$this->_helper->layout->disableLayout();
		 $this->_helper->layout->setLayout('layout');	
		 $form = new Application_Form_Artworkcreditpay();
         $this->view->form = $form;
		$request  =	$this->getRequest()->getParams();
		$id = $this->_getParam('id', 0);
		$this->view->id=$id;
		/////////////////Artwork detail///////////////////////////////////////////////////////////////////
		$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
			$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->where('id = ?', $id));				
				$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
					foreach($artworkuploadlist_model_result as $event){
						$cat_id= $event['category'];
						$owner_id= $event['owner_id'];
						$frame= $event['frame'];
						$medium= $event['medium'];
						if($frame==0) $frame='No'; else $frame='Yes'; 
						$title= $event['title'];
						 $artwork_id=$event['id'];
						$edition= $event['edition'];
						$size= $event['size'];
						$auction_sold=$event['auction_comm'];
					}
					$this->view->auction_sold=$auction_sold;
					$this->view->title=$title;
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
			$row=$user->getMemberID($namespace->email); 
			$this->view->firstname	=	$row[0]['first_name'];
			$this->view->email	=	$row[0]['email'];
			$this->view->user_id	=	$row[0]['id'];
			$bidder_id				=	$row[0]['id'];
			$record['address1']	=	$row[0]['billing_address'];
			$record['city']	=	$row[0]['billing_city'];
			$record['state']	=	$row[0]['billing_state'];
			$record['zip']	=	$row[0]['billing_zip'];
			$record['country']	=	$row[0]['billing_country'];
			$record['saddress1']	=	$row[0]['shipping_address'];
			$record['scity']	=	$row[0]['shipping_city'];
			$record['sstate']	=	$row[0]['shipping_state'];
			$record['szip']	=	$row[0]['shipping_zip'];
			$record['scountry']	=	$row[0]['shipping_country'];
			$record['creditcardtype']   = $row[0]['credit_card'];
			$record['creditcardnumber']   = $row[0]['credit_card_number'];
			$record['exp_date_month']   = $row[0]['exp_month'];
			$record['exp_date_year']   = $row[0]['exp_year'];
			$record['cvv_number']   = $row[0]['cvv2'];
			$form->populate($record);
		///////////////////////////Artwork's max  bids///////////////////////////////////////( [maxbid] => 9099.00 [bidder_id] => 1 [current_bid] => 9099.00 ) 
		$artworkuploadmaxbid_model   = new Application_Model_DbTable_Artworkbid();
		$artworkuploadmaxbid_model_result = $artworkuploadmaxbid_model->getBidMaxlist($id);	 
		
		$product_price=$artworkuploadmaxbid_model_result[0]['maxbid'];
		$this->view->product_price=$product_price;
		$artex_amount=number_format($product_price*($auction_sold/100), 2, '.', '');
		$this->view->artex_amount=$artex_amount;
		/////////////////PAY INFO FROM GLOBAL TABLE////////////////////////////////
			$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
			$rec1 =	$allgloble->getglobelinfoAll();
			$merchant_id=$rec1[0]['username'];
			$password=$rec1[0]['password'];
			$signature=$rec1[0]['signature'];	
		 if ($this->getRequest()->isPost()) {
 $paymentType=urlencode('Authorization');
$creditcardtype   = $request['creditcardtype'];
$creditcardnumber   = $request['creditcardnumber'];
$exp_date_month   = $request['exp_date_month'];
$exp_date_year   = $request['exp_date_year'];
$cvv_number   = $request['cvv_number'];

$firstname   = $request['firstname'];
$lastName   = $row[0]['last_name'];
$user_id   = $request['user_id'];
$amount   = $request['amount'];
$address1 = urlencode($request['address1']);
$address2 = urlencode($request['address2']);
$city = urlencode($request['city']);
$state = urlencode($request['state']);
$zip = urlencode($request['zip']);
$countryid = urlencode($request['country']);				// US or other valid country code
$currencyID = urlencode('USD');							// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
$country_res = new Application_Model_DbTable_ArtCountry();	
$country_ar=$country_res->getcountryCode($countryid);
$country=$country_ar['code'];
////////////////////////Artex amount////////////////////////////
		
		////////////////////////Scout amount////////////////////////////
		//$scout_amount=number_format($amount*($scout_percent/100), 2, '.', '');
		// die();
		/////////////////////////////////////////////////////////////////////////////
  


$nvpStr_ =	"&PAYMENTACTION=$paymentType&AMT=$artex_amount&CREDITCARDTYPE=$creditcardtype&ACCT=$creditcardnumber".
					"&EXPDATE=$exp_date_month$exp_date_year&CVV2=$cvv_number&FIRSTNAME=$firstname&LASTNAME=$lastName".
					"&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$countryid&CURRENCYCODE=$currencyID";
 ////////////////////////////////////////////////////////////////////////////
 global $environment;
 $environment = 'sandbox';	// or 'beta-sandbox' or 'live'

	
		// Set up your API credentials, PayPal end point, and API version.
		 $API_UserName = urlencode($merchant_id);
		 $API_Password = urlencode($password);
		 $API_Signature = urlencode($signature);


		
		//$API_UserName = urlencode('deepak_1324646632_biz_api1.inoday.com');
		//$API_Password = urlencode('1324646670');
		//$API_Signature = urlencode('An5ns1Kso7MWUdW4ErQKJJJ4qi4-Az1utoWx0MLHX5Xe77eTOq6J3URQ');
		
		
		
		
		$API_Endpoint = "https://api-3t.paypal.com/nvp";
		
		if("sandbox" === $environment || "beta-sandbox" === $environment) {
			$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
		}
		$version = urlencode('51.0');
	
		// Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
	
		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
	
		// Set the API operation, version, and API signature in the request.
		 echo $nvpreq = "METHOD=DoDirectPayment&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
	
		// Set the request as a POST FIELD for curl.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
	
		// Get response from the server.
	 $httpResponse = curl_exec($ch);
	//echo "<pre>";print_r(curl_getinfo($ch)); die();
		if(!$httpResponse) {
			exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
		}
	
		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);
	
		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i => $value) {
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}
	
		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
			exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
		}
	

 		//$checkpaymentfor	=	explode('_',$request['order_id']);
		
		//echo '<pre>';print_r($httpParsedResponseAr);//["TRANSACTIONID"]die;
		
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
//////////////////////Get mail content set by admin///////////////////////
$getmailcontent	=	new Application_Model_ArtMailFormats();
$mailcontent		=	$getmailcontent->getMailContentById(16);
$formated_message=$mailcontent['content'];
$formated_subject=$mailcontent['subject'];
//////////////////////////////////////////////////////////////////////////////	

$name   = $firstname." ".$last_name;
$user_details   = new Application_Model_Artuser();
$userdetails=$user_details->getUserdetails($bidder_id); //print_r($userdetails);
$to=$userdetails[0]['email'];
		$message = sprintf($formated_message,$name,$title,$frame,$medium,$edition,$size);  
											$subject=$formated_subject;
											/*$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
											Zend_Mail::setDefaultTransport($tr);
											
											$mail = new Zend_Mail();
											$mail->setBodyHtml($message);
											$mail->setFrom('admin@artex.com', 'www.artex.com');
											$mail->addTo($to, $name);
											$mail->addCc($rec1[0]['contact_email'], 'Aministrator');
											$mail->setSubject($subject);
											$mail->send($tr);*/
			
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

//////////////////////////////////////////////////////////////////////

//add art_artworks_listing_payments table
///////////////////////////////////////////////////////////////////////////////////
			 $status=1;
		$paymentlist_model   = new Application_Model_DbTable_Artworkpayment();
		$paymentlist_model_result = $paymentlist_model->getPaymentlist($id);
		$res=$paymentlist_model_result[0]['id'];
	
		$dbFieldpay = array('artwork_id' => $id,'listing_pay_id' => $res,'paymentby_id' => $bidder_id,'type'=>'artex_percentage', 'transaction_id' => $httpParsedResponseAr["TRANSACTIONID"], 'transaction_date' =>   date("Y-m-d H:i:s"), 'amount' => $amount, 'status' => $status,'credit_card' => $creditcardtype, 	'credit_card_number' => $creditcardnumber, 	'exp_month' => $exp_date_month, 	'exp_year' => $exp_date_year, 	'cvv2' => $cvv_number,'payment_type'=>'2' );

	
			$transaction   = new Application_Model_DbTable_Artworkbidtransaction();
			$res_transaction=$transaction->addartworkbidtransaction($dbFieldpay);
//////////////////////////////////////////////////////////////////////////////////////////////	
//update payment status art_artworks table
//////////////////////////////////////////////////////////////////////////////////////////////
		$art   = new Application_Model_DbTable_Artworkupload();
			$data = array(
            'seller_payment_status'		=> '1'
        	);
			$rec = $art->updateartworkpaystatus($id,$data);
////////////////////////////////////MAIL for successful payment//////////////////////////////////////////////////////////
		$base_Url  = $this->getRequest()->getbaseUrl();
		echo "<script language='JavaScript'>window.location.href='http://". $_SERVER['HTTP_HOST'].$base_Url."/Profile/welcome#list:sold'</script>";
		//$this->_helper->redirector->gotosimple('welcome#list%3Asold','Profile',true,array('msg'=>'Thank you for your credit card payment')) ;

			}
		else
		{
			//////////////////////Get mail content set by admin///////////////////////
			$getmailcontent	=	new Application_Model_ArtMailFormats();
			$mailcontent		=	$getmailcontent->getMailContentById(17);
			$formated_message=$mailcontent['content'];
			$formated_subject=$mailcontent['subject'];
			//////////////////////////////////////////////////////////////////////////////	
//////////////////////////////////////////////////////////////////////
$name   = $firstname." ".$last_name;
$user_details   = new Application_Model_Artuser();
$userdetails=$user_details->getUserdetails($bidder_id); //print_r($userdetails);
$to=$userdetails[0]['email'];
		$message = sprintf($formated_message,$name,$title,$frame,$medium,$edition,$size);  
											$subject=$formated_subject;
											/*$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
											Zend_Mail::setDefaultTransport($tr);
											
											$mail = new Zend_Mail();
											$mail->setBodyHtml($message);
											$mail->setFrom('admin@artex.com', 'www.artex.com');
											$mail->addTo($to, $name);
											$mail->addCc($rec1[0]['contact_email'], 'Aministrator');
											$mail->setSubject($subject);
											$mail->send($tr);*/
			
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//add art_artworks_listing_payments table
///////////////////////////////////////////////////////////////////////////////////
		$status=0;	
		
		$paymentlist_model   = new Application_Model_DbTable_Artworkpayment();
		$paymentlist_model_result = $paymentlist_model->getPaymentlist($id);
		$res=$paymentlist_model_result[0]['id'];
		
		$dbFieldpay = array('artwork_id' => $id,'listing_pay_id' => $res,'paymentby_id' => $bidder_id,'type'=>'artex_percentage', 'transaction_id' => $httpParsedResponseAr["TRANSACTIONID"], 'transaction_date' =>   date("Y-m-d H:i:s"), 'amount' => $amount, 'status' => $status,'credit_card' => $creditcardtype, 	'credit_card_number' => $creditcardnumber, 	'exp_month' => $exp_date_month, 	'exp_year' => $exp_date_year, 	'cvv2' => $cvv_number,'payment_type'=>'2' );
	
			$transaction   = new Application_Model_DbTable_Artworkbidtransaction();
			$res_transaction=$transaction->addartworkbidtransaction($dbFieldpay);
//////////////////////////////////////////////////////////////////////////////////////////////
//update payment status art_artworks table
//////////////////////////////////////////////////////////////////////////////////////////////			
			$tutorial   = new Application_Model_DbTable_Artworkupload();
	 		 
			$rec = $tutorial->updateartworkpaystatus($id,$status);
	 		$this->_helper->redirector->gotosimple('creditpay','artworkdetails',true,array('msg'=>'Credit card payment failed.Please try again')) ;
		}
} 
	
		
	}
	public function feedbackratingAction()
	{
		$this->_helper->layout->disableLayout();
		$request  =	$this->getRequest()->getParams(); 
		$viewtype = isset($request['view']) ? $request['view'] : 'grid'; 
		$viewtab = isset($request['viewtab']) ? $request['viewtab'] : 'bought'; 
		$per_page= $this->_getParam('per_page',10);
		$userid= $this->_getParam('id',0);
		$user_details = $this->getuserdetails($userid);
		$this->view->user_details = $user_details;

		$artworkposne = $this->userInfo->artworkposne($userid);	
		$arr=explode(':',$artworkposne[0]);
		$this->view->positive = $arr[1];
		$this->view->negative = $arr[0];
		$this->view->ok = $arr[2];
		//$total_bought=count($watchlist);
		
		$artworkuser = $this->userInfo->getfeedback($userid);	
		$feedbackper=($arr[1]/count($artworkuser))*100;
		$this->view->feedbackper = $feedbackper;
		
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($artworkuser);
		$paginator->setItemCountPerPage($per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
		$this->view->page = $page;
		$this->view->per_page = $per_page;
		$this->view->userid = $userid;
		
		$this->view->grid = $viewtype;
			
		$this->view->bought = $viewtab;

		/////////////////////////////////////////////////////////////////////

		$sold = $this->userInfo->sold($userid);	
		$total_sold=count($sold);
		$page = $this->_getParam('page',1);
		$paginator1 = Zend_Paginator::factory($sold);
		$paginator1->setItemCountPerPage($per_page);
		$paginator1->setCurrentPageNumber($page);
		$this->view->paginator1 = $paginator1;
		$this->view->total_bought = $total_bought;
		$this->view->total_sold = $total_sold;
		$this->view->page = $page;
		$this->view->per_page = $per_page;
		
	}
	
	private function getuserdetails($user_id) {
		$user_details['userInfo']   = $this->userInfo->getUserProfile($user_id);
		$user_details['userArt'] 	= $this->userInfo->getUserArt($user_id);
		$user_details['countInbox'] = $this->message->countInboxMsg($user_id);
		$user_details['bidding'] 	= $this->userInfo->bidding($user_id); // Show bidding item
		$user_details['liveart'] 	= $this->userInfo->liveart($user_id); // Show live artwork
		$user_details['watchlist'] 	= $this->userInfo->watchlist($user_id); // Show watchlist
		$user_details['feedback'] 	= $this->userInfo->feedback($user_id); // Show feedback count on artwork
	    $user_details['display_sub_menu']   = $this->getRequest()->getActionName(); 
		return $user_details;
	}
	
	public function uploadimageAction()
	{ 
		$this->_helper->layout->disableLayout();
		$request = $this->getRequest()->getParams();
		$namespace = new Zend_Session_Namespace(); 
		/////////////////////////////////////////////////////////
		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
		$rec =	$allgloble->getglobelinfoAll();
		$this->view->rec=$rec;
		$profile_size=$rec[0]['profile_size'];
		$image_type=$rec[0]['image_type'];
		$this->view->artworksize=$profile_size;
		$profilesize=$profile_size*1024;
		
		ini_set("POST_MAX_SIZE","10M");
		ini_set("UPLOAD_MAX_SIZE","10M");
		
		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = explode(',',$rec[0]['image_type']);
		// max file size in bytes
		$sizeLimit =$profile_size*1024;
		//$sizeLimit = 10 * 1024 * 1024;
		
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
		//echo $uploads  = BASE_URL.'uploads/';
		$result = $uploader->handleUpload('../public/images/profile_image/');
		// to pass data through iframe you will need to encode all html tags { Success :true}
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES).'~';
		die();//'USPS'

	}	
	//************************************ Image cropping  Action 22 March 2012 *****************************	
	
	public function cropAction()
	{$namespace = new Zend_Session_Namespace();
		$this->_helper->layout->disableLayout();
		if ($this->getRequest()) 
		{
			$this->_helper->layout->disableLayout();
			$request=	$this->getRequest()->getParams();
			 
			if(isset($request['path']))
			{
				/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
	 			$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
				$rec =	$allgloble->getglobelinfoAll();
				$this->view->rec=$rec;
				$image_small_width='80';
				$image_small_height='80';
				$image_large_width='150';
				$image_large_height='150';
				$profile_size=$rec[0]['profile_size'];
				$image_type=$rec[0]['image_type'];
				$this->view->profilesize=$profile_size;
				$profilesize=$profile_size*1024;
				
				require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "setup.php";

				require_once '../library/Zend/Filter/ImageSize.php';
				/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
				$namespace 		= new Zend_Session_Namespace(); 
					
				$jpeg_quality 	= 200;
			
				$BaseUrl= 'http://'.$_SERVER['HTTP_HOST'].$base_Url;
				$h=$request['h'];
				$w=$request['w'];
				$change = $request['abc'];
				 $src = $request['path'];
				 $path_info = pathinfo($src);
				 $ext = strtolower($path_info['extension']);
				
				$desti = 'images/profile_image/'.$change;	
			
				$virtulImg	=  $change;	
					
				if($ext=='jpg' || $ext=='jpeg')
				{
					$img_r = imagecreatefromjpeg($src);
					$dst_r = imagecreatetruecolor($w, $h);
					imagecopyresampled($dst_r,$img_r,0,0,$request['x'],$request['y'],$w,$h,$request['w'],$request['h']);
					$chkImg = imagejpeg($dst_r,$desti,$jpeg_quality);
					if($chkImg==1)
					{
						$filter = new Zend_Filter_ImageSize();
						$output_s = $filter->setHeight($image_small_height)
							->setWidth($image_small_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/images/profile_image/small/')
							->filter('../public/images/profile_image/'.$virtulImg);
						
						$output_l = $filter->setHeight($image_large_height)
							->setWidth($image_large_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/images/profile_image/large/')
							->filter('../public/images/profile_image/'.$virtulImg);
							
						$un_link = 'images/profile_image/'.$change;
						//@unlink($un_link);
						$my_Proimages	= BASE_URL.'/images/profile_image/small/';
			$my_Proimages_crop='/public/images/profile_image/';;	
			$total_image=10;
			$rest_image=$total_image-count($namespace->filename);
			$img1='~<img alt="Profile Picture" src="'.$my_Proimages.'/'.$namespace->filename[0].'"><br><a href="javascript:void(0)" onclick="delimage(0)">Delete</a><script type="text/javascript">
// Popup window code
function newPopup(url) { 
	popupWindow = window.open(
	url,"popUpWindow","height=400,width=500,left=10,top=10,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no")
}
</script>';
$img1.="<a href=\"JavaScript:newPopup('/public/Profile/crop/crop.php?p=".$my_Proimages_crop."/&amp;q=".$namespace->filename[0]."');\">";								
$img1.='<img src="'. BASE_URL.'/images/edit_blog.jpg" alt="Edit Rotate" align="absmiddle"  /> Edit Photo
								 </a></div>';
	
		echo $img1;
			exit;
					}
				}
				else if($ext=='png')
				{
					
					$img_r = imagecreatefrompng($src); /* Attempt to open */
					$dst_r = imagecreatetruecolor($request['w'], $request['h']);
					
					imagecopyresampled($dst_r,$img_r,0,0,$request['x'],$request['y'],$w,$h,$request['w'],$request['h']);
					$chkImg = imagepng($dst_r,$desti,9);
					if($chkImg==1)
					{
						$filter = new Zend_Filter_ImageSize();
						$output_s = $filter->setHeight($image_small_height)
							->setWidth($image_small_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/images/profile_image/small/')
							->filter('../public/images/profile_image/'.$virtulImg);
						$output_l = $filter->setHeight($image_large_height)
							->setWidth($image_large_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/images/profile_image/large/')
							->filter('../public/images/profile_image/'.$virtulImg);
						
						$un_link = 'images/profile_image/'.$change;
						//@unlink($un_link);
						$my_Proimages	= BASE_URL.'/images/profile_image/small/';
			$my_Proimages_crop='/public/images/profile_image/';	
			$total_image=10;
			$rest_image=$total_image-count($namespace->filename);
			$img1='~<img alt="Profile Picture" src="'.$my_Proimages.'/'.$namespace->filename[0].'"><br><a href="javascript:void(0)" onclick="delimage(0)">Delete</a><script type="text/javascript">
// Popup window code
function newPopup(url) { 
	popupWindow = window.open(
	url,"popUpWindow","height=400,width=500,left=10,top=10,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no")
}
</script>';
$img1.="<a href=\"JavaScript:newPopup('/public/Profile/crop/crop.php?p=".$my_Proimages_crop."/&amp;q=".$namespace->filename[0]."');\">";								
$img1.='<img src="'. BASE_URL.'/images/edit_blog.jpg" alt="Edit Rotate" align="absmiddle"  /> Edit Photo
								 </a></div>';
	
		echo $img1;
			exit;
					}
					
				}
				else if($ext=='gif')
				{						
					$img_r = imagecreatefromgif($src); /* Attempt to open */
					$dst_r = imagecreatetruecolor($request['w'], $request['h']);
					
					imagecopyresampled($dst_r,$img_r,0,0,$request['x'],$request['y'],$w,$h,$request['w'],$request['h']);
					$chkImg = imagegif($dst_r,$desti,9);
					if($chkImg==1)
					{
						$filter = new Zend_Filter_ImageSize();
						$output_s = $filter->setHeight($image_small_height)
							->setWidth($image_small_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/images/profile_image/small/')
							->filter('../public/images/profile_image/'.$virtulImg);
						$output_l = $filter->setHeight($image_large_height)
							->setWidth($image_large_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/images/profile_image/large/')
							->filter('../public/images/profile_image/'.$virtulImg);
						
						$un_link = 'images/profile_image/'.$change;
						//@unlink($un_link);
						$my_Proimages	= BASE_URL.'/images/profile_image/small/';
			$my_Proimages_crop='/public/images/profile_image/';	
			$total_image=10;
			$rest_image=$total_image-count($namespace->filename);
			$img1='~<img alt="Profile Picture" src="'.$my_Proimages.'/'.$namespace->filename[0].'"><br><a href="javascript:void(0)" onclick="delimage(0)">Delete</a><script type="text/javascript">
// Popup window code
function newPopup(url) { 
	popupWindow = window.open(
	url,"popUpWindow","height=400,width=500,left=10,top=10,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no")
}
</script>';
$img1.="<a href=\"JavaScript:newPopup('/public/Profile/crop/crop.php?p=".$my_Proimages_crop."/&amp;q=".$namespace->filename[0]."');\">";								
$img1.='<img src="'. BASE_URL.'/images/edit_blog.jpg" alt="Edit Rotate" align="absmiddle"  /> Edit Photo
								 </a></div>';
	
		echo $img1;
			exit;
					}
				} 
				echo '<script language="JavaScript">window.close();</script> '; 	
			
			
			}
			
			$img  = $request['q'];

			$urlpath =BASE_URL.'images/profile_image/'.$img; 
			$path ='../public/images/profile_image/'.$img;
			$size = getimagesize(trim($path));
			
			if($size[0]=='' && $size[1]=='')
			{
				
				echo "Your image is not stored in proper way..";
				echo "<br>This may cause of spaces in between name of your image..";
				echo "<br><br>Please remove the spaces from your image name and upload..";
				echo "<br><br><b>Thanks<br> Team ArtEx</b><br><br>";
				echo "<h2></h2>";
			
				exit;
			}
			
			if($size[0]>375 || $size[1]>325)
			{ 
				$jpeg_quality=150;
				$origWidth = $size[0];
				$origHeight = $size[1];
				$desti		= 'images/profile_image/'.$img;	
				// Change dimensions to fit maximum width and height
				$resizedWidth = $origWidth;
				$resizedHeight = $origHeight;
				$maxResizeWidth = 450;
				$maxResizeHeight = 370;
				if($resizedWidth > $maxResizeWidth) 
				{
					$aspectRatio = $maxResizeWidth / $resizedWidth;
					$resizedWidth = round($aspectRatio * $resizedWidth);
					$resizedHeight = round($aspectRatio * $resizedHeight);
				}
				if($resizedHeight > $maxResizeHeight) 
				{
					$aspectRatio = $maxResizeHeight / $resizedHeight;
					$resizedWidth = round($aspectRatio * $resizedWidth);
					$resizedHeight = round($aspectRatio * $resizedHeight);
				}
				
				$path_info 		= pathinfo($path);
				$ext			= strtolower($path_info['extension']);

				if($ext=='jpg' || $ext=='jpeg')
				{ 
					$img_r = imagecreatefromjpeg($path);
					$dst_r = imagecreatetruecolor($resizedWidth, $resizedHeight);
					imagecopyresampled($dst_r,$img_r,0,0,0,0,$resizedWidth,$resizedHeight,$size['0'],$size['1']);
					imagejpeg($dst_r,$desti,$jpeg_quality);
				}
				if($ext=='png')
				{
					$img_r = imagecreatefrompng($path);
					$dst_r = imagecreatetruecolor($resizedWidth, $resizedHeight);
					imagecopyresampled($dst_r,$img_r,0,0,0,0,$resizedWidth,$resizedHeight,$size['0'],$size['1']);
					imagepng($dst_r,$desti,9);
				}
				if($ext=='gif')
				{
					$img_r = imagecreatefromgif($path);
					$dst_r = imagecreatetruecolor($resizedWidth, $resizedHeight);
					imagecopyresampled($dst_r,$img_r,0,0,0,0,$resizedWidth,$resizedHeight,$size['0'],$size['1']);
					imagegif($dst_r,$desti.$img,9);
				}
			}
			
			$this->view->img	= $img;
			$this->view->path	= $urlpath;
			$this->view->path1	= $path;
			
		}
	}
	
	 public function delimageAction()
 {
 	$this->_helper->layout->disableLayout();
	$namespace = new Zend_Session_Namespace();
 	$request  =	$this->getRequest()->getParams();

	$index=$request['index']; 
	//echo "<pre>";
	$namespace->filename;
	//print_r($namespace->filename); 
	$file=$namespace->filename[$index]; 
	@unlink('../public/images/profile_image/'.$file);
	@unlink('../public/images/profile_image/small/'.$file);
	@unlink('../public/images/profile_image/large/'.$file);
	unset($namespace->filename[$index]);
	$namespace->filename=array_values($namespace->filename);
	$art   	 = new Application_Model_DbTable_Profile();
	$rec = $art->deleteimageprofileByName($file);
	
	
 }
 public function cmsAction(){
		if($this->namespace->email=='')
		{ 
			$this->_redirect('index/index');
		}
		 $this->_helper->layout->setLayout('layout');
			$id = $this->_getParam('page', 0);	
			$sec = $this->_getParam('sec', 0);
			$categorylist_model   = new Application_Model_DbTable_Categorylist();
			$categorylist_model_result = $categorylist_model->fetchAll($categorylist_model->select()->where('id = ?', $id));	
			$this->view->categorylist_model_result = $categorylist_model_result;
				
			/////////////get section name through sectionid////////////////////////////////////////
			$sectionlist_model   = new Application_Model_DbTable_Sectionlist();
			$sectionlist_model_result = $sectionlist_model->fetchAll($sectionlist_model->select()->where('id = ?', $sec)->where('status = ?', 1));	
			$this->view->sectionlist_model_result = $sectionlist_model_result;
			/////////////get sub category list through categoryid////////////////////////////////////////
			$subcategorylist_model   = new Application_Model_DbTable_Subcategorylist();
			$subcategorylist_model_result = $subcategorylist_model->fetchAll($subcategorylist_model->select()->where('category_id = ?', $id)->where('status = ?', 1));	
			$this->view->subcategorylist_model_result = $subcategorylist_model_result;
			$request  =	$this->getRequest()->getParams();
			
			
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
	}
	
}


/** Zend_Db_Table_Abstract
 * Handle file uploads via XMLHttpRequest
 */
class qqUploadedFileXhr {
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
	 
    function save($path,$filename) { 
		//echo $filename;  die('g');
		
		$namespace = new Zend_Session_Namespace(); 
		
        $input = fopen("php://input", "r");
		
		
        $temp = tmpfile();
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);
        
        if ($realSize != $this->getSize()){            
            return false;
        }
        
        $target = fopen($path, "w");        
        fseek($temp, 0, SEEK_SET);
		
       $entry= stream_copy_to_stream($temp, $target);
	   //echo $entry;
	   if(isset($entry))
		{
		/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
	 			$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
				$rec =	$allgloble->getglobelinfoAll();
				$this->view->rec=$rec;
				$image_small_width='80';
				$image_small_height='80';
				$image_large_width='150';
				$image_large_height='150';
				$profile_size=$rec[0]['profile_size'];
				$image_type=$rec[0]['image_type'];
				$this->view->profilesize=$profile_size;
				$profilesize=$profile_size*1024;
				
				
				$my_Proimages	= BASE_URL.'/images/profile_image/small/';
		/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////		
		require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "setup.php";

		require_once '../library/Zend/Filter/ImageSize.php';
		$filter = new Zend_Filter_ImageSize();
		$output_s = $filter->setHeight($image_small_height)
			->setWidth($image_small_width)
			->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
			->setThumnailDirectory('../public/images/profile_image/small/')
			->filter('../public/images/profile_image/'.$filename);
		$output_l = $filter->setHeight($image_large_height)
			->setWidth($image_large_width)
			->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
			->setThumnailDirectory('../public/images/profile_image/large/')
			->filter('../public/images/profile_image/'.$filename);
			$my_Proimages_crop	= '/public/images/profile_image/';
			$dbField	= array();
			$dbField	= array('img_user_id'=>$namespace->userid,'img_primary'=>'','img_name'=>$filename,'img_add_date'=>date("Y-m-d H:i:s"));
			$f[]=$filename;

			$namespace->filename=$f;
			//print_r($namespace->filename);
			$total_image=10;
			
			$rest_image=$total_image-count($namespace->filename);
			$img1='<img alt="Profile Picture" src="'.$my_Proimages.'/'.$namespace->filename[0].'"><br><a href="javascript:void(0)" onclick="delimage(0)">Delete</a><script type="text/javascript">
// Popup window code
function newPopup(url) { 
	popupWindow = window.open(
	url,"popUpWindow","height=400,width=500,left=10,top=10,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no")
}
</script>';
$img1.="<a href=\"JavaScript:newPopup('/public/Profile/crop/crop.php?p=".$my_Proimages_crop."/&amp;q=".$namespace->filename[0]."');\">";								
$img1.='<img src="'. BASE_URL.'/images/edit_blog.jpg" alt="Edit Rotate" align="absmiddle"  /> Edit Photo
								 </a></div>';
		

			echo $der[0].'~'.$img1.'~';
		
		
	}
    fclose($target);
    	return true;
    }
    function getName() {
        return $_GET['qqfile'];
    }
    function getSize() {
        if (isset($_SERVER["CONTENT_LENGTH"])){
            return (int)$_SERVER["CONTENT_LENGTH"];            
        } else {
            throw new Exception('Getting content length is not supported.');
        }      
    }   
}

/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 */
class qqUploadedFileForm {  
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {
		
        if(!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)){
            return false;
        }
		
       // return true;
    }
    function getName() {
        return $_FILES['qqfile']['name'];
    }
    function getSize() {
        return $_FILES['qqfile']['size'];
    }
}

class qqFileUploader {
    private $allowedExtensions = array();
   private $sizeLimit = 1024;
   //private $sizeLimit = 10485760;
    private $file;
	
    function __construct(array $allowedExtensions = array(), $sizeLimit = 10485760){ 
	
		$allowedExtensions = array_map("strtolower", $allowedExtensions);
            
        $this->allowedExtensions = $allowedExtensions;        
        $this->sizeLimit = $sizeLimit;
        
        $this->checkServerSettings();       

        if (isset($_GET['qqfile'])) {
			$this->file = new qqUploadedFileXhr();
        } elseif (isset($_FILES['qqfile'])) {
            $this->file = new qqUploadedFileForm();
        } else {
            $this->file = false; 
        }
    }
    
    private function checkServerSettings(){        
        $postSize = $this->toBytes(ini_get('post_max_size'));
        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));        
        
        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
            $size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';             
            die("{'error':'increase post_max_size and upload_max_filesize to $size'}");    
        }        
    }
    
    private function toBytes($str){
        $val = trim($str);
        $last = strtolower($str[strlen($str)-1]);
        switch($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;        
        }
        return $val;
    }
    
    /**
     * Returns array('success'=>true) or array('error'=>'error message')
     */
	 
    function handleUpload($uploadDirectory, $replaceOldFile = FALSE){
	$namespace 	= new Zend_Session_Namespace();
        if (!is_writable($uploadDirectory)){
            return array('error' => "Server error. Upload directory isn't writable.");
        }
        
        if (!$this->file){
            return array('error' => 'No files were uploaded.');
        }
        
        $size = $this->file->getSize();
        
        if ($size == 0) {
            return array('error' => 'File is empty.');
        }
        
        if ($size > $this->sizeLimit) {
            return array('Error' => 'File is too large. Maximum file size is '.($this->sizeLimit / 1024).'KB.');
        }
        
        $pathinfo = pathinfo($this->file->getName());
        $filename = $pathinfo['filename'];
        
        $ext = $pathinfo['extension'];
		
        if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
            $these = implode(', ', $this->allowedExtensions);
            return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');
        }
        
        if(!$replaceOldFile){
            /// don't overwrite previous files that were uploaded
            while (file_exists($uploadDirectory . $filename . '.' . $ext)) {
			
			if(count($namespace->filename)==0)  $filename .= 'image1';
			else
                $filename .= date('Y_M_dhis');
            }
        }
		else {
        if(count($namespace->filename)==0)  $filename .= 'image1';
		}
        if ($this->file->save($uploadDirectory . $filename . '.' . strtolower($ext),$filename.'.'.strtolower($ext))){
            return array('success'=>true);
        } else {
            return array('error'=> 'Could not save uploaded file.' .
                'The upload was cancelled, or server error encountered');
        }  
    }    
}   // Uploader Class
?>

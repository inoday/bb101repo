<?php

class ArtworkuploadController extends Zend_Controller_Action
{
	public function init()
    {
		$this->namespace= new Zend_Session_Namespace();
    	$this->request  = $this->getRequest()->getParams();
    	$this->userInfo	=	new Application_Model_DbTable_Profile();
		$this->country	=	new Application_Model_DbTable_ArtCountry();
		$this->baseUrl  = $this->getRequest()->getbaseUrl();
		$this->message	=	new Application_Model_ArtInbox();
		$this->view->action=$this->request['action'];
		$this->view->ind=$this->request['ind'];
		//$this->view->headScript()->appendFile('/js/settingvalidation.js');
	}
	private function getuserdetails($user_id) {
		$user_details['userInfo']   = $this->userInfo->getUserProfile($user_id);
		$user_details['userArt'] 	= $this->userInfo->getUserArt($user_id);
		$user_details['countInbox'] = $this->message->countInboxMsg($user_id);
		$user_details['bidding'] 	= $this->userInfo->bidding($user_id); // Show bidding item
		$user_details['liveart'] 	= $this->userInfo->liveart($user_id); // Show live artwork
		$user_details['watchlist'] 	= $this->userInfo->watchlist($user_id); // Show watchlist
		$user_details['feedback'] 	= $this->userInfo->feedback($user_id); // Show feedback count on artwork
		$user_details['display_sub_menu']   = $this->getRequest()->getActionName();
		return $user_details;
	}
	public function indexAction()
        {

		$namespace = new Zend_Session_Namespace();
		 // die;
		$this->_helper->layout->disableLayout();
		$this->_helper->layout->setLayout('layout');
		$artwork   	 = new Application_Model_DbTable_Artworkupload();
		$bannerlist_model   = new Application_Model_DbTable_ArtBanner();
		$this->view->getbannerlist = $bannerlist_model->getBannerlistByPage(2);
		if($namespace->email =='')
		{
			$this->_helper->redirector->gotosimple('index','index',true);
		}

		$param = $this->getRequest()->getParams();

		///////////////// INFO FROM GLOBAL TABLE////////////////////////////////
 		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();
		$rec =	$allgloble->getglobelinfoAll();
		$auction_comm=$rec[0]['auction_sold'];
		$scout_percent=$rec[0]['scout_percent'];
		$buyers_premium=$rec[0]['buyers_premium'];
		$auction_duration=$rec[0]['auction_duration'];
		$advertising_duration=$rec[0]['advertising_auction_duration'];
		$listing_duration=$rec[0]['max_auction_duration'];
		$listing_fee=$rec[0]['auction_percent'];
		$reserve_price=$rec[0]['auction_reserve_price'];
		$artwork_size=$rec[0]['artwork_size'];
		$this->view->artworksize=$artwork_size;

		$path='../public/help.ini';
		$configData = new Zend_Config_Ini ($path);
		$configDatafb = $configData->toArray();
		$fbconfigread = $configDatafb ['production'];
		$this->view->ini_array = $fbconfigread;
		/****************** Get login user details ****************************/
		$user_details = $this->getuserdetails($namespace->userdetails[0]['id']);
		$this->view->user_details = $user_details;
		//////////////////////FORM/////////////////////////////////////////////////
		$form = new Application_Form_Artworkuploadfrm();
        $this->view->form = $form;
		$id   = $this->_getParam('id', 0);
		$back = $this->_getParam('back', 0); //die;
		//////////////////////POST VALUE/////////////////////////////////////////////////
		if($id==0)
		{

			$checkArray = explode('/',$_SERVER['HTTP_REFERER'] ) ;
			if( $back != 0 )
			{
				//die('in');
				$namespace->filename=null;
			}

			else
			{
					echo "else";
			}

			if($param['value']=='update')
			{

		 		$this->view->post=unserialize(stripslashes($_POST['post']));
			 	$form->populate($this->view->post);
			 }
			else if($param['value']=='submit')
			 {
				if($param['ser']=='un')
				{
					$ar1=serialize($_POST);
					$ar=unserialize(stripslashes($ar1));
				}
				else {


				$ar=unserialize(stripslashes($_POST['post']));
				 }

		$dbField = array('category' => $ar['category'], 'title' => $ar['title'], 'subject' =>  $ar['subject'],'medium' =>  $ar['medium'],'style' =>  $ar['style'],'keywords' =>  $ar['keywords'],'width' =>  $ar['width'], 'height' => $ar['height'], 'depth' => $ar['depth'], 'convert' => $ar['convert'], 'created_in' => $ar['created_in'],'date_completed'=>$ar['date_completed'],'descrption'=>$ar['descrption'],'presentation'=>$ar['presentation'],'owner_id'=>$namespace->userdetails[0]['id'],'create_date'=>date("Y-m-d H:i:s"),'auction_duration'=>$auction_duration,'advertising_duration'=>$advertising_duration, 'listing_duration'=>$listing_duration, 	'listing_fee'=>$listing_fee, 'auction_reserve_price'=>$reserve_price, 'auction_comm'=>$auction_comm, 'buyers_premium'=>$buyers_premium, 'scout_percent'=>$scout_percent,'approval_status'=>'2');
		//print_r($dbField); die;
		$image=array();$filear=$namespace->userid;

		for($i=0;$i<count($namespace->filename);$i++)
		{
			if($i==0) $img=$namespace->filename[$i][$filear.'fname'];
				$image[]=$namespace->filename[$i][$filear.'fname'];
		}

		//$image=$namespace->filename[$filear.'fname'];
		//print_r($image); die();

		/////////////////////////INSERT IN table art_inbox//////////////////////////////////////
		$dbField1 = array('receiver_id' => $namespace->userdetails[0]['id'],'sender_id' => '0', 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $subject,'message'=> $message,'folder_id'=> '0');
		$inbox_res   	 = new Application_Model_DbTable_Artworkinbox();
		$rec2 	= $inbox_res->addartworkinbox($dbField1);

		if(empty($image) || empty($dbField)) {

			$this->_helper->redirector->gotosimple('index','Artworkupload') ;
		}

		///////////////////////////////////////////////////////////////////////////////////////////
		$rec1 	= $artwork->addartwork($dbField,$image);
		$msg	=	'Artwork added successfully.';
		//////////////////////Mail to moderator ///////////////////

		//////////////////////Get mail content set by admin///////////////////////
					$getmailcontent	=	new Application_Model_ArtMailFormats();
					$mailcontent		=	$getmailcontent->getMailContentById(6);
					 $formated_message=$mailcontent['content'];
					$formated_subject=$mailcontent['subject'];
					////////////////////////////////////////////////////////

					$siteUrl='http://'.$_SERVER['SERVER_NAME'];
					$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$this->baseUrl;
					 $message = sprintf($formated_message,$siteUrl,$img,$baseUrl,$rec1,'');
					$subject = sprintf($formated_subject,$ar['title']);

					$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com');
					Zend_Mail::setDefaultTransport($tr);

					$mail = new Zend_Mail();
					$mail->setBodyHtml($message);
					$mail->setFrom('admin@artex.com', 'www.artex.com');
					$mail->addTo($rec[0]['moderator_email'], 'Moderator');
					$mail->setSubject($subject);
					$mail->send($tr);
		/////////////////////Mail to owner ///////////////////
		//////////////////////Get mail content set by admin///////////////////////
					$getmailcontent	=	new Application_Model_ArtMailFormats();
					$mailcontent		=	$getmailcontent->getMailContentById(18);
					$formated_message=$mailcontent['content'];
					$formated_subject=$mailcontent['subject'];
					////////////////////////////////////////////////////////
					$namespace 	= new Zend_Session_Namespace();
					$user   	 = new Application_Model_ArtGroup();
					$namespace->email;
					$row=$user->getMemberID($namespace->email);
					$name=$row[0]['firstname']." ".$row[0]['last_name'];
					$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$this->baseUrl;
					$message = sprintf($formated_message,$name,$param['title'],$baseUrl,$rec1,$param['title']);
					$subject = sprintf($formated_subject,$param['title']);
					$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com');
					Zend_Mail::setDefaultTransport($tr);

					$mail = new Zend_Mail();
					$mail->setBodyHtml($message);
					$mail->setFrom('admin@artex.com', 'www.artex.com');
					$mail->addTo($namespace->email);
					$mail->setSubject($subject);
					$mail->send($tr);

					$namespace->filename=null;
					$this->_helper->redirector->gotosimple('moderated','Profile',true,array('msg'=>$msg)) ;
					//$namespace->filename
				}

				else
				{


					/*
					echo $checkArray = explode('/',$_SERVER['HTTP_REFERER'] ) ; die;
					if(!in_array('preview', $checkArray))
					{
 						$namespace->filename=null;
						die('if');
					}
					else
					{
						die('else');
					} */
				}

		 }
 		else if($id>0)
		{

		$artworkuploadlist = new Application_Model_DbTable_Artworkupload();
		foreach ($artworkuploadlist->getArtworkupload($id) as $key => $value) {
				$record[$key]	=	stripslashes($value);
			}

			$form->populate($record);
			$this->view->record=$record;
			$this->view->convert=$record['convert'];
			$this->view->id=$id;
			$artworkuploadimageslist_model   = new Application_Model_DbTable_Artworkuploadimages();
			$artworkuploadimageslist_model_result = $artworkuploadimageslist_model->fetchAll($artworkuploadimageslist_model->select()->where('artwork_id = ?', $id));
			$this->view->artworkuploadimageslist_model_result = $artworkuploadimageslist_model_result;
				if($param['value']=='update')
				{
				$this->view->post=unserialize(stripslashes($_POST['post']));

				$form->populate($this->view->post);
				}
				else if($param['value']=='submit')
				{
				if($param['ser']=='un')
				{
					$ar1=serialize($_POST);
					$ar=unserialize(stripslashes($ar1));
				}
				else {
				$ar=unserialize(stripslashes($_POST['post'])); }
				$dbField = array('category' => $ar['category'], 'title' => $ar['title'], 'subject' =>  $ar['subject'],'medium' =>  $ar['medium'],'style' =>  $ar['style'],'keywords' =>  $ar['keywords'],'width' =>  $ar['width'], 'height' => $ar['height'], 'depth' => $ar['depth'], 'convert' => $ar['convert'], 'created_in' => $ar['created_in'],'date_completed'=>$ar['date_completed'],'descrption'=>$ar['descrption'],'presentation'=>$ar['presentation'],'price'=>$ar['price'],'reserve_price'=>$ar['reserve_price'],'buy_it_now'=>$ar['buy_it_now'],'owner_id'=>$namespace->userdetails[0]['id'],'approval_status'=>'2','create_date'=>date("Y-m-d H:i:s"));
				//print_r($dbField);die;
				$image=array();
				//$image=$namespace->filename;
				$filear=$namespace->userid;
				for($i=0;$i<count($namespace->filename);$i++)
					{
						$image[]=$namespace->filename[$i][$filear.'fname'];
					}
				$rec = $artworkuploadlist->updateartwork($id,$dbField,$image);
				$msg = 'Artwork updated successfully.';
				$namespace->filename=null;
				$this->_helper->redirector->gotosimple('moderated','Profile',true,array('msg'=>$msg)) ;
				}
		}

	}
	public function ajaxAction()
    {
	$this->_helper->layout->disableLayout();
	//////////////////////FORM/////////////////////////////////////////////////
		$this->_helper->layout->disableLayout();
		$form = new Application_Form_Artworkuploada();
        $this->view->form = $form;
	}
	public function uconvertAction()
    {
	$this->_helper->layout->disableLayout();
	//////////////////////FORM/////////////////////////////////////////////////
		$this->_helper->layout->disableLayout();
		$form = new Application_Form_Artworkuploada();
        $this->view->form = $form;
	}

	public function previewAction()
    	{
		$namespace = new Zend_Session_Namespace();
		if($namespace->email =='')
		{
			$this->_helper->redirector->gotosimple('index','index',true);
		}
		$bannerlist_model   = new Application_Model_DbTable_ArtBanner();
		$this->view->getbannerlist = $bannerlist_model->getBannerlistByPage(2);
		/****************** Get login user details ****************************/
		$user_details = $this->getuserdetails($namespace->userdetails[0]['id']);
		$this->view->user_details = $user_details;
		//////////////////////POST VALUE/////////////////////////////////////////////////
		//print_r($_POST); die();
		$this->view->post=$_POST;
	}
	public function pricingnpostageinfoAction()
    {
		$this->_helper->layout->disableLayout();
		$this->_helper->layout->setLayout('layout');
		$namespace = new Zend_Session_Namespace();

		if($namespace->email =='')
		{
			$this->_helper->redirector->gotosimple('index','index',true);
		}
		$bannerlist_model   = new Application_Model_DbTable_ArtBanner();
		$this->view->getbannerlist = $bannerlist_model->getBannerlistByPage(2);
		$param = $this->getRequest()->getParams();
		/////////////////////help text/////////////////////////////////////
		$path='../public/help.ini';
		$configData = new Zend_Config_Ini ($path);
		$configDatafb = $configData->toArray();
		$fbconfigread = $configDatafb ['production'];
		$this->view->ini_array = $fbconfigread;
		/****************** Get login user details ****************************/
		$user_details = $this->getuserdetails($namespace->userdetails[0]['id']);
		$this->view->user_details = $user_details;
		//////////////////////FORM/////////////////////////////////////////////////
		$form = new Application_Form_Artworkuploadfrm();
        $this->view->form = $form;
		$id = $this->_getParam('id', 0);
		$this->view->id=$id;
		//////////////////////POST VALUE/////////////////////////////////////////////////

		/****************** Get login user details ****************************/

	// echo 'Updated successfully.';die('gf');
		if($id>0)
		{

		$artworkuploadlist = new Application_Model_DbTable_Artworkupload();
		foreach ($artworkuploadlist->getArtworkupload($id) as $key => $value) {
				$record[$key]	=	stripslashes($value);
			}
			//($record);
			//$form->populate($record);
			$this->view->record=$record;
			$this->view->convert=$record['convert'];
			$this->view->id=$id;

			$artworkuploadimageslist_model   = new Application_Model_DbTable_Artworkuploadimages();
			$artworkuploadimageslist_model_result = $artworkuploadimageslist_model->fetchAll($artworkuploadimageslist_model->select()->where('artwork_id = ?', $id));
			$this->view->artworkuploadimageslist_model_result = $artworkuploadimageslist_model_result;
				if($param['value']=='update')
				{
				$this->view->post=unserialize(stripslashes($_POST['post']));
				$form->populate($this->view->post);
				}
				else if($param['value']=='submit')
				{

				if($param['ser']=='un')
				{
					$ar1=serialize($_POST);//extract
					$ar=unserialize(stripslashes($ar1));
				}
				else {
				$ar=unserialize(stripslashes($_POST['post'])); }
				if($ar['stauts']) $status=$ar['stauts']; else $status=$ar['stauts1'];
				$dbField = array('sell_type'=>$stauts,'price'=>$ar['price'],'reserve_price'=>$ar['reserve_price'],'buy_it_now'=>$ar['buy_it_now'],'location'=>$ar['location'],'service'=>$ar['service'],'postage_cost'=>$ar['postage_cost'],'dispatch_time'=>$ar['dispatch_time'],'modify_date'=>date("Y-m-d H:i:s"));
		//////////////////////POST VALUE/////////////////////////////////////////////////
				$image=array();
				$artworkuploadlist = new Application_Model_DbTable_Artworkupload();
				$rec = $artworkuploadlist->updateartworkprice($id,$dbField);
				//print_r($dbField);
				$msg = 'Artwork price updated successfully.';
				$namespace->filename=null;
				$this->_helper->redirector->gotosimple('creditartworkuploadpay','artworkupload',true,array('msg'=>$msg,'id'=>$id)) ;
				}
		}
	}
	public function artworkuploadAction()
    {
		$namespace = new Zend_Session_Namespace();

		if($namespace->email =='')
		{
			$this->_helper->redirector->gotosimple('index','index',true);
		}
        $this->_helper->layout->disableLayout();
		$this->_helper->layout->setLayout('layout');
		$form = new Application_Form_Artworkupload();
        $this->view->form = $form;
		$adapter = new Zend_File_Transfer();
		$adapter->setDestination('../public/upload');  // set path for upload folder
		$adapter->setOptions(array('ignoreNoFile' => true));// remove validation to upload all files
		$files = $adapter->getFileInfo();
		$param = $this->getRequest()->getParams();

/////////////GET user id from user email (store in session)////////////////////////////////////////////////
		$namespace 	= new Zend_Session_Namespace();
		$user   	 = new Application_Model_ArtGroup();
		$namespace->email;
		$row=$user->getMemberID($namespace->email);

 /////////////END GET user id from user email (store in session)///////////////
	/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
 		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();
		$rec =	$allgloble->getglobelinfoAll();
		$this->view->rec=$rec;
		$image_small_width=$rec[0]['image_small_width'];
		$image_small_height=$rec[0]['image_small_height'];
		$image_mid_width=$rec[0]['image_mid_width'];
		$image_mid_height=$rec[0]['image_mid_height'];
		$image_large_width=$rec[0]['image_large_width'];
		$image_large_height=$rec[0]['image_large_height'];
		$artwork_size=$rec[0]['artwork_size'];
		$image_type=$rec[0]['image_type'];
		$this->view->artworksize=$artwork_size;
		$artworksize=$artwork_size*1024;

		/// check file size from global info
		$i=1; foreach($files as $fileID => $fileInfo) {
			if($fileInfo['size']<=$artworksize) {}
			else
			{
				$msg	=	'Image'.$i.' file size more than defined file size.';
				$this->_helper->redirector->gotosimple('moderated','Profile',true,array('msg'=>$msg)) ;
			}
			$i++;
		}

		$formData = $this->getRequest()->getParams();
		$tutorial   	 = new Application_Model_DbTable_Artworkupload();
		$id = $this->_getParam('id', 0);
		if($id==0)
			$this->view->bodyCopy = "Add Artwork";
		else
			$this->view->bodyCopy = "Edit Artwork";
///////////////////DELETE IMAGE IF UPLOAD NEW IMAGE IN EDIT SECTION/////////////////////
		if ($id > 0) {
			foreach($files as $fileID => $fileInfo) {
				if($fileID=='image1' && $fileInfo['name']!='' && $param['img1']!='') {
					$rec = $tutorial->deleteimageartwork($param['imgid1']);
					@unlink('../public/upload/small/'.$param['img1']);
					@unlink('../public/upload/mid/'.$param['img1']);
					@unlink('../public/upload/large/'.$param['img1']);}
				if($fileID=='image2' && $fileInfo['name']!='' && $param['img2']!=''){
					$rec = $tutorial->deleteimageartwork($param['imgid2']);
					@unlink('../public/upload/small/'.$param['img2']);
					@unlink('../public/upload/mid/'.$param['img2']);
					@unlink('../public/upload/large/'.$param['img2']);}
				if($fileID=='image3' && $fileInfo['name']!='' && $param['img3']!=''){
					$rec = $tutorial->deleteimageartwork($param['imgid3']);
					@unlink('../public/upload/small/'.$param['img3']);
					@unlink('../public/upload/mid/'.$param['img3']);
					@unlink('../public/upload/large/'.$param['img3']);}
				if($fileID=='image4' && $fileInfo['name']!='' && $param['img4']!=''){
					$rec = $tutorial->deleteimageartwork($param['imgid4']);
					@unlink('../public/upload/small/'.$param['img4']);
					@unlink('../public/upload/mid/'.$param['img4']);
					@unlink('../public/upload/large/'.$param['img4']);}
				if($fileID=='image5' && $fileInfo['name']!='' && $param['img5']!=''){
					$rec = $tutorial->deleteimageartwork($param['imgid5']);
					@unlink('../public/upload/small/'.$param['img4']);
					@unlink('../public/upload/mid/'.$param['img4']);
					@unlink('../public/upload/large/'.$param['img5']);}
			}
		}

	 	if ($this->getRequest()->isPost()) {

			if ($form->isValid($formData)) {
				////////////IMAGE UPLOAD//////////////////////////////////////
	 			$adapter = new Zend_File_Transfer();
	 			$path='../public/upload';
				$renameFilter = new Zend_Filter_File_Rename( $path );
				$adapter->setDestination('../public/upload');  // set path for upload folder
				$adapter->setOptions(array('ignoreNoFile' => true));	// remove validation to upload all files
				$files = $adapter->getFileInfo();//echo "<pre>";print_r($files);
		  		$param = $this->getRequest()->getParams();

				foreach($files as $fileID => $fileInfo)
				{
					if(!$fileInfo['name']=='')
					{
						$renameFilter->addFile( array('source' => $fileInfo['tmp_name'], 'target' => $row[0]['id'].'_'.$param['category'].'_'.$param['title'].'_'.$fileID.'_'.$fileInfo['name'], 'overwrite' => true ) );// rename file on desination folder
						$image[]=$row[0]['id'].'_'.$param['category'].'_'.$param['title'].'_'.$fileID.'_'.$fileInfo['name'];
						$adapter->addFilter($renameFilter);
					}
					else $image[]='';

				}//end for each
// add filters to Zend_File_Transfer_Adapter_Http
// receive all files
				$adapter->receive();

				/////////////////////////////for thumbnail/////////////////////////////////////////////
				require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "setup.php";

				require_once '../library/Zend/Filter/ImageSize.php';

				for($i=0;$i<count($image);$i++)
				{
					if($image[$i]!=''){
						$filter = new Zend_Filter_ImageSize();
						$output_s = $filter->setHeight($image_small_height)
						    ->setWidth($image_small_width)
						    ->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
						    ->setThumnailDirectory('../public/upload/small/')
						    ->filter('../public/upload/'.$image[$i]);

						$output_m = $filter->setHeight($image_mid_height)
						    ->setWidth($image_mid_width)
						    ->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
						    ->setThumnailDirectory('../public/upload/mid/')
						    ->filter('../public/upload/'.$image[$i]);

						$output_l = $filter->setHeight($image_large_height)
						    ->setWidth($image_large_width)
						    ->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
						    ->setThumnailDirectory('../public/upload/large/')
						    ->filter('../public/upload/'.$image[$i]);

					}
				}

				$dbField = array('category' => $param['category'], 'title' => $param['title'], 'price' =>  $param['price'],'edition' =>  $param['edition'],'frame' =>  $param['frame'],'keywords' =>  $param['keywords'],'frame_details' =>  $param['frame_details'], 'size' => $param['size'], 'primary_colors' => '', 'medium' => $param['medium'], 'style' => '','bid_start_date'=>$param['bid_start_date'],'location'=>'', 'shipping_type'=>$param['shipping_type'],descrption=>$param['descrption'] ,'owner_id'=>$row[0]['id'],'create_date'=>date("Y-m-d H:i:s"));
				if($id==0){
				///////////////////////Mail to moderator ///////////////////
					//$rec[0]['moderator_email']='reeta.verma@inoday.com';
	//////////////////////Get mail content set by admin///////////////////////
					$getmailcontent	=	new Application_Model_ArtMailFormats();
					$mailcontent		=	$getmailcontent->getMailContentById(8);
					$formated_message=$mailcontent['content'];
					$formated_subject=$mailcontent['subject'];
					////////////////////////////////////////////////////////
					$message = sprintf($formated_message,$param['title']);
					$subject = sprintf($formated_subject,$param['title']);

					$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com');
					Zend_Mail::setDefaultTransport($tr);

					$mail = new Zend_Mail();
					$mail->setBodyHtml($message);
					$mail->setFrom('admin@artex.com', 'www.artex.com');
					$mail->addTo($rec[0]['moderator_email'], 'Moderator');
					$mail->setSubject($subject);
					$mail->send($tr);

					$rec = $tutorial->addartwork($dbField,$image);
					$msg	=	'Artwork added successfully.';
					$this->_helper->redirector->gotosimple('moderated','Profile',true,array('msg'=>$msg)) ;
				}
				else {
					$rec = $tutorial->updateartwork($id,$dbField,$image);
					$msg = 'Artwork updated successfully.';
					$this->_helper->redirector->gotosimple('moderated','Profile',true,array('msg'=>$msg)) ;
				}
			}//end if valid data
			else {
                $form->populate($formData);
            }
		}


		if ($id > 0) {
			$artworkuploadlist = new Application_Model_DbTable_Artworkupload();

			//getting the 'sectionlist' for edit
			foreach ($artworkuploadlist->getArtworkupload($id) as $key => $value) {
				$record[$key]	=	stripslashes($value);
			}
			$form->populate($record);
			$artworkuploadimageslist_model   = new Application_Model_DbTable_Artworkuploadimages();
			$artworkuploadimageslist_model_result = $artworkuploadimageslist_model->fetchAll($artworkuploadimageslist_model->select()->where('artwork_id = ?', $id));
			$this->view->artworkuploadimageslist_model_result = $artworkuploadimageslist_model_result;
		}
	}

	function userartworkuploadlistAction()
	{
		$namespace = new Zend_Session_Namespace();

		if($namespace->email =='')
		{
			$this->_helper->redirector->gotosimple('index','index',true);
		}
		/****************** Get login user details ****************************/
		$user_details = $this->getuserdetails($namespace->userdetails[0]['id']);
		$this->view->user_details = $user_details;
		/////////////GET user id from user email (store in session)/////////////////////////////////////
		$namespace 	= new Zend_Session_Namespace();
		$user   	 = new Application_Model_ArtGroup();
		$namespace->email;
		$row=$user->getMemberID($namespace->email);
		$this->view->row = $row;
 /////////////END GET user id from user email (store in session)///////////////////////////////////

		$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
		$artworkuploadlist_model_result = $artworkuploadlist_model->userartwork($row[0]['id']);
		$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;

		$request  =	$this->getRequest()->getParams();
        if(isset($request['msg']))
		{
	        $request =	$this->getRequest()->getParams();
			$this->view->msg = $request['msg'];
		}
		$page = $this->_getParam('page',1);
		//send the page no.
		$this->view->page = $page;

		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($artworkuploadlist_model_result);
		$paginator->setItemCountPerPage(10);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
	}
	function payforartworkuploadlistAction()
	{
		$namespace = new Zend_Session_Namespace();

		if($namespace->email =='')
		{
			$this->_helper->redirector->gotosimple('index','index',true);
		}

/////////////GET user id from user email (store in session)////////////////////////////////////////////////
		$namespace 	= new Zend_Session_Namespace();
		$user   	 = new Application_Model_ArtGroup();
		$namespace->email;
		$row=$user->getMemberID($namespace->email);
 /////////////END GET user id from user email (store in session)///////////////////////////////////////

		$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
		$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->where('payment_status = 0')->where('owner_id =?',$row[0]['id']));
		$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;

		$request  =	$this->getRequest()->getParams();
        if(isset($request['msg']))
		{
	    	$request =	$this->getRequest()->getParams();
			$this->view->msg = $request['msg'];
		}
		$page = $this->_getParam('page',1);
		//send the page no.
		$this->view->page = $page;

		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($artworkuploadlist_model_result);
		$paginator->setItemCountPerPage(10);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
	}

	function artworkuploadpayAction()
	{
		$namespace = new Zend_Session_Namespace();

		if($namespace->email =='')
		{
			$this->_helper->redirector->gotosimple('index','index',true);
		}
/////////////////////////////////////////////////////////////////////////////
		$request  =	$this->getRequest()->getParams();
		$id = $this->_getParam('id', 0);
/////////////////Artwork detail///////////////////////////////////////////////////////////////////
		$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
		$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->where('id = ?', $id));
		$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
		foreach($artworkuploadlist_model_result as $event){
			$reserve_price= $event['reserve_price'];
			$auction_reserve_price= $event['auction_reserve_price'];
			$listing_fee= $event['listing_fee'];
		}
///////////////////////////////Mail /////////////////////////////////////////////

		if($reserve_price>0)
		{
		$auction_reserve_price=$auction_reserve_price;} else { $auction_reserve_price=0;}
		/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();
		$rec =	$allgloble->getglobelinfoAll();
		$merchant_email=$rec[0]['merchant_email'];
		//$merchant_email='reeta910@gmail.com';
/////////////GET user id from user email (store in session)////////////////////////////////////////////////
		$namespace 	= new Zend_Session_Namespace();
		$user   	 = new Application_Model_ArtGroup();
		$namespace->email;
		$row=$user->getMemberID($namespace->email);

 /////////////END GET user id from user email (store in session)//////////////////////////////////////
		$order_id=$id.":".rand();
 		$base_Url  = $this->getRequest()->getbaseUrl();
 		$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$base_Url;

		require_once('../library/Zend/paypal.class.php');  // include the class file
		$p = new paypal_class;             // initiate an instance of the class
		$p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
		//$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url

		// setup a variable for this script (ie: 'http://www.micahcarrick.com/paypal.php')
		//calculate amount http://artex.inoday.com/public/Artworkupload/artworkuploadpay/
		$amount=$listing_fee+$auction_reserve_price;
		$this_script = $baseUrl.'/Artworkupload/artworkuploadpay/';

// if there is not action variable, set the default action of 'process'
		if (empty($_GET['action'])) $_GET['action'] = 'process';

		switch ($_GET['action']) {

			case 'process':      // Process and order...

		      	$p->add_field('business', $merchant_email);
			  	$p->add_field('order_id', $order_id);
		      	$p->add_field('return', $this_script.'?action=success&id='.$id.'&orderid='.$order_id);
		      	$p->add_field('cancel_return', $this_script.'?action=cancel&id='.$id.'&orderid='.$order_id);

			  	$p->add_field('notify_url',  $baseUrl.'/Artworkupload/ipn');
		      	$p->add_field('item_name', 'Artwork Payment');
		      	$p->add_field('amount', $amount);

		      	$p->submit_paypal_post(); // submit the fields to paypal
		      //$p->dump_fields();      // for debugging, output a table of all the fields
		    break;

		    case 'success':      // Order was successful...

				/////////////////Artwork detail///////////////////////////////////////////////////////////////////
				$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
				$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->where('id = ?', $id));
				$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
				foreach($artworkuploadlist_model_result as $event){
					$title= $event['title'];
				}
				$artworkuploadimageslist_model   = new Application_Model_DbTable_Artworkuploadimages();
				$artworkuploadimageslist_model_result = $artworkuploadimageslist_model->fetchAll($artworkuploadimageslist_model->select()->where('artwork_id = ?', $id));

				foreach($artworkuploadimageslist_model_result as $artimage){
					$art_image=$artimage['image'];
				}
///////////////////////////////Mail /////////////////////////////////////////////
	//////////////////////Get mail content set by admin///////////////////////
				$getmailcontent	=	new Application_Model_ArtMailFormats();
				$mailcontent		=	$getmailcontent->getMailContentById(8);
				$formated_message=$mailcontent['content'];
				$formated_subject=$mailcontent['subject'];

				$name=$row[0]['firstname']." ".$row[0]['last_name'];
				$siteUrl='http://'.$_SERVER['SERVER_NAME'];
				$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$this->baseUrl;
				$message = sprintf($formated_message,$name,$title,$siteUrl,$art_image,$baseUrl,$id ,'');
				$subject=$formated_subject;
				$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com');
				Zend_Mail::setDefaultTransport($tr);

				$mail = new Zend_Mail();
				$mail->setBodyHtml($message);
				$mail->setFrom('admin@artex.com', 'www.artex.com');
				$mail->addTo($row[0]['email'], $name);

				$mail->setSubject($subject);
				$mail->send($tr);

				$status=1;
				$data = array(
            'payment_status'		=> $status,
			'bid_start_date'		=> date("Y-m-d H:i:s")
        	);
				/////////////////////////INSERT IN table art_inbox//////////////////////////////////////
					$dbField = array('receiver_id' => $row[0]['id'],'sender_id' => '0', 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $subject,'message'=> $message,'folder_id'=> '0');
					$inbox_res   	 = new Application_Model_DbTable_Artworkinbox();
					$rec = $inbox_res->addartworkinbox($dbField);
					///////////////////////////////////////////////////////////////////////////////////////////
				$dbFieldpay = array('artwork_id' => $id, 'transaction_id' => $order_id, 'transaction_date' =>   date("Y-m-d H:i:s"), 'amount' => $amount, 'status' => $status,'credit_card' => $creditcardtype, 	'credit_card_number' => $creditcardnumber, 	'exp_month' => $exp_date_month, 	'exp_year' => $exp_date_year, 	'cvv2' => $cvv_number,'payment_type'=>'1' );

				$payment   = new Application_Model_DbTable_Artworkuploadpayment();
				$res=$payment->addartworkpayment($dbFieldpay);

		      	$tutorial   = new Application_Model_DbTable_Artworkupload();

			  	$rec = $tutorial->updateartworkpaystatus($id,$data);
	  $this->_helper->redirector->gotosimple('listed','Profile',true,array('msg'=>'Thank you for your payment','ind'=>'1')) ;

      // You could also simply re-direct them to another page, or your own
      // order status page which presents the user with the status of their
      // order based on a database (which can be modified with the IPN code
      // below).Your transaction ID for this payment is: 63D98494LH445060X.
      	   	break;

   			case 'cancel':       // Order was canceled...


      // The order was canceled before being completed.
	  /////////////////Artwork detail////////////////////////////////
				$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
				$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->where('id = ?', $id));
				$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
				foreach($artworkuploadlist_model_result as $event){
					$title= $event['title'];
				}
///////////////////////////////Mail /////////////////////////////////////////////
	//////////////////////Get mail content set by admin///////////////////////
				$getmailcontent	=	new Application_Model_ArtMailFormats();
				$mailcontent		=	$getmailcontent->getMailContentById(9);
				$formated_message=$mailcontent['content'];
				$formated_subject=$mailcontent['subject'];

				$name=$row[0]['firstname']." ".$row[0]['last_name'];
				$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$this->baseUrl;
				$message = sprintf($formated_message,$name,$amount,$title,$baseUrl,'');
				$subject=$formated_subject;
				$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com');
				Zend_Mail::setDefaultTransport($tr);

				$mail = new Zend_Mail();
				$mail->setBodyHtml($message);
				$mail->setFrom('admin@artex.com', 'www.artex.com');
				$mail->addTo($row[0]['email'], $name);
				$mail->addCc($rec[0]['contact_email'], 'Aministrator');
				$mail->setSubject($subject);
				$mail->send($tr);
					/////////////////////////INSERT IN table art_inbox//////////////////////////////////////
					$dbField = array('receiver_id' => $row[0]['id'],'sender_id' => '0', 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $subject,'message'=> $message,'folder_id'=> '0');
					$inbox_res   	 = new Application_Model_DbTable_Artworkinbox();
					$rec = $inbox_res->addartworkinbox($dbField);
					///////////////////////////////////////////////////////////////////////////////////////////
 				$status=0;

				$dbFieldpay = array('artwork_id' => $id, 'transaction_id' => '', 'transaction_date' =>   date("Y-m-d H:i:s"), 'amount' => $amount, 'status' => $status,'credit_card' => $creditcardtype, 	'credit_card_number' => $creditcardnumber, 	'exp_month' => $exp_date_month, 	'exp_year' => $exp_date_year, 	'cvv2' => $cvv_number,'payment_type'=>'1' );

				$payment   = new Application_Model_DbTable_Artworkuploadpayment();
				$res=$payment->addartworkpayment($dbFieldpay);

	      		$tutorial   = new Application_Model_DbTable_Artworkupload();
		  		$status=0;
				$data = array(
            'payment_status'		=> $status
        	);
		  		$rec = $tutorial->updateartworkpaystatus($id,$data);
		  		$this->_helper->redirector->gotosimple('moderated','Profile',true,array('msg'=>'The payment was canceled.Please try again','ind'=>'1')) ;

	       	break;

   			case 'ipn':          // Paypal is calling page for IPN validation...

      		if ($p->validate_ipn()) {
         // For this example, we'll just email ourselves ALL the data.

		 $dbFieldpay = array('artwork_id' => $id, 'transaction_id' => '', 'transaction_date' =>   date("Y-m-d H:i:s"), 'amount' => $amount, 'status' => $status,'credit_card' => $creditcardtype, 	'credit_card_number' => $creditcardnumber, 	'exp_month' => $exp_date_month, 	'exp_year' => $exp_date_year, 	'cvv2' => $cvv_number,'payment_type'=>'123' );

				$payment   = new Application_Model_DbTable_Artworkuploadpayment();
				$res=$payment->addartworkpayment($dbFieldpay);
	         	$subject = 'Instant Payment Notification - Recieved Payment';
	         	$to = 'YOUR EMAIL ADDRESS HERE';    //  your email
	         	$body =  "An instant payment notification was successfully recieved\n";
	         	$body .= "from ".$p->ipn_data['payer_email']." on ".date('m/d/Y');
	         	$body .= " at ".date('g:i A')."\n\nDetails:\n";

         		foreach ($p->ipn_data as $key => $value) { $body .= "\n$key: $value"; }
	  				$rec = $tutorial->updateartworkpaystatus($id,$key);
	  $this->_helper->redirector->gotosimple('listed','Profile',true,array('msg'=>'Thank you for your payment','ind'=>'1')) ;
      			}
      		break;
 		}
	}
	function ipnAction()
	{
		$this->_helper->layout->disableLayout();
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

			// Additional headers
			$headers .= 'To: test <reeta.inoday@gmail.com>' . "\r\n";
			$headers .= 'From: ArtEx <admin@artex.com>' . "\r\n";
			 mail('reeta.inoday@gmail.com', 'the subject', 'the message',$headers);


		require_once('../library/Zend/paypal.class.php');  // include the class file
		$p = new paypal_class;
		$tutorial   = new Application_Model_DbTable_Artworkupload();
		$rec =$tutorial->updateipn('test');
		//$data=serialize($_POST);
		foreach ($_POST as $key => $value) {
		$value = urlencode(stripslashes($value));
		$req .= "&$key=$value";
		}
		//$rec = $tutorial->updateipn($data);
		//foreach ($p->ipn_data as $key => $value) { $body = "\n$key: $value";
	  	$rec =$tutorial->updateipn($req);
			//}
	}
	function creditartworkuploadpayAction()
	{
		$namespace = new Zend_Session_Namespace();

		if($namespace->email =='')
		{
			$this->_helper->redirector->gotosimple('index','index',true);
		}
 		$this->_helper->layout->disableLayout();
		$this->_helper->layout->setLayout('layout');
		$form = new Application_Form_Artworkcredit();
        $this->view->form = $form;
		$this->view->bodyCopy = "Credit Paypal Payment";
		$request  =	$this->getRequest()->getParams();
		$id = $this->_getParam('id', 0);
	//////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////Artwork detail///////////////////////////////////
		$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
		$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->where('id = ?', $id));
		$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
		foreach($artworkuploadlist_model_result as $event){
			$reserve_price= $event['reserve_price'];
			$auction_reserve_price= $event['auction_reserve_price'];
			$listing_fee= $event['listing_fee'];
		}
		$this->view->reserve_price = $reserve_price;
	/////////////GET user id from user email (store in session)//////////////////////////////////////
		$namespace 	= new Zend_Session_Namespace();
		$user   	 = new Application_Model_ArtGroup();
		$namespace->email;
		$row=$user->getMemberID($namespace->email);
	//@UPdate Record in srt_user_details
	/****************** Get login user details ****************************/
		$user_details = $this->getuserdetails($namespace->userdetails[0]['id']);
		$this->view->user_details = $user_details;
		//////////////////////FORM/////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////
	if($record['creditcardnumber']!='')
	{
			$dbField_details= array('billing_address' => $request['address1'], 'billing_city' => $request['city'], 'billing_state' =>  $request['state'], 'billing_country' => $request['country'], 'billing_zip' => $request['zip'],'user_id'=>$request['user_id'],'credit_card'=>$record['creditcardtype'] ,'credit_card_number'=>$record['creditcardnumber'],'exp_month'=>$record['exp_date_month'] ,'exp_year'=>$record['exp_date_year'],'cvv2'=>$record['cvv_number'] );
			$updateuser   = new Application_Model_DbTable_Registration();
			$ret = $updateuser->adduser_details($dbField_details,$request['user_id']);
	}////////////////////
	 /////////////END GET user id from user email (store in session)/////////////////////
		if($reserve_price>0)
		{
		$auction_reserve_price=$auction_reserve_price;} else { $auction_reserve_price=0;}
		/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();
		$rec1 =	$allgloble->getglobelinfoAll();
		$merchant_id=$rec1[0]['username'];//deepak_1324646632_biz_api1.inoday.com
		$password=$rec1[0]['password'];//1324646670
		$signature=$rec1[0]['signature'];//	An5ns1Kso7MWUdW4ErQKJJJ4qi4-Az1utoWx0MLHX5Xe77eTOq6J3URQ

		$this->view->firstname	=	$row[0]['first_name'];
		$this->view->email	=	$row[0]['email'];
		$this->view->user_id	=	$row[0]['id'];
		$record['address1']	=	$row[0]['billing_address'];
		$record['city']	=	$row[0]['billing_city'];
		$record['state']	=	$row[0]['billing_state'];
		$record['zip']	=	$row[0]['billing_zip'];
		$record['country']	=	$row[0]['billing_country'];
		$record['creditcardtype']			=$row[0]['credit_card'];
		$record['creditcardnumber']		=$row[0]['credit_card_number'];
		$record['exp_date_month']				=$row[0]['exp_month'] ;
		$record['exp_date_year']				=$row[0]['exp_year'];
		$record['cvv_number']					=$row[0]['cvv2'];
		$this->view->amount	=	$listing_fee+$auction_reserve_price;
		$this->view->id	=	$id;
		$form->populate($record);
 		if ($this->getRequest()->isPost()) {
 			$paymentType=urlencode('Authorization');
			$creditcardtype   = $request['creditcardtype'];
			$creditcardnumber   = $request['creditcardnumber'];
			$exp_date_month   = $request['exp_date_month'];
			$exp_date_year   = $request['exp_date_year'];
			$cvv_number   = $request['cvv_number'];

			$firstname   = $request['firstname'];
			$lastName   = $row[0]['last_name'];
			$user_id   = $request['user_id'];
			$amount   = $request['amount'];
			$address1 = urlencode($request['address1']);
			$address2 = urlencode($request['address2']);
			$city = urlencode($request['city']);
			$state = urlencode($request['state']);
			$zip = urlencode($request['zip']);
			$country = urlencode($request['country']);				// US or other valid country code
			$currencyID = urlencode('GBP');							// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')

 			$nvpStr_ =	"&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditcardtype&ACCT=$creditcardnumber".
					"&EXPDATE=$exp_date_month$exp_date_year&CVV2=$cvv_number&FIRSTNAME=$firstname&LASTNAME=$lastName".
					"&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID";
 ////////////////////////////////////////////////////////////////////////////
 			global $environment;
 			$environment = 'sandbox';	// or 'beta-sandbox' or 'live'
			// Set up your API credentials, PayPal end point, and API version.
		 	$API_UserName = urlencode($merchant_id);
		 	$API_Password = urlencode($password);
		 	$API_Signature = urlencode($signature);



			/*$API_UserName = urlencode('reeta9_1339842902_biz_api1.gmail.com');
		 	$API_Password = urlencode('1339842938');
		 	$API_Signature = urlencode('AiPC9BjkCyDFQXbSkoZcgqH3hpacAKF4.i0OHMMaZs9MSgmlugtDObFI');*/

			$API_Endpoint = "https://api-3t.paypal.com/nvp";

			if("sandbox" === $environment || "beta-sandbox" === $environment) {
				$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
			}
			$version = urlencode('51.0');

		// Set the curl parameters.
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
			curl_setopt($ch, CURLOPT_VERBOSE, 1);

			// Turn off the server and peer verification (TrustManager Concept).
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);

			// Set the API operation, version, and API signature in the request.
			$nvpreq = "METHOD=DoDirectPayment&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

			// Set the request as a POST FIELD for curl.
			curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

			// Get response from the server.
	 		$httpResponse = curl_exec($ch);

			if(!$httpResponse) {
				exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
			}

		// Extract the response details.
			$httpResponseAr = explode("&", $httpResponse);

			$httpParsedResponseAr = array();
			foreach ($httpResponseAr as $i => $value) {
				$tmpAr = explode("=", $value);
				if(sizeof($tmpAr) > 1) {
					$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
				}
			}

			if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
				exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
			}

			if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {

	/////////////////Artwork detail///////////////////////////////////
			$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
			$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->where('id = ?', $id));
			$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
			foreach($artworkuploadlist_model_result as $event){
				$title= $event['title'];
			}
			$artworkuploadimageslist_model   = new Application_Model_DbTable_Artworkuploadimages();
				$artworkuploadimageslist_model_result = $artworkuploadimageslist_model->fetchAll($artworkuploadimageslist_model->select()->where('artwork_id = ?', $id));

				foreach($artworkuploadimageslist_model_result as $artimage){
					$art_image=$artimage['image'];
				}
	///////////////////////////////Mail ///////////////////////////
	//////////////////////Get mail content set by admin///////////////////////
			$getmailcontent	=	new Application_Model_ArtMailFormats();
			$mailcontent		=	$getmailcontent->getMailContentById(8);
			$formated_message=$mailcontent['content'];
			$formated_subject=$mailcontent['subject'];
	////////////////////////BID mail to current bidder////////////////////////////////
			$name=$request['firstname']." ".$row[0]['last_name'];
			$siteUrl='http://'.$_SERVER['SERVER_NAME'];
			$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$this->baseUrl;
			$message = sprintf($formated_message,$name,$title,$siteUrl,$art_image,$baseUrl,$id ,'');
			$subject=$formated_subject;
			$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com');
			Zend_Mail::setDefaultTransport($tr);

			$mail = new Zend_Mail();
			$mail->setBodyHtml($message);
			$mail->setFrom('admin@artex.com', 'www.artex.com');
			$mail->addTo($row[0]['email'], $name);
			$mail->addCc($rec1[0]['contact_email'], 'Aministrator');
			$mail->setSubject($subject);
			$mail->send($tr);
			/////////////////////////INSERT IN table art_inbox//////////////////////////////////////
					$dbField = array('receiver_id' => $row[0]['id'],'sender_id' => '0', 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $subject,'message'=> $message,'folder_id'=> '0');
					$inbox_res   	 = new Application_Model_DbTable_Artworkinbox();
					$rec = $inbox_res->addartworkinbox($dbField);
					///////////////////////////////////////////////////////////////////////////////////////////
			///////////////////////add art_artworks_listing_payments table/////////////////////////////////
			$status=1;

			$dbFieldpay = array('artwork_id' => $id, 'transaction_id' => $httpParsedResponseAr["TRANSACTIONID"], 'transaction_date' =>   date("Y-m-d H:i:s"), 'amount' => $amount, 'status' => $status,'credit_card' => $creditcardtype, 	'credit_card_number' => $creditcardnumber, 	'exp_month' => $exp_date_month, 	'exp_year' => $exp_date_year, 	'cvv2' => $cvv_number,'payment_type'=>'2' );

			$payment   = new Application_Model_DbTable_Artworkuploadpayment();
			$res=$payment->addartworkpayment($dbFieldpay);
		///////////////////////////update payment status art_artworks table/////////////////
			$tutorial   = new Application_Model_DbTable_Artworkupload();
			$data = array(
            'payment_status'		=> $status,
			'bid_start_date'		=> date("Y-m-d H:i:s")
        	);
			$rec = $tutorial->updateartworkpaystatus($id,$data);
		$this->_helper->redirector->gotosimple('listed','Profile',true,array('msg'=>'Thank you for your credit card payment','ind'=>'1')) ;

			}
			else
			{
			/////////////////Artwork detail//////////////////////////////////
				$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
				$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->where('id = ?', $id));
				$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
				foreach($artworkuploadlist_model_result as $event){
					$title= $event['title'];
				}
		///////////////////////////Mail ///////////////////////////////////////
		//////////////////////Get mail content set by admin////////////////////
				$getmailcontent	=	new Application_Model_ArtMailFormats();
				$mailcontent	=	$getmailcontent->getMailContentById(9);
				$formated_message=$mailcontent['content'];
				$formated_subject=$mailcontent['subject'];

				$name=$request['firstname']." ".$row[0]['last_name'];
				$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$this->baseUrl;
				$message = sprintf($formated_message,$name,$amount,$title,$baseUrl,'');
				$subject=$formated_subject;
				$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com');
				Zend_Mail::setDefaultTransport($tr);

				$mail = new Zend_Mail();
				$mail->setBodyHtml($message);
				$mail->setFrom('admin@artex.com', 'www.artex.com');
				$mail->addTo($row[0]['email'], $name);
				$mail->addCc($rec1[0]['contact_email'], 'Aministrator');
				$mail->setSubject($subject);
				$mail->send($tr);
			/////////////////////////INSERT IN table art_inbox//////////////////////////////////////
					$dbField = array('receiver_id' => $row[0]['id'],'sender_id' => '0', 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $subject,'message'=> $message,'folder_id'=> '0');
					$inbox_res   	 = new Application_Model_DbTable_Artworkinbox();
					$rec = $inbox_res->addartworkinbox($dbField);
					///////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////add art_artworks_listing_payments table//////////////////////////
				$status=0;
				$data = array(
            'payment_status'		=> $status
        	);
				$dbFieldpay = array('artwork_id' => $id, 'transaction_id' => $httpParsedResponseAr["TRANSACTIONID"], 'transaction_date' =>   date("Y-m-d H:i:s"), 'amount' => $amount, 'status' => $status,'credit_card' => $creditcardtype, 	'credit_card_number' => $creditcardnumber, 	'exp_month' => $exp_date_month, 	'exp_year' => $exp_date_year, 	'cvv2' => $cvv_number,'payment_type'=>'2' );

				$payment   = new Application_Model_DbTable_Artworkuploadpayment();
				$res=$payment->addartworkpayment($dbFieldpay);
		//////////////////////////////////////update payment status art_artworks table/////////////////////
				$tutorial   = new Application_Model_DbTable_Artworkupload();

				$rec = $tutorial->updateartworkpaystatus($id,$data);
	 		$this->_helper->redirector->gotosimple('moderated','Profile',true,array('msg'=>'Credit card payment failed.Please try again','ind'=>'1')) ;

			}
		}
	}
	/////////////////START FOR ADMIN PANEL//////////////////
	///////////////////artworkuploadlist////////////////////
	/*
	@	show artworkuploadlist from the table
	@	Add by : Reeta Verma
	@		On: 13-02-2012
	**/
	public function artworkuploadhelpAction()
	{
		$namespace = new Zend_Session_Namespace();
		$request =	$this->getRequest()->getParams();
		if($namespace->adminemail =='')
		{
			$this->_helper->redirector->gotosimple('index','Admin',true);
		}
        $this->_helper->layout->setLayout('adminlayout');
		$form = new Application_Form_Artworkuploadhelp();
        $this->view->form = $form;
		$path='../public/help.ini';

		if($_POST)
		{

		$config = new Zend_Config_Ini($path,'production',array('skipExtends'=> true,'allowModifications' => true));
		$config->artwork_tag = $request['artwork_tag'];
		$config->size = $request['size'];
		$config->converter = $request['converter'];
		$config->date_completed = $request['date_completed'];
		$config->describe_artwork = $request['describe_artwork'];
		$config->presentation = $request['presentation'];
		$config->upload_photo = $request['upload_photo'];
		$config->reserve_price = $request['reserve_price'];
     	$writer = new Zend_Config_Writer_Ini();
		$writer->write ($path,$config);

		}
		$configData = new Zend_Config_Ini ($path);
		$configDatafb = $configData->toArray();
		$fbconfigread = $configDatafb ['production'];
		$form->populate($fbconfigread);
	}
	public function artworkuploadlistAction()
	{
		$namespace = new Zend_Session_Namespace();

		if($namespace->adminemail =='')
		{
			$this->_helper->redirector->gotosimple('index','Admin',true);
		}
        $this->_helper->layout->setLayout('adminlayout');
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		$this->view->adminemail = $adminemail;
		if($adminemail == ''){
           	$this->_helper->redirector('index');
        }

		$artworkuploadlist_model = new Application_Model_DbTable_Artworkupload();

		//////////////////////////////PAYMENT DETAILS////////////////////////////////////////////////////////////////////
		$artworkuploadlistpayment_model = new Application_Model_DbTable_Artworkuploadpayment();
		$artworkuploadlistpayment_model_result = $artworkuploadlistpayment_model->fetchAll($artworkuploadlistpayment_model->select()->where('status = ?', 1)->group('artwork_id'));
		$this->view->artworkuploadlistpayment_model_result = $artworkuploadlistpayment_model_result;

		$request  =	$this->getRequest()->getParams();
        if(isset($request['msg']))
		{
		    $request =	$this->getRequest()->getParams();
			$this->view->msg = $request['msg'];
		}
		$page = $this->_getParam('page',1);
		//send the page no.
		$this->view->page = $page;

		////////////////////////added by reeta (06-03-2012)//////////////////////////////////////////////////////////////////
		$searchelement = $request['searchelement'];
		$sortBy	  =	$request['sortBy'];
		$category =	$request['category'];
		$owner_id =	$request['owner_id'];

		if($request['searchelement']!='')
		{
			$namespace->psquestion = $request['searchelement'];
			$this->view->psquestion = $namespace->psquestion;
		}
		$artworkuploadlist_model_result = $artworkuploadlist_model->getartworkadmin($searchelement,$sortBy,$category,$owner_id);
		$global_info = new Application_Model_DbTable_ArtGlobelinfo();
		$auction_percent = $global_info->getglobelinfoAll();
		$this->view->auction_percent = $auction_percent;
		$this->view->sortBy = $sortBy;
		$this->view->category = $category;
		$this->view->owner_id = $owner_id;
		$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
		/////////////////////////////end added by reeta (06-03-2012)//////////////////////////////////////////////////////
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($artworkuploadlist_model_result);
		$paginator->setItemCountPerPage(20);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
	}

/*
@	Add by : Reeta Verma
@	On: 23-01-2012
**/
	public function viewartworkuploadlistAction()
	{
		$namespace = new Zend_Session_Namespace();

		if($namespace->adminemail =='')
		{
			$this->_helper->redirector->gotosimple('index','Admin',true);
		}
        $this->_helper->layout->setLayout('adminlayout');
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		$this->view->adminemail = $adminemail;
		if($adminemail == ''){
           	$this->_helper->redirector('index');
        }

		$request  =	$this->getRequest()->getParams();
		$id = $this->_getParam('id', 0);
		$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
		$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->where('id = ?', $id));
		$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;

		$artworkuploadimageslist_model   = new Application_Model_DbTable_Artworkuploadimages();
		$artworkuploadimageslist_model_result = $artworkuploadimageslist_model->fetchAll($artworkuploadimageslist_model->select()->where('artwork_id = ?', $id));
		$this->view->artworkuploadimageslist_model_result = $artworkuploadimageslist_model_result;

        if(isset($request['msg']))
        {
	    	$request =	$this->getRequest()->getParams();
			$this->view->msg = $request['msg'];
		}
	}
/*
@	Delete Category from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/
	public function  deleteartworkuploadlistuserAction()
	{
		$namespace = new Zend_Session_Namespace();

		if($namespace->email =='')
		{
			$this->_helper->redirector->gotosimple('index','index',true);
		}
		$request 		=	$this->getRequest()->getParams();
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']);
		}
		else
		{
			$id			=	$request['id'];
		}
		$artworkuploadlist			=	new Application_Model_DbTable_Artworkupload();
		$deleteartworkuploadlist		=	$artworkuploadlist->deleteartworkuploadtemplist($id);
		$this->view->deleteartworkuploadlist	=	$deleteartworkuploadlist;

		$sec			=	$request['sec'];
		$msg	=	'Artwork deleted successfully.';
		if($sec=='usr')
			$this->_helper->redirector->gotosimple('moderated','Profile',true,array('msg'=>$msg)) ;
		else
			$this->_helper->redirector->gotosimple('artworkuploadlist','artworkupload',true,array('msg'=>$msg)) ;
	}
/*
@	Delete Category from the table
@	Add by : Reeta Verma
@		On: 20-01-2012
**/
	public function  deleteartworkuploadlistAction()
	{
		$namespace = new Zend_Session_Namespace();

		if($namespace->adminemail =='')
		{
			$this->_helper->redirector->gotosimple('index','Admin',true);
		}
		$request 		=	$this->getRequest()->getParams();
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']);
		}
		else
		{
			$id			=	$request['id'];
		}
		$artworkuploadlist			=	new Application_Model_DbTable_Artworkupload();
		$deleteartworkuploadlist		=	$artworkuploadlist->deleteartworkuploadtemplist($id);
		$this->view->deleteartworkuploadlist	=	$deleteartworkuploadlist;

		$sec			=	$request['sec'];
		$msg	=	'Artwork deleted successfully.';
		if($sec=='usr')
			$this->_helper->redirector->gotosimple('moderated','Profile',true,array('msg'=>$msg)) ;
		else
			$this->_helper->redirector->gotosimple('artworkuploadlist','artworkupload',true,array('msg'=>$msg)) ;
	}
/*
@
@	Add by : Reeta Verma
@		On: 21-01-2012
**/
	public function  updateartworkuploadliststatusAction()
	{
		$namespace = new Zend_Session_Namespace();

		if($namespace->adminemail =='')
		{
			$this->_helper->redirector->gotosimple('index','Admin',true);
		}
		$request 		=	$this->getRequest()->getParams();
		$page			=	$request['page'];
		if($request['amount']!='')
		{
			$updatePay = new Application_Model_DbTable_Artworkupload();
			 $msg =	'Payment successfully.';
			$updatePay->updatePayment($request['id']);

			$insertPayListing = new Application_Model_DbTable_Artworkuploadpayment();
			$insertPayListing->updatePaymentListing($request['id'],$request['amount']);

			$this->_helper->redirector->gotosimple('artworkuploadlist','artworkupload',true,array('msg'=>$msg, 'page'=>$page)) ;
		}
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']);
		}
		else
		{
			$id			=	$request['id'];
		}
		$page			=	$request['page'];
		$status			=	$request['status'];
		/////////////////Mail srtwork owner for approval///////////
		/////////////////GLOBAL TABLE////////////////////////////////
		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();
		$rec =	$allgloble->getglobelinfoAll();

		$artworkuploadlist_model   = new Application_Model_DbTable_Artworkupload();
		$artworkuploadlist_model_result = $artworkuploadlist_model->fetchAll($artworkuploadlist_model->select()->where('id = ?', $id));
		$this->view->artworkuploadlist_model_result = $artworkuploadlist_model_result;
		foreach($artworkuploadlist_model_result as $event){
			$title= $event['title'];
			$user_id= $event['owner_id'];
		}

		$user_details = new Application_Model_Artuser();
		$userdetails=$user_details->getUserdetails($user_id); //print_r($userdetails);
		$to=$userdetails[0]['email'];

		$name = $userdetails[0]['firstname']." ".$userdetails[0]['last_name'];
		if($status==1)
			{
				$baseUrl = 'http://'.$_SERVER['HTTP_HOST'];
				 /////////////////////Get mail content set by admin///////////////////////
				$getmailcontent	= new Application_Model_ArtMailFormats();
				$mailcontent = $getmailcontent->getMailContentById(7);
				$formated_message=$mailcontent['content'];
				$formated_subject=$mailcontent['subject'];
				/////////////////////Get mail content set by admin///////////////////////
				$app='Accepted';
				$message = sprintf($formated_message,$name,$baseUrl);
				$subject=$formated_subject;
				$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com');
				Zend_Mail::setDefaultTransport($tr);

				$mail = new Zend_Mail();
				$mail->setBodyHtml($message);
				$mail->setFrom('admin@artex.com', 'www.artex.com');
				$mail->addTo($to, $name);
				$mail->addCc($rec[0]['contact_email'], 'Aministrator');
				$mail->setSubject($subject);
				$mail->send($tr);
				/////////////////////////INSERT IN table art_inbox//////////////////////////////////////
					$dbField = array('receiver_id' => $user_id,'sender_id' => '0', 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $subject,'message'=> $message,'folder_id'=> '0');
					$inbox_res   	 = new Application_Model_DbTable_Artworkinbox();
					$rec = $inbox_res->addartworkinbox($dbField);
					///////////////////////////////////////////////////////////////////////////////////////////
			}
		else
			{
				 /////////////////////Get mail content set by admin///////////////////////
				$getmailcontent	= new Application_Model_ArtMailFormats();
				$mailcontent = $getmailcontent->getMailContentById(19);
				$formated_message=$mailcontent['content'];
				$formated_subject=$mailcontent['subject'];
				/////////////////////Get mail content set by admin///////////////////////
				$app='Accepted';
				$base_Url  = $this->getRequest()->getbaseUrl();
				$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$base_Url;
				$message = sprintf($formated_message,$name,$baseUrl,'');
				$subject=$formated_subject;
				$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com');
				Zend_Mail::setDefaultTransport($tr);

				$mail = new Zend_Mail();
				$mail->setBodyHtml($message);
				$mail->setFrom('admin@artex.com', 'www.artex.com');
				$mail->addTo($to, $name);
				$mail->addCc($rec[0]['contact_email'], 'Aministrator');
				$mail->setSubject($subject);
				$mail->send($tr);
				/////////////////////////INSERT IN table art_inbox//////////////////////////////////////
				$dbField = array('receiver_id' => $user_id,'sender_id' => '0', 'receive_date'=>date("Y-m-d H:i:s"),'subject'=> $subject,'message'=> $message,'folder_id'=> '0');
				$inbox_res   	 = new Application_Model_DbTable_Artworkinbox();
				$rec = $inbox_res->addartworkinbox($dbField);
				///////////////////////////////////////////////////////////////////////////////////////////
			}

		$user =	new Application_Model_DbTable_Artworkupload();
		$updateArtworkuploadliststatus		=	$user->updateArtworkuploadliststatus($id,$status);
		$this->view->updateArtworkuploadliststatus	=	$updateArtworkuploadliststatus;

		if($status=='1')
			$msg	=	'Published successfully.';
		else
			$msg	=	'Unpublished successfully.';
			if($request['p']=='view')
		$this->_helper->redirector->gotosimple('viewartworkuploadlist','artworkupload',true,array('id'=>$id,'page'=>$page)) ;
		else
		$this->_helper->redirector->gotosimple('artworkuploadlist','artworkupload',true,array('msg'=>$msg,'page'=>$page)) ;
	}

	public function addartworkuploadAction()
    {
		$namespace = new Zend_Session_Namespace();

		if($namespace->adminemail =='')
		{
			$this->_helper->redirector->gotosimple('index','Admin',true);
		}
       	$this->_helper->layout->disableLayout();
		$this->_helper->layout->setLayout('adminlayout');
		$form = new Application_Form_Artworkupload();
        $this->view->form = $form;
		$adapter = new Zend_File_Transfer();
		$adapter->setDestination('../public/upload');  // set path for upload folder
		$adapter->setOptions(array('ignoreNoFile' => true));	// remove validation to upload all files// limit to 102400 100K
		$files = $adapter->getFileInfo();
		$param = $this->getRequest()->getParams();

		/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
 		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();
		$rec =	$allgloble->getglobelinfoAll();
		$this->view->rec=$rec;
		$image_small_width=$rec[0]['image_small_width'];
		$image_small_height=$rec[0]['image_small_height'];
		$image_mid_width=$rec[0]['image_mid_width'];
		$image_mid_height=$rec[0]['image_mid_height'];
		$image_large_width=$rec[0]['image_large_width'];
		$image_large_height=$rec[0]['image_large_height'];
		$artwork_size=$rec[0]['artwork_size'];
		$this->view->artworksize=$artwork_size;
		$artworksize=$artwork_size*1024;

		/// check file size from global info
		$i=1; foreach($files as $fileID => $fileInfo) {
			if($fileInfo['size']<=$artworksize) {}
			else
			{
				$msg = 'Image'.$i.' file size more than defined file size.';
				$this->_helper->redirector->gotosimple('artworkuploadlist','Artworkupload',true,array('msg'=>$msg)) ;
			}
			$i++;
		}
/////////////////////////////for thumbnail/////////////////////////////////////////////

/////////////GET user id from user email (store in session)////////////////////////////////////

		$owner_id=$param['owner_id'];
 /////////////END GET user id from user email (store in session)///////////////////////////////

		$formData = $this->getRequest()->getParams();
		$tutorial   	 = new Application_Model_DbTable_Artworkupload();
		$id = $this->_getParam('id', 0);
		if($id==0)
			$this->view->bodyCopy = "Add Artwork";
		else
			$this->view->bodyCopy = "Edit Artwork";
///////////////////DELETE IMAGE IF UPLOAD NEW IMAGE IN EDIT SECTION////////////////////////////
		if ($id > 0) {
			foreach($files as $fileID => $fileInfo) {

				if($fileID=='image1' && $fileInfo['name']!='' && $param['img1']!='') {
					$rec = $tutorial->deleteimageartwork($param['imgid1']);
					@unlink('../public/upload/small/'.$param['img1']);
					@unlink('../public/upload/mid/'.$param['img1']);
					@unlink('../public/upload/large/'.$param['img1']);
				}
				if($fileID=='image2' && $fileInfo['name']!='' && $param['img2']!=''){
					$rec = $tutorial->deleteimageartwork($param['imgid2']);
					@unlink('../public/upload/small/'.$param['img2']);
					@unlink('../public/upload/mid/'.$param['img2']);
					@unlink('../public/upload/large/'.$param['img2']);
				}
				if($fileID=='image3' && $fileInfo['name']!='' && $param['img3']!=''){
					$rec = $tutorial->deleteimageartwork($param['imgid3']);
					@unlink('../public/upload/small/'.$param['img3']);
					@unlink('../public/upload/mid/'.$param['img3']);
					@unlink('../public/upload/large/'.$param['img3']);
				}
				if($fileID=='image4' && $fileInfo['name']!='' && $param['img4']!=''){
					$rec = $tutorial->deleteimageartwork($param['imgid4']);
					@unlink('../public/upload/small/'.$param['img4']);
					@unlink('../public/upload/mid/'.$param['img4']);
					@unlink('../public/upload/large/'.$param['img4']);
				}
				if($fileID=='image5' && $fileInfo['name']!='' && $param['img5']!=''){
					$rec = $tutorial->deleteimageartwork($param['imgid5']);
					@unlink('../public/upload/small/'.$param['img4']);
					@unlink('../public/upload/mid/'.$param['img4']);
					@unlink('../public/upload/large/'.$param['img5']);
				}
			}
		}

		if ($this->getRequest()->isPost()) {

			if ($form->isValid($formData)) {

			////////////IMAGE UPLOAD/////////////////////////////

	 			$adapter = new Zend_File_Transfer();
	 			$path='../public/upload';
				$renameFilter = new Zend_Filter_File_Rename( $path );

				$adapter->setDestination('../public/upload');  // set path for upload folder
				$adapter->setOptions(array('ignoreNoFile' => true));	// remove validation to upload all files
				$files = $adapter->getFileInfo();
		  		$param = $this->getRequest()->getParams();

				foreach($files as $fileID => $fileInfo)
				{
					if(!$fileInfo['name']=='')
					{
						$renameFilter->addFile( array('source' => $fileInfo['tmp_name'], 'target' => $owner_id.'_'.$param['category'].'_'.$param['title'].'_'.$fileID.'_'.$fileInfo['name'], 'overwrite' => true ) );// rename file on desination folder
						$image[]=$owner_id.'_'.$param['category'].'_'.$param['title'].'_'.$fileID.'_'.$fileInfo['name'];
						$adapter->addFilter($renameFilter);
					}
					else $image[]='';

				}//end for each

				// receive all files
				$adapter->receive();

				require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "setup.php";

				require_once '../library/Zend/Filter/ImageSize.php';

				for($i=0;$i<count($image);$i++)
				{
					if($image[$i]!='')
					{
						$filter   = new Zend_Filter_ImageSize();
						$output_s = $filter->setHeight($image_small_height)
						    ->setWidth($image_small_width)
						    ->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
						    ->setThumnailDirectory('../public/upload/small/')
			    			->filter('../public/upload/'.$image[$i]);

						$output_m = $filter->setHeight($image_mid_height)
						    ->setWidth($image_mid_width)
						    ->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
						    ->setThumnailDirectory('../public/upload/mid/')
						    ->filter('../public/upload/'.$image[$i]);

						$output_l = $filter->setHeight($image_large_height)
						    ->setWidth($image_large_width)
						    ->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
						    ->setThumnailDirectory('../public/upload/large/')
						    ->filter('../public/upload/'.$image[$i]);

					}
				}

				if($id==0){
					$dbField = array('category' => $param['category'], 'title' => $param['title'], 'price' =>  $param['price'],'edition' =>  $param['edition'],'frame' =>  $param['frame'],'keywords' =>  $param['keywords'],'frame_details' =>  $param['frame_details'], 'size' => $param['size'], 'primary_colors' => '', 'medium' => $param['medium'], 'style' => '','bid_start_date'=>$param['bid_start_date'],'location'=>'', 'shipping_type'=>$param['shipping_type'],descrption=>$param['descrption'] ,'owner_id'=>$param['owner_id'],'create_date'=>date("Y-m-d H:i:s"));
					$rec = $tutorial->addartwork($dbField,$image);
					$msg	=	'Artwork added successfully.';
					$this->_helper->redirector->gotosimple('artworkuploadlist','Artworkupload',true,array('msg'=>$msg)) ;
				}
				else {
					$dbField = array('category' => $param['category'], 'title' => $param['title'], 'price' =>  $param['price'],'edition' =>  $param['edition'],'frame' =>  $param['frame'],'keywords' =>  $param['keywords'],'frame_details' =>  $param['frame_details'], 'size' => $param['size'], 'primary_colors' => '', 'medium' => $param['medium'], 'style' => '','bid_start_date'=>$param['bid_start_date'],'location'=>'', 'shipping_type'=>$param['shipping_type'],descrption=>$param['descrption'] ,'create_date'=>date("Y-m-d H:i:s"));
					$rec = $tutorial->updateartwork($id,$dbField,$image);
					$msg	=	'Artwork updated successfully.';
					$this->_helper->redirector->gotosimple('artworkuploadlist','Artworkupload',true,array('msg'=>$msg)) ;
				}
			}//end if valid data
			else {
        		$form->populate($formData);
        	}
		}


		if ($id > 0) {
			$artworkuploadlist = new Application_Model_DbTable_Artworkupload();

				//getting the 'sectionlist' for edit
			foreach ($artworkuploadlist->getArtworkupload($id) as $key => $value) {
				$record[$key]	=	stripslashes($value);
				if($key=='owner_id')
				{
					$record['owner_idd']=stripslashes($value);
				}
			}
			$this->view->owner=$record['owner_idd'];
			$form->populate($record);

			$artworkuploadimageslist_model   = new Application_Model_DbTable_Artworkuploadimages();
			$artworkuploadimageslist_model_result = $artworkuploadimageslist_model->fetchAll($artworkuploadimageslist_model->select()->where('artwork_id = ?', $id));
			$this->view->artworkuploadimageslist_model_result = $artworkuploadimageslist_model_result;
		}
	}
/////////////////////END FOR ADMIN PANEL/////////////////////////////////////////////////////

	public function uploadimageAction()
	{
		$this->_helper->layout->disableLayout();
		$request = $this->getRequest()->getParams();
		$namespace = new Zend_Session_Namespace();
		$filear=$namespace->userid;
		if(count($namespace->filename)>=10) { //print_r($namespace->filename);
			$my_Proimages	= BASE_URL.'/upload/small/';
		 $der[0]= "You have uploaded all 10 files";
		$total_image=10;
			$rest_image=$total_image-count($namespace->filename);
			$img1='<table cellpadding="5" cellspacing="5">
        	<tr>';
			for($i=0;$i<count($namespace->filename);$i++)
			{

            	$img1.='<td><img src="'.$my_Proimages.'/'.$namespace->filename[$i][$filear.'fname'].'" alt="" /><br/><a href="javascript:void(0)" onclick="delimage('. $i.')">Delete</a><script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
	url,"popUpWindow","height=400,width=500,left=10,top=10,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no")
}
</script>';
$img1.="<a href=\"JavaScript:newPopup('/public/Artworkupload/crop/crop.php?p=".$my_Proimages_crop."/&amp;q=".$namespace->filename[$i][$filear.'fname']."');\">";
$img1.='<img src="'. BASE_URL.'/images/edit_blog.jpg" alt="Edit Rotate" align="absmiddle"  /> Choose Thumbnail
								 </a></td>';
			}

            $img1.='</tr>
        </table>';
		if(count($namespace->filename)<10) {
        $img1.='<p class="floatR marginR5 font13">Your Pic: '.count($namespace->filename).'  | '.$rest_image.' more can be added</p>';
		}
		$img1.='<input type="hidden" name="count_img" id="count_img" value="'.count($namespace->filename).'" />';
			//$img1	='<span class="title">Uploaded Image</span><img src="'.BASE_URL.'timthumb.php?src='.$my_Proimages.'/'.$filename.'&h=151&w=172&zc=1" alt="My Picture" />';
			echo $der[0].'~'.$img1.'~';
		die();

		}
		/////////////////////////////////////////////////////////
		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();
		$rec =	$allgloble->getglobelinfoAll();
		$this->view->rec=$rec;
		$image_small_width=$rec[0]['image_small_width'];
		$image_small_height=$rec[0]['image_small_height'];
		$image_mid_width=$rec[0]['image_mid_width'];
		$image_mid_height=$rec[0]['image_mid_height'];
		$image_large_width=$rec[0]['image_large_width'];
		$image_large_height=$rec[0]['image_large_height'];
		$artwork_size=$rec[0]['artwork_size'];
		$image_type=$rec[0]['image_type'];
		$this->view->artworksize=$artwork_size;
		$artworksize=$artwork_size*1024;

		ini_set("POST_MAX_SIZE","10M");
		ini_set("UPLOAD_MAX_SIZE","10M");

		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		 $allowedExtensions = explode(',',$rec[0]['image_type']);
		// max file size in bytes
		$sizeLimit =$artwork_size*1024;
		//$sizeLimit = 10 * 1024 * 1024;

		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
		$uploads  = BASE_URL.'../public/upload/';
		$result = $uploader->handleUpload('../public/upload/');
		// to pass data through iframe you will need to encode all html tags { Success :true}
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES).'~';
		die();//'USPS'

	}
	//************************************ Image cropping  Action 22 March 2012 *****************************

	public function cropAction()
	{$namespace = new Zend_Session_Namespace();
		$this->_helper->layout->disableLayout();
		if ($this->getRequest())
		{
			$this->_helper->layout->disableLayout();
			$request=	$this->getRequest()->getParams();

			if(isset($request['path']))
			{
				/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
	 			$allgloble = new Application_Model_DbTable_ArtGlobelinfo();
				$rec =	$allgloble->getglobelinfoAll();
				$this->view->rec=$rec;
				$image_small_width=$rec[0]['image_small_width'];
				$image_small_height=$rec[0]['image_small_height'];
				$image_mid_width=$rec[0]['image_mid_width'];
				$image_mid_height=$rec[0]['image_mid_height'];
				$image_big_width=$rec[0]['image_big_width'];
				$image_big_height=$rec[0]['image_big_height'];
				$image_large_width=$rec[0]['image_large_width'];
				$image_large_height=$rec[0]['image_large_height'];
				$artwork_size=$rec[0]['artwork_size'];
				$image_type=$rec[0]['image_type'];
				$this->view->artworksize=$artwork_size;
				$artworksize=$artwork_size*1024;

				require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "setup.php";

				require_once '../library/Zend/Filter/ImageSize.php';
				/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
				$namespace 		= new Zend_Session_Namespace();

				$jpeg_quality 	= 200;

				$BaseUrl= 'http://'.$_SERVER['HTTP_HOST'].$base_Url;
				$h=$request['h'];
				$w=$request['w'];
				$change = $request['abc'];
				$src = $request['path'];
				 $path_info = pathinfo($src);
				 $ext = strtolower($path_info['extension']);

				$desti = 'upload/'.$change;

				$virtulImg	=  $change;

				if($ext=='jpg' || $ext=='jpeg')
				{
					$img_r = imagecreatefromjpeg($src);
					$dst_r = imagecreatetruecolor($w, $h);
					imagecopyresampled($dst_r,$img_r,0,0,$request['x'],$request['y'],$w,$h,$request['w'],$request['h']);
					$chkImg = imagejpeg($dst_r,$desti,$jpeg_quality);
					if($chkImg==1)
					{
						$filter = new Zend_Filter_ImageSize();
						$output_s = $filter->setHeight($image_small_height)
							->setWidth($image_small_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/upload/small/')
							->filter('../public/upload/'.$virtulImg);
						/*$output_m = $filter->setHeight($image_mid_height)
							->setWidth($image_mid_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/upload/mid/')
							->filter('../public/upload/'.$virtulImg);
						$output_b = $filter->setHeight($image_big_height)
							->setWidth($image_big_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/upload/big/')
							->filter('../public/upload/'.$virtulImg);
						$output_l = $filter->setHeight($image_large_height)
							->setWidth($image_large_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/upload/large/')
							->filter('../public/upload/'.$virtulImg);*/

						$un_link = 'upload/'.$change;
						//@unlink($un_link);
						$filear=$namespace->userid;
						$my_Proimages	= BASE_URL.'/upload/small/';
			$my_Proimages_crop=BASE_URL.'/upload/';
			$total_image=10;
			$rest_image=$total_image-count($namespace->filename);
			$img1='~<table cellpadding="5" cellspacing="5"><tr>';
			for($i=0;$i<count($namespace->filename);$i++)
			{
			if($change==$namespace->filename[$i][$filear.'fname']) $namespace->filename[$i][$filear.'crop']=1;
            	$img1.='<td><img src="'.$my_Proimages.'/'.$namespace->filename[$i][$filear.'fname'].'" alt="" /><br/><input type="hidden" name="crop'.$i.'" id="crop'.$i.'" value="'.$namespace->filename[$i][$filear.'crop'].'" size="5" /><a href="javascript:void(0)" onclick="delimage('. $i.')">Delete</a><script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
url,"popUpWindow","height=400,width=500,left=10,top=10,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no")
}
</script>';
				$img1.=' <div class="edit_pic">';
$img1.="<a href=\"JavaScript:newPopup('/public/Artworkupload/crop/crop.php?p=".$my_Proimages_crop."/&amp;q=".$namespace->filename[$i][$filear.'fname']."');\">	";
$img1.='<img src="'. BASE_URL.'/images/edit_blog.jpg" alt="Edit Rotate" align="absmiddle"  /> Choose Thumbnail
								 </a></div></td>';
			}

            $img1.='</tr>
        </table>';
		if(count($namespace->filename)<10) {
        $img1.='<p class="floatR marginR5 font13">Your Pic: '.count($namespace->filename).'  | '.$rest_image.' more can be added</p>';
		}
	$img1.='<input type="hidden" name="count_img" id="count_img" value="'.count($namespace->filename).'" />';
		echo $img1;
			exit;
					}
				}
				else if($ext=='png')
				{

					$img_r = imagecreatefrompng($src); /* Attempt to open */
					$dst_r = imagecreatetruecolor($request['w'], $request['h']);

					imagecopyresampled($dst_r,$img_r,0,0,$request['x'],$request['y'],$w,$h,$request['w'],$request['h']);
					$chkImg = imagepng($dst_r,$desti,9);
					if($chkImg==1)
					{
						$filter = new Zend_Filter_ImageSize();
						$output_s = $filter->setHeight($image_small_height)
							->setWidth($image_small_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/upload/small/')
							->filter('../public/upload/'.$virtulImg);
						/*$output_m = $filter->setHeight($image_mid_height)
							->setWidth($image_mid_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/upload/mid/')
							->filter('../public/upload/'.$virtulImg);
						$output_b = $filter->setHeight($image_big_height)
							->setWidth($image_big_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/upload/big/')
							->filter('../public/upload/'.$virtulImg);
						$output_l = $filter->setHeight($image_large_height)
							->setWidth($image_large_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/upload/large/')
							->filter('../public/upload/'.$virtulImg);*/

						$un_link = 'upload/'.$change;
						//@unlink($un_link);
						$filear=$namespace->userid;
						$my_Proimages	= BASE_URL.'/upload/small/';
			$my_Proimages_crop=BASE_URL.'/upload/';
			$total_image=10;
			$rest_image=$total_image-count($namespace->filename);
			$img1='~<table cellpadding="5" cellspacing="5"><tr>';
			for($i=0;$i<count($namespace->filename);$i++)
			{
			if($change==$namespace->filename[$i][$filear.'fname']) $namespace->filename[$i][$filear.'crop']=1;
            	$img1.='<td><img src="'.$my_Proimages.'/'.$namespace->filename[$i][$filear.'fname'].'" alt="" /><br/><input type="hidden" name="crop'.$i.'" id="crop'.$i.'" value="'.$namespace->filename[$i][$filear.'crop'].'" size="5" /><a href="javascript:void(0)" onclick="delimage('. $i.')">Delete</a><script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
url,"popUpWindow","height=400,width=500,left=10,top=10,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no")
}
</script>';
				$img1.=' <div class="edit_pic">';
$img1.="<a href=\"JavaScript:newPopup('/public/Artworkupload/crop/crop.php?p=".$my_Proimages_crop."/&amp;q=".$namespace->filename[$i][$filear.'fname']."');\">	";
$img1.='<img src="'. BASE_URL.'/images/edit_blog.jpg" alt="Edit Rotate" align="absmiddle"  /> Choose Thumbnail
								 </a></div></td>';
			}

            $img1.='</tr>
        </table>';
		if(count($namespace->filename)<10) {
        $img1.='<p class="floatR marginR5 font13">Your Pic: '.count($namespace->filename).'  | '.$rest_image.' more can be added</p>';
		}
	$img1.='<input type="hidden" name="count_img" id="count_img" value="'.count($namespace->filename).'" />';
		echo $img1;
			exit;
					}

				}
				else if($ext=='gif')
				{
					$img_r = imagecreatefromgif($src); /* Attempt to open */
					$dst_r = imagecreatetruecolor($request['w'], $request['h']);

					imagecopyresampled($dst_r,$img_r,0,0,$request['x'],$request['y'],$w,$h,$request['w'],$request['h']);
					$chkImg = imagegif($dst_r,$desti,9);
					if($chkImg==1)
					{
						$filter = new Zend_Filter_ImageSize();
						$output_s = $filter->setHeight($image_small_height)
							->setWidth($image_small_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/upload/small/')
							->filter('../public/upload/'.$virtulImg);
						/*$output_m = $filter->setHeight($image_mid_height)
							->setWidth($image_mid_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/upload/mid/')
							->filter('../public/upload/'.$virtulImg);
						$output_b = $filter->setHeight($image_big_height)
							->setWidth($image_big_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/upload/big/')
							->filter('../public/upload/'.$virtulImg);
						$output_l = $filter->setHeight($image_large_height)
							->setWidth($image_large_width)
							->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
							->setThumnailDirectory('../public/upload/large/')
							->filter('../public/upload/'.$virtulImg);*/

						$un_link = 'upload/'.$change;
						//@unlink($un_link);
						$filear=$namespace->userid;
						$my_Proimages	= BASE_URL.'/upload/small/';
			$my_Proimages_crop=BASE_URL.'/upload/';
			$total_image=10;
			$rest_image=$total_image-count($namespace->filename);
			$img1='~<table cellpadding="5" cellspacing="5"><tr>';
			for($i=0;$i<count($namespace->filename);$i++)
			{
			if($change==$namespace->filename[$i][$filear.'fname']) $namespace->filename[$i][$filear.'crop']=1;
            	$img1.='<td><img src="'.$my_Proimages.'/'.$namespace->filename[$i][$filear.'fname'].'" alt="" /><br/><input type="hidden" name="crop'.$i.'" id="crop'.$i.'" value="'.$namespace->filename[$i][$filear.'crop'].'" size="5" /><a href="javascript:void(0)" onclick="delimage('. $i.')">Delete</a><script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
url,"popUpWindow","height=400,width=500,left=10,top=10,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no")
}
</script>';
				$img1.=' <div class="edit_pic">';
$img1.="<a href=\"JavaScript:newPopup('/public/Artworkupload/crop/crop.php?p=".$my_Proimages_crop."/&amp;q=".$namespace->filename[$i][$filear.'fname']."');\">	";
$img1.='<img src="'. BASE_URL.'/images/edit_blog.jpg" alt="Edit Rotate" align="absmiddle"  /> Choose Thumbnail
								 </a></div></td>';
			}

            $img1.='</tr>
        </table>';
		if(count($namespace->filename)<10) {
        $img1.='<p class="floatR marginR5 font13">Your Pic: '.count($namespace->filename).'  | '.$rest_image.' more can be added</p>';
		}
	$img1.='<input type="hidden" name="count_img" id="count_img" value="'.count($namespace->filename).'" />';
		echo $img1;
			exit;
					}
				}
				echo '<script language="JavaScript">window.close();</script> ';


			}

			$img  = $request['q'];

			$urlpath =BASE_URL.'upload/'.$img;
			$path ='../public/upload/'.$img;
			$size = getimagesize(trim($path));

			if($size[0]=='' && $size[1]=='')
			{

				echo "Your image is not stored in proper way..";
				echo "<br>This may cause of spaces in between name of your image..";
				echo "<br><br>Please remove the spaces from your image name and upload..";
				echo "<br><br><b>Thanks<br> Team ArtEx</b><br><br>";
				echo "<h2></h2>";

				exit;
			}

			if($size[0]>375 || $size[1]>325)
			{
				$jpeg_quality=150;
				$origWidth = $size[0];
				$origHeight = $size[1];
				$desti		= 'upload/'.$img;
				// Change dimensions to fit maximum width and height
				$resizedWidth = $origWidth;
				$resizedHeight = $origHeight;
				$maxResizeWidth = 450;
				$maxResizeHeight = 370;
				if($resizedWidth > $maxResizeWidth)
				{
					$aspectRatio = $maxResizeWidth / $resizedWidth;
					$resizedWidth = round($aspectRatio * $resizedWidth);
					$resizedHeight = round($aspectRatio * $resizedHeight);
				}
				if($resizedHeight > $maxResizeHeight)
				{
					$aspectRatio = $maxResizeHeight / $resizedHeight;
					$resizedWidth = round($aspectRatio * $resizedWidth);
					$resizedHeight = round($aspectRatio * $resizedHeight);
				}

				$path_info 		= pathinfo($path);
				$ext			= strtolower($path_info['extension']);

				if($ext=='jpg' || $ext=='jpeg')
				{
					$img_r = imagecreatefromjpeg($path);
					$dst_r = imagecreatetruecolor($resizedWidth, $resizedHeight);
					imagecopyresampled($dst_r,$img_r,0,0,0,0,$resizedWidth,$resizedHeight,$size['0'],$size['1']);
					imagejpeg($dst_r,$desti,$jpeg_quality);
				}
				if($ext=='png')
				{
					$img_r = imagecreatefrompng($path);
					$dst_r = imagecreatetruecolor($resizedWidth, $resizedHeight);
					imagecopyresampled($dst_r,$img_r,0,0,0,0,$resizedWidth,$resizedHeight,$size['0'],$size['1']);
					imagepng($dst_r,$desti,9);
				}
				if($ext=='gif')
				{
					$img_r = imagecreatefromgif($path);
					$dst_r = imagecreatetruecolor($resizedWidth, $resizedHeight);
					imagecopyresampled($dst_r,$img_r,0,0,0,0,$resizedWidth,$resizedHeight,$size['0'],$size['1']);
					imagegif($dst_r,$desti.$img,9);
				}
			}

			$this->view->img	= $img;
			$this->view->path	= $urlpath;
			$this->view->path1	= $path;

		}
	}

 public function delimageAction()
 {
 	$this->_helper->layout->disableLayout();
	$namespace = new Zend_Session_Namespace();
 	$request  =	$this->getRequest()->getParams();
	$filear=$namespace->userid;
	$index=$request['index'];
	//echo "<pre>";
	$namespace->filename;
	//print_r($namespace->filename);
	 $file=$namespace->filename[$index][$filear.'fname'];
	@unlink('../public/upload/'.$file);
	@unlink('../public/upload/small/'.$file);
	@unlink('../public/upload/mid/'.$file);
	@unlink('../public/upload/big/'.$file);
	@unlink('../public/upload/large/'.$file);
	unset($namespace->filename[$index]);
	$namespace->filename=array_values($namespace->filename);
	$art   	 = new Application_Model_DbTable_Artworkupload();
	$rec = $art->deleteimageartworkByName($file);


 }

}

/** Zend_Db_Table_Abstract
 * Handle file uploads via XMLHttpRequest
 */
class qqUploadedFileXhr {
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
	  private $inputName;        /**     * @param string $inputName; defaults to the javascript default: 'qqfile'     */
	  public function __construct($inputName = 'qqfile'){        $this->inputName = $inputName;    }
    function save($path,$filename) {
		//echo $filename;

		$namespace = new Zend_Session_Namespace();

        $input = fopen("php://input", "r");


        $temp = tmpfile();
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);

        if ($realSize != $this->getSize()){
            return false;
        }

        $target = fopen($path, "w");
        fseek($temp, 0, SEEK_SET);

       $entry= stream_copy_to_stream($temp, $target);
	   //echo $entry;
	   if(isset($entry))
		{
		/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
	 			$allgloble = new Application_Model_DbTable_ArtGlobelinfo();
				$rec =	$allgloble->getglobelinfoAll();
				$this->view->rec=$rec;
				$image_small_width=$rec[0]['image_small_width'];
				$image_small_height=$rec[0]['image_small_height'];
				$image_mid_width=$rec[0]['image_mid_width'];
				$image_mid_height=$rec[0]['image_mid_height'];
				$image_big_width=$rec[0]['image_big_width'];
				$image_big_height=$rec[0]['image_big_height'];
				$image_large_width=$rec[0]['image_large_width'];
				$image_large_height=$rec[0]['image_large_height'];
				$artwork_size=$rec[0]['artwork_size'];
				$image_type=$rec[0]['image_type'];
				$this->view->artworksize=$artwork_size;
				$artworksize=$artwork_size*1024;


				$my_Proimages	= BASE_URL.'/upload/small/';
		/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
		require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "setup.php";

		require_once '../library/Zend/Filter/ImageSize.php';
		$filter = new Zend_Filter_ImageSize();


		$path_info = pathinfo($filename);
		$ext = strtolower($path_info['extension']);

		$output_s = $filter->setHeight($image_small_height)
			->setWidth($image_small_width)
			->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
			->setThumnailDirectory('../public/upload/small/')
			->filter('../public/upload/'.$filename);
		$output_m = $filter->setHeight($image_mid_height)
			->setWidth($image_mid_width)
			->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
			->setThumnailDirectory('../public/upload/mid/')
			->filter('../public/upload/'.$filename);
		$output_b = $filter->setHeight($image_big_height)
			->setWidth($image_big_width)
			->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
			->setThumnailDirectory('../public/upload/big/')
			->filter('../public/upload/'.$filename);
		$output_l = $filter->setHeight($image_large_height)
			->setWidth($image_large_width)
			->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
			->setThumnailDirectory('../public/upload/large/')
			->filter('../public/upload/'.$filename);
			$my_Proimages_crop	= BASE_URL.'/upload/';
			$dbField	= array();
			$dbField	= array('img_user_id'=>$namespace->userid,'img_primary'=>'','img_name'=>$filename,'img_add_date'=>date("Y-m-d H:i:s"));
			$filear=$namespace->userid;
			$f[]=array($filear.'fname'=>$filename,$filear.'crop'=>'0');
			//print_r($f);
			//echo "<br>1";
			//print_r($namespace->filename); die;
			//echo "<br>2";
			$namespace->filename=array_merge((array)$namespace->filename, (array)$f);
			//print_r($namespace->filename);
			$total_image=10;

			$rest_image=$total_image-count($namespace->filename);
			$img1='<table cellpadding="5" cellspacing="5">
        	<tr>';
			for($i=0;$i<count($namespace->filename);$i++)
			{

            	$img1.='<td><img src="'.$my_Proimages.'/'.$namespace->filename[$i][$filear.'fname'].'" alt="" /><br/><input type="hidden" name="crop'.$i.'" id="crop'.$i.'" value="'.$namespace->filename[$i][$filear.'crop'].'" size="5" /><a href="javascript:void(0)" onclick="delimage('. $i.')">Delete</a><script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
	url,"popUpWindow","height=400,width=500,left=10,top=10,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no")
}
</script>';
$img1.="<div class='edit_pic'><a href=\"JavaScript:newPopup('/public/Artworkupload/crop/crop.php?p=".$my_Proimages_crop."/&amp;q=".$namespace->filename[$i][$filear.'fname']."');\">";
$img1.='<img src="/public/images/edit_blog.jpg" alt="Edit Rotate" align="absmiddle"  />Choose Thumbnail
								 </a></div></div>

				</td>';
			}

            $img1.='</tr>
        </table>';
		if(count($namespace->filename)<10) {
        $img1.='<p class="floatR marginR5 font13">Your Pic: '.count($namespace->filename).'  | '.$rest_image.' more can be added</p>';
		}
		$img1.='<input type="hidden" name="count_img" id="count_img" value="'.count($namespace->filename).'" />';
			//$img1	='<span class="title">Uploaded Image</span><img src="'.BASE_URL.'timthumb.php?src='.$my_Proimages.'/'.$filename.'&h=151&w=172&zc=1" alt="My Picture" />';
			echo $der[0].'~'.$img1.'~';


	}
    fclose($target);
    	return true;
    }
    function getName() {
        return $_GET['qqfile'];
    }
    function getSize() {
        if (isset($_SERVER["CONTENT_LENGTH"])){
            return (int)$_SERVER["CONTENT_LENGTH"];
        } else {
            throw new Exception('Getting content length is not supported.');
        }
    }
}

/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 */
class qqUploadedFileForm {
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
	  private $inputName;        /**     * @param string $inputName; defaults to the javascript default: 'qqfile'     */
	  public function __construct($inputName = 'qqfile'){
	  $this->inputName = $inputName;
	  }
    function save($path,$filename) {

        if(!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)){
            return false;
        }
		else {

		/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
	 			$allgloble = new Application_Model_DbTable_ArtGlobelinfo();
				$rec =	$allgloble->getglobelinfoAll();
				$this->view->rec=$rec;
				$image_small_width=$rec[0]['image_small_width'];
				$image_small_height=$rec[0]['image_small_height'];
				$image_mid_width=$rec[0]['image_mid_width'];
				$image_mid_height=$rec[0]['image_mid_height'];
				$image_big_width=$rec[0]['image_big_width'];
				$image_big_height=$rec[0]['image_big_height'];
				$image_large_width=$rec[0]['image_large_width'];
				$image_large_height=$rec[0]['image_large_height'];
				$artwork_size=$rec[0]['artwork_size'];
				$image_type=$rec[0]['image_type'];
				$this->view->artworksize=$artwork_size;
				$artworksize=$artwork_size*1024;


				$my_Proimages	= BASE_URL.'/upload/small/';
		/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
		require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "setup.php";

		require_once '../library/Zend/Filter/ImageSize.php';
		$filter = new Zend_Filter_ImageSize();


		$path_info = pathinfo($filename);
		$ext = strtolower($path_info['extension']);

		$output_s = $filter->setHeight($image_small_height)
			->setWidth($image_small_width)
			->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
			->setThumnailDirectory('../public/upload/small/')
			->filter('../public/upload/'.$filename);
		$output_m = $filter->setHeight($image_mid_height)
			->setWidth($image_mid_width)
			->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
			->setThumnailDirectory('../public/upload/mid/')
			->filter('../public/upload/'.$filename);
		$output_b = $filter->setHeight($image_big_height)
			->setWidth($image_big_width)
			->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
			->setThumnailDirectory('../public/upload/big/')
			->filter('../public/upload/'.$filename);
		$output_l = $filter->setHeight($image_large_height)
			->setWidth($image_large_width)
			->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
			->setThumnailDirectory('../public/upload/large/')
			->filter('../public/upload/'.$filename);
			$my_Proimages_crop	= BASE_URL.'/upload/';
			$dbField	= array();
			$dbField	= array('img_user_id'=>$namespace->userid,'img_primary'=>'','img_name'=>$filename,'img_add_date'=>date("Y-m-d H:i:s"));
			$filear=$namespace->userid;
			$f[]=array($filear.'fname'=>$filename,$filear.'crop'=>'0');
			//print_r($f);
			//echo "<br>1";
			//print_r($namespace->filename); die;
			//echo "<br>2";
			$namespace->filename=array_merge((array)$namespace->filename, (array)$f);
			//print_r($namespace->filename);
			$total_image=10;

			$rest_image=$total_image-count($namespace->filename);
			$img1='<table cellpadding="5" cellspacing="5">
        	<tr>';
			for($i=0;$i<count($namespace->filename);$i++)
			{

            	$img1.='<td><img src="'.$my_Proimages.'/'.$namespace->filename[$i][$filear.'fname'].'" alt="" /><br/><input type="hidden" name="crop'.$i.'" id="crop'.$i.'" value="'.$namespace->filename[$i][$filear.'crop'].'" size="5" /><a href="javascript:void(0)" onclick="delimage('. $i.')">Delete</a><script type="text/javascript">
// Popup window code
function newPopup(url) {
	popupWindow = window.open(
	url,"popUpWindow","height=400,width=500,left=10,top=10,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no")
}
</script>';
$img1.="<div class='edit_pic'><a href=\"JavaScript:newPopup('/public/Artworkupload/crop/crop.php?p=".$my_Proimages_crop."/&amp;q=".$namespace->filename[$i][$filear.'fname']."');\">";
$img1.='<img src="/public/images/edit_blog.jpg" alt="Edit Rotate" align="absmiddle"  />Choose Thumbnail
								 </a></div></div>

				</td>';
			}

            $img1.='</tr>
        </table>';
		if(count($namespace->filename)<10) {
        $img1.='<p class="floatR marginR5 font13">Your Pic: '.count($namespace->filename).'  | '.$rest_image.' more can be added</p>';
		}
		$img1.='<input type="hidden" name="count_img" id="count_img" value="'.count($namespace->filename).'" />';
			//$img1	='<span class="title">Uploaded Image</span><img src="'.BASE_URL.'timthumb.php?src='.$my_Proimages.'/'.$filename.'&h=151&w=172&zc=1" alt="My Picture" />';
			echo $der[0].'~'.$img1.'~';



        return true;
		}
    }
    function getName() {
        return $_FILES['qqfile']['name'];
    }
    function getSize() {
        return $_FILES['qqfile']['size'];
    }
}

class qqFileUploader {
    private $allowedExtensions = array();
   private $sizeLimit = 1024;
   //private $sizeLimit = 10485760;
    private $file;

    function __construct(array $allowedExtensions = array(), $sizeLimit = 10485760){

		$allowedExtensions = array_map("strtolower", $allowedExtensions);

        $this->allowedExtensions = $allowedExtensions;
        $this->sizeLimit = $sizeLimit;

        $this->checkServerSettings();

       if (!empty($_FILES)) {
        // files upload understood by php
			$this->file = new qqUploadedFileForm();
		} else if (isset($_GET['qqfile'])) {
			$this->file = new qqUploadedFileXhr();
		} else {
			$this->file = false;
		}

    }

    private function checkServerSettings(){
        $postSize = $this->toBytes(ini_get('post_max_size'));
        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));

        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
            $size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';
            die("{'error':'increase post_max_size and upload_max_filesize to $size'}");
        }
    }

    private function toBytes($str){
        $val = trim($str);
        $last = strtolower($str[strlen($str)-1]);
        switch($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;
        }
        return $val;
    }

    /**
     * Returns array('success'=>true) or array('error'=>'error message')
     */

    function handleUpload($uploadDirectory, $replaceOldFile = FALSE){
	$namespace 	= new Zend_Session_Namespace();
        if (!is_writable($uploadDirectory)){
            return array('error' => "Server error. Upload directory isn't writable.");
        }

        if (!$this->file){
            return array('error' => 'No files were uploaded.');
        }

        $size = $this->file->getSize();

        if ($size == 0) {
            return array('error' => 'File is empty.');
        }

        if ($size > $this->sizeLimit) {
            return array('Error' => 'File is too large. Maximum file size is '.($this->sizeLimit / 1024).'KB.');
        }

        $pathinfo = pathinfo($this->file->getName());
        $filename = $pathinfo['filename'];

       $ext = $pathinfo['extension'];
		if($ext=='jpeg') $ext='jpg';
        if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
           $these = implode(', ', $this->allowedExtensions);
            return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');
        }


        if(!$replaceOldFile){
            /// don't overwrite previous files that were uploaded
            while (file_exists($uploadDirectory . $filename . '.' . $ext)) {

			if(count($namespace->filename)==0)  $filename .= 'image1';
			else
                $filename .= date('Y_M_dhis');
            }
        }
		else {
        if(count($namespace->filename)==0)  $filename .= 'image1';
		}
        if ($this->file->save($uploadDirectory . $filename . '.' . strtolower($ext),$filename.'.'.strtolower($ext))){
            return array('success'=>true);
        } else {
            return array('error'=> "Could not save uploaded file.' .'The upload was cancelled, or server error encountered");
        }
    }
}   // Uploader Class

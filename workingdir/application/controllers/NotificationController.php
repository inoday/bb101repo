<?php
     

class NotificationController extends Zend_Controller_Action
{
	///////////////////////////CATEGORY//////////////////////////
/*
@	show notificationlist from the table
@	Add by : Reeta Verma
@		On: 03-02-2012
**/ 
		public function notificationlistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
			

			$notificationlist_model   = new Application_Model_DbTable_ArtNotification();
			$notificationlist_model_result = $notificationlist_model->fetchAll();	
			
			
			////////////////////////////////////////////////////////////////////////////////////////
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			$page = $this->_getParam('page',1);
			//send the page no.
			$this->view->page = $page;			
					//////////////////////////////////////////////////////////////////////////////////////	
			//added by reeta (06-03-2012)
			////////////////////////////////////////////////////////////////////////////
			 $this->view->sortBy	=	$request['sortBy']; 
			if($request['searchelement']!='')
			{
				//serching all element with the table
				$searchelement	=	$request['searchelement'];
				//show the search element on the search box
				$namespace->psquestion = $request['searchelement'];
				$this->view->psquestion = $namespace->psquestion;	
				
				if(isset($request['sortBy']))
			{
				$notificationlist_model_result = $notificationlist_model->fetchAll($notificationlist_model->select()->where('subject Like ?', '%'.$searchelement.'%')->orWhere('message Like ?', '%'.$searchelement.'%')->order($request['sortBy']));	
			}
			else
				$notificationlist_model_result = $notificationlist_model->fetchAll($notificationlist_model->select()->where('subject Like ?', '%'.$searchelement.'%')->orWhere('message Like ?', '%'.$searchelement.'%'));	
		}
		else if($request['sortBy']!='')
			{
				$notificationlist_model_result = $notificationlist_model->fetchAll($notificationlist_model->select()->where('subject Like ?', '%'.$searchelement.'%')->orWhere('message Like ?', '%'.$searchelement.'%')->order($request['sortBy']));	
			}


			
		$this->view->notificationlist_model_result = $notificationlist_model_result;
		//end added by reeta (06-03-2012)
			/////////////////////////////////////////////////////////////////////////////	
			

			$page = $this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($notificationlist_model_result);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			$this->view->paginator = $paginator;
		}
		/*

@	Add by : Reeta Verma
@		On: 03-02-2012
**/		
	public function addnotificationlistAction(){
		
		$this->_helper->layout->setLayout('adminlayout');
	
		
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		
		$this->view->adminemail = $adminemail;
		//if admin session is logout it's go to index page
		if($adminemail == ''){
			$this->_helper->redirector('index');
		}

		$id = $this->_getParam('id', 0);
		if($id==0)
			$this->view->bodyCopy = "Add Notification";
		else
			$this->view->bodyCopy = "Edit Notification";
	    $form = new Application_Form_ArtNotification();
		
        $this->view->form = $form;
        if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getParams();
			//print_r($formData);die();
            if ($form->isValid($formData)) {
				$all   	  = $form->getValue('all'); 
				$subject  = $form->getValue('subject'); 
				$message  = $form->getValue('message');
				$send_date= $form->getValue('send_date');
				if($all=='1')
					$receipients     = 'all';
				else
					$receipients     = implode(',',$form->getValue('receipients'));
					
				$addDate 	= date("Y-m-d H:i:s");
				$tutorial   = new Application_Model_DbTable_ArtNotification();
			
				if($id==0){
					//add the 'cmslist'
					//////Send mail to all reciepient///////////////////////
					/*if($all=='1')
					{
						$allgroup =	new Application_Model_ArtGroup();
			   			$record	= $allgroup->getMemberName();  
						
						for ($c=0;$c<count($record);$c++)  {  
							$to=$record[$c]['email'];
							$firstname=$record[$c]['receipients'];
						 	// To send HTML mail, 
						 	///////////////// INFO FROM GLOBAL TABLE////////////////////////////////
							$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
							$rec =	$allgloble->getglobelinfoAll();
							$this->view->rec=$rec;
						 ///////////////////////////////////////////////////////////////////////
				 			$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
							Zend_Mail::setDefaultTransport($tr);
					
							$mail = new Zend_Mail();
							$mail->setBodyHtml($message);
							$mail->setFrom('admin@artex.com', 'www.artex.com');
							$mail->addTo($to, 'Some Recipient');
							$mail->addCc($rec[0]['contact_email'], 'Aministrator');
							$mail->setSubject($subject);
							$mail->send($tr);
						}
					}
					else
					{
						$allgroup =	new Application_Model_ArtGroup();
			    		$record	= $allgroup->getMemberallName($receipients);
						
						for ($c=0;$c<count($record);$c++) {  
							$to=$record[$c]['email'];
							$firstname=$record[$c]['rname'];
						 // To send HTML mail, 
						 ///////////////// INFO FROM GLOBAL TABLE////////////////////////////////
							$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
							$rec =	$allgloble->getglobelinfoAll();
							$this->view->rec=$rec;
						///////////////////////////////////////////////////////////////////////
							$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com'); 
							Zend_Mail::setDefaultTransport($tr);
							
							$mail = new Zend_Mail();
							$mail->setBodyHtml($message);
							$mail->setFrom('admin@artex.com', 'www.artex.com');
							$mail->addTo($to, 'Some Recipient');
							$mail->addCc($rec[0]['contact_email'], 'Aministrator');
							$mail->setSubject($subject);
							$mail->send($tr);
						}
					}*/
					///////////////////////////////////////////////////////////////////////////////////////
					$tutorial->addnotificationlist($subject,$message,$send_date,$receipients,$addDate);
					$msg	=	'Notification added successfully.';
					$this->_helper->redirector->gotosimple('notificationlist','notification',true,array('msg'=>$msg,'sec'=>$section_id)) ;
					
				}
				else {
				    //edit the 'cmslist'
				  $tutorial->updatenotificationlist($id,$subject,$message,$send_date,$receipients,$addDate);
				  $msg	=	'Notification update successfully.';
					$this->_helper->redirector->gotosimple('notificationlist','notification',true,array('msg'=>$msg,'sec'=>$section_id)) ;
				}
				
            } else {

                $form->populate($formData);
            }
        }
		
		$id = $this->_getParam('id', 0);
				
		if ($id > 0) {
			$notificationlist = new Application_Model_DbTable_ArtNotification();
			
			//getting the 'cmslist' for edit	
			foreach ($notificationlist->getnotificationlist($id) as $key => $value) {
			if($key=='receipients')
			{
			if(stripslashes($value)=='all')
			{
			$allgroup	=	new Application_Model_ArtGroup();
		    $record1		=	$allgroup->getMemberName();
			for ($c=0;$c<count($record1);$c++) {  
			$set[]=$record1[$c]['options']; 
			}
			$record['receipients']	=	$set;
			$record['all']	='1';
			}
			else {
			$record['receipients']	=	explode(',',stripslashes($value));$record['all']	='0';}
			}
			else
			{
				$record[$key]	=	stripslashes($value);
			}
			}

			$form->populate($record);
		}
	}
	
	/*
@	Add by : Reeta Verma
@		On: 03-02-2012
**/		
	public function  deletenotificationlistAction()
	{ 
	$chk=$_POST['chk']; 
	if(count($chk)>0)
		{
		for($i=0;$i<count($chk);$i++)
		{

		$id			=	$chk[$i];
		$notificationlist			=	new Application_Model_DbTable_ArtNotification();
		$deletenotificationlist		=	$notificationlist->deletenotificationlist($id);
		$this->view->deletenotificationlist	=	$deletenotificationlist;
		}
		}
		else
		{
		$request 		=	$this->getRequest()->getParams();
		$id			=	$request['id'];
		$notificationlist			=	new Application_Model_DbTable_ArtNotification();
		$deletenotificationlist		=	$notificationlist->deletenotificationlist($id);
		$this->view->deletenotificationlist	=	$deletenotificationlist;
		}
		$msg	=	'Notification deleted successfully.';
		$this->_helper->redirector->gotosimple('notificationlist','notification',true,array('msg'=>$msg)) ;

	}	
/*
@	Add by : Reeta Verma
@		On: 03-02-2012
**/		
	public function  updatenotificationliststatusAction()
	{ $chk=$_POST['chk']; 
		if(count($chk)>0)
		{
		for($i=0;$i<count($chk);$i++)
		{
		$request 		=	$this->getRequest()->getParams();
		$id			=	$chk[$i];
		 $status			=	$request['status'];

		$user			=	new Application_Model_DbTable_ArtNotification();
		$updatenotificationliststatus		=	$user->updatenotificationliststatus($id,$status);
		$this->view->updatenotificationliststatus	=	$updatenotificationliststatus;
		}
		}
		else
		{
		$request 		=	$this->getRequest()->getParams();
		$id			=	$request['id'];
		$status			=	$request['status'];

		$user			=	new Application_Model_DbTable_ArtNotification();
		$updatenotificationliststatus		=	$user->updatenotificationliststatus($id,$status);
		$this->view->updatenotificationliststatus	=	$updatenotificationliststatus;
		}
		if($status=='1')
		$msg	=	'Published successfully.';
		else
		$msg	=	'Unpublished successfully.';
		$this->_helper->redirector->gotosimple('notificationlist','notification',true,array('msg'=>$msg)) ;

	}	
/*
@	Add by : Reeta Verma
@		On: 03-02-2012
**/		
	public function viewnotificationlistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
	
			////////////////////////////////////////////////////////////////////////////////////////
			$id = $this->_getParam('id', 0);	
			$notificationlist_model   = new Application_Model_DbTable_ArtNotification();
			$notificationlist_model_result = $notificationlist_model->fetchAll($notificationlist_model->select()->where('id = ?', $id));	
			$this->view->notificationlist_model_result = $notificationlist_model_result;
			////////////////////////////////////////////////////////////////////////////////////
			
			$notificationlist = new Application_Model_DbTable_ArtNotification();
			
			//getting the 'cmslist' for edit	
			foreach ($notificationlist->getnotificationlist($id) as $key => $value) {
			if($key=='receipients')
			{
			if(stripslashes($value)!='all')
			{ 
			$allgroup	=	new Application_Model_ArtGroup();
		    $record		=	$allgroup->getMemberallName($value);

			}
			}

			}
			$this->view->record = $record;
			//print_r($record);
			///////////////////////////////////////////////////////////////////////////////////////
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}
		}

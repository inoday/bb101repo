<?php 
/*
@	controller for 'Group registration from front end' 
@	Add by : Abhishek
@		On: 17-01-2012
**/
class RegistrationController extends Zend_Controller_Action {
	
	public function init() {
		
		$namespace = new Zend_Session_Namespace();
		$this->request = $this->getRequest()->getParams();
	}
	
	public function groupregistrationAction() {
		$namespace = new Zend_Session_Namespace();
		 
		 if($namespace->email != '')
		 { 
			$this->_redirect('Profile/welcome');
		 }
		 $this->_helper->layout->disableLayout();
		 $this->_helper->layout->setLayout('layout');	
		 $form = new Application_Form_Registration();
         $this->view->form = $form;
         $Loginform = new Application_Form_Userlogin();
         $this->view->Loginform = $Loginform;
         $param = $this->getRequest()->getParams();

         if (isset($param['firstname']))
		 { 
			 if($param['firstname']!='')
			 {	     
			   	$firstname 	= $param['firstname'];
			    $lastname 		= $param['lastname'];
				$house_no 		= $param['house_no'];
				$street 		= $param['street'];
				$city 			= $param['city'];
				$zip 			= $param['zip'];
				
				if($param['newsletter']!=''){
					$newsletter 	= $param['newsletter'];
				}else{
					$newsletter 	= 'no';
				}
			    $scoutcode 	= '1';
			    $email 		= $param['email'];
			    $useremail 	= $param['userid'];
			    $password		= md5($param['password']);
				$original_password		= $param['password'];
			    $activation_value = mt_rand();
			    
			    //$message = 'Please click on given link to activate your account in Artex<a href="http://artex/public/accountactivate/">'.$activation_value;'user_id' => $useremail,
			    $dbField = array();
				$dbField = array('first_name' => $firstname, 'last_name' => $lastname, 'scoutcode' =>  $scoutcode, 'email' => $email,'user_id'=>$useremail, 'password' => $password, 'original_password'=>$original_password, 'activation_word' => $activation_value,'group_id'=>'3','create_date'=>date("Y-m-d H:i:s") );
				$dbField_details=array('house_no' => $house_no,'street' => $street,'city' => $city,'zip' => $zip, 'newsletter' => $newsletter);
			 	//-------------------------Send Mail----------------------------------// 
			 	$baseUrl = 'http://'.$_SERVER['HTTP_HOST'].$this->baseUrl;
				$to  = $email; // note the comma
	//////////////////////Get mail content set by admin///////////////////////
	$getmailcontent	=	new Application_Model_ArtMailFormats();
	$mailcontent		=	$getmailcontent->getMailContentById(1);
	$formated_message=$mailcontent['content'];
	$formated_subject=$mailcontent['subject'];
	////////////////////////////////////////////////////////
				$subject = $formated_subject;
				 $formated_message;	
				 $message = sprintf($formated_message,$firstname,$useremail,$email,$baseUrl,$activation_value,'');
			 		
						 
					// To send HTML mail, the Content-type header must be set
				$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com');
				Zend_Mail::setDefaultTransport($tr);
				
				$mail = new Zend_Mail();
			 	$mail->setBodyHtml($message);
		    	$mail->setFrom('admin@artex.com', 'www.artex.com');
		    	$mail->addTo($to, 'Some Recipient');
		    	$mail->setSubject($subject);
		    	$mail->send($tr);
							
//------------------------------------END------------------------------------------//
				$insertuser   = new Application_Model_DbTable_Registration();
			     
				$ret = $insertuser->adduser($dbField,$dbField_details);
				$_SESSION['msg'] = $firstname.' added successfully';
				$this->_redirect('Index/thanks/'.$status);
		 			
			 }
			 else
			 {
			    $msg = 'Please enter the value';
				$this->_redirect('Registration/groupregistration/'.$status);
			 }
	    }
/////////////////FACEBOOK INFO FROM GLOBAL TABLE////////////////////////////////
/*$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
$rec =	$allgloble->getglobelinfoAll();
$appid=$rec[0]['app_id'];
$secret=$rec[0]['secret'];
$domain=$rec[0]['domain'];

///////////////////////////////////////////////////////////////////////////////////////
	  
	  	$params = array(
				  'appId'  => $appid,
				  'secret' => $secret,
				  'domain' => $domain
					);
		
		$facebook = new Facebook($params);

			// Get User ID
		$namespace = new Zend_Session_Namespace();					  
		$this->view->loginUrl = $facebook->getLoginUrl(array('scope' => 'email'));*/
		 
		/////////////////////For Google login//////////////////////////
		
		$auth = TBS_Auth::getInstance();

        $providers = $auth->getIdentity();
	 	if ($this->_hasParam('provider')) {
            $provider = $this->_getParam('provider');

            switch ($provider) {
			 	case "google":
                    if ($this->_hasParam('code')) {
                        $adapter = new TBS_Auth_Adapter_Google(
                                $this->_getParam('code'));
                        $result = $auth->authenticate($adapter);
                    }
                    break;

            } 
            // What to do when invalid
            if (!$result->isValid()) {
                $auth->clearIdentity($this->_getParam('provider'));
                throw new Exception('Error!!');
            } else {
                $this->_redirect('/user/connect');
            }
        } else { // Normal login page
            $this->view->googleAuthUrl = TBS_Auth_Adapter_Google::getAuthorizationUrl();
		}
		/////////////////////End For Google login///////////////////////////
	}
	
	public function checkuniqueemailAction() {
		
		$userEmail = isset($this->request['ckemail']) ? $this->request['ckemail'] : '';
		$passemail = new Application_Model_DbTable_Registration();
		$status    = $passemail->checkEmail($userEmail); 
		if($status>0){
			echo $userEmail.' is already registered. Please enter different email Id';
		}else{
			echo '';
		}
		die;
	}
	public function checkuniqueuseridAction() {
		
		$uid = isset($this->request['ckuid']) ? $this->request['ckuid'] : '';
		$passuid = new Application_Model_DbTable_Registration();
		$status    = $passuid->checkUserId($uid);
		$s_uid='';
		if($status>0){
			$suid=$this->request['ckuid'].rand(5, 100);
			$status1    = $passuid->checkUserId($suid);
			$s_uid.=$status1>0?'':$suid;
			if($s_uid!='') $s_uid=$s_uid.', ';
			$suid=rand(5, 100).$this->request['ckuid'];
			$status1    = $passuid->checkUserId($suid);
			$s_uid.=$status1>0?'':$suid;
			if($s_uid!='') $s_uid=$s_uid.', ';
			$suid=rand(5, 100).$this->request['ckuid'].rand(5, 100);
			$status1    = $passuid->checkUserId($suid);
			$s_uid.=$status1>0?'':$suid;
			echo $uid.' is already registered. Please enter different user Id';
		}else{
			echo '';
		}
		die;//Good news! This user ID is available for you to use.<br>Or suggested user Id: '.$s_uid
	}
}
?>

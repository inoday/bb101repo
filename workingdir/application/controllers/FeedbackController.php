<?php
     
/*
@	Add by : Reeta Verma
@		On: 03-02-2012
**/

class FeedbackController extends Zend_Controller_Action
{
	///////////////////////////CATEGORY//////////////////////////
	  public function init()
    {   
	    
      	$namespace = new Zend_Session_Namespace();
		 
		if($namespace->adminemail =='') 
		{  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
    }
/*
@	Add by : Reeta Verma
@		On: 03-02-2012
**/ 
		public function feedbacklistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
			

			$feedbacklist_model   = new Application_Model_DbTable_ArtFeedback();
			$feedbacklist_model_result = $feedbacklist_model->fetchAll();	
			
			
			////////////////////////////////////////////////////////////////////////////////////////
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			$page = $this->_getParam('page',1);
			//send the page no.
			$this->view->page = $page;			
							//////////////////////////////////////////////////////////////////////////////////////	
			//added by reeta (06-03-2012)
			////////////////////////////////////////////////////////////////////////////
			$this->view->sortBy = $request['sortBy'];
			if($request['searchelement']!='')
			{
				//serching all element with the table
				$searchelement	=	$request['searchelement'];
				//show the search element on the search box
				$namespace->psquestion = $request['searchelement'];
				$this->view->psquestion = $namespace->psquestion;	
				
				if($request['sortBy']!='')
			{
				$feedbacklist_model_result = $feedbacklist_model->fetchAll($feedbacklist_model->select()->where('subject Like ?', '%'.$searchelement.'%')->orWhere('message Like ?', '%'.$searchelement.'%')->orWhere('email Like ?', '%'.$searchelement.'%')->order($request['sortBy']));	
			}
			else
				$feedbacklist_model_result = $feedbacklist_model->fetchAll($feedbacklist_model->select()->where('subject Like ?', '%'.$searchelement.'%')->orWhere('message Like ?', '%'.$searchelement.'%')->orWhere('email Like ?', '%'.$searchelement.'%'));	
		}
		else if($request['sortBy']!='')
			{
				$feedbacklist_model_result = $feedbacklist_model->fetchAll($feedbacklist_model->select()->where('subject Like ?', '%'.$searchelement.'%')->orWhere('message Like ?', '%'.$searchelement.'%')->orWhere('email Like ?', '%'.$searchelement.'%')->order($request['sortBy']));	
			}


			
		$this->view->feedbacklist_model_result = $feedbacklist_model_result;
		//end added by reeta (06-03-2012)
			/////////////////////////////////////////////////////////////////////////////		
			

			$page = $this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($feedbacklist_model_result);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			$this->view->paginator = $paginator;
		}

	
	/*
@	Add by : Reeta Verma
@		On: 03-02-2012
**/		
	public function  deletefeedbacklistAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		$feedbacklist			=	new Application_Model_DbTable_ArtFeedback();
		$deletefeedbacklist		=	$feedbacklist->deletefeedbacklist($id);
		$this->view->deletefeedbacklist	=	$deletefeedbacklist;

		
		$msg	=	'Feedback deleted successfully.';
		$this->_helper->redirector->gotosimple('feedbacklist','feedback',true,array('msg'=>$msg)) ;

	}	

/*
@	Add by : Reeta Verma
@		On: 03-02-2012
**/		
	public function viewfeedbacklistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
	
			////////////////////////////////////////////////////////////////////////////////////////
			$id = $this->_getParam('id', 0);	
			$feedbacklist_model   = new Application_Model_DbTable_ArtFeedback();
			$feedbacklist_model_result = $feedbacklist_model->fetchAll($feedbacklist_model->select()->where('id = ?', $id));	
			$this->view->feedbacklist_model_result = $feedbacklist_model_result;
			
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
}
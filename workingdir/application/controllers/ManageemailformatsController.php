<?php
/*
@	controller for 'Manage Email Formats' module
@	Add by : Anil Rawat
@		On: 29-10-2011
**/

class ManageemailformatsController extends Zend_Controller_Action
{
  
     public function init()
    {   
      	$namespace = new Zend_Session_Namespace();
		//$namespace->adminemail 	= $request['email'];
		//echo'<pre>';print_r($namespace->adminemail);die; 
		if($namespace->adminemail =='') 
		{  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
    }
	
	public function  emailformatAction()
	{ 
		$this->_helper->layout->setLayout('adminlayout');
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		$this->view->adminemail = $adminemail;
		if($adminemail = ''){
			$this->_helper->redirector->gotosimple('index','Admin',true);
		}
		
		$request = $this->getRequest()->getParams();
		$mailformat	=	new Application_Model_ArtMailFormats();
		$mailformat_result = $mailformat->getall();			
		$this->view->paginator = $mailformat_result; 
		
			
	}		
		
	public function  emailcontentAction()
	{ 
			$this->_helper->layout->setLayout('adminlayout');
			$request = $this->getRequest()->getParams();
			if(isset($request['msg'])){ $this->view->msg = $request['msg'];}
			$id	=	$request['title'];
					

		if ($this->_request->isPost()) {
			 $editor1 = stripslashes($this->_request->getPost('editor1'));
			 $subject	=	$request['subject'];
							if(!empty($editor1)){
				$update_content	=	new Application_Model_ArtMailFormats();
				$update_content->updateContent($id,$editor1,$subject);
				$msg = 'Email updated successfully';
				$this->_redirect('manageemailformats/emailformat/title/'.$title.'/msg/'.$msg);
				}
				if(empty($editor1))
				{
                $msg = 'Please submit the value';
				$this->_redirect('manageemailformats/emailformat/title/'.$title.'/msg/'.$msg);
				}
				if(empty($request['subject']))
				{
                $msg = 'Please submit the value';
				$this->_redirect('manageemailformats/emailformat/title/'.$title.'/msg/'.$msg);
				}
		}
		$title = $request['title'];
		$getmailcontent	=	new Application_Model_ArtMailFormats();
		$mailcontent		=	$getmailcontent->getMailContentById($id);
		$this->view->mailcontent = $mailcontent;
			
	}
		

}

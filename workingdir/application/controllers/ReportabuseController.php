<?php

class ReportabuseController extends Zend_Controller_Action
{
	public function init()
    {   
    	$namespace = new Zend_Session_Namespace();
		$this->request  = $this->getRequest()->getParams(); 
		$this->view->action=$this->request['action'];
		$this->view->ind=$this->request['ind'];
    }
	

	///////////////////////////ADMIN SECTION//////////////////////////
/*
@	Add by : Reeta Verma
@		On: 24-02-2012
**/ 
		public function reportabuselistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }

			$messages	= new Application_Model_DbTable_Artreportabuse();
			$params 	= $this->getRequest()->getParams();


			$messages_result = $messages->getMessage();	

			
			////////////////////////////////////////////////////////////////////////////////////////
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
				$this->view->msg = $request['msg'];			
			}			
			$page = $this->_getParam('page',1);
			//send the page no.
			$this->view->page = $page;			
			/////////////////////////////////////////////////////////////////////////////////////	
			//added by reeta (06-03-2012)
			////////////////////////////////////////////////////////////////////////////
				$this->view->sortBy	=	$request['sortBy']; 
				$bootstrap = $this->getInvokeArg('bootstrap');
				$resource  = $bootstrap->getPluginResource('db');
				$db 	   = $resource->getDbAdapter();
				$sql='SELECT 	* FROM art_feedbacks_reportabuse Where 1';
				if($request['user_id']!='')
				$sql.=' and user_id = '.$request['user_id'].'';
				if($request['searchelement']!='')
				$sql.=' and subject Like  "%'.$request['searchelement'].'%" OR message Like  "%'.$request['searchelement'].'%"';
				if($request['sortBy']!='')
				$sql.=' order by '.$request['sortBy'].'';
			
				$messages_result = $db->fetchAll($sql); 
			 	$this->view->sortBy	=	$request['sortBy']; 

				$this->view->user_id = $request['user_id'];

				$this->view->messages_result = $messages_result;
			//end added by reeta (06-03-2012)
			/////////////////////////////////////////////////////////////////////////////	
			

			$page = $this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($messages_result);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			$this->view->paginator = $paginator;
		}	
		
	public function  deletereportabuselistAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		
		$abuselist			=	new Application_Model_DbTable_Artreportabuse();
		$deleteabuselist		=	$abuselist->deletereprotabuselist($id);

		
		$msg	=	'Deleted successfully.';
		$this->_helper->redirector->gotosimple('reportabuselist','reportabuse',true,array('msg'=>$msg)) ;

	}	
public function viewreportabuselistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
	
			////////////////////////////////////////////////////////////////////////////////////////
			$id = $this->_getParam('id', 0);	
			$this->view->msgId	=	$this->_getParam('id', 0);
			$abuselist			=	new Application_Model_DbTable_Artreportabuse();
			$abuselist_result = $abuselist->getabuseByID($id);	
			$this->view->abuselist_result=$abuselist_result;
			
			$request  =	$this->getRequest()->getParams();
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			
		}
}

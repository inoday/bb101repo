<?php
/*
@	controller for 'Manage blog management' module
@	Add by : Anil Rawat
@		On: 07-11-2011
**/

class adminmanagebidController extends Zend_Controller_Action
{    
     
    public function init()
    {   
	    
      	$namespace = new Zend_Session_Namespace();
		 
		if($namespace->adminemail =='') 
		{  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
    }
   ///////////////////////////BID//////////////////////////
/*
@	Add by : Reeta Verma
@		On: 01-03-2012
**/ 
		public function artworkbidlistAction()
		{
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
			$request  =	$this->getRequest()->getParams();
			 $this->view->sortBy	=	$request['sortBy']; 
			if($request['sortBy']=='')
			$sortBy='bid_date';
			else
			$sortBy=$request['sortBy'];
			$bidlist_model   = new Application_Model_DbTable_Artworkbid();
			$bidlist_model_result = $bidlist_model->fetchAll($bidlist_model->select()->from(array('art_bidding'),array('countbid' => 'COUNT(*)','au_id'))->group('au_id')->order($sortBy));	
			if($request['bidder_id']!='')
			$bidlist_model_result = $bidlist_model->fetchAll($bidlist_model->select()->from(array('art_bidding'),array('countbid' => 'COUNT(*)','au_id'))->where('bidder_id = ?',$request['bidder_id'])->group('au_id')->order($sortBy));	
			if($request['au_id']!='')
			$bidlist_model_result = $bidlist_model->fetchAll($bidlist_model->select()->from(array('art_bidding'),array('countbid' => 'COUNT(*)','au_id'))->where('au_id = ?',$request['au_id'])->group('au_id')->order($sortBy));
			if($request['au_id']!='' && $request['bidder_id']!='')
			$bidlist_model_result = $bidlist_model->fetchAll($bidlist_model->select()->from(array('art_bidding'),array('countbid' => 'COUNT(*)','au_id'))->where('bidder_id = ?',$request['bidder_id'])->where('au_id = ?',$request['au_id'])->group('au_id')->order($sortBy));
			////////////////////////////////////////////////////////////////////////////////////////
			
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			$page = $this->_getParam('page',1);
			//send the page no.
			$this->view->page = $page;			
						
			
			$this->view->bidder_id = $request['bidder_id'];
			$this->view->au_id = $request['au_id'];
			$this->view->bidlist_model_result = $bidlist_model_result;//echo "<pre>";print_r($bidlist_model_result); die();
		
			
			

			$page = $this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($bidlist_model_result);
			$paginator->setItemCountPerPage(20);
			$paginator->setCurrentPageNumber($page);
			$this->view->paginator = $paginator;
		}

	
	/*
@	Add by : Reeta Verma
@		On: 01-03-2012
**/		
	public function  bidlistAction()
	{
			$request  =	$this->getRequest()->getParams();
            $this->_helper->layout->setLayout('adminlayout');
			$auth     = Zend_Auth::getInstance();
			$identity = $auth->getIdentity();
			$adminemail = $identity;
			$this->view->adminemail = $adminemail;
			if($adminemail == ''){
            	$this->_helper->redirector('index');
            }
			
			$auid = $this->_getParam('auid', 0);
			$this->view->sortBy	=	$request['sortBy']; 
			if($request['sortBy']=='')
			$sortBy='bid_date DESC';
			else
			$sortBy=$request['sortBy'];
			$bidlist_model   = new Application_Model_DbTable_Artworkbid();
			$bidlist_model_result = $bidlist_model->fetchAll($bidlist_model->select()->where('au_id = ' . $auid)->order($sortBy));	
			$this->view->bidlist_model_result = $bidlist_model_result;//echo "<pre>";print_r($bidlist_model_result); die();
			
			////////////////////////////////////////////////////////////////////////////////////////
			
            if(isset($request['msg']))
			{
	            $request =	$this->getRequest()->getParams();
				$this->view->msg = $request['msg'];			
			}			
			$page = $this->_getParam('page',1);
			//send the page no.
			$this->view->page = $page;			
			
			

			$page = $this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($bidlist_model_result);
			$paginator->setItemCountPerPage(20);
			$paginator->setCurrentPageNumber($page);
			$this->view->paginator = $paginator;
		}	


	
/////////////////////////////////////////////////////////////////////////////////////////////////////////
		
}



 
?>
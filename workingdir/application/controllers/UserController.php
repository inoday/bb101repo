<?php 

class UserController extends Zend_Controller_Action
{
    public function loginAction()
    {
        $auth = TBS_Auth::getInstance();

        $providers = $auth->getIdentity();

        // Here the response of the providers are registered
        if ($this->_hasParam('provider')) {
            $provider = $this->_getParam('provider');

            switch ($provider) {
                
                case "google":
                    if ($this->_hasParam('code')) {
                        $adapter = new TBS_Auth_Adapter_Google(
                                $this->_getParam('code'));
                        $result = $auth->authenticate($adapter);
                    }
                    break;

            }
            // What to do when invalid

            if (!$result->isValid()) {
                $auth->clearIdentity($this->_getParam('provider'));
                throw new Exception('Error!!');
            } else {
                $this->_redirect('/user/connect');
            }
        } else { // Normal login page
            $this->view->googleAuthUrl = TBS_Auth_Adapter_Google::getAuthorizationUrl();
        }

    }
    public function connectAction()
    {
        $auth = TBS_Auth::getInstance();
        if (!$auth->hasIdentity()) {
            throw new Zend_Controller_Action_Exception('Not logged in!', 404);
        }
		 foreach ($auth->getIdentity() as $getIdentity)
		 {
		 }
		 $prof=$getIdentity->getApi()->getProfile(); 
/* echo '<pre>';
  print_r($prof);
  die; */
///////////////////////////////////////////////////////////////////////////

$namespace = new Zend_Session_Namespace();
// User Model
$userstuff		= new Application_Model_Artuser();
	
// Check for user existence
$artEmail=$prof['email'];
$status = $userstuff->chkAval_user($artEmail);

if($status)
		{
	// User already exist
		$namespace->email		= $prof['email'];
		$namespace->fname 		= $prof['given_name'];
		$namespace->useremail 	= $prof['id'];
		/////////////GET user id from user email (store in session)////////////////////////////////////////////////
		$namespace 	= new Zend_Session_Namespace();
		$user   	 = new Application_Model_ArtGroup();
		$namespace->email;
		$row=$user->getMemberID($namespace->email);

		
		$namespace->userdetails[0]['id'] = $row[0]['id'];
		
		/***************** Update login time of user **********************/
				$this->getuserLoginTime($namespace->email); 
				 
		/***************** END Update login time of user **********************/
		$this->_helper->redirector->gotosimple('welcome','Profile',true);
		}
		else
		{
			$users['first_name'] = $prof['given_name'];
			$users['last_name']	= $prof['family_name'];
			$users['email']		= $prof['email'];
			
			$users['user_id'] 	= $prof['id'];
			$insertuser		= new Application_Model_DbTable_Registration();
			$dbField_details = array();
			$addStatus 			= $insertuser->adduser($users, $dbField_details);

			
			/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email= $prof['email'];
			$row=$user->getMemberID($namespace->email);

		
			$namespace->userdetails[0]['id'] = $row[0]['id'];
			$namespace->email		= $prof['email'];
			$namespace->fname 		= $prof['given_name'];
			$namespace->userID		= $prof['id'];
			$namespace->useremail 	= $prof['id'];
			/***************** Update login time of user **********************/
			$this->getuserLoginTime($namespace->email); 
				 
			/***************** END Update login time of user **********************/
			$this->_helper->redirector->gotosimple('welcome','Profile',true);
		}
///////////////////////////////////////////////////////////////////////////
 
    }

    public function logoutAction()
    {
		$namespace->userdetails[0] = null;
		$namespace->email = null;
        TBS_Auth::getInstance()->clearIdentity();
        $this->_redirect('/');
    }
	
    public function dashboardAction()
    {
       $namespace = new Zend_Session_Namespace();
		 
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('welcome','Profile',true);	
		}
    }
    
	private function getuserLoginTime($email)
	{
		date_default_timezone_set('Asia/Calcutta');
		/***************** Update login time of user **********************/
		$dbField['login_time'] = date('Y-m-d H:i:s');
		$usertime = new Application_Model_Artuser();
		$usertime->updateForgetPassword($dbField,$email);
		/***************** END Update login time of user **********************/
	}
}

<?php
class AdminmemberController extends Zend_Controller_Action {
	
	public function init()
    {   
    	$this->namespace= new Zend_Session_Namespace(); 
    	$this->request  = $this->getRequest()->getParams();
		$this->userInfo	=	new Application_Model_DbTable_Profile();
		$this->country	=	new Application_Model_DbTable_ArtCountry();
    }
    
	function memberAction() {
		
		$namespace = new Zend_Session_Namespace();
		if($namespace->adminemail =='') 
		{  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
			
        $this->_helper->layout->setLayout('adminlayout');
		$this->view->title = "Admin Home Page";
		 
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		 
		$this->view->adminemail = $adminemail;
		 
		if($adminemail = ''){
			$this->_helper->redirector('');
		}
		
		
	    ################################ Update user profile By Abhishek ##############################
		if (isset($this->request['submit'])) {
			
			$userid 								= $this->request['user_id'];
			
			$userArray['first_name'] 				= $this->request['first_name'];
			$userArray['last_name'] 				= $this->request['last_name'];
			$userArray['email'] 					= $this->request['email'];
			$userArray['user_id'] 					= $this->request['userid'];
			$userArray['active'] 					= $this->request['active'];
			if($this->request['password']!=''){
				$userArray['password'] 				= md5($this->request['password']);
				$userArray['original_password'] 	= $this->request['password'];
			}
			 
			$detailsArray['street'] 				= $this->request['street'];
			$detailsArray['house_no'] 				= $this->request['house_no'];
			$detailsArray['country'] 				= $this->request['country'];
			$detailsArray['city']					= $this->request['city'];
			$detailsArray['zip'] 					= $this->request['zip'];
			$detailsArray['dob'] 					= $this->request['dob'];
			$detailsArray['biography'] 				= $this->request['biography'];
			
			$detailsArray['billing_address'] 		= $this->request['baddress'];
			$detailsArray['billing_country'] 		= $this->request['bcountry'];
			$detailsArray['billing_city'] 			= $this->request['bcity'];
			$detailsArray['billing_state'] 			= $this->request['bstate'];
			$detailsArray['billing_zip'] 			= $this->request['bzip'];
			$detailsArray['billing_contact_number'] = $this->request['bphone'];
			$detailsArray['shipping_address'] 		= $this->request['saddress'];
			$detailsArray['shipping_country'] 		= $this->request['scountry'];
			$detailsArray['shipping_city'] 			= $this->request['scity'];
			$detailsArray['shipping_state']			= $this->request['sstate'];
			$detailsArray['shipping_zip'] 			= $this->request['szip'];
			$detailsArray['shipping_contact_number']= $this->request['sphone'];
			$detailsArray['paypal_email']			= $this->request['pemail'];
			
			############################# Upload image ##################################
			$path='../public/images/profile_image';
			require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "setup.php";
			$renameFilter = new Zend_Filter_File_Rename( $path );
			$adapter = new Zend_File_Transfer();
 					
			$adapter->setDestination($path);  // set path for upload folder
			$adapter->setOptions(array('ignoreNoFile' => true));	// remove validation to upload all files
			$files = $adapter->getFileInfo();
			if($files['profile_image']['name']!='')
			{
			$fileType = explode('.',$files['profile_image']['name']);
			
///////////////////////////Check validation against global value set by admin////////////////////////////////////
/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
 			$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
			$rec =	$allgloble->getglobelinfoAll();
			$checkimgType = explode(',', $rec[0]['image_type']);
			if(!in_array($fileType[1], $checkimgType)){ 
				$msg  ='Profile image type is not allowed.';
				$this->_helper->redirector->gotosimple('member','Adminmember',true,array('msg'=>$msg)) ;
			}
			//($checkimgType);die;
			$profile_size=$rec[0]['profile_size'];
			$this->view->profilesize=$profile_size;
			$profilesize=$profile_size*1024;
				 
				/// check file size from global info
			foreach($files as $fileID => $fileInfo) { 
				if($fileInfo['size']<=$profilesize) {}
				else
				{
					$msg  ='Profile image size more than defined file size.';
					$this->_helper->redirector->gotosimple('member','Adminmember',true,array('err_msg'=>$msg)) ;
				}

			}
			if($userid==''){
				$prefix = time();
			}else{
				$prefix = $userid;
			}
			 
	  		$imageName = $prefix.'_'.$userArray['first_name'].'_'.$files['profile_image']['name'];
	  		
			if ($files['profile_image']['name']!=''){
				/*************************** Delete previous profile image************************/
				
				$filenameL = 'images/profile_image/large/'.$this->request['prev_img'];
				$filenames = 'images/profile_image/small/'.$this->request['prev_img'];
				if (file_exists($filenameL) || file_exists($filenameS)) {
					 @unlink($filenameL);
					 @unlink($filenameS);  
				}
				/*************************** END Delete previous profile image************************/
				
				$renameFilter->addFile( array('source' => $files['profile_image']['tmp_name'], 'target' => $imageName, 'overwrite' => true ) );// rename file on desination folder
				$adapter->addFilter($renameFilter);
				$adapter->receive();
				
				$filter   = new Zend_Filter_ImageSize();
				$output_s = $filter->setHeight(50)
						    ->setWidth(50)
						    ->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
						    ->setThumnailDirectory('../public/images/profile_image/small/') 
						    ->filter('../public/images/profile_image/'.$imageName);
						
				$output_l = $filter->setHeight(150)
						    ->setWidth(150)
						    ->setOverwriteMode(Zend_Filter_ImageSize::OVERWRITE_ALL)
						    ->setThumnailDirectory('../public/images/profile_image/large/')
						    ->filter('../public/images/profile_image/'.$imageName);
						    
				$detailsArray['profile_image']	= $imageName;
			}
	  		} 
			############################# END Upload image##################################
			 
			if (isset($userid) && $userid != '') 
			{	
				$this->userInfo->updateuser($userArray,$detailsArray,$userid);
				$this->view->msg = 'Your profile updated successfully.'; 
			}else{
				 $insertuser   = new Application_Model_DbTable_Registration();
			    	$useridarr=array('create_date' => date('Y-m-d H:i:s'));
					$merg_arr=array_merge((array)$userArray, (array)$useridarr);  
					$ret = $insertuser->adduser($merg_arr,$detailsArray);
				 
				 $this->view->msg = 'User added successfully.';
			}
			
		}
		################################ END Update user profile ##############################
		$searchelement	=	isset($this->request['searchelement']) ? $this->request['searchelement'] : '';
		$sortBy	=	isset($this->request['sortBy']) ? $this->request['sortBy'] : '';
	    $allgroup	=	new Application_Model_ArtGroup();
		$users		=	$allgroup->getMember($searchelement,$sortBy);
		$page = $this->_getParam('page',1);
		
		$this->view->page = $page;
						
		//////////////////////////////////////////////////////////////////////////////////////	
		//added by reeta (06-03-2012)
		////////////////////////////////////////////////////////////////////////////
		 $this->view->sortBy	=	$this->request['sortBy']; 
		if(isset($this->request['searchelement']))
		{
			//serching all element with the table
			//show the search element on the search box
			$namespace->psquestion = $this->request['searchelement'];
			$this->view->psquestion = $namespace->psquestion;	

			$users		=	$allgroup->getMember($searchelement,$sortBy);
		}
		$this->view->err_msg = isset($this->request['err_msg']) ? $this->request['err_msg'] : '';
	    $this->view->users = $users;
		//end added by reeta (06-03-2012)
		$paginator = Zend_Paginator::factory($users);
		$paginator->setItemCountPerPage(10);
		$paginator->setCurrentPageNumber($page);
		$this->view->paginator = $paginator;
		$this->view->users = $users;
		
	}
	
	public function  deletememberAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		
		$user			=	new Application_Model_ArtGroup();
		$deletemember		=	$user->deletemember($id);
		$this->view->deletemember	=	$deletemember;
	
		$msg	=	'User deleted successfully.';
		$_SESSION['msg'] = $msg;
		$this->_helper->redirector->gotosimple('member','Adminmember',true,array('msg'=>$msg)) ;

	}
	
	public function addmemberAction() { 
		
		$namespace = new Zend_Session_Namespace();
		if($namespace->adminemail =='') 
		{  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
			
        $this->_helper->layout->setLayout('adminlayout');
		$this->view->title = "Admin Home Page";
		 
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		 
		$this->view->adminemail = $adminemail;
		 
		if($adminemail = ''){
			$this->_helper->redirector('');
		}
         
        $param = $this->getRequest()->getParams();
        
		 
		$allgroup	=	new Application_Model_ArtGroup();
		
		$group = $allgroup->getUsers();
		$this->view->getusertype = $group;
		/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
 		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
		$rec =	$allgloble->getglobelinfoAll();
		$profile_size=$rec[0]['profile_size'];
		$this->view->profilesize=$profile_size;
/////////////////////////////////////////////////////////////////////////////////////////
	}
	
	public function updatememberAction() { 
		
		$namespace = new Zend_Session_Namespace();
		if($namespace->adminemail =='') 
		{  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
			
        $this->_helper->layout->setLayout('adminlayout');
		$this->view->title = "Admin Home Page";
		 
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		 
		$this->view->adminemail = $adminemail;
		 
		if($adminemail = ''){
			$this->_helper->redirector('');
		}
		 
/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
 		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
		$rec =	$allgloble->getglobelinfoAll();
		$profile_size=$rec[0]['profile_size'];
		$this->view->profilesize=$profile_size;
/////////////////////////////////////////////////////////////////////////////////////////
        $param = $this->getRequest()->getParams();
        
	 	$alluser	=	new Application_Model_Artuser();
		
	    $editusers		=	$alluser->getUserdetails($param['id']);
	   
		$this->view->editusers = $editusers;       
	}

/**
@	Abhishek has done below which is current not in use because of irrelevant code
@	commented on 15-03-2012
@	comment by Uma Shankar


	public function  updatememberstatusAction()
	{ 
		
		$namespace = new Zend_Session_Namespace();
		if($namespace->adminemail =='') 
		{  
			$this->_helper->redirector->gotosimple('index','Admin',true);	
		}
			
        $this->_helper->layout->setLayout('adminlayout');
		$this->view->title = "Admin Home Page";
		 
		$auth     = Zend_Auth::getInstance();
		$identity = $auth->getIdentity();
		$adminemail = $identity;
		 
		$this->view->adminemail = $adminemail;
		 
		if($adminemail = ''){
			$this->_helper->redirector('');
		}
         
		 
		 
/////////////////IMAGE INFO FROM GLOBAL TABLE////////////////////////////////
	 			$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
				$rec =	$allgloble->getglobelinfoAll();
				$profile_size=$rec[0]['profile_size'];
				$this->view->profilesize=$profile_size;
/////////////////////////////////////////////////////////////////////////////////////////
        $param = $this->getRequest()->getParams();
        
	 	$alluser	=	new Application_Model_Artuser();
		
	    $editusers		=	$alluser->getUserdetails($param['id']);
	   
		// echo '<pre>';
		 //print_r($editusers);die;
		$this->view->editusers = $editusers;
		 
		 
        
	}	
**/	
public function  updatememberstatusAction()
	{ 
		$request 		=	$this->getRequest()->getParams();
		if(count($request['chk'])>0)
		{
			$id=implode(',',$request['chk']); 
		}
		else
		{
			$id			=	$request['id'];
		}
		 $status			=	$request['status'];

		$user			=	new Application_Model_ArtGroup();
		$updatememberstatus		=	$user->updatememberstatus($id,$status);
		$this->view->updatememberstatus	=	$updatememberstatus;

		if($status=='1')
		$_SESSION['msg'] =	'Activate successfully.';
		else
		$_SESSION['msg'] =	'Inactivate successfully.';
		$this->_helper->redirector->gotosimple('member','adminmember',true,array('msg'=>$msg)) ;


	}		
	public function checkuniqueemailAction() {
		
		$userEmail = isset($this->request['ckemail']) ? $this->request['ckemail'] : '';
		$passemail = new Application_Model_DbTable_Registration();
		$status    = $passemail->checkEmail($userEmail);
		if($status>0){
			echo $userEmail.'Email is already registered. <br> Please enter different email Id';
		}else{
			echo '';
		}
		die;
	}
	public function checkuniqueuseridAction() {
		
		$uid = isset($this->request['ckuid']) ? $this->request['ckuid'] : '';
		$passuid = new Application_Model_DbTable_Registration();
		$status    = $passuid->checkUserId($uid);
		if($status>0){
			echo $uid.' is already registered. Please enter different user Id';
		}else{
			echo '';
		}
		die;
	}
	public function checkuniqueemaileditAction() {
		
		$userEmail = isset($this->request['ckemail']) ? $this->request['ckemail'] : '';
		$id = isset($this->request['id']) ? $this->request['id'] : '';
		$passemail = new Application_Model_DbTable_Registration();
		$status    = $passemail->checkEmailedit($userEmail,$id);
		if($status>0){
			echo $userEmail.' is already registered. <br> Please enter different email Id';
		}else{
			echo '';
		}
		die;
	}
	public function checkuniqueuserideditAction() {
	
		$uid = isset($this->request['ckuid']) ? $this->request['ckuid'] : '';
		$id = isset($this->request['id']) ? $this->request['id'] : '';
		$passuid = new Application_Model_DbTable_Registration();
		$status    = $passuid->checkUserIdedit($uid,$id);
		if($status>0){
			echo $uid.' is already registered. Please enter different user Id';
		}else{
			echo '';
		}
		die;
	}
}
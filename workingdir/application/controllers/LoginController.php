<?php 
/*
@	controller for 'Group registration from front end' 
@	Add by : Abhishek
@	On: 17-01-2012
**/
class LoginController extends Zend_Controller_Action {
	
	public function init() {
		
		$namespace = new Zend_Session_Namespace();
	}
	
		public function  loginAction(){ 
		
		global $Loginform;
		$this->_helper->layout->disableLayout();
		$this->_helper->layout->setLayout('layout');
      	$request	=	$this->getRequest()->getParams(); 
		
		if ($_COOKIE['loginemail']!='') {
		$request['loginemail']=$_COOKIE['loginemail'];
		$request['pwd']=$_COOKIE['pwd'];
		$request['ltype']=$_COOKIE['ltype'];
		} 
		$filter = new Zend_Filter_Decrypt();
	    $bootstrap = $this->getInvokeArg('bootstrap');
		$resource = $bootstrap->getPluginResource('db');
		$db = $resource->getDbAdapter();
		$adapter = new Zend_Auth_Adapter_DbTable($db);
		$adapter->setTableName('art_user');
		$adapter->setIdentityColumn('email');
        $adapter->setCredentialColumn('password');
       
		if(isset($request['loginemail']) && isset($request['pwd']))
		{   
		if($request['remember']==true)
		{
		setcookie ("loginemail", $request['loginemail'], time() + 3600,'/' );
		setcookie ("pwd", $request['pwd'], time() + 3600,'/');
		setcookie ("ltype", $request['ltype'], time() + 3600,'/');
		}  
			$this->view->loginPass	= md5($request['pwd']); 
			$this->view->setEmail	= $request['loginemail'];
			$this->view->setUserid	= $request['loginemail'];
			$this->view->setMsg	= 'Invalid username or password';
			$addEntry	=	new Application_Model_DbTable_Registration();
			
			if($request['ltype']=='Email') {
			$result	 	= 	$addEntry->chkLoginemail($request['loginemail'],md5($request['pwd'])); 
			 } else {
			$result	 	= 	$addEntry->chkLogin($request['loginemail'],md5($request['pwd']));  }
			//print_r($result);echo $result[0];die();
			 
			if($result[0]!=1)
			{
				setcookie ("loginemail", "", time() - 3600,'/' );
				setcookie ("pwd", "", time() - 3600,'/');
				setcookie ("ltype", "", time() - 3600,'/');
				echo $errmsg = "Login ID or password not matched OR account not activate";
				die;
			}
			else
			{	
				
				$namespace = new Zend_Session_Namespace();  
				$namespace->luserid = $request['loginemail'];
				$namespace->passowrd = md5($request['pwd']);
				$namespace->userdetails = $result[1];
				$namespace->fname = $namespace->userdetails[0]['first_name'];
				$namespace->lname = $namespace->userdetails[0]['last_name'];
				
				$user   	 = new Application_Model_ArtGroup();
				if($request['ltype']=='Email')
				$row=$user->getMemberID($namespace->luserid);
				else
				$row=$user->getMemberdetailsByID($namespace->luserid);
				$user_id=$row[0]['id'];
				$namespace->userid= $row[0]['id'];
				$namespace->email= $row[0]['email'];
				$namespace->userID= $row[0]['user_id'];
				/***************** Update login time of user **********************/
				$this->getuserLoginTime($namespace->email);
				

				/***************** END Update login time of user **********************/
								
				if ($_COOKIE['loginemail']!='') {
				//echo "<pre>";print_r($_SERVER); die();
				$this->_helper->redirector->gotosimple('index','Index',true);
				}
				
				 die;
			}
		}
    }

   	public function  floginAction(){ 
		/////////////////FACEBOOK INFO FROM GLOBAL TABLE////////////////////////////////
 		$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
		$rec =	$allgloble->getglobelinfoAll();
		$appid=$rec[0]['app_id'];
		$secret=$rec[0]['secret'];
		$domain=$rec[0]['domain'];
		$params = array(
				  'appId'  => $appid,
				  'secret' => $secret,
				  'domain' => $domain
					);
		$facebook = new Facebook($params);

		// Get User ID
		$user = null;
		
		$user = $facebook->getUser();
		$namespace = new Zend_Session_Namespace();					  

		if($user)
		{
			$loginUser = $facebook->api('/me');
		
			$appId	 = $facebook->getAppId();
	
			$status = $this->_checkAndUpdateUser($loginUser);
	
			if($status == 2 || $status == 3)
			{
				// User Model
				$userstuff		= new Application_Model_DbTable_Registration();
	
				$logoutUrl =  $facebook->getLogoutUrl(array('scope' => 'email')) ;
	
				// get user data
				$userData = $userstuff->getUserData($loginUser['email']);
	
				//$namespace->useremail 	= $userData[0]['email'];
				$namespace->email		= $userData[0]['email'];
				$namespace->fname 		= $userData[0]['firstname'];
				$namespace->lname      		= $userData[0]['lastname'];
				$namespace->userID		= $userData[0]['user_id'];
				$namespace->useremail 	= $userData[0]['id'];
				$namespace->userdetails[0]['id'] = $userData[0]['id'];
				$namespace->logouturl	= $logoutUrl;
				
				/***************** Update login time of user **********************/
				$this->getuserLoginTime($namespace->email); 
				/***************** END Update login time of user **********************/
				
				echo '<script language="JavaScript">opener.location.href="http://artex.inoday.com/public/index.php/Profile/welcome/";window.close();</script> ';  
			} 
		}
		else { 
			$this->view->loginUrl = $facebook->getLoginUrl(array('scope' => 'email')); 
		}
    }
    
	public function flogoutAction()
	{
	/////////////////FACEBOOK INFO FROM GLOBAL TABLE////////////////////////////////
		$namespace = new Zend_Session_Namespace();  

               
		unset($namespace->email);
		$namespace->email 		= null;
		$namespace->passowrd 	= null;
		$namespace->userdetails	= null;
	 	$allgloble = new Application_Model_DbTable_ArtGlobelinfo();	
		$rec =	$allgloble->getglobelinfoAll();
		$appid=$rec[0]['app_id'];
		$secret=$rec[0]['secret'];
		$domain=$rec[0]['domain'];
		$params = array(
				  'appId'  => $appid,
				  'secret' => $secret,
				  'domain' => $domain
					);
		$facebook = new Facebook($params);
		$facebook->getLogoutUrl($params);//echo '<pre>';print_r($facebook->getAccessToken());echo '<br>';
		$ac_token=$facebook->getAccessToken();//echo $ac_token;
		echo '<script language="JavaScript">window.location.href="https://www.facebook.com/logout.php?next=http%3A%2F%2Fartex.inoday.com%2Fpublic%2Findex.php%2FLogin%2Flogout%2F&access_token='.$ac_token.'&scope=email";</script> ';
		die;
	}
	
	public function  logoutAction(){		

		
		
		$namespace = new Zend_Session_Namespace();  
		
               	unset($namespace->email);
		$namespace->email 		= null;
		$namespace->passowrd 	= null;
		$namespace->userdetails	= null;
		Zend_Session::namespaceUnset();
		setcookie ("loginemail", "", time() - 3600,'/' );
		setcookie ("pwd", "", time() - 3600,'/');
		setcookie ("ltype", "", time() - 3600,'/');
		$auth = TBS_Auth::getInstance();

		TBS_Auth::getInstance()->clearIdentity();
  		$curl_connection = curl_init('https://www.google.com/accounts/Logout');
  		curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
    	curl_setopt($curl_connection, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
		curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);
  		$result = curl_exec($ch);  /* Execute the HTTP command. */
  		curl_close($ch);
	
        $this->_helper->redirector->gotosimple('index','Index',true);
        
    }
	private function _checkAndUpdateUser($artUser)
	{
		if(!$artUser)
		{
			return 1;
		}
		$artEmail = $artUser['email'];
		
	// User Model
		$userstuff		= new Application_Model_Artuser();
	
	// Check for user existence
		 $status = $userstuff->chkAval_user($artEmail);
		
		if($status)
		{
	// User already exist
			return 2;
		}
	 
	// Add user
		$users['first_name'] = $artUser['first_name'];
		$users['last_name']	= $artUser['last_name'];
		$users['email']		= $artUser['email'];
		
		$users['user_id']	= $artUser['id'];
		$users['active']	= '1';
		$users['create_date']=date("Y-m-d H:i:s");
		//$addStatus			= $userstuff->adduser($users);
		$insertuser		= new Application_Model_DbTable_Registration();
		$dbField_details = array();
		$addStatus = $insertuser->adduser($users, $dbField_details);
	//echo '<pre>';
	//print_r($addStatus);
	//die;
		return $addStatus ? 3: 4;
	}
	
	public  function forgetemailAction() {
		$request	=	$this->getRequest()->getParams();
		$this->_helper->layout->disableLayout();
		$this->_helper->layout->setLayout('layout');	
		$userstuff		= new Application_Model_Artuser();
		$status = $userstuff->chkAval_user($request['forgetemail']);
		//////////////////////Get mail content set by admin///////////////////////
		$getmailcontent	=	new Application_Model_ArtMailFormats();
		$mailcontent		=	$getmailcontent->getMailContentById(2);
		$formated_message=$mailcontent['content'];
		$formated_subject=$mailcontent['subject'];
		////////////////////////BID mail to current bidder////////////////////////////////
		if($status)
		{  
			$userdetails = $userstuff->getUserData($request['forgetemail']); // get user name
			//print_r($userdetails);
			$name = $userdetails[0]['first_name'];
			
			$password= $userdetails[0]['original_password'];
			if($password=='')
			{
				echo 'We are unable to process your request, because you are register to Facebook or Gmail';die;
			}
			else 
			{
			//$userstuff->updateForgetPassword($dbField,$request['forgetemail']); // update password
	 		$baseUrl = 'http://'.$_SERVER['HTTP_HOST'];
			$to  = $request['forgetemail'];
			$subject = $formated_subject;
			$message = sprintf($formated_message,$userdetails[0]['first_name'],$password,$baseUrl);
		 	
			// To send HTML mail, the Content-type header must be set
			$tr = new Zend_Mail_Transport_Sendmail('admin@artex.com');
			Zend_Mail::setDefaultTransport($tr);
			
			$mail = new Zend_Mail();
		 	$mail->setBodyHtml($message);
	    	$mail->setFrom('admin@artex.com', 'www.artex.com');
	    	$mail->addTo($to, $name);
	    	$mail->setSubject($subject);
	    	$mail->send($tr);
	    	
			echo 'Your recovery password has been sent in your mail.';die;
			}
		}else{
			
			echo 'This email is not registered in artex';die;
		}
		 
	}
	
	private function getuserLoginTime($email)
	{
		date_default_timezone_set('Asia/Calcutta');
		/***************** Update login time of user **********************/
		$dbField['login_time']	= date('Y-m-d H:i:s');
		$usertime = new Application_Model_Artuser();
		$usertime->updateForgetPassword($dbField,$email);
		/***************** END Update login time of user **********************/
	}
}
?>

<?php
/*******************************************************
@@	Blog Controller
@@	Added on 22 Dec 2011 By Anil Rawat
********************************************************/	
class BlogController extends Zend_Controller_Action
{

    public function init()
    {
      	$namespace = new Zend_Session_Namespace(); 
		$request    = $this->getRequest()->getParams();
		//$dtips_model = new Application_Model_DbTable_FbDatingSafetyTips;
		//$dtipsmodel  = $dtips_model->datingtipsselAll();
		//Zend_Layout::getMvcInstance()->assign('datingtips',$dtipsmodel);	
		/////////////GET user id from user email (store in session)////////////////////////////////////////////////
			$namespace 	= new Zend_Session_Namespace();
			$user   	 = new Application_Model_ArtGroup();
			$namespace->email;
		$row=$user->getMemberID($namespace->email);
		$namespace->userid= $row[0]['id'];
		$myName		= $row[0]['id'];
    }
	
/********************************************************************
@@ Index Action
********************************************************************/
	 public function indexAction()
    {      
		$bootstrap 	= 	$this->getInvokeArg('bootstrap');
		$resource  	= 	$bootstrap->getPluginResource('db');
		$db     	= 	$resource->getDbAdapter();
		$user		= new Application_Model_ArtGroup();
		$namespace 	= new Zend_Session_Namespace(); //@ 
		
		
	} //@@ End of Index action. 
	
	public function myblogAction()
	{  
	$namespace = new Zend_Session_Namespace();
		 
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		}
		$bootstrap 	= 	$this->getInvokeArg('bootstrap');
		$resource  	= 	$bootstrap->getPluginResource('db');
		$db     	= 	$resource->getDbAdapter();
		$extraStuff	= new Application_Model_ArtExtraFunction();
		$namespace 	= new Zend_Session_Namespace(); //@
	  if(isset($namespace->userid))
	  {
		$FbUser		= new Application_Model_ArtGroup();
		$request 	= $this->getRequest()->getParams();		
		$blogMe	= $extraStuff->userBlogedMe($namespace->userid,$db); 
		// for the pagination
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($blogMe);
		$paginator->setItemCountPerPage(8);
		$paginator->setCurrentPageNumber($page);
		$this->view->viewblogMe = $paginator;
		$this->view->totRecords	=	count($blogMe); 
		$this->view->page_name	=	BASE_URL."Blog/myblog";
		if (isset($params['pn'])) 
		{ 
			$this->view->pn = preg_replace('#[^0-9]#i', '', $params['pn']); 
		} 
		else 
		{ 
			$this->view->pn = 1;
		} 
		$itemsPerPage = 8; 
		
		$this->view->lastPage = ceil($this->view->totRecords / $itemsPerPage);
		
		if (($this->view->pn < 1)) 
		{ 
			$this->view->pn = 1; 
		} 
		else if ($this->view->pn > $this->view->lastPage) 
		{ 
			 $this->view->pn = $this->view->lastPage; 
		} 
		if($this->view->pn == 0)
		{
			$this->view->pn = 1;
		}
		$this->view->sub1 = $this->view->pn - 1;
		$this->view->sub2 = $this->view->pn - 2;
		$this->view->sub3 = $this->view->pn - 3;
		$this->view->sub4 = $this->view->pn - 4;
		$this->view->add1 = $this->view->pn + 1;
		$this->view->add2 = $this->view->pn + 2;
		$this->view->add3 = $this->view->pn + 3;
		$this->view->add4 = $this->view->pn + 4;
		$this->view->limit = 'LIMIT ' .($this->view->pn - 1) * $itemsPerPage .',' .$itemsPerPage; 
		$resshow		= 	8;
		$this->view->limitRecords	=	count($resshow);
		$this->view->totArray	=	$resshow;
        $namespace 	= new Zend_Session_Namespace(); //@
        $namespace  = $namespace->userid;
        $this->view->currentuseid = $namespace;
		}	
	} 
	     /***for a visiter*****/
	public function viewallblogAction()
		{
		  $bootstrap 	= 	$this->getInvokeArg('bootstrap');
		  $resource  	= 	$bootstrap->getPluginResource('db');
		  $db     	    = 	$resource->getDbAdapter();
		  $extraStuff	= new Application_Model_ArtExtraFunction();
		  $namespace 	= new Zend_Session_Namespace(); //@
		  $FbUser		= new Application_Model_ArtGroup();
		  $request 	= $this->getRequest()->getParams();		
		  $blogall	= $extraStuff->userBloged($db);
		  $namespace 	= new Zend_Session_Namespace(); //@
        $namespace  = $namespace->userid;
        $this->view->currentuseid = $namespace;
		  ////****** for pagination **********/////
			$page = $this->_getParam('page',1);
		    $paginator = Zend_Paginator::factory($blogall);
		    $paginator->setItemCountPerPage(8);
		    $paginator->setCurrentPageNumber($page);
		    $this->view->paginator = $paginator;
		  
		} 
		/***for a visiter*****/
		public function  viewablogdetailAction()
	{  
		$bootstrap 	= 	$this->getInvokeArg('bootstrap');
		$resource  	= 	$bootstrap->getPluginResource('db');
		$db     	= 	$resource->getDbAdapter();
	   $extraStuff	= new Application_Model_ArtExtraFunction();
	   $request22 	= $this->getRequest()->getParams();
	   $blogMeto	= $extraStuff->userBlogedMevdetail($db);
	   $this->view->blogMeto = $blogMeto;
	  $namespace 	= new Zend_Session_Namespace();
	  $currentuseid = $namespace->userid;
	  $this->view->currentuseid = $currentuseid;
	  $request1 	= $this->getRequest()->getParams();
	  $blog_id =$request1['blog_id'];
	  $getvblog	= new Application_Model_DbTable_ArtBlog();
	  $getviewablog = $getvblog->getviewblog($blog_id);
	  $this->view->getviewablog = $getviewablog;
	  $request2 	= $this->getRequest()->getParams();
	  $blog_id =$request2['blog_id'];
	  $extrav	= new Application_Model_DbTable_ArtBlog(); 	
	  $blogMeview	= $extrav->getblogview($blog_id);
	  $this->view->blogMeview = $blogMeview;
	  $request 	= $this->getRequest()->getParams();
	  $request   =  $request['blog_id'];
	  $commentto	= $extraStuff->usercommentme($request,$db);
	  $this->view->commentto = $commentto;	  	   
	}
	
	public function viewablogAction()
	{  $namespace = new Zend_Session_Namespace();
		 
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		}
		$bootstrap 	= 	$this->getInvokeArg('bootstrap');
		$resource  	= 	$bootstrap->getPluginResource('db');
		$db     	= 	$resource->getDbAdapter();
	   $extraStuff	= new Application_Model_ArtExtraFunction();
	   $request22 	= $this->getRequest()->getParams(); 
	   $request21   =  $request22['bloguser_id']; 
	   $blogMeto	= $extraStuff->userBlogedMev($request21,$db);
	   $this->view->blogMeto = $blogMeto;
	  $namespace 	= new Zend_Session_Namespace();
	  $currentuseid = $namespace->userid;
	  $this->view->currentuseid = $currentuseid;
	  $request1 	= $this->getRequest()->getParams(); 
	  $blog_id =$request1['blog_id'];
	  $getvblog	= new Application_Model_DbTable_ArtBlog(); 
	  $getviewablog = $getvblog->getviewblog($blog_id);
	  $this->view->getviewablog = $getviewablog;
	  $request2 	= $this->getRequest()->getParams();
	  $blog_id =$request2['blog_id'];
	  $extrav	= new Application_Model_DbTable_ArtBlog(); 	
	  $blogMeview	= $extrav->getblogview($blog_id);//print_r($blogMeview);die();
	  $this->view->blogMeview = $blogMeview;
	  $request 	= $this->getRequest()->getParams();
	  $request   =  $request['blog_id'];
	  $commentto	= $extraStuff->usercommentme($request,$db);
	  $this->view->commentto = $commentto;	  	   
	}
	
	public function commentblogAction()
	{ $namespace = new Zend_Session_Namespace();
		 
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		}
	  $params 	= $this->getRequest()->getParams();
	  $namespace 	= new Zend_Session_Namespace();  
	  $comment     = $params['comment'];
	  $blog_id     = $params['blog_id'];
	  $creator = $params['bloguser_id'];
	  $user_id     = $namespace->userid;
	  $commentblog = array();
	  if($comment=='')
	  {
	  $this->_helper->redirector->gotosimple('viewablog','Blog',true,
	          array('blog_id'=>$blog_id,'bloguser_id'=>$creator,'ijuh'=>$err1)) ; 
	  }
	  else
	  {
	  $commentblog = array('blog_id'=> $params['blog_id'],'user_id'=> $params['creator'],'comment'  => $params['comment'],
		'user_id'=> $namespace->userid,'create_date'=> date("Y-m-d H:i:s"),'publish'=> 1  );
	  $commentto	 =	new Application_Model_DbTable_ArtComment();
	  $ret	=	$commentto->insertcomment($commentblog);
	  if (isset($ret))
	  {
	  $this->_helper->redirector->gotosimple('viewablog','Blog',true,
	          array('blog_id'=>$blog_id,'bloguser_id'=>$creator,'ijuh'=>$err1)) ; 
	  }
	  }
	}
	
	public function addblogAction()
	{
	 
	   $namespace = new Zend_Session_Namespace();
		 
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		} 
	  $this->view->bloguser_id = $namespace->userid;
	  $request 	= $this->getRequest()->getParams(); 
	  if (!isset($request['blog_id']))
	  { 
	  if (isset($request['bloguser_id']))
	  { 	 
	   if($request['bloguser_id']!=''&& $request['title']!='' && $request['description']!='' )
	   { 
		try
		{ 
		$adapter = new Zend_File_Transfer_Adapter_Http();
		$adapter->addValidator('Count',false, array('min'=>1, 'max'=>3))
		->addValidator('Size',false,array('max' => 600000000))
		->addValidator('Extension',false,array('extension' => 'gif','jpg','jpeg','png','case' => false));
		$adapter->setDestination("../public/images/blog_images/");
		$files = $adapter->getFileInfo();
		foreach($files as $fieldname=>$fileinfo)
		{
		if (($adapter->isUploaded($fileinfo['name'])) && ($adapter->isValid($fileinfo['name'])))
		{ 
		$extension = substr($fileinfo['name'], strrpos($fileinfo['name'], '.') + 1);
		$filename = 'file_'.date('Ymdhs').'.'.$extension;
		$adapter->addFilter('Rename',array('target'=>'../public/images/blog_images/'.$filename,'overwrite'=>true));
		$entry = $adapter->receive($fileinfo['name']);
		if(isset($entry))
		{
		$dbField = array();
		$dbField = array('creator'=> $request['bloguser_id'],'image'=>$filename,'title'         => $request['title'],'description'=> $request['description'],
		           'publish'=>'1','create_date'=> date("Y-m-d H:i:s") );

		$tutorial	 =	new Application_Model_DbTable_ArtBlog();
		if($ret == 'err1')
		{
		$err1	=	'Value already exist';
		$this->_helper->redirector->gotosimple('myblog','Blog',true,array('ijuh'=>$err1)) ;
		}
		$tutorial	 =	new Application_Model_DbTable_ArtBlog();
		$ret	=	$tutorial->insertblog($dbField);
		$this->_helper->redirector->gotosimple('myblog','Blog',true,array('ijuh'=>$err1,'page'=>$page)) ; 
		}
		}
		}
        }
        catch (Exception $ex)
        {
			echo "Exception!\n";
			echo $ex->getMessage();
        }
      }
  
    }
	}
	
	  //  *************** Edit receive list Action ********************
            $param 	= $this->getRequest()->getParams();			
			if(isset($param['blog_id']))
			{   
			    $allblog	=	new Application_Model_DbTable_ArtBlog();
				$rec =	$allblog->getblog($param['blog_id']);
				$this->view->blog_id          =	$rec['id'];
				$this->view->id          =	$rec['id'];
				$this->view->title		  =	$rec['title'];
				$this->view->image		  =	$rec['image'];
				$this->view->description =	$rec['description'];
				$this->view->bloguser_id	  =	$rec['creator'];
			}
			
			//----------------------Edit  Action ---------------------------------//
				
		$request = $this->getRequest()->getParams();
		if (isset($request['blog_id'])) 
	    { 
		 $oldFile = $request['oldfile']; 
		 try
		 {//die('2');
		 $adapter = new Zend_File_Transfer_Adapter_Http();
		 $adapter->addValidator('Count',false, array('min'=>1, 'max'=>3))
		 ->addValidator('Size',false,array('max' => 60000000))
		 ->addValidator('Extension',false,array('extension' => 'gif','jpg','jpeg','png','case' => false));
		 $adapter->setDestination("../public/images/blog_images/");
		 $files = $adapter->getFileInfo();
		 foreach($files as $fieldname=>$fileinfo)
		  {//die('4');
          if (($adapter->isUploaded($fileinfo['name'])) && ($adapter->isValid($fileinfo['name'])))
			{
				$extension = substr($fileinfo['name'], strrpos($fileinfo['name'], '.') + 1);
				$filename = 'file_'.date('Ymdhs').'.'.$extension;
			}
			else
			{//die('5');
				$filename  = $oldFile ;
			}			
			if ( $filename != '')
			{//die('6');
				$adapter->addFilter('Rename',array('target'=>'..//public/images/blog_images//'.$filename,'overwrite'=>true));
				$entry = $adapter->receive($fileinfo['name']);
			 if(isset($entry))
			 {//die('7');
				$dbField = array();
		        $dbField = array('id'=> $request['blog_id'],
				    'creator'=> $request['bloguser_id'],'image'=>$filename,	'title'  => $request['title'],'description'=> $request['description'],'publish'=>'1','modify_date'=> date("Y-m-d H:i:s") );
		        $tutorial	 =	new Application_Model_DbTable_ArtBlog();
		        $ret	=	$tutorial->updateblog($dbField);
		        $err1	=	'Value edit successfully';
		       $this->_helper->redirector->gotosimple('myblog','Blog',true,array('ijuh'=>$err1,'page'=>$page)) ; 
		     }
		    }
		  }
         }
        catch (Exception $ex)
         {//die('3');
			echo "Exception!\n";
			echo $ex->getMessage();
         }
        }		
  }
  
  
  
 public function deletebcommentAction()
 {
  $namespace = new Zend_Session_Namespace();
		 
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		}
 $namespace = new Zend_Session_Namespace(); 
   $request 	= $this->getRequest()->getParams();
   $id = $request['id'];
   $blog_id = $request['blog_id'];
   if($request['bloguser_id']=='')
    $request['bloguser_id']=$namespace->userid;
  $creator = $request['bloguser_id']; 
   $dcomment = new Application_Model_DbTable_ArtComment();
   $deletecomment	= $dcomment->deletecomment($id);
   $this->view->deletecomment	=	$deletecomment;
   $this->_redirect('Blog/viewablog/blog_id/'.$blog_id.'/bloguser_id/'.$creator);
 
 }
 
 public function  deleteablogallAction()
		{ 
 $namespace = new Zend_Session_Namespace();
		 
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		}
        	$request 		=	$this->getRequest()->getParams();
			$blog_id		=	$request['blog_id'];
			$deleteb		=	new Application_Model_DbTable_ArtBlog();
			$deleteblog		=	$deleteb->deleteablog($blog_id);
			$this->view->deleteblog	=	$deleteblog;
			$msg = 'delete the blog '.$blog_title.' successfully';
			$this->_redirect('Blog/viewallblog/');
		}	

 public function  deleteablogAction()
		{ 
 $namespace = new Zend_Session_Namespace();
		 
		if($namespace->email =='') 
		{  
			$this->_helper->redirector->gotosimple('index','index',true);	
		}
        	$request 		=	$this->getRequest()->getParams();
			$blog_id		=	$request['blog_id'];
			$deleteb		=	new Application_Model_DbTable_ArtBlog();
			$deleteblog		=	$deleteb->deleteablog($blog_id);
			$this->view->deleteblog	=	$deleteblog;
			$msg = 'delete the blog '.$blog_title.' successfully';

			$this->_redirect('Blog/myblog/');

		}		
} // End of class

?>

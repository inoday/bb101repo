<?php
class Zend_View_Helper_Artuseractivity extends Zend_View_Helper_Abstract
{
	/*
	 @ Fetch news of artwork from art_news table to show
	 @ Add by : Abhishek
	 @ On: 24-02-2012
	**/
	
	public function Artuseractivity() {
		
		$activitylist = new Application_Model_DbTable_Artuseractivity();
		
		$activitylist_result = $activitylist->Artuseractivity();	
		 
		return $activitylist_result;
	}
	
	
}
?>
<?php 
/**
 @  Show content on all common file like header.phtnl
 @	Add by : Abhishek
 @	On: 12-03-2012
 */
class Zend_View_Helper_Profiledetails extends Zend_View_Helper_Abstract
{	 
	public function profiledetails()
	{
		/*
		 @ Call function for fetch list of profile to show on header.phtml page
		 @ Add by : Abhishek
		 @ On: 12-03-2012
		*/		
        $profileSection = $this->profilelist();    
        return $profileSection;
	}
	
	/*
	 @ Add by : Abhishek
	 @ On: 12-03-2012
	*/
	public function profilelist() {
		$namespace= new Zend_Session_Namespace(); 
		
		$profileArray = array();
		
		$profileInfo =	new Application_Model_DbTable_Profile();

		$message	 =	new Application_Model_ArtInbox();
		$profileArray['countInbox']  = $message->countInboxMsg($namespace->userdetails[0]['id']);
		$profileArray['bidding']    = $profileInfo->bidding($namespace->userdetails[0]['id']); // Show bidding item
		$profileArray['liveart']    = $profileInfo->liveart($namespace->userdetails[0]['id']); // Show liveart 
		$profileArray['watchlist']   = $profileInfo->watchlist($namespace->userdetails[0]['id']); // Show watchlist
		$profileArray['feedback']   = $profileInfo->feedback($namespace->userdetails[0]['id']); // Show feedback
		$profileArray['follower']   = $profileInfo->tofollower($namespace->userdetails[0]['id']); // Show feedback
		$profileArray['following']   = $profileInfo->tofollowing($namespace->userdetails[0]['id']); // Show feedback
		return $profileArray;
	}	
}
?>

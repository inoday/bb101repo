<?php 
/**
 @  Show SEO related content header.phtnl
 @	Add by : Abhishek
 @	On: 19-03-2012
 */
class Zend_View_Helper_Seocontent extends Zend_View_Helper_Abstract
{
	public function seocontent()
	{
		/*
		 @ Call function for fetch list of category to show on index page
		 @ Add by : Abhishek
		 @ On: 19-03-2012
		**/
        $seo = $this->seolist();
        return $seo;
	}
	
	/*
	 @ Fetch title, meta tag, description from art_globelinfo table and call it on header.phtml
	 @ Add by : Abhishek
	 @ On: 19-03-2012
	**/
	private function seolist() {
		$header = Array();
		$getseocontent = new Application_Model_DbTable_ArtGlobelinfo();
		$seocontent = $getseocontent->getglobelinfoAll();
		//echo '<pre>';print_r($seocontent);die;
		$header['header'] = $seocontent;
		return $header;
	}	
}
?>
<?php 
/**
 @  Show content on all common file like header.phtnl
 @	Add by : Abhishek
 @	On: 14-02-2012
 */
class Zend_View_Helper_Headercontent extends Zend_View_Helper_Abstract
{
	 
	public function headercontent()
	{
		/*
		 @ Call function for fetch list of category to show on index page
		 @ Add by : Abhishek
		 @ On: 13-02-2012
		**/
		
        $catList = $this->_categorylist();
        return $catList;
		 
	}
	
	
	/*
	 @ Fetch value of all category from art_artwork_categories table and call it in indexAction
	 @ Add by : Abhishek
	 @ On: 13-02-2012
	**/
	private function _categorylist() {
		$header = Array();
		$categorylist = new Application_Model_DbTable_ArtCategory();
		$categorylist_result = $categorylist->fetchAll();	
		return $categorylist_result;
	}
	
	
}
?>
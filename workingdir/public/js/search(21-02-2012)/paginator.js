jQuery(function($){
	   // alert('aaaaaaaaaa');
		var container = $('#ajaxcontent');
		var overlay = $('<div>').addClass('loading overlay');
		$('#paging').find('a').live('click', function(){
		var href = this.href;
		var pos = this.rel == 'next' ? '-120%' : '120%';
		if (Modernizr.history) {
		history.pushState(location.pathname, '', href);
		}
		container.find('.imgThumbAreaBox').animate({
		left: pos
		}, 'slow', function(){
		var dataContainer = container.find('.imgThumbAreaBox').addClass('loading');
		$.get(href, { format: 'html' }, function(data){
		dataContainer.removeClass('loading');
		container.html(data);
		}, 'html');
		});
		return false;
		});
		var initialPath = location.pathname;
		$(window).bind('popstate', function(e){
		// Prevent popstate handler dealing with initial page load
		if (location.pathname == initialPath) {
		initialPath = null;
		return;
		}
		container.append(overlay);
		$.get(location.pathname, { format: 'html' }, function(data){
		container.html(data);
		}, 'html');
		});
		});
$("#togg").toggle(
      function () {
        $("#serchoption").css({"display":"none"});
        $("#togg").text('(Show filter options)');
		$(".filterArea").css({"height":"52px"});
        
      },
      function () {
        $("#serchoption").css({"display":"block"});
        $("#togg").text('(Hide filter options)');
		$(".filterArea").css({"height":"205px"});
      } 
     
    );
	
   $("#gridview").click(
      function () {
        $("#imgThumbAreaList").css({"display":"none"});
        $("#imgThumbAreaBox").css({"display":"block"});
        $("#current_view").html('grid');
      }     
    );
	
	$("#listview").click(
      function () {
        $("#imgThumbAreaBox").css({"display":"none"});
        $("#imgThumbAreaList").css({"display":"block"});
        $("#current_view").html('list');
      }     
    );
		
	/*function for search page scroll on top By Abhishek*/
	$(document).ready(function(){
		   
			$(".goTop").click(function(){
		          $('html, body').animate({ scrollTop: 0 }, 2000);
		});
	});

	/* Validate search box By Abhishek*/
	function checkSearch(){
	 
		if(jQuery.trim($('#searchbox').val())==''){
				 
				alert('Please enter search category');
				 
				return false;	 
		}
	}
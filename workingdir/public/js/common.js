var TimeToFade = 800.0;

function toggle_browse(eid) {
	
	var element = document.getElementById(eid);
	element.style.display = 'block';
	document.getElementById('panel_search').style.display = 'none';
	if(element.style.opacity ==1){
	
		document.getElementById('panel_search').style.display = 'none';
		element.style.opacity = 0;
	}else{
		element.FadeState = element.FadeState == 2 ? -1 : 1;
		element.FadeTimeLeft = TimeToFade;
		setTimeout("animateFade(" + new Date().getTime() + ",'" + eid + "')", 33);
	}element.style.display = 'block';
}
function toggle_search(eid) {
	$(".multiple").removeAttr("checked");
	var element = document.getElementById(eid);
	element.style.display = 'block';
	document.getElementById('panel_browse').style.display = 'none';
	if(element.style.opacity==1){
	
		document.getElementById('panel_browse').style.display = 'none';
		element.style.opacity = 0;
	}else{
		element.FadeState = element.FadeState == 2 ? -1 : 1;
		element.FadeTimeLeft = TimeToFade;
		setTimeout("animateFade(" + new Date().getTime() + ",'" + eid + "')", 33);   
	}element.style.display = 'block';
}
function animateFade(lastTick, eid)
{  
  var curTick = new Date().getTime();
  var elapsedTicks = curTick - lastTick;
 
  var element = document.getElementById(eid);

  if(element.FadeTimeLeft <= elapsedTicks)
  {
    element.style.opacity = element.FadeState == 1 ? '1' : '0';
    element.style.filter = 'alpha(opacity = ' + (element.FadeState == 1 ? '100' : '0') + ')';
    element.FadeState = element.FadeState == 0 ? 2 : -2;
    return;
  }
    
  element.FadeTimeLeft -= elapsedTicks;
  var newOpVal = element.FadeTimeLeft/TimeToFade;
  if(element.FadeState == 1)
    newOpVal = 1 - newOpVal;

  element.style.opacity = newOpVal;
  element.style.filter = 'alpha(opacity = ' + (newOpVal*100) + ')';
 
  setTimeout("animateFade(" + curTick + ",'" + eid + "')", 33);
}

$(document).ready(function(){
	//To switch directions up/down and left/right just place a "-" in front of the top/left attribute
	
	//Vertical Sliding for Banner
	$('.banner_slide').hover(function(){ 
		$(".banner_img", this).stop().animate({top:'-352px'},{queue:false,duration:400});
	}, function() { 
		$(".banner_img", this).stop().animate({top:'0px'},{queue:false,duration:400});
	});
});


$(document).ready(function(){
	//To switch directions up/down and left/right just place a "-" in front of the top/left attribute
	
	//Vertical Sliding for index.phtml content
	$('.slidedown').hover(function(){
		$(".cover", this).stop().animate({top:'-148px'},{queue:false,duration:300});
	}, function() {
		$(".cover", this).stop().animate({top:'0px'},{queue:false,duration:300});
	});
});


$(window).load(function() {
	$("#scroller").smoothDivScroll({});
});

function checkSearch(){
	
	if(jQuery.trim($('#searchbox').val())==''){
		alert('Please enter search category');
		 
		return false;
	}
}

$(document).ready(function(){
	   
	$(".goTop").click(function(){
          $('html, body').animate({ scrollTop: 0 }, 2000);
	});
});
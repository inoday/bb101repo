$(function(){

				$('#cropbox').Jcrop({
					//aspectRatio: 1,
					onChange: showPreview,
					onSelect: showPreview,					
					onSelect: updateCoords
				});

				$('.preview').click(function(){
					
					$('.preview').parent().css('border','2px solid black');
					$(this).parent().css('border','2px solid red');
					$('#th').val($(this).parent().height());
					$('#tw').val($(this).parent().width());
				})
				$('#cropperForm').submit(function()
						{
		var fields	= $("#cropperForm").serialize();  
		var baseUrl	= $('#BASE_URL').val();	
		if (parseInt($('#w').val())) 
				{ 
					if(parseInt($('#w').val())<=32 || parseInt($('#h').val())<=24)
					{
					alert('Please select crop region biggervvv(min width 32 and height 24).');
					return false;
					}
					
				}
		if ($('#w').val()=='')
		{

			alert('Please select a crop region then press submit.');
			return false;

		}
		
		url			= baseUrl+'/Profile/crop/';
		$('#cropper_button').html('<img src="'+baseUrl+'/images/loader.gif" id="loader">');
		
		data	= fields+'&BaseUrl='+baseUrl; //alert('g'+data);
		$.ajax({
			url: url,
			data: data ,
			type: 'post',
			cache: false,
			
			error: function () {
				alert('error');
			},
			success: function (data) 
			{ 
				var w	=	data.split('~'); //alert(w[1]);
				//alert('Image cropped successfully');
				$('#cropper_button').html('<b>Edit Your Image</b>');
				window.opener.document.getElementById( 'Image_Primary_Content' ).innerHTML=w[1];
				
				window.close();
				//******************* Facebox Rel **************
				
				return false;
				//window.location.href='';
			}
		});	
		return false;
	});
			});

			function updateCoords(c)
			{
				$('#x').val(c.x);
				$('#y').val(c.y);
				$('#w').val(c.w);
				$('#h').val(c.h);
			};

			function checkCoords()
			{
				if (parseInt($('#w').val())) return true;
				alert('Please select a crop region then press submit.');
				return false;
			};

			function showPreview(coords)
			{
				if (parseInt(coords.w) > 0)
				{
					$('.preview').each(function(){
					
						$(this).parent().width(coords.w*$(this).parent().attr('factor'));
						$(this).parent().height(coords.h*$(this).parent().attr('factor'));
						
						var rx = $(this).parent().width() / coords.w;
						var ry = $(this).parent().height() / coords.h;
						
						$(this).css({
							width: Math.round(rx * 500) + 'px',
							height: Math.round(ry * 370) + 'px',
							marginLeft: '-' + Math.round(rx * coords.x) + 'px',
							marginTop: '-' + Math.round(ry * coords.y) + 'px'
						});

					});


				}
			}			

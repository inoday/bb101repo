var TimeToFade = 1000.0;

function toggle_browse(eid) {
	 
	var element = document.getElementById(eid);
	 
	document.getElementById('panel_search').style.opacity = 0;
	if(element.style.opacity ==1){
		element.style.display = 'none';
		element.style.opacity = 0;
	}else{
		element.FadeState = element.FadeState == 2 ? -1 : 1;
		element.FadeTimeLeft = TimeToFade;
		setTimeout("animateFade(" + new Date().getTime() + ",'" + eid + "')", 33);
	element.style.display = 'block';}
}
function toggle_search(eid) {
	 
	var element = document.getElementById(eid);
	 
	document.getElementById('panel_browse').style.opacity = 0;
	if(element.style.opacity==1){
		element.style.display = 'none';
		element.style.opacity = 0;
	}else{
		element.FadeState = element.FadeState == 2 ? -1 : 1;
		element.FadeTimeLeft = TimeToFade;
		setTimeout("animateFade(" + new Date().getTime() + ",'" + eid + "')", 33);   
	element.style.display = 'block';}
}
function animateFade(lastTick, eid)
{  
  var curTick = new Date().getTime();
  var elapsedTicks = curTick - lastTick;
 
  var element = document.getElementById(eid);

  if(element.FadeTimeLeft <= elapsedTicks)
  {
    element.style.opacity = element.FadeState == 1 ? '1' : '0';
    element.style.filter = 'alpha(opacity = ' + (element.FadeState == 1 ? '100' : '0') + ')';
    element.FadeState = element.FadeState == 0 ? 2 : -2;
    
    return;
  }
   
 
  element.FadeTimeLeft -= elapsedTicks;
  var newOpVal = element.FadeTimeLeft/TimeToFade;
  if(element.FadeState == 1)
    newOpVal = 1 - newOpVal;

  element.style.opacity = newOpVal;
  element.style.filter = 'alpha(opacity = ' + (newOpVal*100) + ')';
 
  setTimeout("animateFade(" + curTick + ",'" + eid + "')", 33);
}

/*$(document).observe('dom:loaded', function() {  
	$$("div.photo a").each(function(el) { new FancyZoom(el); })
	new FancyZoom('medium_box_link', {width:400, height:300});
});*/
jQuery(document).ready(function($) {
	var baseUrl	= $('#BASE_URL').val(); 
	$('a[rel*=facebox]').facebox({
        loadingImage : baseUrl+'images/browse_search/loader.gif',
        closeImage   : baseUrl+'images/browse_search/closelabel.png'
      })
    })